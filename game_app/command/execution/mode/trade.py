# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.properties import sell_price_function, buy_price_function, PARAMETERS_COUNTING_IN_TRADE, \
    SKILLS_COUNTING_IN_TRADE

from game_app.command.execution.others import Command
from game_app.models import Item, NPCEquipment, PlayerEquipment, Player


class TradePriceRates(object):
    def get_parameter(self, player, parameter_name_list, sum, is_parameter=True):
        for parameter_name in parameter_name_list:

            if is_parameter is True:
                parameter_value = player.get_parameter(parameter_name)
            else:
                parameter_value = player.get_skill_by_name(parameter_name)

            if parameter_value is None:
                parameter_value = 0

            sum += parameter_value

        return sum

    def get_price(self, item_value, buyer, seller):

        parameter_name_list = PARAMETERS_COUNTING_IN_TRADE
        skill_name_list = SKILLS_COUNTING_IN_TRADE

        is_buying = type(buyer) is Player

        buyer_sum = 0
        buyer_sum = self.get_parameter(buyer, parameter_name_list, buyer_sum, True)
        buyer_sum = self.get_parameter(buyer, skill_name_list, buyer_sum, False)

        seller_sum = 0
        seller_sum = self.get_parameter(seller, parameter_name_list, seller_sum, True)
        seller_sum = self.get_parameter(seller, skill_name_list, seller_sum, False)

        if is_buying:
            rate = buy_price_function(buyer_sum, seller_sum)
        else:
            rate = sell_price_function(buyer_sum, seller_sum)

        return round(item_value * rate, 2)


class BuyCommand(Command):
    def execute(self):
        player = self.get_player_from_request()

        asset_in_map = player.asset_in_map
        command_arguments = " ".join(self.message_array[1:])

        if command_arguments.isdigit():
            index = int(command_arguments)

            if index <= 0:
                message = 'Niepoprawny index przedmiotu'
                return self.response.no_effect(message)

            npc_item = asset_in_map.get_item_from_equipment_by_number(index)

            if npc_item is not None:
                item_name = npc_item.item.name
            else:
                message = 'Brak przedmiotu o takim indexie'
                return self.response.no_effect(message)

        else:
            item_name = command_arguments

            try:
                item = Item.objects.get(name=item_name)
            except Item.DoesNotExist:
                message = "Taki przedmiot nie istnieje. Nie możesz go sprzedać"
                return self.response.no_effect(message)

            try:
                npc_equipment = NPCEquipment.objects.get(asset_in_map=asset_in_map, item=item)
            except NPCEquipment.DoesNotExist:
                message = "%s nie posiada takiego przedmiotu." % asset_in_map.asset.name
                return self.response.no_effect(message)

        item_price = TradePriceRates().get_price(npc_item.item.value, player, asset_in_map.asset)

        if item_price > player.gold:
            message = "Nie stać cię, by kupić ten przedmiot"
            return self.response.no_effect(message)

        else:
            player.gold -= item_price
            player.save()

            asset_in_map.gold += item_price
            asset_in_map.save()

            npc_item.delete()
            PlayerEquipment(item=npc_item.item, player=player).save()

            message = 'Kupiłeś przedmiot %s.' % item_name
            return self.response.success(message)


class SellCommand(Command):
    def execute(self):
        player = self.get_player_from_request()

        asset_in_map = player.asset_in_map
        command_arguments = " ".join(self.message_array[1:])

        if command_arguments.isdigit():
            index = int(command_arguments)

            if index <= 0:
                message = 'Niepoprawny index przedmiotu'
                return self.response.no_effect(message)

            player_item = player.get_item_from_inventory_by_number(index)

            if player_item is not None:
                item_name = player_item.item.name
            else:
                message = 'Brak przedmiotu o takim indexie'
                return self.response.no_effect(message)

        else:
            item_name = command_arguments

            try:
                item = Item.objects.get(name=item_name)
            except Item.DoesNotExist:
                message = "Taki przedmiot nie istnieje. Nie możesz go sprzedać"
                return self.response.no_effect(message)

            try:
                player_equipment = PlayerEquipment.objects.get(player=player, item=item)
            except PlayerEquipment.DoesNotExist:
                message = "Nie posiadasz takiego przedmiotu."
                return self.response.no_effect(message)

        item_price = TradePriceRates().get_price(player_item.item.value, asset_in_map.asset, player)

        if item_price > asset_in_map.gold:
            message = "Sprzedawca nie ma wystarczającej ilości złota, by kupić ten przedmiot od ciebie."
            return self.response.no_effect(message)

        else:
            player.gold += item_price
            player.save()

            asset_in_map.gold -= item_price
            asset_in_map.save()

            player_item.delete()
            NPCEquipment(item=player_item.item, asset_in_map=asset_in_map).save()

            message = 'Sprzedałeś przedmiot %s.' % item_name
            return self.response.success(message)


class ShowStoreCommand(Command):
    def execute(self):
        player = self.get_player_from_request()

        if len(self.message_array) == 1:
            message = player.get_player_and_asset_items_description()
            return self.response.success(message)
        else:
            message = "Użycie komendy: %s." % self.name
            return self.response.no_effect(message)


