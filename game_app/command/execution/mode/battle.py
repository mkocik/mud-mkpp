# -*- coding: utf-8 -*-
from __future__ import unicode_literals


from game_app.command.highlighter import Highlighter
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.command.execution.others import Command
from game_app.controllers.basic_controllers import UserController

from game_app.models import Player


class FightCommand(Command):
    def execute(self):

        message_array = self.message_array

        player = self.get_player_from_request()

        if player.asset_in_map:
            message = "Nie możesz uczestniczyć w walce w czasie rozmowy z postacią niezależną."
            return self.response.no_effect(message)

        if len(message_array) == 1:
            location = player.location
            monster_in_map = location.get_monster_in_map()

            if monster_in_map is None:
                message = "Na tym polu nie ma żadnego potwora!"
                return self.response.no_effect(message)

            else:

                if monster_in_map.player:
                    message = "%s walczy już z innym graczem" % monster_in_map.monster.name
                    return self.response.no_effect(message)
                else:
                    player.set_creature(0)
                    monster_in_map.set_player(player)

                    message = "Rozpoczynasz walkę!"
                    message = Highlighter.attention(message)
                    return self.response.success(message)

        elif len(message_array) == 3 and message_array[1] == 'z':

            location = player.location
            players_in_map = location.get_players()

            attacked_user_name = message_array[2]
            attacked_user = UserController.get_user_from_user_name(attacked_user_name)

            try:
                attacked_player = Player.objects.get(user=attacked_user)
            except Player.DoesNotExist:
                return self.response.no_effect('Taki gracz nie istnieje')

            if attacked_player not in players_in_map:
                attacked_user_name = Highlighter.bold(attacked_user_name)
                message = "Na tym polu nie ma gracza %s." % attacked_user_name
                return self.response.no_effect(message)

            elif attacked_player.asset_in_map:
                message = "Nie możesz zaatakować gracza, który jest zajęty (np. rozmawia z postacią NPC)."
                return self.response.no_effect(message)

            elif attacked_user == self.user:
                message = "Nie możesz zaatakować sam siebie."
                return self.response.no_effect(message)

            elif ws_handler.is_player_connected(attacked_user) is False:
                message = "Ten użytkownik nie jest zalogowany. Nie możesz go zaatakować."
                return self.response.no_effect(message)

            else:

                for opponent in players_in_map:
                    if opponent.user.username == attacked_user_name:

                        if opponent.creature == -1:
                            message_to_opponent = "Zostałeś zaatakowany przez gracza %s!" % Highlighter.bold(
                                self.user.username)
                            message_to_opponent = Highlighter.attention(message_to_opponent)

                            lista = [opponent.user]
                            ws_handler.say_to_users(lista, message_to_opponent)

                            opponent.stealing_from = None
                            opponent.set_creature(player.user.pk)
                            player.set_creature(opponent.user.pk)

                            message = "Zaatakowałeś gracza %s!" % Highlighter.bold(attacked_user_name)
                            message = Highlighter.attention(message)

                            return self.response.success(message)
                        else:
                            message = "Gracz %s jest teraz w trybie walki. Nie możesz go zaatakować." \
                                      % opponent.user.username
                            return self.response.no_effect(message)

                return self.response.no_effect("Twój przeciwnik zniknął z niewiadomych przyczyn")

        else:
            message = "Składnia: %s z nick_gracza" % self.name
            return self.response.bad_syntax(message)


class AttackCommand(Command):
    def execute(self):
        from game_app.command.execution.fight.user_fight import UserFightController
        from game_app.utils.discover.discover_command import get_command_properties
        player = self.get_player_from_request()
        command_property = get_command_properties[self.name]

        self.response = UserFightController().user_attack(self.request.original_request, player, command_property)
        return self.response






