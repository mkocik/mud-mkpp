# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.battle.interfaces import Fighter, NPCInterface

from game_app.command.execution.others import Command
from game_app.controllers.basic_controllers import AssetController
from MUD_ADMIN.game.properties import *


class SearchCommand(Command):
    def execute(self):
        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) >= 1:
            location = player.location
            assets_in_map = location.get_assets_in_map()

            if assets_in_map:
                message_array = self.message_array
                asset_name = " ".join(message_array[1:])

                if len(self.message_array) > 1:
                    try:
                        index = int(asset_name)
                        asset_in_map = location.get_asset_in_map_by_index(index)
                        if asset_in_map is None:
                            message = "Brak assetu o podanym indeksie."
                            return self.response.no_effect(message)
                    except ValueError:
                        asset_in_map = location.get_specific_asset_in_map(asset_name)
                else:
                    asset_in_map = assets_in_map[0]

                if asset_in_map is None:
                    message = "Nie ma na tym polu assetu o takiej nazwie."
                    return self.response.no_effect(message)

                asset_object = AssetController.get_asset_object(asset_in_map)

                if player.is_player_banned(asset_in_map):
                    message = "Postać czujnie na Ciebie spogląda. Nie uda Ci się zrobić nic niepostrzeżenie."
                    return self.response.no_effect(message)

                if search_npc_success_chance_function(Fighter(player), NPCInterface(asset_in_map.asset)):
                    player.stealing_from = asset_in_map
                    player.save()

                    message = asset_object.search_npc()
                else:
                    player.ban_player(asset_in_map)
                    message = "Niepowodzenie przy przeszukiwaniu postaci."
                    player.stealing_from = None
                    player.save()

                return self.response.success(message)

            else:
                message = "Na tym polu nie ma żadnego obiektu."
                return self.response.no_effect(message)
        else:
            message = "Składnia: %s imię/numer postaci/npc" % self.name
            return self.response.bad_syntax(message)


class StealCommand(Command):
    def execute(self):
        player = self.get_player_from_request()

        if player.stealing_from is not None:

            if len(self.message_array) == 2:

                asset_in_map = player.stealing_from

                try:
                    index = int(self.message_array[1])
                except ValueError:
                    message = "Indeks musi być liczbą naturalną"
                    return self.response.no_effect(message)

                if player.is_player_banned(asset_in_map):
                    message = "Postać czujnie na Ciebie spogląda. Nie uda Ci zrobić nic niepostrzeżenie."
                    return self.response.no_effect(message)

                item = (asset_in_map.steal_from_asset(player,index))

                if item is None:
                    message = "Brak przedmiotu o podanym indeksie"
                    return self.response.no_effect(message)

                if steal_from_NPC_success_chance_function(Fighter(player),NPCInterface(asset_in_map.asset),item):
                    message = "Pomyślnie skradziono przedmiot: " + item.name
                else:
                    message = "Nieudana próba ukradnięcia przedmiotu: " + item.name
                    player.ban_player(asset_in_map)
                    player.stealing_from = None
                    player.save()

                return self.response.success(message)
            else:
                message = "Użycie komendy: %s numer_przedmiotu." % self.name
                return self.response.no_effect(message)
        else:
            message = "Nie jesteś teraz w trybie kradzieży. Nie możesz wykonać tej komendy."
            return self.response.no_effect(message)
