# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.asset.definition.asset import SimpleNPC

from game_app.command.highlighter import Highlighter
from game_app.command.location import MoveController
from game_app.command.execution.others import Command
from game_app.controllers.basic_controllers import AssetController
from game_app.models import Map, MapLevel


class MoveCommand(Command):
    def __init__(self, request, direction):
        Command.__init__(self, request)
        self.direction = direction

    def execute(self):
        player = self.get_player_from_request()
        location = player.location
        asset_in_map = location.get_assets_in_map()

        if asset_in_map == SimpleNPC.type.name:
            asset_object = AssetController.get_asset_object(asset_in_map)
            asset_object.exit_graph(player)
            player.is_busy = False
            player.save()

        location = player.move(self.direction)

        if location:

            moving_controller = MoveController()
            message_object = moving_controller.get_full_description_unsafe_mode(location, player)

            if message_object.in_combat:
                player.set_creature(0)

            message = message_object.message

            completion_message = player.check_if_quest_is_accomplished()

            return self.response.success(message + completion_message)

        else:

            message = 'Wybrany kierunek prowadzi donikąd.'
            message = Highlighter.wrong_direction(message)
            return self.response.no_effect(message)


class TeleportCommand(Command):
    def execute(self):
        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) == 4:
            x = message_array[1]
            y = message_array[2]
            level = message_array[3]

            try:
                level = MapLevel.objects.get(level=level)
            except MapLevel.DoesNotExist:
                return self.response.no_effect("Taki poziom nie istnieje")

            try:
                map = Map.objects.get(x=x, y=y, level=level)
                player.set_location(map)
            except Map.DoesNotExist:
                return self.response.no_effect("Takie pole nie istnieje")

            command_object = LookCommand(self.request)
            return command_object.execute()

        else:
            message = "Zła składnia"
            return self.response.no_effect(message)


class LookCommand(Command):
    def execute(self):
        player = self.get_player_from_request()
        location = player.location

        moving_controller = MoveController()
        message = moving_controller.get_full_description_safe_mode(location, player)

        return self.response.success(message)


class PassageCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) > 1:

            del message_array[0]
            msg = " ".join(message_array)

            passage = player.get_passage()

            if passage:

                if passage.asset:
                    asset_object = AssetController.get_asset_object(passage)
                    return asset_object.execute(player)

            if player.enter_passage(msg) is False:
                message = "Brak przejścia o podanej nazwie"
                return self.response.no_effect(message)

            else:
                command_object = LookCommand(self.request)
                return command_object.execute()

        elif len(message_array) == 1 and player.get_passage():

            passage = player.get_passage()

            if passage:

                if passage.asset:
                    name_info = Highlighter.bold(passage.riddle.name)
                    message = "Musisz najpierw rozwiązać zagadkę: %s!" % name_info

                    asset_object = AssetController.get_asset_object(passage.riddle)
                    message += '<br>' + asset_object.execute(player)
                    message = Highlighter.italic(message)

                    return self.response.no_effect(message)

            player.enter_passage(player.get_passage().name)
            command_object = LookCommand(self.request)
            return command_object.execute()

        else:
            message = "Składnia: %s nazwa_przejścia" % self.name
            return self.response.bad_syntax(message)