# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.battle.attack import AttributeBattleModification

from game_app.battle.interfaces import Fighter

from MUD_ADMIN.game.battle.winner_bonus import IWinnerBonus

from game_app.command.execution.fight.user_monster_fight import UserMonsterFightController
from game_app.command.process.response import BattleResponseObject
from MUD_ADMIN.game.properties import attack_probability_function, attack_strength_function
from game_app.controllers.battle_controllers import AttackEffectController
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.models import *
from game_app.command.highlighter import Highlighter
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics

class UserFightController(object):
    def user_attack(self, request, player, attack_property):
        from MUD_ADMIN.game.battle.attack_definition import kill

        if attack_property != kill:
            if attack_property.take_cost(player) is None:
                response = BattleResponseObject(player)
                message = "Koszt ataku jest zbyt wysoki."
                return response.no_effect(message)

        if player.creature == 0:
            response = UserMonsterFightController().user_attacking_monster(request, player, attack_property)
            return response

        elif player.creature > 0:
            return UserUserFightController().user_attacking_player(request, player, attack_property)

        else:
            response = BattleResponseObject(player)
            message = "Nie walczysz teraz z nikim."
            return response.no_effect(message)


class UserUserFightController(object):
    def opponent_is_wrong(self, player, opponent):
        response = BattleResponseObject(player)

        if opponent is None:
            message = "Rywal, z którym walczyłeś nie jest już graczem naszej gry."
            player.set_creature(-1)
            return response.no_effect(message)

        elif opponent.location != player.location:
            message = "Twój rywal znajduje się na innym polu. Nie możesz go zaatakować."
            player.set_creature(-1)
            return response.no_effect(message)

        elif opponent.creature != player.user.pk or player.creature != opponent.user.pk:
            message = "Twój przeciwnik nie walczy teraz z tobą lub ty nie walczysz z nim. Nie możesz go zaatakować."
            player.set_creature(-1)
            return response.no_effect(message)

        elif ws_handler.is_player_connected(opponent.user) is False:
            message = "Gracz, z którym walczyłeś się wylogował. Wygrałeś!"
            player.set_creature(-1)
            player.regain_stamina()
            return response.no_effect(message)

        else:
            return None

    def user_attacking_player(self, request, player, attack_property):
        response = BattleResponseObject(player)
        opponent = player.get_opponent()

        opponent_is_wrong = self.opponent_is_wrong(player, opponent)

        if opponent_is_wrong is not None:
            return opponent_is_wrong
        else:
            attr_modifications = AttributeBattleModification(player, attack_property.mod_attributes)
            attr_modifications.execute() #toDo

            attack_probability = attack_probability_function(player, opponent)
            random_int = random.uniform(0, 100)

            if attack_probability <= random_int:
                message = "Twój przeciwnik chybił, nie odniosłeś obrażeń."
                message = Highlighter.battle(message)
                message = Highlighter.frame(message)

                ws_handler.say_to_users([opponent.get_ws_id()], message)
                message = "Chybiłeś. Twój przeciwnik nie odniósł obrażeń."
                attr_modifications.undo() #toDO
                return response.no_effect(message)


            player = Player.objects.get(pk=player.pk)

            ws_handler.stop_effects_to_next_hit(opponent.get_ws_id())
            injury = attack_strength_function(Fighter(player), Fighter(opponent))

            opponent_armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(opponent)
            injury = IFighter.get_real_damage(injury, opponent_armor)

            hp = opponent.reduce_hp(injury)

            attr_modifications.undo() #toDo
            player = Player.objects.get(pk=player.pk)

            string_injury = str(injury) + " pkt"
            string_injury = Highlighter.bold(string_injury)

            string_hp = str(hp) + " pkt"
            string_hp = Highlighter.bold(string_hp)

            if hp > 0:
                effect_message = AttackEffectController().execute_effects(attack_property.effect_array,
                                                                          player, opponent, injury)

                #hp may change during effects execution,
                #we should check again if opponent is still alive
                opponent = Player.objects.get(pk=opponent.pk)

                if opponent.get_hp() > 0:
                    message = "Otrzymałeś obrażenia: %s. Zostało ci: %s." % (string_injury, string_hp)
                    message = Highlighter.battle(message)
                    message += '<br> ' + effect_message
                    message = Highlighter.frame(message)

                    ws_handler.say_to_users([opponent.get_ws_id()], message)

                    ws_handler.say_to_users([opponent.get_ws_id()], self.create_update_array(opponent))

                    message = "Twój rywal otrzymał obrażenia: %s. Zostało mu: %s." % (string_injury, string_hp)
                    message += '<br> ' + effect_message

                    return response.success(message)


            ws_handler.stop_all_effects(player)
            ws_handler.stop_all_effects(opponent)

            opponent.resurrection()

            triumph_response = IWinnerBonus(player, opponent).execute()

            player.regain_stamina()

            loser_message = "Zostałeś zabity. Znajdujesz się ponownie na polu startowym."
            loser_message = Highlighter.frame(loser_message)

            winner_message = "Twój rywal otrzymał obrażenia: %s." % string_injury
            winner_message += " Pokonałeś go."

            player = Player.objects.get(pk=player.pk)
            player.set_creature(-1)

            if triumph_response:
                triumph_message = triumph_response.winner_message

                if triumph_response.loser_message != '':
                    loser_message += '<br>' + triumph_response.loser_message

            else:
                triumph_message = ''

            ws_handler.say_to_users([opponent.get_ws_id()], loser_message)
            hp = 120 #wstaw defaultowa wartosc zycia
            ws_handler.say_to_users([opponent.get_ws_id()], "Życie-" + str(hp))
            return response.success(winner_message + '<br>' + triumph_message)

    def create_update_array(self, opponent):
        if type(opponent) is Player:
            attributes = opponent.get_all_attributes()
            attrs_array = {}
            for attr in attributes:
                attrs_array[attr.get_attr_id()] = attr.current_state

            attributes_max = opponent.get_all_attributes_with_mods()
            attrs_array_max = {}
            for attr in attributes_max:
                attrs_array_max[attr.get_attr_id()] = attr.max_state

            parameters = opponent.get_all_parameters()
            param_array = {}

            for param in parameters:
                param_array[param.get_param_id()] = param.state

            armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(opponent)

            c = {}

            c['param_and_attr_update'] = 'true'
            c['armor'] = armor
            c['xp'] = opponent.experience
            c['parameters'] = str(param_array)
            c['attributes'] = str(attrs_array)
            c['attributes_max'] = str(attrs_array_max)

            return c

        else:
            return {}