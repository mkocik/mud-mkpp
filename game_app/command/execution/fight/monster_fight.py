# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.battle.attack import AttributeBattleModification
from game_app.battle.effect import Paralysis
from game_app.controllers.basic_controllers import UserController

from game_app.controllers.battle_controllers import AttackEffectController

from game_app.command.process.response import BattleResponseObject, ResponseObject
from game_app.utils.websockets.battle_websocket import ws_handler
from MUD_ADMIN.game.properties import attack_probability_function, attack_strength_function
from game_app.models import *
from game_app.command.highlighter import Highlighter
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class MonsterFightController(object):
    def monster_monster_attack(self, request, attack_property):
        response = ResponseObject()

        monster1_pk = request.GET['monster1_pk']
        monster1 = Monster.objects.get(pk=monster1_pk)

        monster2_pk = request.GET['monster2_pk']
        monster2 = Monster.objects.get(pk=monster2_pk)

        location = Map.objects.last()

        monster1_in_map = location.get_monster_in_map_simulation(monster1)
        monster2_in_map = location.get_monster_in_map_simulation(monster2)

        if monster1_in_map is None or monster2_in_map is None:
            return response.no_effect("")

        if ws_handler.creature_has_effect(monster1_in_map, Paralysis):
            return response.success('')  #monster is paralysed, cannot attack

        attr_modifications = AttributeBattleModification(monster1, attack_property.mod_attributes)
        attr_modifications.execute()  #toDO

        attack_probability = attack_probability_function(monster1, monster2)
        random_int = random.randint(0, 100)
        ################

        if attack_probability <= random_int:
            message = "Potwór nie trafia w przeciwnika."
            message = Highlighter.battle(message)
            attr_modifications.undo()  #toDO
            return response.success(message)

        ws_handler.stop_effects_to_next_hit(monster2_in_map.get_ws_id())

        injury = attack_strength_function(monster1)
        injury = IFighter.get_real_damage(injury)

        hp = monster2_in_map.reduce_hp(injury)
        attr_modifications.undo()  #toDO

        string_hp = str(hp) + " pkt. "
        string_hp = Highlighter.bold(string_hp)

        injury_message = str(injury) + " pkt."
        injury_message = Highlighter.bold(injury_message)

        if hp > 0:
            effect_message = AttackEffectController().execute_effects(attack_property.effect_array,
                                                                      monster1_in_map, monster2_in_map, injury)

            #hp may change during effects execution,
            #we should check again if opponent is still alive
            monster2_in_map = location.get_monster_in_map_simulation(monster2)

            if monster2_in_map.get_hp() > 0:
                message = "Uderza potwora %s i zabiera mu %s życia. Zostało mu: %s" \
                          % (monster2.name, injury_message, string_hp)
                message += '<br> ' + effect_message
                message = Highlighter.battle(message)

                return response.success(message)

        monster2_in_map.resurrection()
        message = "Potwór %s zwycięża." % monster1.name

        return response.success(message)

    def monster_attack(self, request, attack_property):
        player = UserController.get_player_from_user(request.user)
        response = BattleResponseObject(player)

        location = player.location

        monster_in_map = location.get_monster_in_map()

        if monster_in_map is None:
            return response.no_effect("")

        if ws_handler.creature_has_effect(monster_in_map, Paralysis):
            return response.success('')  #monster is paralysed, cannot attack

        monster = monster_in_map.monster
        attr_modifications = AttributeBattleModification(monster, attack_property.mod_attributes)
        attr_modifications.execute()  #toDO

        player_armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player)

        attack_probability = attack_probability_function(monster, player)
        random_int = random.randint(0, 100)

        if attack_probability <= random_int:
            message = "Potwór chybił. Nie straciłeś punktów życia."
            message = Highlighter.battle(message)
            attr_modifications.undo()  #toDO
            return response.success(message)

        ws_handler.stop_effects_to_next_hit(player.get_ws_id())

        injury = attack_strength_function(monster, player)
        injury = IFighter.get_real_damage(injury, player_armor)

        hp = player.reduce_hp(injury)
        attr_modifications.undo()  #toDO

        string_hp = str(hp) + " pkt. "
        string_hp = Highlighter.bold(string_hp)

        injury_message = str(injury) + " pkt."
        injury_message = Highlighter.bold(injury_message)

        if hp > 0:
            effect_message = AttackEffectController().execute_effects(attack_property.effect_array,
                                                                      monster_in_map, player, injury)

            #hp may change during effects execution,
            #we should check again if opponent is still alive
            player = Player.objects.get(pk=player.pk)

            if player.get_hp() > 0:
                message = "Potwór uderza cię i zabiera ci %s życia. Zostało ci: %s" % (injury_message, string_hp)
                message += '<br> ' + effect_message
                message = Highlighter.battle(message)
                return response.success(message)

        monster_in_map.set_player(None)
        player.resurrection()
        message = "Zostałeś zabity. Znajdujesz się ponownie na polu startowym."

        response.resurrection = True
        return response.success(message)


class MonsterLootController(object):
    def monster_loot(self, monster_in_map, player):

        message = ""
        gold_from_monster = random.randint(monster_in_map.monster.gold_min, monster_in_map.monster.gold_max)
        #player.increase_gold(gold_from_monster)

        if gold_from_monster > 0:
            player.location.drop_gold(gold_from_monster)
            message += monster_in_map.monster.name + " upuszcza " + GOLD.lower() + ": " + str(gold_from_monster)

        loots_available = {}
        first_msg = True

        try:
            monster_loots = MonsterLoot.objects.filter(monster=monster_in_map.monster)
        except MonsterLoot.DoesNotExist:
            return ""

        for loot in monster_loots:
            for i in range(0, loot.probability):
                loots_available[loot.item] = loot.amount

        if loots_available:

            for amount in range(0, monster_in_map.monster.max_loot_amount):

                random_int = random.randint(0, 100)
                if random_int <= monster_in_map.monster.item_probability:

                    keys_list = []
                    for key in loots_available.keys():
                        keys_list.append(key)

                    item = random.choice(keys_list)
                    amount = loots_available[item]

                    for it in range(0, amount):
                        monster_in_map.map.put_item(item)

                        if first_msg and it == 0:
                            message += "<br>" + monster_in_map.monster.name + " upuszcza: " + item.name + " x " + str(
                                amount)
                            first_msg = False

                        elif it == 0:
                            message += ", " + item.name + " x " + str(amount)
        return message