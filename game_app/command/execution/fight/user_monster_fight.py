# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.battle.attack import AttributeBattleModification

from game_app.controllers.battle_controllers import AttackEffectController
from game_app.command.execution.fight.monster_fight import MonsterLootController
from game_app.command.process.response import BattleResponseObject
from MUD_ADMIN.game.properties import attack_probability_function, attack_strength_function
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.models import *
from game_app.command.highlighter import Highlighter
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class UserMonsterFightController():
    def user_attacking_monster(self, request, player, attack_property):
        response = BattleResponseObject(player)
        location = player.location

        monster_in_map = location.get_monster_in_map()

        if monster_in_map is None:
            message = "Na tym polu nie ma żadnego potwora!"
            return response.no_effect(message)

        else:
            monster = monster_in_map.monster
            attr_modifications = AttributeBattleModification(player, attack_property.mod_attributes)
            attr_modifications.execute()  #toDO

            monster_armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player)
            attack_probability = attack_probability_function(player, monster)
            random_int = random.uniform(0, 100)

            if attack_probability <= random_int:
                message = "Chybiłeś. Potwór nie odniósł obrażeń."
                attr_modifications.undo()  #toDO
                return response.no_effect(message)

            ws_handler.stop_effects_to_next_hit(monster_in_map.get_ws_id())

            injury = attack_strength_function(player, monster)
            injury = IFighter.get_real_damage(injury, monster_armor)  #armor reduces damage

            hp = monster_in_map.reduce_hp(injury)
            attr_modifications.undo()  #toDO

            string_injury = str(injury) + " pkt "
            string_injury = Highlighter.bold(string_injury)

            string_hp = str(hp) + " pkt"
            string_hp = Highlighter.bold(string_hp)

            if hp > 0:
                effect_message = AttackEffectController().execute_effects(attack_property.effect_array,
                                                                          player, monster_in_map, injury)

                #hp may change during effects execution,
                #we should check again if opponent is still alive
                monster_in_map = MonsterInMap.objects.get(pk=monster_in_map.pk)
                if monster_in_map.hp > 0:
                    message = "Potwór otrzymał obrażenia: %s. Zostało mu: %s." % (string_injury, string_hp)
                    message += '<br> ' + effect_message
                    return response.success(message)

            ws_handler.stop_all_effects(player)
            ws_handler.stop_all_effects(monster_in_map)

            xp = monster_in_map.monster.experience
            string_xp = str(xp) + " pkt"
            string_xp = Highlighter.bold(string_xp)

            message = "Potwór otrzymał obrażenia: %s." % string_injury
            message += " Pokonałeś go." + " <br> " + "Otrzymujesz %s doświadczenia." % string_xp
            message += "<br>" + MonsterLootController().monster_loot(monster_in_map, player)

            player.regain_stamina()

            monster_in_map.delete()

            if location.get_monster_in_map() is None:
                completion_message = player.check_if_quest_is_accomplished()
                message += completion_message

            player.set_creature(-1)
            player.increase_experience(xp)
            return response.success(message)