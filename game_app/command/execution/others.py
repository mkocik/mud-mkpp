# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import abc

from game_app.command.highlighter import Highlighter
from game_app.command.settings.command_types import OpeningChestTypeCommand, TalkingTypeCommand
from game_app.controllers.basic_controllers import UserController


class Command():
    def __init__(self, request):
        from game_app.command.process.response import ResponseObject

        self.request = request
        self.user = self.request.user

        self.response = ResponseObject(UserController.get_player_from_user(self.request.user))
        self.message_array = self.request.message_array
        self.name = self.request.command_name

    @abc.abstractmethod
    def execute(self):
        return

    def get_player_from_request(self):
        return UserController.get_player_from_user(self.user)


class ManualCommand(Command):
    def execute(self):
        from game_app.command.process.response import ResponseObject

        request = self.request

        player = UserController.get_player_from_user(request.user)
        help_command = HelpCommand(request)
        message_array = request.message_array

        if len(message_array) == 2:
            #print('aaaa')
            return BattleHelpCommand(self.request).execute()

        if len(message_array) > 2:
            return ResponseObject().incorrect_syntax()

        if player.is_in_combat():
            return help_command.execute_in_battle_mode(player)

        if player.is_opening_chest():
            return help_command.execute_in_open_chest_mode()

        if player.in_trade:
            return help_command.execute_in_trade_mode()

        if player.is_talking():
            return help_command.execute_in_talk_mode()

        if player.stealing_from is not None:
            return help_command.execute_in_steal_mode()

        return help_command.execute()


class HelpCommand(Command):
    def execute(self):
        from game_app.command.settings.command_settings import discover_world_commands, steal_commands, \
            other_commands, attack_commands, asset_commands, multiplayer_commands, item_commands, special_attacks


        all_commands = [discover_world_commands, multiplayer_commands, attack_commands,
                        item_commands, asset_commands, other_commands, special_attacks, steal_commands]

        manual_message = ''

        for commands in all_commands:
            manual_message += self.list_one_command_category(commands)

        manual_message = Highlighter.manual(manual_message)
        return self.response.success(manual_message)

    def list_one_command_category(self, commands):
        if len(commands)>0:
            manual_message = Highlighter.bold(commands[0].type.title) + '<br>'
            commands = sorted(commands, key=lambda x: x.order)

            for command in commands:
                command_name = command.name
                shortcut = command.shortcut
                description = command.get_description()

                command_use = Highlighter.underline(command_name)

                if shortcut is not None:
                    command_use = command_use + ", " + shortcut
                    command_use = Highlighter.underline(command_use)

                message = command_use + " - " + description
                message += "<br>"
                manual_message += message

            manual_message += "<br>"

            return manual_message
        return ""

    def execute_in_battle_mode(self, player):
        from game_app.command.settings.command_settings import special_attacks

        message = self.list_one_command_category(special_attacks)
        return self.response.success(message)

    def execute_in_open_chest_mode(self):
        message = Highlighter.bold(OpeningChestTypeCommand.title) + '<br>'
        message += OpeningChestTypeCommand.manual
        return self.response.success(message)

    def execute_in_talk_mode(self):
        message = Highlighter.bold(TalkingTypeCommand.title) + '<br>'
        message += TalkingTypeCommand.manual
        return self.response.success(message)

    def execute_in_trade_mode(self):
        from game_app.command.settings.command_settings import trade_commands
        message = self.list_one_command_category(trade_commands)
        return self.response.success(message)

    def execute_in_steal_mode(self):
        from game_app.command.settings.command_settings import steal_commands
        message = self.list_one_command_category(steal_commands)
        return self.response.success(message)


class BattleHelpCommand(Command):
    def execute(self):
        from game_app.command.settings.command_settings import special_attacks

        request = self.request
        message_array = request.message_array
        number = message_array[1]

        if number.isdigit() is False:
            return self.response.no_effect('Brak ataku specjalnego o takim indeksie.')

        number = int(number)

        if number == 0 or number > len(special_attacks):
            return self.response.no_effect('Brak ataku specjalnego o takim indeksie.')
        else:
            attacks = sorted(special_attacks, key=lambda x: x.order)

            attack = attacks[number-1]
            return self.response.success(attack.__str__())




class MapCommand(Command):
    def execute(self):
        self.response.set_show_map()
        return self.response.success("")