# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from MUD_ADMIN.game.properties import user_colors
from game_app.command.highlighter import Highlighter
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.command.execution.others import Command
from game_app.controllers.basic_controllers import UserController
from game_app.command.location import LocationController


class PlayersCommand(Command):
    def execute(self):

        player = self.get_player_from_request()
        location = player.location
        return LocationController.get_other_players_description(player.user, location)


class SayCommand(Command):
    def execute(self):

        location = self.get_player_from_request().location
        players_in_location = location.get_players()

        available_players = []
        for player in players_in_location:
            user = player.user
            if ws_handler.is_player_connected(user) and ws_handler.is_player_fighting(
                    user) is False and ws_handler.is_player_busy(user) is False and ws_handler.is_player_stealing(user) is False:
                available_players.append(player)

        message_array = self.message_array

        if len(message_array) > 1:
            #deleting name of the command
            del message_array[0]

            player = UserController.get_player_from_user(self.user)
            message = " ".join(message_array)
            color = user_colors[player.color]
            message = Highlighter.color(message, color)

            user_name = Highlighter.speaker_name(self.user.username)
            message = "Gracz " + user_name + " mówi: " + message

            users = []
            for player in available_players:
                user = player.user
                if user != self.user:
                    users.append(user)

            ws_handler.say_to_users(users, message)
            return self.response.success("")
        else:
            message = "Składnia: %s wiadomość" % self.name
            return self.response.bad_syntax(message)


class WhisperCommand(Command):
    def execute(self):

        message_array = self.message_array
        location = self.get_player_from_request().location
        players_in_location = location.get_players()

        if len(message_array) > 2:

            all_users = UserController.get_all_users()
            exists = False

            for opponent_user in all_users:
                if opponent_user.username == message_array[1]:
                    exists = True

            if exists is False:
                message = "Gracz o podanym nicku nie istnieje"
                return self.response.bad_syntax(message)

            for opponent_player in players_in_location:
                opponent_user = opponent_player.user

                if opponent_user.username == message_array[1]:

                    if opponent_user.username == self.user.username:
                        message = "Nie możesz szeptać do samego siebie"
                        return self.response.no_effect(message)

                    if ws_handler.is_player_connected(opponent_user) is False:
                        message = "Gracz nie jest zalogowany"
                        return self.response.no_effect(message)

                    if ws_handler.is_player_fighting(opponent_user):
                        message = "Gracz walczy i nie odbierze wiadomości"
                        return self.response.no_effect(message)

                    if ws_handler.is_player_busy(opponent_user) or ws_handler.is_player_stealing(opponent_user):
                        message = "Gracz jest zajęty i nie odbierze wiadomości"
                        return self.response.no_effect(message)

                    #deleting name of the command and receiver name
                    del message_array[0]
                    del message_array[0]

                    player = UserController.get_player_from_user(self.user)
                    message = " ".join(message_array)
                    color = user_colors[player.color]
                    message = Highlighter.color(message, color)

                    user_name = Highlighter.speaker_name(self.user.username)

                    whispering = " szepcze: "
                    whispering = Highlighter.whisper(whispering)

                    message = "Gracz " + user_name + whispering + message
                    user_to_send = [opponent_user]
                    ws_handler.say_to_users(user_to_send, message)

                    return self.response.success("")

            message = "Gracz przebywa na innym polu"
            return self.response.no_effect(message)

        else:

            message = "Składnia: %s nick_gracza wiadomość" % self.name
            return self.response.bad_syntax(message)