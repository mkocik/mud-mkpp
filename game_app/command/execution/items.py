# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.highlighter import Highlighter
from game_app.command.execution.asset import AssetCommand
from game_app.command.execution.others import Command
from MUD_ADMIN.game.properties import *
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics

class GetCharacterCard(Command):
    def execute(self):
        player = self.get_player_from_request()
        message_array = self.message_array
        if len(message_array) == 1:
            message=Highlighter.color("Atrybuty postaci: <br>", "blue")
            attributes = player.get_all_attributes_with_mods()
            for attr in attributes:
                message+=attr.attribute.name + " " + Highlighter.bold(str(attr.current_state)) + "/" + Highlighter.bold(str(attr.max_state)) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Parametry postaci: <br>", "blue")
            parameters = player.get_all_parameters_with_mods()
            for param in parameters:
                message+=param.parameter.name + ": " + Highlighter.bold(str(param.state)) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Cechy postaci: <br>", "blue")
            for char in all_characteristics:
                message += char.name + ": " + Highlighter.bold(str(char(player))) + "<br/>"

            message+="<br>"

            message += Highlighter.color("Doświadczenie:", "blue") + str(player.experience) + "<br>"
            message += Highlighter.color("Poziom:", "blue") + str(experience_function(player.experience)) + "<br>"

            message+="<br>"

            skills = player.get_all_skills()

            if(len(skills) > 0):
                message += Highlighter.color("Umiejętności: <br>", "blue")
                for skill in skills:
                    message+=skill.skill.name + ": " + Highlighter.bold(str(skill.value)) + "<br/>"

                message+="<br>"


            return self.response.success(message)
        else:
            message = "Składnia: %s" % self.name
            return self.response.bad_syntax(message)

class GetSkills(Command):
    def execute(self):
        from game_app.models.PlayerModel import Skill,PlayerSkill
        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) == 2:

            if message_array[1] == "wszystkie" or message_array[1] == "all":
                message = ""
                skills = Skill.objects.all()
                counter = 1

                for skill in skills:
                    message+= Highlighter.bold(str(counter)) + ". " + Highlighter.bold(skill.name) +  ": " + skill.description  + "<br>"
                    counter+=1

                if message=="":
                    message = "Brak dostępnych umiejętności"

                return self.response.success(message)

            elif message_array[1] == "moje" or message_array[1] == "my":
                message = ""
                skills = PlayerSkill.objects.filter(player=player)

                for skill in skills:
                    message+= skill.skill.name +  ": " + Highlighter.bold(str(skill.value)) + "<br>"

                if message=="":
                    message = "Brak przydzielonych punktów umiejętności"

                return self.response.success(message)

            else:
                message = "Składnia: %s wszystkie(all)/moje(my)" % self.name
                return self.response.bad_syntax(message)

        else:
            message = "Składnia: %s wszystkie(all)/moje(my)" % self.name
            return self.response.bad_syntax(message)

class SetCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) == 1:

            amount = str(player.distribution_points) + " pkt"
            amount = Highlighter.bold(amount)
            skill_amount = str(player.skills_points) + " pkt"
            skill_amount = Highlighter.bold(skill_amount)
            command_help = Highlighter.bold("przydziel nazwa_atrybutu/numer_umiejetnosci liczba_punktów")

            message = "W tej chwili posiadasz: " + str(
                amount) + " atrybutów oraz "+str(skill_amount)+" umiejętności do podziału. <br> Zrobisz to poleceniem: " + command_help

            return self.response.success(message)

        elif len(message_array) == 3:

            del message_array[0]

            try:

                number = int(message_array[0])
                skill = player.get_skill(number)

                try:
                    amount = int(message_array[1])

                except ValueError:
                    message = "Ilość punktów musi być liczbą naturalną."
                    return self.response.bad_syntax(message)

                if amount <= 0:
                    message = "Ilość punktów musi być liczbą dodatnią."
                    return self.response.bad_syntax(message)

                if amount > player.skills_points:
                    message = "Posiadasz  "+str(player.skills_points)+" pkt umiejętności do podziału."
                    return self.response.bad_syntax(message)

                player.add_skill(skill,amount)

                message = "Prawidłowo rozdzielono punkty umiejętności."
                return self.response.success(message)
            except ValueError:

                param = player.get_parameter_object(message_array[0])

                if param is not None:

                    try:
                        amount = int(message_array[1])

                    except ValueError:
                        message = "Ilość punktów musi być liczbą naturalną."
                        return self.response.bad_syntax(message)

                    if amount <= 0:
                        message = "Ilość punktów musi być liczbą dodatnią."
                        return self.response.bad_syntax(message)

                    if amount <= player.distribution_points:
                        player.add_to_param(param, amount)
                        player.remove_distribution_points(amount)
                        amount = Highlighter.bold(str(amount))

                        message = "Prawidłowo rozdzielono %s pkt atrybutów." % amount
                        message = Highlighter.information(message)
                        return self.response.success(message)

                    else:
                        message = "Nie masz wystarczającej liczby punktów atrybutów."
                        return self.response.bad_syntax(message)

                else:
                    message = "Nie znaleziono atrybutu o podanej nazwie."
                    return self.response.bad_syntax(message)
            except:
                message = "Błędny indeks umiejętności. Sprawdź indeksy poleceniem wyświetlającym wszystkie umiejętności."
                return self.response.bad_syntax(message)

        else:
            message = "Składnia: %s nazwa_atrybutu/indeks_umiejetnosci liczba_punktów" % self.name
            return self.response.bad_syntax(message)


class GetCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        message = ""

        if player.location.get_monster_in_map() is not None:
            message += "Nie możesz zabrać nic z pola na którym jest potwór."
            return self.response.bad_syntax(message)

        if len(message_array) > 1:

            if message_array[1] == GOLD.lower():
                amount = player.get_gold_from_map()

                if amount is None:
                    message += "Na tym polu nie znajduje się: " + GOLD.lower()
                    return self.response.bad_syntax(message)

                else:
                    message += "Zabrano " + GOLD.lower() + ": " + str(amount)
                    return self.response.success(message)

        if player.get_inventory_amount() >= MAX_EQUIPMENT_AMOUNT:
            message += "Twój ekwipunek jest pełen."
            return self.response.bad_syntax(message)

        elif len(message_array) > 1:
            del message_array[0]

            if message_array[0] == "wszystko" or message_array[0] == "all":
                items_in_map = player.location.get_items()

                if (player.get_inventory_amount() + len(items_in_map)) > MAX_EQUIPMENT_AMOUNT:
                    message += "Przedmioty nie zmieszczą się w Twoim ekwipunku."
                    return self.response.bad_syntax(message)

                else:
                    amount = player.get_gold_from_map()
                    if amount != None:
                        message += "Zabrano " + GOLD.lower() + ": " + str(amount) + "<br>"

                    if items_in_map:

                        for item_in_map in items_in_map:
                            player.add_to_equipment(item_in_map.item)
                            player.location.remove_item(item_in_map)

                        message += "Zabrałeś wszystkie przedmioty z pola."
                        completion_message = player.check_if_quest_is_accomplished()

                        return self.response.success(message + completion_message)

                    else:

                        if message == "":  #only if we didnt get any gold with this command
                            message += "Nie znaleziono przedmiotów."
                            return self.response.bad_syntax(message)

                        else:
                            completion_message = player.check_if_quest_is_accomplished()
                            return self.response.success(message + completion_message)

            elif len(message_array) == 2:

                try:

                    number = int(message_array[0])
                    amount = int(message_array[1])

                    if (player.get_inventory_amount() + amount) > MAX_EQUIPMENT_AMOUNT:
                        message += "Przedmioty nie zmieszczą się w Twoim ekwipunku."
                        return self.response.bad_syntax(message)

                    item_in_map = player.location.get_item_by_id_from_dict(number, amount)

                    if item_in_map is None:
                        message += "Nie znaleziono przedmiotu o podanym indeksie."
                        return self.response.bad_syntax(message)

                    elif item_in_map is False:
                        message += "Błędna ilość przedmiotów do wzięcia."
                        return self.response.bad_syntax(message)

                    player.location.get_items_by_name(item_in_map.item.name, amount, player)
                    item_name = Highlighter.bold(item_in_map.item.name)

                    message += "Podniosłeś " + item_name + " (" + str(amount) + ") "
                    completion_message = player.check_if_quest_is_accomplished()

                    return self.response.success(message + completion_message)

                except ValueError:
                    item_name = " ".join(message_array)
                    try:
                        number = int(item_name)
                        item_in_map = player.location.get_item_by_id_from_dict(number)

                        if item_in_map is None:
                            message += "Nie znaleziono przedmiotu o podanym indeksie."
                            return self.response.bad_syntax(message)

                        else:
                            player.add_to_equipment(item_in_map.item)
                            item_name = item_in_map.item.name
                            player.location.remove_item(item_in_map)

                            message += "Podniosłeś " + Highlighter.bold(item_name)
                            completion_message = player.check_if_quest_is_accomplished()

                            return self.response.success(message + completion_message)

                    except ValueError:
                        item_in_map = player.location.get_item(item_name)

                        if item_in_map:
                            player.add_to_equipment(item_in_map.item)
                            player.location.remove_item(item_in_map)

                            message += "Podniosłeś " + Highlighter.bold(item_name)
                            completion_message = player.check_if_quest_is_accomplished()

                            return self.response.success(message + completion_message)

                        else:
                            message += "Nie znaleziono przedmiotu o podanej nazwie."
                            return self.response.bad_syntax(message)
            else:
                item_name = " ".join(message_array)

                try:
                    number = int(item_name)
                    item_in_map = player.location.get_item_by_id_from_dict(number)

                    if item_in_map is None:
                        message += "Nie znaleziono przedmiotu o podanym indeksie."
                        return self.response.bad_syntax(message)

                    else:
                        player.add_to_equipment(item_in_map.item)
                        item_name = item_in_map.item.name
                        player.location.remove_item(item_in_map)

                        message += "Podniosłeś " + Highlighter.bold(item_name)
                        completion_message = player.check_if_quest_is_accomplished()

                        return self.response.success(message + completion_message)

                except ValueError:
                    item_in_map = player.location.get_item(item_name)

                    if item_in_map:
                        player.add_to_equipment(item_in_map.item)
                        player.location.remove_item(item_in_map)

                        message += "Podniosłeś " + Highlighter.bold(item_name)
                        completion_message = player.check_if_quest_is_accomplished()

                        return self.response.success(message + completion_message)

                    else:
                        message += "Nie znaleziono przedmiotu o podanej nazwie."
                        return self.response.bad_syntax(message)

        elif len(message_array) == 1 and len(player.location.get_items()) == 1:
            item_in_map = player.location.get_single_item()
            item_name = item_in_map.item.name

            player.add_to_equipment(item_in_map.item)
            player.location.remove_item(item_in_map)

            message += "Podniosłeś " + Highlighter.bold(item_name)
            completion_message = player.check_if_quest_is_accomplished()

            return self.response.success(message + completion_message)

        else:
            message += "Składnia: %s nazwa_przedmiotu/numer_przedmiotu" % self.name
            return self.response.bad_syntax(message)


class RemoveCommand(Command):
    def execute(self):
        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) == 2:

            try:
                number = int(message_array[1])

            except ValueError:
                message = "Numer przedmiotu musi być liczbą naturalną."
                return self.response.bad_syntax(message)

            result = player.delete_item_from_inventory_by_number(number)

            if result is None:
                message = "Brak przedmiotu o takim indeksie."
                return self.response.bad_syntax(message)

            else:
                message = "Pomyślnie wyrzucono przedmiot."
                return self.response.success(message)

        elif len(message_array) == 3:

            try:
                number = int(message_array[1])
                amount = int(message_array[2])

            except ValueError:
                message = "Numer musi być liczbą naturalną."
                return self.response.bad_syntax(message)

            result = player.delete_item_from_inventory_by_number(number, amount)

            if result is None:
                message = "Brak przedmiotu o takim indeksie lub błędna ilośc."
                return self.response.bad_syntax(message)

            else:
                message = "Pomyślnie wyrzucono przedmiot x " + str(amount)
                return self.response.success(message)

        else:
            message = "Składnia: %s numer_przedmiotu" % self.name
            return self.response.bad_syntax(message)


class InventoryCommand(Command):
    def execute(self):

        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) == 1:

            inventory = player.get_all_inventory_as_dict()

            if inventory is None or not inventory:
                message = "Twój ekwipunek jest pusty."
                return self.response.no_effect(message)

            item_counter = 1
            message = Highlighter.information(Highlighter.bold("Ekwipunek: "))

            for key, value in inventory.items():
                message = message + Highlighter.bold(str(item_counter) + ".") + Highlighter.information(
                    key.item.name) + " (" + str(value) + "), "
                item_counter = item_counter + 1

            message = message[:-2]
            return self.response.success(message)

        elif len(message_array) == 2:

            try:
                number = int(message_array[1])

            except ValueError:
                message = "Numer przedmiotu musi być liczbą naturalną."
                return self.response.bad_syntax(message)

            item_eq = player.get_item_from_inventory_by_number(number)

            if item_eq is None:
                message = "Brak przedmiotu o takim indeksie."
                return self.response.bad_syntax(message)

            item_name = Highlighter.bold(item_eq.item.name)

            item_details_message = Highlighter.information("Szczegóły przedmiotu " + item_name)
            item_value_message = "Wartość: " + Highlighter.bold(str(item_eq.item.value))
            item_type_message = " Typ: " + Highlighter.bold(item_eq.item.item_type.name) + item_eq.item.get_param_desc()

            message = item_details_message + "<br>" + item_value_message + "<br>" + item_type_message + "<br><br> " \
                      + item_eq.item.description

            return self.response.success(message)

        else:
            message = "Składnia: " + self.name + " lub " + self.name + " numer_przedmiotu"
            return self.response.bad_syntax(message)


class CharacterCommand(AssetCommand):
    def execute(self):

        player = self.get_player_from_request()
        slots = player.get_all_equipped_slots()

        if slots is not None and slots:

            message = "Noszone przedmioty:"
            index = 1

            for slot in slots:
                row = "%s. %s: %s" % (str(index), slot.item.item_type.equipment_slot.name, slot.item.name)
                row = Highlighter.bold(row)

                message = message + "<br>" + row
                index += 1

            message = Highlighter.information(message)
            return self.response.success(message)

        else:
            message = "Brak noszonych przedmiotów."
            return self.response.bad_syntax(message)


class InCommand(AssetCommand):
    def execute(self):

        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) == 2:

            try:
                number = int(message_array[1])

            except ValueError:
                message = "Numer przedmiotu musi być liczbą naturalną."
                return self.response.bad_syntax(message)

            item_eq = player.get_item_from_inventory_by_number(number)

            if item_eq is None:
                message = "Brak przedmiotu o takim indeksie."
                return self.response.bad_syntax(message)

            is_slot_empty = player.is_slot_empty(item_eq.item)

            if is_slot_empty is not None:

                if is_slot_empty:
                    player.equip_item(item_eq.item)
                    message = "Pomyślnie włożono " + Highlighter.bold(item_eq.item.name)
                    return self.response.success(message)

                else:
                    message = "Już nosisz przedmiot wybranego typu."
                    return self.response.bad_syntax(message)

            else:
                message = "Przedmiotu nie da się włożyć."
                return self.response.bad_syntax(message)

        else:
            message = "Składnia: " + self.name + " numer_obiektu."
            return self.response.bad_syntax(message)


class OutCommand(Command):
    def execute(self):
        player = self.get_player_from_request()
        message_array = self.message_array

        if len(message_array) == 2:

            try:
                number = int(message_array[1])

            except ValueError:
                message = "Numer przedmiotu musi być liczbą naturalną."
                return self.response.bad_syntax(message)

            result = player.remove_from_equipped_slots_by_index(number)

            if result is None:
                message = "Nie nosisz żadnych przedmiotów."
                return self.response.bad_syntax(message)

            elif result is False:
                message = "Nie nosisz przedmiotu o podanym indeksie."
                return self.response.bad_syntax(message)

            else:
                message = "Pomyślnie sciągnięto przedmiot"
                return self.response.success(message)

        else:
            message = "Składnia: " + self.name + " numer_obiektu."
            return self.response.bad_syntax(message)