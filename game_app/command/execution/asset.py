# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.asset.complex.riddle import AbstractRiddle

from game_app.command.highlighter import Highlighter
from game_app.command.execution.others import Command
from game_app.controllers.basic_controllers import AssetController


class AssetCommand(Command):
    def execute(self):

        if len(self.message_array) >= 1:
            player = self.get_player_from_request()
            location = player.location

            assets_in_map = location.get_assets_in_map()

            if assets_in_map:

                message_array = self.message_array
                asset_name = " ".join(message_array[1:])

                if len(self.message_array) > 1:
                    try:
                        index = int(asset_name)
                        asset_in_map = location.get_asset_in_map_by_index(index)
                        if asset_in_map is None:
                            message = "Brak assetu o podanym indeksie."
                            return self.response.no_effect(message)
                    except ValueError:
                        asset_in_map = location.get_specific_asset_in_map(asset_name)
                else:
                    asset_in_map = assets_in_map[0]
                    asset_name = asset_in_map.asset.name

                if asset_in_map is None:
                    message = "Nie ma na tym polu assetu o takiej nazwie."
                    return self.response.no_effect(message)

                asset_object = AssetController.get_asset_object(asset_in_map)

                from game_app.command.settings.command_settings import get_command_from_shortcut

                if self.name in get_command_from_shortcut.keys():
                    command_shortcut = get_command_from_shortcut[self.name]
                else:
                    command_shortcut = None

                if asset_object.get_command_to_execute() not in [self.name, command_shortcut]:
                    message = "Podano błędną komendę do użycia tego obiektu. Nie zostanie on użyty."
                    return self.response.no_effect(message)

                if location.get_monster_in_map() is not None:
                    message = "Na tym polu znajduje się potwór. Musisz go najpierw pokonać."
                    return self.response.no_effect(message)

                if player.is_player_banned(asset_in_map):
                    message = "Postać czujnie na Ciebie spogląda i odmawia rozmowy z Tobą."
                    return self.response.no_effect(message)

                if len(self.message_array) == 1:
                    return asset_object.execute(player)

                #if asset_object.type.name.lower() != asset_name.lower():
                    #message = "Podano błędną nazwę obiektu. Nie zostanie on użyty."
                    #return self.response.no_effect(message)

                return asset_object.execute(player)

            else:
                message = "Na tym polu nie ma żadnego obiektu."
                return self.response.no_effect(message)
        else:
            message = "Składnia: " + self.name + " nazwa_obiektu."
            return self.response.bad_syntax(message)


class TalkCommand(Command):
    def execute(self):

        player = self.get_player_from_request()

        if player.asset_in_map is None:
            message = "Nie rozmawiasz teraz z żadną postacią niezależną NPC."
            return self.response.no_effect(message)

        location = player.location

        if player.asset_in_map:
            asset_object = AssetController.get_asset_object(player.asset_in_map)
            return asset_object.go_to_next_state(self.name, player)

        else:
            player.asset_in_map = None
            player.save()

            message = "Na tym polu nie ma żadnej postaci niezależnej."
            return self.response.no_effect(message)


class AnswerCommand(Command):
    def execute(self):

        message_array = self.message_array
        player = self.get_player_from_request()

        if len(message_array) > 1:

            location = player.location
            passage = location.get_passage()

            if passage:

                #assuming that there is only one passage on field
                passage = passage[0]

                asset_object = AssetController.get_asset_object(passage)

                if isinstance(asset_object, AbstractRiddle):

                    answer = " ".join(message_array[1:])

                    message = asset_object.get_response(answer)

                    if asset_object.check_answer(answer):

                        if passage:
                            player.enter_passage(passage.name)
                            message += '<br>' + 'Zostałeś przeniesiony na pole (%s,%s).' % (
                                passage.to_map.x, passage.to_map.y)

                            message = Highlighter.italic(message)

                            completion_message = player.check_if_quest_is_accomplished()

                            return self.response.success(message + completion_message)
                        else:
                            return self.response.no_effect('Nie istnieje przejście, '
                                                           'do którego powinieneś zostać przeniesiony')

                    else:
                        message = Highlighter.italic(message)
                        return self.response.success(message)

                else:
                    message = "To nie jest obiekt typu zagadka. Nie mógł ci zadać żadnego pytania."
                    return self.response.no_effect(message)

            else:
                message = "Na tym pole nie ma żadnego przejścia. Nie może być więc żadnej zagadki"
                return self.response.no_effect(message)

        else:
            message = "Składnia: %s treść_odpowiedzi" % self.name
            return self.response.bad_syntax(message)


class OpenChestCommand(Command):
    def execute(self):
        player = self.get_player_from_request()

        if player.asset_in_map is None:
            message = "Nie otwierasz teraz żadnej skrzyni."
            return self.response.no_effect(message)

        if player.asset_in_map:

            asset_object = AssetController.get_asset_object(player.asset_in_map)
            return asset_object.go_to_next_state(self.name, player)

        else:
            player.asset_in_map = None
            player.save()
            message = "Na tym polu nie ma żadnej skrzyni."
            return self.response.no_effect(message)


class QuestListCommand(Command):
    def execute(self):

        player = self.get_player_from_request()
        quest_list = player.get_quest_list()

        if quest_list:

            message_array = self.message_array
            from game_app.utils.discover.discover_graph import get_quest_by_name

            if len(message_array) == 1:

                manual_message = '----------- Dziennik zadań -----------' + '<br><br>'

                for index, quest in enumerate(quest_list, start=1):

                    if quest.state == -1:
                        is_done_info = ' - wykonano'
                    elif quest.state == 0:
                        is_done_info = ' - wykonano, nagroda czeka'
                    else:
                        is_done_info = ''

                    quest_object = get_quest_by_name(quest.quest.name)
                    quest_name = Highlighter.bold(quest.quest.name)

                    npc_name = quest.asset_in_map.asset.__unicode__()
                    quest_info = "%s. %s - %s:" % (str(index), npc_name, quest_name)

                    description_info = '<br>&nbsp;&nbsp;&nbsp;&nbsp;' + quest_object.description

                    if is_done_info:
                        quest_info += is_done_info
                    else:
                        quest_info += description_info

                    manual_message += quest_info + '<br> <br>'

                return self.response.success(manual_message)

            elif len(message_array) == 2:

                number_string = message_array[1]

                if number_string.isdigit() is False:
                    return self.response.bad_syntax('Drugim argumentem w tym poleceniu musi być liczba.')

                index = int(number_string) - 1

                if index >= len(quest_list):
                    return self.response.no_effect('Brak takiego questu w twoim dzienniku aktywności.')

                quest = quest_list[index]
                quest_object = get_quest_by_name(quest.quest.name)

                npc_name = quest.asset_in_map.asset.__unicode__()

                manual_message = 'Ten quest otrzymałeś od: %s. <br><br> Składa się on z następujących zadań: <br>' % npc_name

                for index, task in enumerate(quest_object.tasks, start=1):

                    if index < quest.state:
                        is_done_info = ' - wykonano'
                    else:
                        is_done_info = ''

                    manual_message += str(index) + '. ' + task.get_manual() + is_done_info + '<br>'

                return self.response.success(manual_message)

            else:
                message = "Składnia: %s / %s (nr_questu)." % (self.name, self.name)
                return self.response.bad_syntax(message)

        else:
            return self.response.no_effect('Nie dostałeś do tej pory żadnego questu.')

