# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.command.process.response import ResponseObject
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.utils.discover.discover_asset import all_asset_class_dictionary
from game_app.models import *


class LocationController():
    @staticmethod
    def _get_location_message(location):
        message = 'Twoja obecna lokalizacja to (%s,%s,%s).' % (str(location.x), str(location.y), location.level.name)
        message += " Rodzaj terenu: %s." % location.land_type.land_name

        if location.get_passage() is not None and len(location.get_passage()) != 0:
            message += "<br>Możliwe przejścia z tego pola: "

            for passage in location.get_passage():
                message = message + "<b>" + passage.name + "</b>, "

            message = message[:-2]

        message += "<br>"
        return Highlighter.location(message)


    @staticmethod
    def _get_description_message(location):
        message = location.description
        return Highlighter.description(message)

    @staticmethod
    def _get_surroundings_message(location):
        surroundings = location.get_surroundings()

        message = "Możesz poruszyć się na: "

        from game_app.command.settings.command_settings import translate_english_direction_to_polish

        for directionName in surroundings.keys():
            message = message + translate_english_direction_to_polish[directionName] + ", "

        if message.endswith(', '):
            message = message[:-2]
            message += "."

        return Highlighter.surroundings(message)

    @staticmethod
    def get_other_players_description(requested_user, location):
        response = ResponseObject()

        players_in_location = location.get_players()

        available_players = [player for player in players_in_location
                             if ws_handler.is_player_connected(player.user)]

        if len(available_players) > 1:
            message = "Na danym polu znajdują się następujący gracze: " + "<br>"

            index = 1
            for player in available_players:

                user = player.user
                if user != requested_user:
                    user_name = user.username
                    user_name = Highlighter.bold(user_name)

                    row = str(index) + ". " + user_name
                    row = Highlighter.players(row)

                    if index != len(available_players):
                        row += "<br>"

                    index += 1

                    row = Highlighter.indent(row)

                    message += row

            return response.success(message)
        else:
            message = 'Jesteś jedynym graczem na tym polu'
            return response.no_effect(message)


    @staticmethod
    def get_partial_description(location):
        location_message = LocationController._get_location_message(location)
        description_message = LocationController._get_description_message(location)
        surroundings_message = LocationController._get_surroundings_message(location)

        message = location_message + '<br>' + description_message + '<br>' + surroundings_message

        return message

    @staticmethod
    def choose_random_monster_from_location(location):

        monsters_land_type = location.get_monster_land_types()
        monsters_land_type_available = []

        for monster_land_type in monsters_land_type:
            for probability in range(monster_land_type.probability):
                monsters_land_type_available.append(monster_land_type)

        #if no monster for this land complex
        if len(monsters_land_type_available) == 0:
            return None

        monster_land_type = random.choice(monsters_land_type_available)
        monster = monster_land_type.monster
        monster_in_map = location.create_monster(monster)

        return monster_in_map

    @staticmethod
    def choose_random_item_from_location(location):

        items_land_type = location.get_item_land_types()
        items_land_type_available = []

        for item_land_type in items_land_type:
            for probability in range(item_land_type.probability):
                items_land_type_available.append(item_land_type)

        #if no item for this land complex
        if len(items_land_type_available) == 0:
            return None

        item_land_type = random.choice(items_land_type_available)
        item = item_land_type.item
        item_in_map = location.create_item(item)

        return item_in_map

    @staticmethod
    def get_map_fields(location):
        level = location.level

        #size of the map
        difference = 3

        min_x = location.x - difference
        max_x = location.x + difference + 1
        min_y = location.y - difference
        max_y = location.y + difference + 1

        # Creates 2D list
        map_array = [[None for x in range(min_x, max_x)] for x in range(min_y, max_y)]

        for x in range(min_x, max_x):
            for y in range(min_y, max_y):

                try:
                    map = Map.objects.get(x=x, y=y, level=level)
                except Map.DoesNotExist:
                    map = None

                x_coor = x - min_x
                y_coor = y - min_y

                if map:
                    land_name = map.land_type.land_name

                    passage = map.is_passage()

                    user = (x == location.x) and (y == location.y)

                    map_array[y_coor][x_coor] = {"y": y_coor, "x": x_coor, "land_name": land_name, "user": user,
                                                 "passage": passage}

                else:
                    map_array[y_coor][x_coor] = {"y": y_coor, "x": x_coor, "land_name": "pusta", "user": False,
                                                 "passage": False}

        return map_array

    @staticmethod
    def get_asset_description(location):

        assets_in_map = location.get_assets_in_map()

        if assets_in_map:

            description = ''
            asset_counter = 1

            if(len(assets_in_map)>0):
                description += "<br/>"

            for asset_in_map in assets_in_map:

                if isinstance(asset_in_map, NPC):
                    asset_class = all_asset_class_dictionary[asset_in_map.type_name]

                    npc_name = Highlighter.bold(asset_in_map.name)

                    message = Highlighter.bold(str(asset_counter)) + ". Spotkałeś postać niezależną: " + npc_name + ". " + asset_class.description + "<br/>"
                    description += Highlighter.information(message) + '<br>'


                elif asset_in_map:
                    asset_class = all_asset_class_dictionary[asset_in_map.asset.type_name]
                    asset_name = Highlighter.bold(asset_in_map.asset.name)

                    message = Highlighter.bold(str(asset_counter)) + ". Trafiłeś na: " + asset_name + ". " + asset_class.description + "<br/>"
                    description += Highlighter.information(message)

                asset_counter += 1

            return description

        else:
            return ""


class MoveController():
    def __init__(self):
        self.in_combat = False

    def get_partial_description(self, location, player, safe_mode):

        partial_description = LocationController.get_partial_description(location)
        other_players_description_response = LocationController.get_other_players_description(player, location)
        item_description = self.get_item_description(location, safe_mode)
        gold_description = self.get_gold_description(location)
        asset_description = LocationController.get_asset_description(location)
        monster_description = self.get_monster_description(location, player, safe_mode)

        message = partial_description

        if other_players_description_response.status == 'success':
            message += '<br><br>' + other_players_description_response.message

        if gold_description:
            message += '<br>' + gold_description

        if item_description:
            message += '<br>' + item_description

        if asset_description:
            message += '<br>' + asset_description

        if monster_description:
            message += '<br>' + monster_description

        return message

    def get_full_description_safe_mode(self, location, player):
        return self.get_partial_description(location, player, True)

    def get_full_description_unsafe_mode(self, location, player):
        message = self.get_partial_description(location, player, False)
        return LocationMessage(message, self.in_combat)

    def get_gold_description(self, location):
        message = ""
        gold = location.get_gold_in_map()
        if gold > 0:
            message = "<br>Na polu znajduje się " + GOLD.lower() + ": " + (Highlighter.bold(str(gold))) + "<br>"
        return message

    def get_item_description(self, location, is_safe_mode_active):
        items_in_map = location.get_items_in_map_as_dict()

        if items_in_map is not None and items_in_map:

            message = "<br>Znalazłeś "
            item_counter = 1

            for item_in_map, amount in items_in_map.items():

                if amount == 1:
                    message = message + (Highlighter.bold(str(item_counter) + ". ")) + Highlighter.information(
                        item_in_map.item.name) + ", "

                else:
                    message = message + (Highlighter.bold(str(item_counter) + ". ") + Highlighter.information(
                        item_in_map.item.name)) + " (" + str(amount) + "), "

                item_counter += 1

            message = message[:-2]
        else:

            if len(location.get_players()) > 1 or is_safe_mode_active:  #if there is a player on the current field
                return ""

            random_int = random.randint(0, 100)

            try:
                start_location_id = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                                    level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)).pk
            except Map.DoesNotExist:
                start_location_id = 0

            if random_int <= location.land_type.item_probability and location.pk != start_location_id:

                item_in_map = LocationController.choose_random_item_from_location(location)

                if item_in_map is None:
                    return ""

                item = item_in_map.item
            else:
                return ""

            message = Highlighter.information("<br>Znalazłeś ") + (Highlighter.bold("1. ")) + Highlighter.information(
                item.name)

        return message

    def get_monster_for_player_quest(self, player):
        from MUD_ADMIN.game.asset.quest.quest_structure import KillMonsterTask

        player_quests = player.get_quest_objects_and_quests_in_db()

        for quest_object, quest_in_db in player_quests:
            task = quest_object.tasks[quest_in_db.state - 1]

            if type(task) is KillMonsterTask and task.get_map() == player.location:
                return task.get_monster()
        return None

    def get_monster_description(self, location, player, is_safe_mode_active):

        monster_in_map = location.get_monster_in_map()

        if monster_in_map:
            monster = monster_in_map.monster

            if monster_in_map.player is not None:
                monster_name = Highlighter.bold(monster.name)
                return "Na tym polu znajduje się %s, który walczy obecnie z innych graczem." % monster_name

        else:
            if len(location.get_players()) > 1 or is_safe_mode_active:  #if there is a player on the current field
                return ""


            monster = self.get_monster_for_player_quest(player)

            if monster is not None:
                monster_in_map = location.create_monster(monster)
                monster = monster_in_map.monster
            else:
                random_int = random.randint(0, 100)

                #we cannot meet monster in starting map
                if random_int <= location.land_type.monster_probability:

                    monster_in_map = LocationController.choose_random_monster_from_location(location)
                    if monster_in_map is None:
                        return ""
                    monster = monster_in_map.monster
                else:
                    return ""

        introduction = "<br>" + Highlighter.monster_description("Spotkałeś potwora ")
        monster_name = Highlighter.monster_name(monster.name + ": ")
        monster_description = Highlighter.monster_description(monster.description)

        message = introduction + monster_name + "<br>" + monster_description

        try:
            start_location_id = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                                level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)).pk
        except Map.DoesNotExist:
            start_location_id = 1

        if location.pk != start_location_id:
            attack_message = self.monster_is_attacking(monster_in_map, player, is_safe_mode_active)

            if attack_message:
                attack_message = Highlighter.attack(attack_message)
                message += "<br>" + attack_message

        return message

    def monster_is_attacking(self, monster_in_map, player, is_safe_mode_active):
        if is_safe_mode_active:
            return None

        random_int = random.randint(0, 100)
        monster = monster_in_map.monster

        if random_int <= monster.aggression:
            self.in_combat = True
            monster_in_map.set_player(player)
            message = "Zostałeś zaatakowany przez %s!" % monster.name

        else:
            self.in_combat = False
            monster_in_map.set_player(None)
            message = None

        return message


class LocationMessage:
    def __init__(self, message, in_combat):
        self.message = message
        self.in_combat = in_combat