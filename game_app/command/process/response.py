# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from pydoc import html

from game_app.command.highlighter import Highlighter


class Request(object):
    def __init__(self, request):
        from game_app.command.settings.command_settings import command_shortcut, get_command_from_shortcut

        self.original_request = request
        self.user = request.user
        self.message_array = (html.escape(request.GET['command'])).split(" ")
        self.command_name = self.message_array[0]

        if self.command_name in command_shortcut:
            self.command_name = get_command_from_shortcut[self.command_name]

    def get_player(self):
        from game_app.models import Player

        try:
            return Player.objects.get(user=self.user)
        except Player.DoesNotExist:
            return None

    def get_user(self):
        return self.user

    def get_message_array(self):
        return self.message_array

    def get_command_name(self):
        return self.command_name

    def get_original_request(self):
        return self.original_request


class ResponseObject:
    def __init__(self, player=None):
        self.status = "initialised"
        self.message = ""

        if player:
            self.user = player.user
            self.location = player.location
        else:
            self.user = None
            self.location = None

        self.show_map = False

    def set_message(self, message):
        self.message = message

    def get_message(self):
        return self.message

    def wrong_command(self, message):
        self.status = 'invalid_command'
        self.message = Highlighter.wrong_command(message)
        return self

    def invalid_command(self):
        message = 'Podana komenda jest nieprawidłowa.'
        return self.wrong_command(message)

    def inappropriate_combat_command(self):
        message = 'Nie można wykonać danej komendy w czasie walki.'
        return self.wrong_command(message)

    def inappropriate_stealing_command(self):
        message = 'Nie można wykonać danej komendy w trybie kradzieży.'
        return self.wrong_command(message)

    def inappropriate_numeric_command(self):
        message = 'Nie można danej komendy w czasie, gdy twoja postać jest zajęta (np. rozmawia).'
        return self.wrong_command(message)

    def incorrect_syntax(self):
        incorrect_syntax_message = "Wprowadzono niewłaściwą liczbę argumentów." \
                                   + "<br>" + "W celu uzuskania więcej informacji wykonaj polecenie pomoc."

        return self.wrong_command(incorrect_syntax_message)

    def not_authenticated(self):
        self.status = 'permission_denied'
        self.message = "Użytkownik nie ma uprawnień do wykonania tego polecenia."
        self.message = Highlighter.wrong_command(self.message)
        return self

    def no_effect(self, message):
        self.status = 'no_effect'
        self.message = message
        return self

    def bad_syntax(self, message):
        self.status = 'no_effect'
        message = Highlighter.bad_syntax(message)
        self.message = message
        return self

    def success(self, message):
        self.status = 'success'
        self.message = message
        return self

    def get_creature(self):
        from game_app.controllers.basic_controllers import UserController

        if self.user:
            player = UserController.get_player_from_user(self.user)
            return player.creature
        else:
            return -1

    def get_player_location(self):
        return self.location

    def set_show_map(self):
        self.show_map = True

    def get_map(self):
        from game_app.command.location import LocationController

        if self.show_map:
            map_array = LocationController.get_map_fields(self.location)
            return map_array
        else:
            return None


class BattleResponseObject(ResponseObject):
    def __init__(self, player):
        ResponseObject.__init__(self, player)
        self.resurrection = False
        self.poison = None

    def set_resurrection(self, resurrection):
        self.resurrection = resurrection

    def get_resurrection(self):
        return self.resurrection

    def set_poison(self, poison):
        self.poison = poison

    def get_poison(self):
        return self.poison


class AssetResponseObject(ResponseObject):
    def __init__(self, player, asset_in_map_id=None, type=None):
        ResponseObject.__init__(self, player)
        self.update_asset_in_map = asset_in_map_id
        self.update_asset_type = type

    def get_update_asset_in_map(self):
        return self.update_asset_in_map

    def get_update_asset_type(self):
        return self.update_asset_type


class IntervalResponse(object):
    def __init__(self, message, stop):
        self.message = message
        self.stop = stop

    def get_message(self):
        return self.message

    def get_stop(self):
        return self.stop
