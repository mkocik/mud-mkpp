# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.command.settings.command_settings import commands_and_shortcuts, open_chest_commands, fight_commands, \
    special_attack_names


class CommandValidation(object):
    def is_command_valid(self, command_name):
        is_normal_command = command_name in commands_and_shortcuts
        is_numeric_command = self.is_numeric_command(command_name)
        is_open_chest_command = self.is_open_chest_command(command_name)
        is_combat_command = self.is_combat_command(command_name)

        return is_normal_command or is_numeric_command or is_open_chest_command or is_combat_command

    def is_open_chest_command(self, command_name):
        return command_name in open_chest_commands

    def is_combat_command(self, command_name):
        return command_name in fight_commands or command_name in special_attack_names

    def is_numeric_command(self, command_name):
        return command_name.isdigit()

    def is_authenticated(self, user):
        return user.is_authenticated()
