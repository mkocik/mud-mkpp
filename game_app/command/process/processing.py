# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.battle.effect import Paralysis

from game_app.command.process.response import ResponseObject, Request
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.command.execution.moving import MoveCommand
from game_app.command.process.validation import CommandValidation
from game_app.utils.discover.discover_command import get_command_properties
from game_app.command.execution.asset import TalkCommand, OpenChestCommand
from game_app.command.execution.others import Command, ManualCommand
from game_app.controllers.basic_controllers import UserController
from game_app.command.settings.command_definition import north, east, west, south
from game_app.command.settings.command_settings import translate_polish_direction_to_english, command_array, \
    open_chest_commands, trade_mode_commands, stealing_mode_commands, manual_commands


class ProcessCommand(object):
    def __init__(self, request):
        self.original_request = request
        self.request = Request(request)
        self.message_array = self.request.message_array
        self.name = self.request.command_name
        self.validation = CommandValidation()

        self.player = UserController.get_player_from_user(self.request.user)
        self.response = ResponseObject(self.player)


    def execute(self):

        if self.validation.is_command_valid(self.name) is False:
            return self.response.invalid_command()

        if self.validation.is_authenticated(self.request.user) is False:
            return self.response.not_authenticated()

        if self.name in manual_commands:
            return ManualCommand(self.request).execute()

        if self.player.is_busy():
            return self.execute_in_busy()

        if self.player.is_stealing():
            return self.execute_in_stealing_mode()

        command_properties = get_command_properties[self.name]

        if command_properties.is_available(self.player) is False:
            return self.response.no_effect('To polecenie nie jest w tej chwili dla ciebie dostępne.')

        if self.player.is_in_combat():
            if ws_handler.creature_has_effect(self.player, Paralysis):
                return self.response.no_effect('Jesteś sparaliżowany, nie możesz wykonać żadnego ruchu.')
            else:
                return self.execute_in_combat()

        return self.do_command()

    def do_command(self):
        command = CommandFactory.get_command_object(self.original_request)
        if command:
            return command.execute()
        else:
            return ResponseObject().incorrect_syntax()

    def execute_in_combat(self):
        if self.validation.is_combat_command(self.name):
            response = self.do_command()
            return response

        else:
            return self.response.inappropriate_combat_command()

    def execute_in_busy(self):
        if self.validation.is_numeric_command(self.name):
            command = TalkCommand(self.request)
            return command.execute()

        if self.player.in_trade and self.name in trade_mode_commands:
            return self.do_command()

        if self.name in open_chest_commands:
            command = OpenChestCommand(self.request)
            return command.execute()

        return self.response.inappropriate_numeric_command()

    def execute_in_stealing_mode(self):
        if self.validation.is_numeric_command(self.name):

            if int(self.message_array[0]) == 0:
                player = Command(self.request).get_player_from_request()
                player.stealing_from = None
                player.save()
                return Command(self.request).response.no_effect("Opuszczono tryb kradzieży")

        if self.name in stealing_mode_commands:
            return self.do_command()
        else:
            return self.response.inappropriate_stealing_command()


class CommandFactory:
    @classmethod
    def get_command_object(cls, request):
        request = Request(request)
        command_name = request.command_name
        arguments = len(request.message_array)

        if command_name in command_array:
            command_properties = get_command_properties[command_name]
            CommandClass = command_properties.command_class

            if command_name in [north.name, south.name, east.name, west.name]:
                command_object = MoveCommand(request, translate_polish_direction_to_english[command_name])

                if arguments == 1:
                    return command_object
                else:
                    return None

            else:
                command_object = CommandClass(request)
                only_one_argument = command_properties.only_one_argument

                if only_one_argument and arguments != 1:
                    return None
                else:
                    return command_object

        else:
            return None