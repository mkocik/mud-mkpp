# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.battle.attack_definition import normal_attack, kill

from game_app.command.settings.command_definition import *
from game_app.command.settings.command_types import BattleTypeCommand, CheatingTypeCommand, SpecialAttackTypeCommand
from game_app.utils.discover.discover_command import command_properties_array

##################################################
# file contains all command settings required to processing user command
#

command_array = list(map(lambda x: x.name, command_properties_array))
command_description = list(map(lambda x: x.description, command_properties_array))
command_shortcut = list(map(lambda x: x.shortcut, command_properties_array))
get_command_from_shortcut = dict(zip(command_shortcut, command_array))

commands_and_shortcuts = command_array + command_shortcut
commands_and_shortcuts = list(filter(lambda x: x is not None, commands_and_shortcuts))

##################################################
# commands divided to specific categories for manual listing
#

discover_world_commands = list(filter(lambda x: x.type is MovingTypeCommand, command_properties_array))
multiplayer_commands = list(filter(lambda x: x.type is MultiplayerTypeCommand, command_properties_array))
attack_commands = list(filter(lambda x: x.type is BattleTypeCommand, command_properties_array)) + [normal_attack]
item_commands = list(filter(lambda x: x.type is ItemTypeCommand, command_properties_array))
asset_commands = list(filter(lambda x: x.type is AssetTypeCommand, command_properties_array))
other_commands = list(filter(lambda x: x.type is OtherTypeCommand, command_properties_array))
cheat_commands = list(filter(lambda x: x.type is CheatingTypeCommand, command_properties_array))
trade_commands = list(filter(lambda x: x.type is TradeTypeCommand, command_properties_array))
steal_commands = list(filter(lambda x: x.type is StealingTypeCommand, command_properties_array))
special_attacks = list(filter(lambda x: x.type is SpecialAttackTypeCommand , command_properties_array))

special_attack_names = list(map(lambda x: x.name, special_attacks))

get_special_attack = {}

for special_attack in special_attacks:
    get_special_attack[special_attack.name] = special_attack

get_special_attack_time = {}

for special_attack in special_attacks:
    get_special_attack_time[special_attack.name] = special_attack.time

    if special_attack.shortcut is not None:
        get_special_attack_time[special_attack.shortcut] = special_attack.time



##################################################
# commands allowed to use in certain modes
#

fight_commands = [normal_attack.name, normal_attack.shortcut, kill.name]
trade_mode_commands = [buy.name, buy.shortcut, sell.name, sell.shortcut, show.name, show.shortcut]
stealing_mode_commands = [search.name, search.shortcut, steal.name, steal.shortcut]
open_chest_commands = ['L', 'P', '0']
manual_commands = [helpp.name, helpp.shortcut]

##################################################
# coordinates settings and directions translation
#

direction_coordinates = [(0, 1, "south"), (1, 0, "east"), (0, -1, "north"), (-1, 0, "west")]

translate_polish_direction_to_english = {
    north.name: "north",
    south.name: "south",
    east.name: "east",
    west.name: "west"
}

translate_english_direction_to_polish = {v: k for k, v in translate_polish_direction_to_english.items()}

