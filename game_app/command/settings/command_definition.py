# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from game_app.command.settings.command_types import MovingTypeCommand, MultiplayerTypeCommand, ItemTypeCommand, \
    AssetTypeCommand, TradeTypeCommand, StealingTypeCommand, OtherTypeCommand, BattleTypeCommand, CheatingTypeCommand
from game_app.command.execution.mode.battle import *
from game_app.command.execution.moving import *
from game_app.command.execution.multiplayer import *
from game_app.command.execution.items import *
from game_app.command.execution.asset import *
from game_app.command.execution.others import *
from game_app.command.execution.mode.trade import *
from game_app.command.execution.mode.steal import *


class CommandProperties:
    def __init__(self, name, description, command_class=None, type=None, shortcut=None, only_one_argument=False,
                 order=99, availability_function = None):
        self.name = name
        self.description = description
        self.shortcut = shortcut
        self.command_class = command_class
        self.type = type
        self.only_one_argument = only_one_argument
        self.order = order
        self.availability_function = availability_function

    def is_available(self, player):
        if self.availability_function is None:
            return True
        else:
            return self.availability_function.function(player)

    def get_name(self):
        return self.name

    def get_description(self):
        return self.description

    def get_command_class(self):
        return self.command_class

    def get_type(self):
        return self.type

    def get_shortcut(self):
        return self.shortcut

    def has_only_one_argument(self):
        return self.only_one_argument

    def get_order(self):
        return self.order

#############################
# Discovering world
#
north = CommandProperties("północ", "pozwala poruszyć się na odpowiednie pole", MoveCommand, MovingTypeCommand, "w",
                          only_one_argument=True, order=1)
south = CommandProperties("południe", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "s",
                          only_one_argument=True, order=2)
east = CommandProperties("wschód", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "d",
                         only_one_argument=True, order=3)
west = CommandProperties("zachód", "analogicznie jak wyżej", MoveCommand, MovingTypeCommand, "a",
                         only_one_argument=True, order=4)
look = CommandProperties("spójrz", "zwraca położenie użytkownika z opisem pola", LookCommand, MovingTypeCommand, "l",
                         only_one_argument=True, order=5)
teleport = CommandProperties("teleport", "", TeleportCommand, CheatingTypeCommand)

#############################
# Multiplayer game
#
player = CommandProperties("gracze", "zwraca graczy na bieżącym polu", PlayersCommand, MultiplayerTypeCommand, "g",
                           only_one_argument=True, order=1)
say = CommandProperties("powiedz", "mówi do graczy na bieżącym polu (powiedz wiadomość)", SayCommand,
                        MultiplayerTypeCommand, "p")
whisper = CommandProperties("szepnij", "pozwala szepnąc do gracza (szepnij nick_gracza wiadomość)", WhisperCommand,
                            MultiplayerTypeCommand, "sz")

########################
# Battle
#
fight = CommandProperties("walcz", "rozpoczyna walkę z napotkanym potworem (walcz) lub graczem (walcz z nazwa_gracza)",
                          FightCommand, BattleTypeCommand, "f", order=1)

#############################
# Item
#

gett = CommandProperties("weź",
                         "zabiera wybrany przedmiot z pola (weź nazwa_przedmiotu/numer [ilość_do_wzięcia] " +
                         "lub weź wszystko lub weź " + GOLD.lower() + ")", GetCommand, ItemTypeCommand, "get", order=2)
remove = CommandProperties("wyrzuć",
                           "wyrzuca wybrany przedmiot z ekwipunku (wyrzuć numer_przedmiotu [ilość_do_wyrzucenia])",
                           RemoveCommand, ItemTypeCommand, "r", order=3)
inventory = CommandProperties("ekwipunek",
                              "pozwala przejrzeć ekwipunek (i) lub zobaczyć opis przedmiotu (i numer_przedmiotu)",
                              InventoryCommand, ItemTypeCommand, "i", order=4)
character = CommandProperties("postać", "wyswietla noszone przedmioty", CharacterCommand, ItemTypeCommand, "c",
                              only_one_argument=True, order=5)

#in is a reserved word in python
inn = CommandProperties("włóż", "zakłada wybrany przedmiot (włóż numer_przedmiotu)", InCommand, ItemTypeCommand, "in",
                        order=6)
out = CommandProperties("zdejmij", "zdejmuje wybrany przedmiot (zdejmij numer_przedmiotu)", OutCommand, ItemTypeCommand,
                        "out", order=7)

#############################
# Asset commands
#
quest_list = CommandProperties("questy", "zwraca dziennik aktywności", QuestListCommand, AssetTypeCommand, "ql",
                               order=1)
use = CommandProperties("użyj", "uzywa obiektu, na który można trafić na mapie (np. Magiczny Kamień)", AssetCommand,
                        AssetTypeCommand, "u")

talk = CommandProperties("rozmowa", "rozpoczyna rozmowę z postacią niezależną", AssetCommand, AssetTypeCommand, "t")
answer = CommandProperties("odpowiedz", "odpowiada na zagadkę, jeśli na nią trafisz", AnswerCommand, AssetTypeCommand)
openn = CommandProperties("otwórz", "próbuje otworzyć skrzynię, jeśli na nią trafisz", AssetCommand, AssetTypeCommand,
                          "o")
passage = CommandProperties("przejdź", "wchodzi w podane przejście (przejdź nazwa_przejścia)", PassageCommand,
                            AssetTypeCommand, "e")

#############################
# Trade commands
#
show = CommandProperties("pokaż", "pokaż zawartość sklepu", ShowStoreCommand, TradeTypeCommand, "show", order=1)
buy = CommandProperties("kup", "kupuje przedmiot od postaci NPC", BuyCommand, TradeTypeCommand, "b", order=2)
sell = CommandProperties("sprzedaj", "sprzedaj przedmiot postaci NPC", SellCommand, TradeTypeCommand, "sp", order=3)

#############################
# Stealing commands
#
search = CommandProperties("przeszukaj", "Przeszukuje postać/npc o podanym imieniu/numerze ", SearchCommand,
                           StealingTypeCommand, "search", order=1)
steal = CommandProperties("ukradnij", "próbuje ukraść przedmiot w trybie kradzieży", StealCommand, StealingTypeCommand,
                          "steal", order=2)

#############################
# Others
#
helpp = CommandProperties("pomoc", "zwraca listę poleceń z opisem", HelpCommand, OtherTypeCommand, "?",
                          only_one_argument=True, order=1)
mapp = CommandProperties("mapa", "zwraca mapę", MapCommand, OtherTypeCommand, "m", only_one_argument=True, order=2)
skills = CommandProperties("umiejętności",
                           "umiejętności wszystkie(all)/moje(my)  wyświetla listę dostępnych umiejętności)", GetSkills,
                           OtherTypeCommand, "skills", order=3)
sett = CommandProperties("przydziel",
                         "pozwala przydzielić punkty atrybutów/umiejętności (przydziel nazwa_atrybutu/numer_umiejętności ilość)",
                         SetCommand, OtherTypeCommand, "add", order=4)
character_card = CommandProperties("karta", "karta - wyświetla kartę postaci gracza", GetCharacterCard,
                                   OtherTypeCommand, "card", order=5)


