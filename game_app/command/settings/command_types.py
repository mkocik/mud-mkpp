# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class CommandType(object):
    pass


class MovingTypeCommand(CommandType):
    title = 'Odkrywanie świata'


class MultiplayerTypeCommand(CommandType):
    title = 'Gra Multiplayer'


class BattleTypeCommand(CommandType):
    title = 'Walka podstawowa'


class SpecialAttackTypeCommand(CommandType):
    title = 'Ataki'


class ItemTypeCommand(CommandType):
    title = 'Przedmioty'


class AssetTypeCommand(CommandType):
    title = 'Obiekty na mapie'


class TradeTypeCommand(CommandType):
    title = 'Handel'


class StealingTypeCommand(CommandType):
    title = 'Kradzież'


class OtherTypeCommand(CommandType):
    title = 'Inne'


class CheatingTypeCommand(CommandType):
    title = 'Kody'


class OpeningChestTypeCommand(CommandType):
    title = 'Otwieranie skrzyni'

    manual = 'L - poruszenie wytrychu w lewo <br>' + \
             'P - poruszenie wytrychu w prawo <br>' + \
             '0 - wyjście <br>'


class TalkingTypeCommand(CommandType):
    title = 'Rozmowa'

    manual = '1,2,3... - wybierasz kwestię, którą chcesz powiedzieć <br>' + \
             '0 - wyjście <br>'
