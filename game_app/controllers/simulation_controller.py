# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import datetime
import pprint
import time
import re

from django.forms import model_to_dict

from MUD.settings import my_logger

from game_app.controllers.simulation_settings import parameter_names, get_time_msg, \
    get_no_of_events_msg, \
    get_hit_percentage_msg, get_hp_level_msg
from game_app.models import PlayableParameter, Attack, MonsterInMap, Map
from game_app.models.MonsterModel import MonsterParameter, MonsterAttack, Monster, SimulationInfo, SimulationData


class InitSimulationController():
    @classmethod
    def prepare_monster_objects(cls, monsters):
        my_monsters = []

        for monster in monsters:
            my_monster = {
                "pk": monster.pk,
                "name": monster.name,
                "hp": monster.default_hp,
                "special_attack_probability": monster.special_attack_probability
            }

            #setting monster parameters
            monster_parameters = MonsterParameter.objects.filter(monster=monster).order_by("pk")
            my_monster["parameters"] = {parameter_names[monster_parameter.parameter.name]: monster_parameter.state
                                        for monster_parameter in monster_parameters}

            #setting monster attacks
            monster_attacks = MonsterAttack.objects.filter(monster=monster)
            my_monster["attacks"] = list(map(lambda monster_attack: monster_attack.attack.name, monster_attacks))

            my_monsters.append(my_monster)

        return my_monsters


class PreSimulationController():
    @classmethod
    def _duplicate_instance_in_db(cls, pk, model):
        instance = model.objects.get(pk=pk)
        kwargs = model_to_dict(instance)
        kwargs.pop('id')
        return model.objects.create(**kwargs)

    @classmethod
    def create_monsters_from_request(cls, request):
        monster1 = cls.create_monster_from_request(request, 1)
        monster2 = cls.create_monster_from_request(request, 2)
        return [monster1, monster2]

    @classmethod
    def create_monster_from_request(cls, request, no):
        monster = cls._duplicate_instance_in_db(request.POST["monster_" + str(no)], Monster)

        for pol_name, eng_name in parameter_names.items():
            parameter = PlayableParameter.objects.get(name=pol_name)
            state = request.POST[eng_name + "_" + str(no)]
            MonsterParameter.objects.create(parameter=parameter, monster=monster, state=state)

        for attack_name in request.POST.getlist("monster_attacks_" + str(no)):
            attack = Attack.objects.get(name=attack_name)
            MonsterAttack.objects.create(attack=attack, monster=monster)

        monster_in_map = MonsterInMap.objects.create(monster=monster, map=Map.objects.last())

        monster_in_map.hp = request.POST['hp_' + str(no)]
        monster_in_map.save()

        return monster


class PostSimulationController():
    @classmethod
    def calculate_simulation_time(cls):
        end_time = datetime.datetime.now()
        start_time = SimulationInfo.objects.last().start_time + datetime.timedelta(hours=2)

        end_time = end_time.replace(tzinfo=None)
        start_time = start_time.replace(tzinfo=None)

        #pprint.pprint((end_time - start_time).total_seconds())

        return (end_time - start_time).total_seconds()

    @classmethod
    def save_info(cls, monsters, first_monster_is_attacking, message):

        simulation_time = cls.calculate_simulation_time()

        if first_monster_is_attacking is True:
            player_number = 1
        else:
            player_number = 2

        monster_name = monsters[0].name

        message = re.sub('<[^<]+?>', '', message)

        if message == "":
            return

        simulation_message = "%s, %s: %s" % (str(simulation_time), monster_name, message)
        #my_logger.info(simulation_message)

        simulation = SimulationInfo.objects.last()
        SimulationData.objects.create(simulation=simulation, player_number=player_number,
                                      event_description=simulation_message)


    @classmethod
    def gather_conclusion(cls, monsters):

        simulation_time = cls.calculate_simulation_time()

        monster1 = monsters[0]
        monster2 = monsters[1]

        monster1_is_dead = MonsterInMap.objects.filter(monster=monster1, map=Map.objects.last()).count() == 0
        monster2_is_dead = MonsterInMap.objects.filter(monster=monster2, map=Map.objects.last()).count() == 0

        winner = ""
        winner_hp_percentage = None

        if monster1_is_dead and monster2_is_dead:
            winner = "nikt"
        elif monster1_is_dead:
            winner = monster2.name
            monster2_in_map = MonsterInMap.objects.get(monster=monster2, map=Map.objects.last())
            winner_hp_percentage = round((monster2_in_map.hp / monster2.default_hp) * 100)
        elif monster2_is_dead:
            winner = monster1.name
            monster1_in_map = MonsterInMap.objects.get(monster=monster1, map=Map.objects.last())
            winner_hp_percentage = round((monster1_in_map.hp / monster1.default_hp) * 100)

        conclusion = "\n\nSymulacja trwała %s sek. " % simulation_time
        conclusion += get_time_msg(simulation_time)

        conclusion += "Walkę wygrał: %s \n" % winner
        conclusion += "\n\n"

        simulation = SimulationInfo.objects.last()
        no_of_events1 = SimulationData.objects.filter(player_number=1, simulation=simulation).count()
        no_of_hits1 = SimulationData.objects.filter(player_number=1, event_description__icontains="nie trafia",
                                                    simulation=simulation).count()
        if no_of_events1 != 0:
            hit_percentage1 = 100 - round((no_of_hits1 / no_of_events1) * 100)
        else:
            hit_percentage1 = 100

        no_of_events2 = SimulationData.objects.filter(player_number=2, simulation=simulation).count()
        no_of_hits2 = SimulationData.objects.filter(player_number=2, event_description__icontains="nie trafia",
                                                    simulation=simulation).count()

        if no_of_hits2 != 0:
            hit_percentage2 = 100 - round((no_of_hits2 / no_of_events2) * 100)
        else:
            hit_percentage2 = 100

        conclusion += get_no_of_events_msg(monster1, no_of_events1)
        conclusion += get_no_of_events_msg(monster2, no_of_events2)

        no_of_effects1 = SimulationData.objects.filter(player_number=1, event_description__icontains="efekty walki",
                                                       simulation=simulation).count()
        no_of_effects2 = SimulationData.objects.filter(player_number=2, event_description__icontains="efekty walki",
                                                       simulation=simulation).count()

        conclusion += "Liczba efektów walki wykonanych przez %s: %s \n" % (monster1.name, no_of_effects1)
        conclusion += "Liczba efektów walki wykonanych przez %s: %s \n" % (monster2.name, no_of_effects2)
        conclusion += "\n\n"

        conclusion += get_hit_percentage_msg(monster1, hit_percentage1)
        conclusion += get_hit_percentage_msg(monster2, hit_percentage2)



        if winner_hp_percentage is not None:
            conclusion += get_hp_level_msg(winner_hp_percentage)

        f = codecs.open("static/application.log", 'w', encoding='utf-8')
        f.truncate()

        f.write(conclusion) # python will convert \n to os.linesep
        f.close() # you can omit in most cases as the destructor will call if

    @classmethod
    def remove_fake_monsters(cls, monsters):
        for monster in monsters:
            monster.delete()