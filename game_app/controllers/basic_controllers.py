# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models import User, Player, NPCEquipment, NPCDefaultEquipment, AssetInMap


class UserController(object):
    @classmethod
    def get_player_from_user(cls, user):
        try:
            return Player.objects.get(user=user)
        except Player.DoesNotExist:
            return None

    @classmethod
    def get_user_from_user_name(cls, user_name):
        try:
            return User.objects.get(username=user_name)
        except User.DoesNotExist:
            return None

    @classmethod
    def get_all_users(cls):
        return User.objects.all()

    @classmethod
    def get_opponent_monster_in_map(cls, user):
        player = cls.get_player_from_user(user)

        monster_in_map = player.location.get_monster_in_map()

        if monster_in_map:
            if monster_in_map.player != player:
                return None
            else:
                return monster_in_map
        else:
            return None


class AssetController(object):
    @classmethod
    def get_asset_object(cls, model_object):
        from game_app.utils.discover.discover_asset import all_asset_class_dictionary

        name = model_object.asset.type_name

        class_object = all_asset_class_dictionary[name]
        return class_object(model_object)


class AssetInMapController(object):
    def update(self, id):
        from MUD_ADMIN.game.asset.complex.chest import AbstractChest
        from MUD_ADMIN.game.asset.complex.npc import AbstractNPC

        asset_in_map = AssetInMap.objects.get(pk=id)

        if asset_in_map:
            if asset_in_map.asset.type_name == AbstractNPC.type.name:

                NPCEquipment.objects.filter(asset_in_map=asset_in_map).delete()
                equipments = NPCDefaultEquipment.objects.filter(npc=asset_in_map.asset)

                for equipment in equipments:
                    NPCEquipment(item=equipment.item, asset_in_map=asset_in_map).save()

            elif asset_in_map.asset.type_name == AbstractChest.type.name:

                asset_in_map.is_used = False
                asset_in_map.save()