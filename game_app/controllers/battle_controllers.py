# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import copy
import random

from game_app.battle.effect import Fatality
from game_app.battle.effect_type import TimeEffectType
from game_app.controllers.basic_controllers import UserController
from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.models import MonsterAttack


class BattleController(object):
    def get_random_monster_attack(self, monster_attacks):
        dict = {}
        sum = 0

        for monster_attack in monster_attacks:
            for i in range(monster_attack.probability):
                dict[sum] = monster_attack
                sum += 1

        random_int = random.randint(0, sum-1)
        return dict[random_int]

    def get_monster_attack(self, request):
        from game_app.command.settings.command_settings import normal_attack, get_special_attack

        player = UserController.get_player_from_user(request.user)
        monster_in_map = player.location.get_monster_in_map()

        if monster_in_map is None:
            return None

        monster = monster_in_map.monster
        monster_attacks = MonsterAttack.objects.filter(monster=monster).order_by('id')

        if monster_attacks.count() > 0 and monster.special_attack_probability <= random.randint(0, 100):
            monster_attack = self.get_random_monster_attack(monster_attacks)
            special_attack = get_special_attack[monster_attack.attack.name]
        else:
            special_attack = normal_attack

        return copy.copy(special_attack)

    def get_monster_attack_from_monster(self, monster):
        from game_app.command.settings.command_settings import normal_attack, get_special_attack
        monster_attacks = MonsterAttack.objects.filter(monster=monster).order_by('id')

        if monster_attacks.count() > 0 and monster.special_attack_probability <= random.randint(0, 100):
            monster_attack = self.get_random_monster_attack(monster_attacks)
            special_attack = get_special_attack[monster_attack.attack.name]
        else:
            special_attack = normal_attack

        return copy.copy(special_attack)


class BattleEffectController(object):
    def execute_interval(self, effect_id):
        from game_app.utils.websockets.battle_websocket import ws_handler

        effect = ws_handler.get_interval_effect(effect_id)

        if effect is None:
            return True

        result = effect.interval_execute()
        stop_interval = result is None
        return stop_interval


class AttackEffectController(object):
    def execute_effects(self, effect_array, attacker, defender, injury):
        attacker_ws_id = attacker.get_ws_id()
        defender_ws_id = defender.get_ws_id()

        caused_effects = ws_handler.get_battle_effects(defender_ws_id)
        caused_own_effects = ws_handler.get_battle_effects(attacker_ws_id)
        caused_time_effects = []
        caused_own_time_effects = []

        effect_message = ''

        if effect_array:
            initial_effect_message = 'Miały miejsce następujące efekty walki: <br>'

            for effect in effect_array:
                random_int = random.randint(0, 100)

                if random_int <= effect.probability:
                    an_effect = copy.copy(effect)
                    an_effect.execute_function(attacker, defender, injury)

                    if type(an_effect) is not Fatality:
                        effect_message += an_effect.get_manual()

                    if an_effect.is_attacking:
                        caused_effects.append(an_effect)
                    else:
                        caused_own_effects.append(an_effect)

                    if type(an_effect.type) is TimeEffectType:
                        if an_effect.is_attacking:
                            caused_time_effects.append(an_effect)
                        else:
                            caused_own_time_effects.append(an_effect)

            if effect_message != '':
                effect_message = initial_effect_message + effect_message

        ws_handler.set_battle_effects(defender_ws_id, caused_effects)
        ws_handler.set_battle_effects(attacker_ws_id, caused_own_effects)

        ws_handler.handle_time_effects(caused_time_effects)
        ws_handler.handle_time_effects(caused_own_time_effects)

        return effect_message