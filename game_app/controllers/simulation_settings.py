# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models import PlayableParameter

parameters = PlayableParameter.objects.all()

parameter_names = {
    "Siła": "strength",
    "Zręczność": "dexterity",
    "Zwinność": "agility",
    "Inteligencja": "intelligence",
    "Wytrzymałość": "durability"
}

time_msg = {
    100000: "bardzo długa walka",
    50000: "długa walka",
    30000: "średniej długosci walka",
    10000: "krótka walka",
    0: "walka błyskawiczna"
}

no_of_events_msg = {
    25: "bardzo wiele ciosów",
    12: "wiele ciosów",
    5: "normalna ilość ciosów",
    0: "niewielka liczba ciosów"
}

hit_percentage_msg = {
    100: "doskonala celność",
    80: "bardzo dobra celność",
    60: "dobra celność",
    40: "słaba celność",
    20: "bardzo słaba celność",
    0: "tragiczna celność"
}

hp_level_msg = {
    100: "bez strat",
    80: "wysoki",
    50: "średni",
    20: "niski",
    0: "bardzo niski"
}


def get_proper_msg(dict, my_number):
    for key, value in dict.items():
        if my_number >= key:
            return dict[key]

    return None


def get_time_msg(my_number):
    return "Była to: " + get_proper_msg(time_msg, my_number) + "\n"


def get_no_of_events_msg(monster, my_number):
    return "%s zadał: %s ciosów, %s" % (monster.name, str(my_number), get_proper_msg(no_of_events_msg, my_number) + "\n")


def get_hit_percentage_msg(monster, my_number):
    return "%s miał celność: %s procent, %s" % (monster.name, my_number, get_proper_msg(hit_percentage_msg, my_number) + "\n")


def get_hp_level_msg(my_number):
    return "Poziom życia zwycięzcy to: %s procent, %s\n" %(my_number, get_proper_msg(hp_level_msg, my_number))