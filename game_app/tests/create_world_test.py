#this 2 lines are needed to mock Client object in Django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")


from django.utils import unittest
from django.test.client import Client
from django.test import TestCase
from django.test.client import RequestFactory


class CreateWorldTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.c = Client()

    def test_create(self):
        # response = self.client.post('/login/', {'username': 'root', 'password': 'root'}, follow=True)

        client = Client()
        client.login(username='root',password='root')

        response = client.get('/editor/create/')

        self.assertEqual(response.status_code, 200)

        # creator = CreateMap()
        # self.assertTrue(creator.execute())


if __name__ == '__main__':
    unittest.main()

