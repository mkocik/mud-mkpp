#this 2 lines are needed to mock Client object in Django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from django.utils import unittest
import random
import string

from game_app.command.settings.command_definition import helpp
from django.contrib.auth.models import User
from django.test.client import Client
from django.test.client import RequestFactory
import ast

class ViewsTest(unittest.TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = Client()

    def test_view(self):

        #as superuser
        result = self.client.login(username="root", password="root")
        self.assertTrue(result)

        #execute command without log in
        view_names = ["/index/", "/register/", "/game/", "/loginFail/", "/logout/", "/login/"]
        for view_name in view_names:
            client = Client()
            response = client.get(view_name)
            self.assertEqual(response.status_code, 200)

        #execute command after log in
        response = self.client.post('/register/', {'username': 'test', 'password': 'test'}, follow=True)
        result = self.client.login(username="test", password="test")
        self.assertTrue(result)
        view_names = ["/index/", "/register/", "/game/", "/loginFail/", "/logout/"]
        for view_name in view_names:
            response = self.client.get(view_name)
            self.assertEqual(response.status_code, 200)

    def test_login(self):

        #relation dictionary
        # 'gameSpace' is in /game/
        # 'mainFormRegister' is in /index/
        game = 'gameSpace'
        index = 'loginForm'

        #user passes logging
        client = Client()
        response = client.post('/register/', {'username': 'test', 'password': 'test'}, follow=True)
        response = client.post('/login/', {'username': 'test', 'password': 'test'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(game in response.content.decode("utf-8"))

        #wrong logging data
        client = Client()
        response = client.get('/logout/')
        response = client.post('/login/', {'username': 'test4', 'password': 'foo'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(index in response.content.decode("utf-8"))

        #wrong password
        client = Client()
        response = client.post('/login/', {'username': 'test', 'password': 'foo'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(index in response.content.decode("utf-8"))

        #superuser
        client = Client()
        response = client.post('/login/', {'username': 'root', 'password': 'root'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(game in response.content.decode("utf-8"))


    def test_register(self):

        #relation dictionary
        # 'gameSpace' is in /game/
        # 'mainFormRegister' is in /index/
        game = 'gameSpace'
        index = 'loginForm'
        register = 'registerView'

        client = Client()
        response = client.post('/register/', {'username': 'test', 'password': 'test'}, follow=True)
        response = client.post('/register/', {'username': 'test', 'password': 'foo'}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(register in response.content.decode("utf-8"))

        client = Client()
        login = ''.join(random.sample(string.digits, 8))
        password = ''.join(random.sample(string.digits, 8))

        response = client.post('/register/', {'username': login, 'password': password}, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertTrue(index in response.content.decode("utf-8"))

    def test_command_exe(self):

        #send AJAX
        command = helpp.name
        client = Client()
        response = client.get('/commandExe/', {'command': command},
                              HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        response.user = User.objects.get(username="test")

        string_response = response.content.decode("utf-8")
        #convert string to match ast.literal_eval format
        string_response = string_response.replace("null", "None")
        string_response = string_response.replace("false", "False")
        string_response = string_response.replace("true", "True")

        response = ast.literal_eval(string_response)  #convert string->directory
        self.assertEqual(response["status"], "permission_denied")

        #send without AJAX
        command = helpp.name
        response = client.get('/commandExe/', {'command': command})

        string_response = response.content.decode("utf-8")
        response = ast.literal_eval(string_response)  #convert string->directory

        self.assertEqual(response["status"], "error")



    def test_monster_attack(self):

        #########################
        #sending with AJAX
        client = Client()
        response = client.post('/register/', {'username': 'test', 'password': 'test'}, follow=True)
        response = client.post('/login/', {'username': 'test', 'password': 'test'}, follow=True)

        response = client.get('/monsterAttack/',
                              HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        string_response = response.content.decode("utf-8")

        #convert string to match ast.literal_eval format
        string_response = string_response.replace("null", "None")
        string_response = string_response.replace("false", "False")

        #convert string->directory
        response = ast.literal_eval(string_response)
        self.assertTrue(response["status"] in ["success", "no_effect"])

        ##############################
        #send without AJAX
        response = client.get('/monsterAttack/')

        string_response = response.content.decode("utf-8")
        #convert string->directory
        response = ast.literal_eval(string_response)

        self.assertEqual(response["status"], "error")


if __name__ == '__main__':
    unittest.main()