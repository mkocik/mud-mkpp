import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from django.test.client import Client
client = Client()
client2 = Client()

client.post('/register/', {"username": "test", "password": "test"})
client.login(username="test", password="test")

client2.post('/register/', {"username": "test2", "password": "test2"})
client2.login(username="test2", password="test2")

from game_app.tests.login_test import *

from game_app.tests.command.processing_test import *
from game_app.tests.command.execution.others_test import *

from game_app.tests.command.execution.items_test import *
from game_app.tests.command.execution.moving_test import *
from game_app.tests.command.execution.multiplayer_test import *
from game_app.tests.command.mode.battle_test import *

from game_app.tests.command.location_test import *
from game_app.tests.command.fight.fight_test import *

from game_app.tests.command.settings.websocket_handler_test import *

from game_app.tests.command.triumph_test import *
from game_app.tests.command.mode.trade_test import *
from game_app.tests.command.mode.steal_test import *
from game_app.tests.command.fight.attack_test import *

from game_app.tests.asset.asset_test import *
from game_app.tests.asset.npc_test import *
from game_app.tests.asset.quest_test import *
from game_app.tests.asset.reward_test import *

from game_app.tests.battle.user_effect_test import *
from game_app.tests.battle.user_monster_effect_test import *
from game_app.tests.battle.monster_effect_test import *


#from game_app.tests.views_test import *
#from game_app.tests.create_world_test import *
#from game_app.tests.create_map_test import *

