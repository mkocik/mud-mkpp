import os

from MUD_ADMIN.game.battle.attack_definition import normal_attack
from game_app.command.settings.command_definition import talk, south


os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.process.processing import CommandFactory
from game_app.controllers.basic_controllers import AssetController, UserController

from game_app.tests import CommandTest, WebsocketTest

from django.utils import unittest

from game_app.models import *
from django.test import TestCase


class NPCTest(TestCase):
    def setUp(self):
        self.location_pk = 1
        self.location = Map.objects.get(pk=self.location_pk)
        CommandTest.custom_set_up(self, self.location_pk)
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def before_conversation(self):
        player = Player.objects.get(user=self.user)

        asset_in_map = player.location.get_assets_in_map()

        if len(asset_in_map) == 0:
            AssetInMap.objects.filter(map=self.location).delete()
            asset_in_map = AssetInMap(map=self.location, asset=NPC.objects.all()[0])
            asset_in_map.save()
        else:
            asset_in_map = AssetInMap.objects.filter(map=self.location)[0]

        npc = AssetController.get_asset_object(asset_in_map)
        self.assertTrue(npc.state is None)

    def start_conversation(self):
        process_command = CommandTest.process_command(self, talk.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

    def some_talk(self, command):
        process_command = CommandTest.process_command(self, command)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

    def player_is_not_busy(self):
        player = Player.objects.get(user=self.user)
        player.asset_in_map = None
        player.save()

    def test_start_and_stop_conversation(self):
        self.player_is_not_busy()

        self._create_asset()

        #1, 2
        self.before_conversation()
        self.start_conversation()

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        asset_in_map = AssetInMap.objects.get(map=self.location)

        self.assertTrue(asset_in_map.state is not None)

        self.some_talk("1")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        #3 goes somewhere
        process_command = CommandTest.process_command(self, south.name)
        response = process_command.execute()

        self.assertTrue(response.status == 'invalid_command')

        self.player_is_not_busy()


    def test_conversation(self):
        self.player_is_not_busy()
        self._create_asset()

        self.start_conversation()
        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        self.some_talk("1")
        self.some_talk("2")

        player = Player.objects.get(user=self.user)
        asset_in_map = player.location.get_specific_asset_in_map("Tester")
        npc = AssetController.get_asset_object(asset_in_map)

        self.assertTrue(npc.state is None)
        self.assertTrue(player.asset_in_map is None)

        self.player_is_not_busy()

    def test_remembering_dialog_state(self):
        self.player_is_not_busy()
        self._create_asset()

        self.start_conversation()

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        self.some_talk("1")
        self.some_talk("2")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is None)

        self.start_conversation()

        player = Player.objects.get(user=self.user)
        asset_in_map = AssetInMap.objects.get(map=self.location)

        dialog_state = DialogState.objects.get(player=player, asset_in_map=asset_in_map)

        self.assertTrue(asset_in_map.state == 1)
        self.assertTrue(dialog_state.state == 1)
        self.assertTrue(player.asset_in_map is not None)

        self.player_is_not_busy()

    def _create_asset(self):
        graph = DialogGraph.objects.get(name="test_graph")

        NPC.objects.filter(name="Tester", graph=graph).delete()
        npc = NPC(name="Tester", graph=graph)
        npc.save()

        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        asset = AssetInMap(map=Map.objects.get(pk=self.location_pk),
                           asset=npc, is_busy=False)

        asset.save()
        return asset


class NPCMultiplayerTest(WebsocketTest):
    def setUp(self):
        WebsocketTest.setUp(self)
        self.location_pk = 1

    def talk(self):
        command = talk.name
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        return response

    def attack(self):
        command = normal_attack.name
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        return response

    def test_talking_two_players_to_one_npc(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        self._create_asset()

        player2 = Player.objects.get(user=self.user2)
        player2.asset_in_map = AssetInMap.objects.all()[0]
        player2.save()

        response = self.talk()
        self.assertTrue(response.status == "success")

    def test_attacking_player_who_is_busy(self):
        player2 = Player.objects.get(user=self.user2)
        player2.asset_in_map = AssetInMap.objects.all()[0]
        player2.save()

        response = self.attack()
        self.assertTrue(response.status == "no_effect")

    def test_multiple_npc_with_the_same_name(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        player = UserController.get_player_from_user(self.user)

        asset_in_map = AssetInMap(asset=Asset.objects.get(name='John'), map=player.location)
        asset_in_map.save()
        asset_in_map = AssetInMap(asset=Asset.objects.get(name='John'), map=player.location)
        asset_in_map.save()

        request = self.factory.get('/commandExe/', {"command": 't John'}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        self.assertTrue(response.status == 'success')

    def _create_asset(self):
        graph = DialogGraph.objects.get(name="test_graph")

        NPC.objects.filter(name="Postać niezależna", graph=graph).delete()
        npc = NPC(name="Postać niezależna", graph=graph)
        npc.save()

        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        asset = AssetInMap(map=Map.objects.get(pk=self.location_pk),
                           asset=npc)

        asset.save()
        return asset


if __name__ == '__main__':
    unittest.main()