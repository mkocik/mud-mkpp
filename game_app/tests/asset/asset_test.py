#this 2 lines are needed to mock Client object in Django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")
from game_app.command.settings.command_definition import use, talk, openn

from game_app.utils.discover.discover_asset import all_asset_class_dictionary

from game_app.tests import CommandTest

from django.utils import unittest

from game_app.models import *
from django.test import TestCase


class AssetCommandTest(TestCase):
    def setUp(self):
        self.location_pk = 1
        CommandTest.custom_set_up(self, self.location_pk)
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def test_execute_without_argument(self):
        self._delete_assets()
        process_command = CommandTest.process_command(self, use.name)
        response = process_command.execute()
        self.assertTrue(response.status == "no_effect")

    def test_execute_without_asset(self):
        process_command = CommandTest.process_command(self, use.name + " foo")
        response = process_command.execute()
        self.assertTrue(response.status == "no_effect")

    def test_execute_with_wrong_asset_name(self):
        self._create_asset('Źródło życia')
        process_command = CommandTest.process_command(self, use.name + " foo")
        response = process_command.execute()
        self.assertTrue(response.status == "no_effect")

    def test_execute_with_wrong_command_name(self):
        asset_name = 'Źródło życia'
        self._create_asset(asset_name)
        process_command = CommandTest.process_command(self, talk.name)
        response = process_command.execute()

        self.assertTrue(response.status == "no_effect")

    def test_execute_with_shortcut_command(self):
        asset_name = 'Źródło życia'
        self._create_asset(asset_name)
        process_command = CommandTest.process_command(self, use.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

    def test_execute_all_assets(self):
        for name, object in all_asset_class_dictionary.items():

            if isinstance(object, AbstractRiddle) is False:
                self._create_asset(name)
                process_command = CommandTest.process_command(self, use.name + " " + name)
                response = process_command.execute()
                self.assertTrue(response.status in ["success", "no_effect"])

    def test_execute_when_monster_on_field(self):
        self._create_monster()
        asset_name = 'Źródło życia'
        self._create_asset(asset_name)

        process_command = CommandTest.process_command(self, use.name)
        response = process_command.execute()

        self.assertTrue(response.status == "no_effect")

        map = Map.objects.get(pk=self.location_pk)
        MonsterInMap.objects.filter(map=map).delete()

    def _create_monster(self):
        map = Map.objects.get(pk=self.location_pk)
        count = MonsterInMap.objects.filter(map=map).count()

        if count == 0:
            MonsterInMap(map=map, monster=Monster.objects.all()[0]).save()


    def _create_asset(self, name):
        NormalAsset.objects.filter(name=name).delete()
        normal_asset = NormalAsset(name=name)
        normal_asset.save()

        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        asset = AssetInMap(map=Map.objects.get(pk=self.location_pk),
                           asset=normal_asset)
        asset.save()
        return asset

    def _delete_assets(self):
        try:
            assets = AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk))
            assets.delete()
        except AssetInMap.DoesNotExist:
            pass


class ChestTest(TestCase):
    def setUp(self):
        self.location_pk = 1
        CommandTest.custom_set_up(self, self.location_pk)
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def test_open(self):
        self._create_asset("prosta łatwa skrzynia")

        player = Player.objects.get(user=self.user)
        player.set_parameters(20, 20, 20, 100)

        process_command = CommandTest.process_command(self, openn.name)
        response = process_command.execute()
        self.assertTrue(response.status == "no_effect")

        player.set_parameters(20, 25, 25, 100)

        process_command = CommandTest.process_command(self, openn.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        self._delete_assets()

    def _create_asset(self, name):
        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        asset = AssetInMap(map=Map.objects.get(pk=self.location_pk),
                           asset=Asset.objects.filter(name=name)[0])
        asset.save()
        return asset

    def _delete_assets(self):
        try:
            assets = AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk))
            assets.delete()
        except AssetInMap.DoesNotExist:
            pass


class AdvancedChestTest(TestCase):
    def setUp(self):
        self.location_pk = 1
        CommandTest.custom_set_up(self, self.location_pk)
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def test_open(self):
        self._create_asset("prosta łatwa skrzynia")

        response = CommandTest.process_command(self, openn.name).execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        response = CommandTest.process_command(self, 'L').execute()
        self.assertTrue(response.status == "success")

        response = CommandTest.process_command(self, 'P').execute()
        self.assertTrue(response.status == "success")

        response = CommandTest.process_command(self, 'P').execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is None)

        self._delete_assets()

    def test_wrong_open(self):
        self._create_asset("prosta łatwa skrzynia")

        response = CommandTest.process_command(self, openn.name).execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)

        response = CommandTest.process_command(self, 'P').execute()
        self.assertTrue(response.status == "no_effect")

        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is None)

        self._delete_assets()

    def _create_asset(self, name):
        chest = Chest.objects.get(graph='L,-,P')

        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        asset = AssetInMap(map=Map.objects.get(pk=self.location_pk),
                           asset=chest)
        asset.save()

        return asset

    def _delete_assets(self):
        try:
            assets = AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk))
            assets.delete()
        except AssetInMap.DoesNotExist:
            pass


if __name__ == '__main__':
    unittest.main()