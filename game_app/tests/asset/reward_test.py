from django.utils import unittest
from django.test import TestCase

from game_app.command.settings.command_definition import talk
from game_app.controllers.basic_controllers import AssetController
from MUD_ADMIN.game.asset.quest.quest import *
from game_app.tests import CommandTest
from game_app.models import *


class QuestHelpTest(TestCase):
    def before_conversation(self):
        player = Player.objects.get(user=self.user)

        asset_in_map = player.location.get_assets_in_map()
        npc = AssetController.get_asset_object(asset_in_map)

        self.assertTrue(npc.state is None)

    def start_conversation(self):
        process_command = CommandTest.process_command(self, talk.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")
        return response

    def some_talk(self, command):
        process_command = CommandTest.process_command(self, command)
        response = process_command.execute()
        # print(response.message)
        self.assertTrue(response.status == "success")

    def _create_npc_with_graph(self, graph_name):
        location_pk = 31

        graph = DialogGraph.objects.get(name=graph_name)

        if NPC.objects.filter(name="Mniszek", graph=graph).count() > 0:
            npc = NPC.objects.get(name="Mniszek", graph=graph)
        else:
            npc = NPC(name="Mniszek", graph=graph)
            npc.save()

        self.npc = npc

        self.location = Map.objects.get(pk=location_pk)

        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(map=self.location,
                                  asset=npc, is_busy=False)

        asset_in_map.save()
        self.asset_in_map = asset_in_map

        return asset_in_map


class QuestTest(QuestHelpTest):
    def setUp(self):
        self.location_pk = 31
        CommandTest.custom_set_up(self, self.location_pk)

        DialogGraph.objects.filter(name='john_graph').delete()
        graph = DialogGraph(name='john_graph')
        graph.save()

        NPC.objects.filter(name="Mnich").delete()
        npc = NPC(name="Mnich", graph=graph)
        npc.save()

    def receiving_reward_after_accomplish_quest(self, quest_name):
        self._create_npc_with_graph("john_graph")

        player = Player.objects.get(user=self.user)

        QuestObject.objects.filter(name=quest_name).delete()
        QuestObject(name=quest_name).save()

        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map, state=0)
        quest.save()

        map = Map.objects.get(x=2, y=2, level=MapLevel.objects.get(level=0))

        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        #monster_in_map = MonsterInMap(monster=Monster.objects.gett(name='Mały robot'), mapp=mapp)
        #monster_in_map.save()

        player.set_location(Map.objects.get(pk=self.location_pk))

        self.start_conversation()

        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name))
        self.assertTrue(quest.state == -1)

        player = Player.objects.get(user=self.user)
        player.asset_in_map = None
        player.save()

        self.asset_in_map.state = None
        self.asset_in_map.save()

    def test_receiving_reward_after_accomplish_quest(self):
        player = Player.objects.get(user=self.user)
        MonsterInMap.objects.filter(map=player.location).delete()

        PlayerParameter.objects.filter(player=player).delete()
        params = PlayableParameter.objects.all()
        for param in params:
            PlayerParameter.objects.create(player=player, parameter=param, state=param.default_val)

        #1
        self.receiving_reward_after_accomplish_quest(reach_field_quest.name)

        #2
        player = Player.objects.get(user=self.user)
        initial_money = player.gold
        initial_experience = player.experience

        self.receiving_reward_after_accomplish_quest(kill_monster_quest.name)

        player = Player.objects.get(user=self.user)
        final_money = player.gold
        final_experience = player.experience

        self.assertTrue(initial_money + 100 == final_money)
        self.assertTrue(initial_experience + 100 == final_experience)

        #3
        player = Player.objects.get(user=self.user)
        PlayerEquipment.objects.filter(player=player, item=Item.objects.get(name='Buty ASCII')).delete()
        equipment = PlayerEquipment(player=player, item=Item.objects.get(name='Buty ASCII'))
        equipment.save()
        initial_durability = player.get_parameter('Wytrzymałość')

        self.receiving_reward_after_accomplish_quest(acquire_item_quest.name)

        player = Player.objects.get(user=self.user)
        final_durability = player.get_parameter('Wytrzymałość')

        self.assertTrue(initial_durability + 10 == final_durability)
        self.assertTrue(PlayerEquipment.objects.filter(player=player, item=Item.objects.get(name='Buty ASCII')).count() == 0)
        #4
        player = Player.objects.get(user=self.user)
        initial_no_of_items = PlayerEquipment.objects.filter(player=player).count()

        self.receiving_reward_after_accomplish_quest(speak_with_npc_quest.name)

        player = Player.objects.get(user=self.user)
        final_no_of_items = PlayerEquipment.objects.filter(player=player).count()

        self.assertTrue(initial_no_of_items < final_no_of_items)

        #5
        player = Player.objects.get(user=self.user)
        initial_experience = player.experience

        self.receiving_reward_after_accomplish_quest(complex_asset_quest.name)

        player = Player.objects.get(user=self.user)
        final_experience = player.experience

        self.assertTrue(initial_experience + 100 == final_experience)


if __name__ == '__main__':
    unittest.main()