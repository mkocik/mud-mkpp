from django.utils import unittest
from django.test import TestCase

from MUD_ADMIN.game.battle.attack_definition import normal_attack
from game_app.command.settings.command_definition import gett, openn, south, talk
from game_app.controllers.basic_controllers import AssetController
from MUD_ADMIN.game.asset.quest.quest import *
from game_app.tests import CommandTest
from game_app.models import *


class QuestHelpTest(TestCase):
    def before_conversation(self):
        player = Player.objects.get(user=self.user)

        asset_in_map = player.location.get_assets_in_map()
        npc = AssetController.get_asset_object(asset_in_map)
        self.assertTrue(npc.state is None)

    def start_conversation(self):
        process_command = CommandTest.process_command(self, talk.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

    def some_talk(self, command):
        process_command = CommandTest.process_command(self, command)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

    def _create_npc_with_graph(self, graph_name):
        location_pk = 31

        graph = DialogGraph.objects.get(name=graph_name)

        if NPC.objects.filter(name="Mniszek", graph=graph).count() > 0:
            npc = NPC.objects.get(name="Mniszek", graph=graph)
        else:
            npc = NPC(name="Mniszek", graph=graph)
            npc.save()

        self.npc = npc

        self.location = Map.objects.get(pk=location_pk)

        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(map=self.location, asset=npc, is_busy=False)

        asset_in_map.save()
        self.asset_in_map = asset_in_map

        return asset_in_map


class QuestTest(QuestHelpTest):
    def setUp(self):
        self.location_pk = 31
        CommandTest.custom_set_up(self, self.location_pk)

        DialogGraph.objects.filter(name='john_graph').delete()
        graph = DialogGraph(name='john_graph')
        graph.save()

        NPC.objects.filter(name="Mnich").delete()
        npc = NPC(name="Mnich", graph=graph)
        npc.save()


    def test_assign_task(self):
        self._create_npc_with_graph("person_ordering_graph")

        self.start_conversation()
        asset_in_map = AssetInMap.objects.get(map=self.location)
        player = Player.objects.get(user=self.user)
        self.assertTrue(asset_in_map.state == 0)
        self.assertTrue(player.asset_in_map is not None)

        self.some_talk("1")
        asset_in_map = AssetInMap.objects.get(map=self.location)
        player = Player.objects.get(user=self.user)
        self.assertTrue(asset_in_map.state == 1)
        self.assertTrue(player.asset_in_map is not None)

        self.some_talk("1")
        asset_in_map = AssetInMap.objects.get(map=self.location)
        player = Player.objects.get(user=self.user)
        self.assertTrue(asset_in_map.state is None)
        self.assertTrue(player.asset_in_map is not None)

        player = Player.objects.get(user=self.user)

        asset_in_map = AssetInMap.objects.get(map=self.location)
        task_is_assigned = asset_in_map.check_if_quest_was_assigned(reach_field_quest.name, player)
        self.assertTrue(task_is_assigned is not None)


    def test_speak_with_npc_quest(self):
        self._create_npc_with_graph("john_graph")
        quest_name = speak_with_npc_quest.name

        player = Player.objects.get(user=self.user)
        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map)
        quest.save()

        self.start_conversation()
        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)
        asset_in_map = AssetInMap.objects.get(map=self.location, asset=self.npc)
        self.assertTrue(asset_in_map.state == 0)

        self.some_talk("2")
        player = Player.objects.get(user=self.user)
        self.assertTrue(player.asset_in_map is not None)
        asset_in_map = AssetInMap.objects.get(map=self.location, asset=self.npc)
        self.assertTrue(asset_in_map.state == 2)

        self.some_talk("2")

        asset_in_map = AssetInMap.objects.get(map=self.location, asset=self.npc)
        self.assertTrue(asset_in_map.state == 4)

        self.some_talk("2")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name='Zdobądź sekretną informację.'),
                                  asset_in_map=asset_in_map)

        self.assertTrue(quest.state == 0)


    def test_reach_field_quest(self):
        self._create_npc_with_graph("john_graph")
        quest_name = reach_field_quest.name

        player = Player.objects.get(user=self.user)
        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map)
        quest.save()

        map = Map.objects.get(x=2, y=2, level=MapLevel.objects.get(level=0))

        MonsterInMap.objects.filter(monster=Monster.objects.get(name='Mały robot'), map=map).delete()
        monster_in_map = MonsterInMap(monster=Monster.objects.get(name='Mały robot'), map=map)
        monster_in_map.save()

        player.set_location(map)
        player.set_creature(0)

        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        process_command = CommandTest.process_command_without_modifications(self, normal_attack.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)
        self.assertTrue(quest.state == 0)


    def test_kill_monster_quest_get_money(self):
        self._create_npc_with_graph("john_graph")
        quest_name = kill_monster_quest.name

        player = Player.objects.get(user=self.user)
        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map)
        quest.save()

        map = Map.objects.get(x=2, y=2, level=MapLevel.objects.get(level=0))

        MonsterInMap.objects.filter(monster=Monster.objects.get(name='Mały robot'), map=map).delete()
        monster_in_map = MonsterInMap(monster=Monster.objects.get(name='Mały robot'), map=map)
        monster_in_map.save()

        initial_gold = player.gold

        player.set_location(map)
        player.set_creature(0)

        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        process_command = CommandTest.process_command_without_modifications(self, normal_attack.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)
        self.assertTrue(quest.state == -1)

        final_gold = player.gold
        self.assertTrue(initial_gold + 100 == final_gold)


    def test_acquire_item_quest(self):
        self._create_npc_with_graph("john_graph")
        quest_name = acquire_item_quest.name

        player = Player.objects.get(user=self.user)

        quest_object = QuestObject.objects.get(name=quest_name)

        Quest.objects.filter(player=player, quest=quest_object,
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=quest_object, asset_in_map=self.asset_in_map)
        quest.save()

        map = Map.objects.get(x=2, y=2, level=MapLevel.objects.get(level=0))
        MonsterInMap.objects.filter(map=map).delete()

        item_name = "Buty ASCII"
        ItemInMap.objects.filter(map=map).delete()
        item_in_map = ItemInMap(item=Item.objects.get(name=item_name), map=map)
        item_in_map.save()

        player.set_location(map)

        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        number = Quest.objects.filter(player=player, quest=quest_object, asset_in_map=self.asset_in_map).count()

        self.assertTrue(number > 0)

        process_command = CommandTest.process_command_without_modifications(self, gett.name + ' 1')
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)
        self.assertTrue(quest.state == 0)

    def test_use_asset_quest(self):
        self._create_npc_with_graph("john_graph")
        quest_name = use_asset_quest.name

        player = Player.objects.get(user=self.user)
        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map)
        quest.save()

        map = Map.objects.get(x=5, y=5, level=MapLevel.objects.get(level=0))
        MonsterInMap.objects.filter(map=map).delete()

        chest = Chest.objects.filter(resistance=25)[0]
        AssetInMap.objects.filter(map=map).delete()
        asset_in_map = AssetInMap(asset=chest, map=map)
        asset_in_map.save()

        player.set_location(map)
        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        process_command = CommandTest.process_command_without_modifications(self, openn.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)

        self.assertTrue(quest.state == 0)

    def test_complex_quest(self):
        self._create_npc_with_graph("john_graph")
        quest_name = complex_asset_quest.name

        player = Player.objects.get(user=self.user)
        Quest.objects.filter(player=player, quest=QuestObject.objects.get(name=quest_name),
                             asset_in_map=self.asset_in_map).delete()
        quest = Quest(player=player, quest=QuestObject.objects.get(name=quest_name), asset_in_map=self.asset_in_map)
        quest.save()

        #first task

        map = Map.objects.get(x=2, y=1, level=MapLevel.objects.get(level=0))

        MonsterInMap.objects.filter(monster=Monster.objects.get(name='Mały robot'), map=map).delete()
        monster_in_map = MonsterInMap(monster=Monster.objects.get(name='Mały robot'), map=map)
        monster_in_map.save()

        player.set_location(map)
        player.set_creature(0)

        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        process_command = CommandTest.process_command_without_modifications(self, normal_attack.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)
        self.assertTrue(quest.state == 2)

        #second task

        process_command = CommandTest.process_command_without_modifications(self, south.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        map = Map.objects.get(x=2, y=2, level=MapLevel.objects.get(level=0))

        MonsterInMap.objects.filter(monster=Monster.objects.get(name='Mały robot'), map=map).delete()
        monster_in_map = MonsterInMap(monster=Monster.objects.get(name='Mały robot'), map=map)
        monster_in_map.save()

        player.set_location(map)
        player.set_creature(0)

        player.set_parameters(1000, 1000, 1000, 1000, 1000)

        process_command = CommandTest.process_command_without_modifications(self, normal_attack.name)
        response = process_command.execute()
        self.assertTrue(response.status == "success")

        player = Player.objects.get(user=self.user)
        quest = Quest.objects.get(player=player, quest=QuestObject.objects.get(name=quest_name),
                                  asset_in_map=self.asset_in_map)
        self.assertTrue(quest.state == 0)


if __name__ == '__main__':
    unittest.main()