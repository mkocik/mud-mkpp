import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.tests import CommandTest
from MUD_ADMIN.game.battle.winner_bonus import WinnerBonusExperience, WinnerBonusGold, WinnerBonusGettingParameter, \
    WinnerBonusGettingItem, WinnerBonusLeavingItem

from django.utils import unittest
from game_app.models import *
from django.test import TestCase


class TriumphTest(TestCase):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)


    def test_execute_WinnerBonusrExperience(self):
        #increase_experience_by_difference
        player = Player.objects.get(user=self.user)
        initial_experience = player.experience

        opponent = Player.objects.get(user=self.user2)
        opponent.experience = 1000
        opponent.save()

        WinnerBonusExperience.increase_experience_by_difference(player, opponent)

        player2 = Player.objects.get(user=self.user)
        opponent2 = Player.objects.get(user=self.user2)

        self.assertTrue(initial_experience < player2.experience)

        #increase_experience_by_difference
        initial_experience = player2.experience
        WinnerBonusExperience.increase_experience_by_difference(player2, opponent2)

        player3 = Player.objects.get(user=self.user)
        opponent3 = Player.objects.get(user=self.user2)
        self.assertTrue(initial_experience < player3.experience)

    def triumph_over_getting_item(self, player, opponent, function):
        initial_equipment = len(opponent.get_equipment())
        initial_player_equipment = len(player.get_equipment())

        function(player, opponent)

        self.assertTrue(initial_equipment > len(opponent.get_equipment()))
        self.assertTrue(initial_player_equipment < len(player.get_equipment()))

    def test_execute_WinnerBonusGettingItem(self):
        player = Player.objects.get(user=self.user)
        opponent = Player.objects.get(user=self.user2)

        items = Item.objects.all()
        for i in range(6):
            PlayerEquipment.objects.create(player=opponent, item=items[i])

        self.triumph_over_getting_item(player, opponent, WinnerBonusGettingItem.take_away_precious_items)
        self.triumph_over_getting_item(player, opponent, WinnerBonusGettingItem.take_away_precious_percentage_of_items)
        self.triumph_over_getting_item(player, opponent, WinnerBonusGettingItem.take_away_random_items)
        self.triumph_over_getting_item(player, opponent, WinnerBonusGettingItem.take_away_random_percentage_of_items)


    def triumph_over_leaving_item(self, player, opponent, function):
        initial_equipment = len(opponent.get_equipment())
        initial_equipment_on_field = ItemInMap.objects.filter(map=player.location).count()

        function(player, opponent)

        self.assertTrue(initial_equipment > len(opponent.get_equipment()))
        self.assertTrue(initial_equipment_on_field < ItemInMap.objects.filter(map=player.location).count())

    def test_execute_WinnerBonusLeavingItem(self):
        player = Player.objects.get(user=self.user)
        opponent = Player.objects.get(user=self.user2)

        items = Item.objects.all()
        for i in range(6):
            equipment = PlayerEquipment(player=opponent, item=items[i])
            equipment.save()

            player_slot = PlayerEquipmentSlot(player=opponent, item=items[i])
            player_slot.save()

        self.triumph_over_leaving_item(player, opponent, WinnerBonusLeavingItem.take_away_random_items)
        self.triumph_over_leaving_item(player, opponent, WinnerBonusLeavingItem.take_away_random_percentage_of_items)
        self.triumph_over_leaving_item(player, opponent, WinnerBonusLeavingItem.take_away_precious_items)
        self.triumph_over_leaving_item(player, opponent, WinnerBonusLeavingItem.take_away_precious_percentage_of_items)



    def test_execute_WinnerBonusGold_take_away_percentage_of_gold(self):
        #increase_experience_by_difference
        player = Player.objects.get(user=self.user)
        initial_player_gold = player.gold

        opponent = Player.objects.get(user=self.user2)
        initial_opponent_gold = opponent.gold

        WinnerBonusGold.take_away_percentage_of_gold(player, opponent)

        player2 = Player.objects.get(user=self.user)
        opponent2 = Player.objects.get(user=self.user2)

        self.assertTrue(initial_player_gold < player2.gold)
        self.assertTrue(initial_opponent_gold > opponent2.gold)

    def test_execute_WinnerBonusGold_leave_percentage_of_gold(self):
        #increase_experience_by_difference
        player = Player.objects.get(user=self.user)
        player.set_location(self.location)

        opponent = Player.objects.get(user=self.user2)
        opponent.set_location(self.location)
        opponent.gold = 100
        opponent.save()
        initial_opponent_gold = opponent.gold

        try:
            gold_in_map = GoldInMap.objects.get(map=player.location)
            money_on_field = gold_in_map.amount
        except GoldInMap.DoesNotExist:
            money_on_field = 0

        WinnerBonusGold.leave_percentage_of_gold(player, opponent)

        opponent2 = Player.objects.get(user=self.user2)

        self.assertTrue(initial_opponent_gold > opponent2.gold)

        money_on_field2 = GoldInMap.objects.get(map=player.location).amount

        self.assertTrue(money_on_field < money_on_field2)

    def test_execute_WinnerBonusGettingParameter(self):
        player = Player.objects.get(user=self.user)
        opponent = Player.objects.get(user=self.user2)

        opponent_parameters = PlayerParameter.objects.filter(player=opponent)

        initial_sum = 0
        for parameter in opponent_parameters:
            initial_sum += parameter.state

        WinnerBonusGettingParameter.decrease_parameter(player, opponent)

        opponent_parameters = PlayerParameter.objects.filter(player=opponent)

        sum = 0
        for parameter in opponent_parameters:
            sum += parameter.state

        self.assertTrue(initial_sum > sum)




if __name__ == '__main__':
    unittest.main()
