import os
from game_app.models import PlayerParameter

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from unittest.mock import Mock
from django.contrib.auth.models import User

from django.test import RequestFactory, Client
from django.utils import unittest
from django.test import TestCase

from MUD_ADMIN.game.battle.attack_definition import eruption, harm_limb, freeze, ogre_hit, kill
from game_app.command.settings.command_settings import special_attacks
from game_app.battle.attack import *
from game_app.command.process.processing import CommandFactory, ProcessCommand


class BasicAttackUsageTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        client = Client()
        client2 = Client()

        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")

        self.player = Player.objects.get(user=self.user)
        self.player2 = Player.objects.get(user=self.user2)

        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player2.set_location(Map.objects.get(pk=1))  #place (0,0)

        self.location = Map.objects.get(pk=3)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)
        self.player.set_creature(self.user2.pk)
        self.player2.set_creature(self.user.pk)

    def special_attack(self, command):
        request = self.factory.get('/commandExe/', {"command": command}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()
        return response

    def special_attack_process_command(self, command):
        request = self.factory.get('/commandExe/', {"command": command}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        response = ProcessCommand(request).execute()
        return response

    def test_special_attack_execution(self):
        for attack in special_attacks:
            response = self.special_attack(attack.name)
            self.assertTrue(response.status in ['success', 'no_effect'])

    def test_ogre_hit(self):
        initial_strength = self.player.get_parameter('Siła')

        self.player.set_parameters(20, 250, 250, 100)
        response = self.special_attack(ogre_hit.name)
        player2 = UserController.get_player_from_user(self.user2)
        final_strength = player2.get_parameter('Siła')

        self.assertTrue(final_strength == initial_strength / 2)

    def test_freeze(self):
        initial_durability = self.player2.get_clear_parameter('Wytrzymałość')

        self.player.set_parameters(20, 250, 250, 100)
        response = self.special_attack(freeze.name)

        player2 = UserController.get_player_from_user(self.user2)
        final_durability = player2.get_clear_parameter('Wytrzymałość')

        self.assertTrue(ws_handler.creature_has_effect(player2, Paralysis) is True)
        self.assertTrue(initial_durability > 0 and final_durability == 0)
        self.assertTrue(response.status == 'success')

        response = self.special_attack(kill.name)
        player2 = UserController.get_player_from_user(self.user2)
        ws_handler.stop_all_effects(player2)

        player2 = UserController.get_player_from_user(self.user2)
        durability_after_all = player2.get_clear_parameter('Wytrzymałość')
        #print(durability_after_all)
        self.assertTrue(durability_after_all == initial_durability)

    def test_harm_limb(self):
        initial_durability = self.player2.get_clear_parameter('Wytrzymałość')
        self.player.set_parameters(20, 250, 250, 100)
        response = self.special_attack(harm_limb.name)

        player2 = UserController.get_player_from_user(self.user2)
        final_durability = player2.get_clear_parameter('Wytrzymałość')

        self.assertTrue(response.status == 'success')
        self.assertTrue(final_durability == initial_durability - 1)

    def test_eruption(self):
        self.player.set_parameters(20, 250, 250, 100)
        response = self.special_attack(eruption.name)
        player2 = UserController.get_player_from_user(self.user2)
        self.assertTrue(ws_handler.creature_has_effect(player2, Paralysis) is True)


    def test_changing_attributes_during_attack(self):
        parameter_name = PlayableParameter.objects.get(name='Zwinność')

        if PlayerParameter.objects.filter(player=self.player, parameter=parameter_name).count() != 1:
            PlayerParameter.objects.filter(player=self.player, parameter=parameter_name).delete()
            PlayerParameter.objects.create(player=self.player, parameter=parameter_name, state=10)

        if PlayerParameter.objects.filter(player=self.player2, parameter=parameter_name).count() != 1:
            PlayerParameter.objects.filter(player=self.player2, parameter=parameter_name).delete()
            PlayerParameter.objects.create(player=self.player2, parameter=parameter_name, state=10)

        initial_agility = 10
        initial_player = Player.objects.get(pk=self.player.pk)
        initial_player.set_parameter('Zręczność', initial_agility)

        attr_mod = AttributeBattleModification(initial_player, kill.mod_attributes)
        attr_mod.execute()

        player = Player.objects.get(pk=self.player.pk)
        final_agility = player.get_parameter('Zręczność')
        self.assertTrue(initial_agility < final_agility)

        attr_mod.undo()

        final_player = Player.objects.get(pk=self.player.pk)
        final_agility_2 = final_player.get_parameter('Zręczność')

        self.assertTrue(initial_agility == final_agility_2)

        for parameter_name in ['Siła', 'Zręczność', 'Zwinność']:
            self.assertTrue(initial_player.get_parameter(parameter_name) == final_player.get_parameter(parameter_name))



if __name__ == '__main__':
    unittest.main()