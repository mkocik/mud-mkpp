#this 2 lines are needed to mock Client object in Django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.execution.fight.monster_fight import MonsterFightController
from game_app.utils.websockets.battle_websocket import ws_handler

from MUD_ADMIN.game.battle.attack_definition import normal_attack


from game_app.command.process import processing
from game_app.controllers.basic_controllers import UserController

from game_app.tests import WebsocketTest

from unittest.mock import Mock

from MUD_ADMIN.game.properties import real_attack_probability_function
from game_app.command.process.processing import CommandFactory

from django.test.client import Client

from django.utils import unittest
from django.test.client import RequestFactory
from game_app.models import *
from django.test import TestCase


class FightingControllerTest(TestCase):
    pass


class FightUserUserTest(WebsocketTest):
    def setUp(self):
        WebsocketTest.setUp(self)
        self.player.set_creature(self.user2.pk)
        self.player2.set_creature(self.user.pk)

    def attack(self):
        command = normal_attack.name
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        return response

    def test_attack_without_opponent(self):
        self.player.set_creature(-1)
        response = self.attack()
        self.assertEqual(response.status, "no_effect")

        self.player.set_creature(99999999)
        response = self.attack()
        self.assertEqual(response.status, "no_effect")
        self.player.set_creature(self.user2.pk)

    def test_opponent_on_other_location(self):
        self.player2.set_location(Map.objects.get(pk=2))
        response = self.attack()
        self.assertEqual(response.status, "no_effect")

    def test_attack_with_not_existing_opponent(self):
        self.player2.set_creature(99999999)
        response = self.attack()
        self.assertEqual(response.status, "no_effect")
        self.player.set_creature(self.user2.pk)

    def test_attack_with_logged_out_opponent(self):
        ws_handler.register(self.user2, Mock())
        ws_handler.unregister(self.user2.username)
        response = self.attack()
        self.assertEqual(response.status, "no_effect")

        player = Player.objects.get(pk=self.player.pk)
        self.assertEqual(player.creature, -1)
        ws_handler.register(self.user2, Mock())

    def test_attack_missing(self):
        self.player.set_parameters(20, 0, 0, 100)
        self.player2.set_parameters(20, 100, 100, 100)

        response = self.attack()
        self.assertEqual(response.status, "no_effect")

    def test_attack_hitting(self):
        self.player.set_parameters(20, 250, 250, 100)
        self.player2.set_parameters(20, 0, 0, 100)
        self.player2.set_hp(10000)

        response = self.attack()
        self.assertEqual(response.status, "success")


    def test_attack_defeating(self):
        self.player.set_parameters(20, 150, 150, 100)
        self.player2.set_parameters(20, 0, 0, 100)
        self.player2.set_hp(1)

        response = self.attack()
        self.assertEqual(response.status, "success")


class FightUserMonsterTest(FightingControllerTest):
    def setUp(self):
        self.c = Client()
        self.factory = RequestFactory()

        self.user = User.objects.get(username="test")
        self.player = Player.objects.get(user=self.user)

        self.request = self.factory.get('/commandExe/', {"command": normal_attack.shortcut},
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.request.user = self.user
        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.command_object = CommandFactory.get_command_object(self.request)

    def test_user_attack_without_combat(self):
        #we need here special request
        request = self.factory.get('/commandExe/', {"command": normal_attack.name},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        command_object = CommandFactory.get_command_object(request)

        response = command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def test_user_attack_without_monster(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=1)).delete()
        response = self.command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def test_user_attack_defeating(self):
        self.player.set_parameters(1000, 1000, 1000, 100)
        self.player.set_creature(0)
        initial_experience = self.player.experience

        self._create_monster()

        response = self.command_object.execute()
        self.assertTrue(response.status == "success")
        player = Player.objects.get(user=self.user)

        self.assertTrue(player.creature, -1)
        self.assertTrue(player.experience > initial_experience)


    def test_user_attack_missing(self):
        self.player.set_creature(0)
        self.player.set_parameters(0, -100, -100, 100)
        self._create_monster()

        response = self.command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def test_user_attack_hitting_target(self):
        self.player.set_creature(0)
        self.player.set_parameters(1, 1000, 1000, 100)
        monster_in_map = self._create_monster()
        monster_in_map.set_hp(10000)

        response = self.command_object.execute()
        self.assertTrue(response.status == "success")

    def test_user_attack_with_wrong_command(self):
        self.player.set_creature(0)
        self.player.set_parameters(1, 1000, 1000, 100)
        monster_in_map = self._create_monster()
        monster_in_map.set_hp(10000)

        request = self.factory.get('/commandExe/', {"command": "foo"},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user

        process_command = processing.ProcessCommand(request)
        response = process_command.execute()

        player = UserController.get_player_from_user(self.user)
        self.assertTrue(player.creature == 0)


    def _create_monster(self):
        player = UserController.get_player_from_user(self.user)

        MonsterInMap.objects.filter(map=Map.objects.get(pk=1)).delete()
        monster_in_map = MonsterInMap(map=Map.objects.get(pk=1), monster=Monster.objects.all()[0],
                                      player=player)

        monster_in_map.save()
        return monster_in_map


class FightMonsterTest(FightingControllerTest):
    def setUp(self):
        self.c = Client()
        self.factory = RequestFactory()

        self.user = User.objects.get(username="test")
        self.player = Player.objects.get(user=self.user)

        self.request = self.factory.get('/monsterAttack/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.request.user = self.user

        self.location = Map.objects.get(pk=1)
        self.player.set_location(self.location)  #place (0,0)
        self.fight_controller = MonsterFightController()

    def test_monster_attack_without_monster(self):
        MonsterInMap.objects.filter(map=self.location).delete()
        response = self.fight_controller.monster_attack(self.request, normal_attack)
        self.assertTrue(response.status == "no_effect")

    def test_monster_attack_missing(self):
        self.player.set_hp(properties.STARTING_HP)
        self.player.set_parameters(100, 0, 0, 100)
        monster_in_map = self._create_monster()
        monster_in_map.monster.set_parameters(0, 0, 0, 100)

        response = self.fight_controller.monster_attack(self.request, normal_attack)
        self.assertTrue(response.status == "success")

    def test_monster_attack_hitting_target(self):
        self.player.set_hp(properties.STARTING_HP)
        self._create_monster()
        response = self.fight_controller.monster_attack(self.request, normal_attack)
        self.assertTrue(response.status == "success")


    def test_monster_attack_defeating(self):
        self.player.set_hp(1)
        self.player.set_parameters(-100, 0, 0, 100)
        monster_in_map = self._create_monster()
        ws_handler.stop_all_effects(monster_in_map)

        response = self.fight_controller.monster_attack(self.request, normal_attack)
        self.assertTrue(response.status == "success")


    def _create_monster(self):
        player = UserController.get_player_from_user(self.user)

        MonsterInMap.objects.filter(map=self.location).delete()
        monster_in_map = MonsterInMap(map=self.location, monster=Monster.objects.all()[0],
                                      player=player)
        monster_in_map.save()
        monster_in_map.monster.set_parameters(10, 10, 10, 100)
        return monster_in_map


class AttackProbabilityTest(TestCase):
    def test_function(self):
        function = real_attack_probability_function
        self.assertTrue(function(5) > 90)
        self.assertTrue(function(-5) < 10)
        self.assertTrue(function(0) == 50)


if __name__ == '__main__':
    unittest.main()
