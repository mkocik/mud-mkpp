import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")
from game_app.command.process.processing import ProcessCommand

from django.test.client import RequestFactory
from game_app.models import *
from django.test import TestCase


class CommandTest(TestCase):
    def custom_set_up(self, location_pk):
        self.factory = RequestFactory()
        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")
        self.location_pk = location_pk
        self.location = Map.objects.get(pk=self.location_pk)

    def process_command(self, command):
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        player = Player.objects.get(user=self.user)
        player.set_location(Map.objects.get(pk=self.location_pk))  #place (0,0)
        player.set_creature(-1)
        process_command = ProcessCommand(request)
        return process_command

    def process_command_without_modifications(self, command):
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        process_command = ProcessCommand(request)
        return process_command



