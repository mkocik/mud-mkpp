import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.settings.command_definition import *

from game_app.tests import CommandTest

from django.utils import unittest
from game_app.models import *


class SetCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        self.player.distribution_points = 10
        self.player.skills_points = 10
        self.player.save()

        try:
            parameters = PlayableParameter.objects.filter(name="test_param", default_val=0)
            parameters.delete()
            parameters = PlayerParameter.objects.filter(player=self.player, state=0)
            parameters.delete()
        except PlayableParameter.DoesNotExist:
            pass

        parameter = PlayableParameter(name="test_param", default_val=0)
        parameter.save()
        skill = Skill(name="test", description="test")
        skill.save()
        attribute = PlayerParameter(player=self.player, parameter=parameter, state=0)
        attribute.save()

    def test_normal_points_add(self):
        response = CommandTest.process_command(self, sett.name + " test_param 10").execute()
        self.assertEqual(response.status, "success")
        #it substracts in VIEW, after execution of command so now it is still 10
        self.assertEqual(self.player.distribution_points, 10)

    def test_string_points_add(self):
        response = CommandTest.process_command(self, sett.name + " test_param string").execute()
        self.assertEqual(response.status, "no_effect")

    def test_to_low_points_add(self):
        response = CommandTest.process_command(self, sett.name + " test_param 15").execute()
        self.assertEqual(response.status, "no_effect")

    def test_bad_attribute(self):
        response = CommandTest.process_command(self, sett.name + " qwertyu 10").execute()
        self.assertEqual(response.status, "no_effect")

    def test_bad_syntax(self):
        response = CommandTest.process_command(self, sett.name + " test_param").execute()
        self.assertEqual(response.status, "no_effect")

    def test_bad_syntax_lower(self):
        response = CommandTest.process_command(self, sett.name + " test_param -1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_skill_points_add(self):
        response = CommandTest.process_command(self, sett.name + " 1 1").execute()
        self.assertEqual(response.status, "success")

    def test_skill_points_add_bad_skill_id(self):
        response = CommandTest.process_command(self, sett.name + " 9999 1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_skill_points_add_bad_skill_id_lower(self):
        response = CommandTest.process_command(self, sett.name + " -1 1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_skill_points_add_bad_skill_points(self):
        response = CommandTest.process_command(self, sett.name + " 1 15").execute()
        self.assertEqual(response.status, "no_effect")

    def test_skill_points_add_bad_skill_points_lower(self):
        response = CommandTest.process_command(self, sett.name + " 1 -1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_skill_points_add_bad_skill_points_2(self):
        response = CommandTest.process_command(self, sett.name + " 1 aa").execute()
        self.assertEqual(response.status, "no_effect")

class GetCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        self.player.move_to_index(1)
        PlayerEquipment.objects.filter(player=self.player).delete()
        ItemType.objects.filter(name="test_type").delete()
        item_type = ItemType(name="test_type")
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        ItemInMap.objects.filter(map=self.player.location).delete()
        self.item_in_map = ItemInMap(map=self.player.location, item=self.item)
        self.item_in_map.save()
        self.monster = self._create_monster()

    def test_get_with_monster(self):
        response = CommandTest.process_command(self, gett.name + " item_test").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_without_monster(self):
        self.monster.delete()
        self.assertEqual(self.player.location.get_items()[0].item.name, 'item_test')
        response = CommandTest.process_command(self, gett.name + " item_test").execute()
        self.assertEqual(response.status, "success")

    def test_get_single_item(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name).execute()
        self.assertEqual(response.status, "success")

    def test_wrong_item_name(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " item_tes").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_all_from_field(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " wszystko").execute()
        self.assertEqual(response.status, "success")

    def test_get_all_from_field_multiple_items(self):
        self.monster.delete()
        self.item_in_map2 = ItemInMap(map=self.player.location, item=self.item)
        self.item_in_map2.save()
        response = CommandTest.process_command(self, gett.name + " wszystko").execute()
        self.assertEqual(response.status, "success")

    def test_get_all_without_items_on_field(self):
        self.monster.delete()
        self.item.delete()
        GoldInMap.objects.filter(map=self.location).delete()

        response = CommandTest.process_command(self, gett.name + " wszystko").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_all_with_nearly_full_bag(self):
        self.monster.delete()
        self.item_in_map2 = ItemInMap(map=self.player.location, item=self.item)
        self.item_in_map2.save()
        PlayerEquipment.objects.filter(player=self.player).delete()
        for iterator in range(0, properties.MAX_EQUIPMENT_AMOUNT - 1):
            eq = PlayerEquipment(player=self.player, item=self.item)
            eq.save()
        response = CommandTest.process_command(self, gett.name + " wszystko").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_with_full_bag(self):
        self.monster.delete()
        PlayerEquipment.objects.filter(player=self.player).delete()
        for iterator in range(0, properties.MAX_EQUIPMENT_AMOUNT):
            eq = PlayerEquipment(player=self.player, item=self.item)
            eq.save()
        response = CommandTest.process_command(self, gett.name + " wszystko").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_from_number(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " 1").execute()
        self.assertEqual(response.status, "success")

    def test_get_with_wrong_number(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " 0").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_wrong_amount(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " 1 10").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_wrong_id_in_amount(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " 5 1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_get_amount_success(self):
        self.monster.delete()
        response = CommandTest.process_command(self, gett.name + " 1 1").execute()
        self.assertEqual(response.status, "success")

    def _create_monster(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        monster = MonsterInMap(map=Map.objects.get(pk=self.location_pk), monster=Monster.objects.all()[0], player=None)
        monster.save()
        return monster


class RemoveCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        PlayerEquipment.objects.filter(player=self.player).delete()
        ItemType.objects.filter(name="test_type").delete()
        item_type = ItemType(name="test_type")
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        PlayerEquipment.objects.filter(player=self.player).delete()
        eq = PlayerEquipment(player=self.player, item=self.item)
        eq.save()

    def test_remove_with_string(self):
        response = CommandTest.process_command(self, remove.name + " string").execute()
        self.assertEqual(response.status, "no_effect")
        self.assertEqual(self.player.get_inventory_amount(), 1)

    def test_remove_with_wrong_index(self):
        response = CommandTest.process_command(self, remove.name + " -1").execute()
        self.assertEqual(response.status, "no_effect")
        self.assertEqual(self.player.get_inventory_amount(), 1)

    def test_remove_with_wrong_syntax(self):
        response = CommandTest.process_command(self, remove.name + " str str").execute()
        self.assertEqual(response.status, "no_effect")
        self.assertEqual(self.player.get_inventory_amount(), 1)

    def test_remove_with_success(self):
        response = CommandTest.process_command(self, remove.name + " 1").execute()
        self.assertEqual(response.status, "success")
        self.assertEqual(self.player.get_inventory_amount(), 0)

    def test_remove_with_amount_success(self):
        response = CommandTest.process_command(self, remove.name + " 1 1").execute()
        self.assertEqual(response.status, "success")

    def test_remove_with_amount_syntax_err(self):
        response = CommandTest.process_command(self, remove.name + " 1 sa").execute()
        self.assertEqual(response.status, "no_effect")

    def test_remove_with_wrong_amount(self):
        response = CommandTest.process_command(self, remove.name + " 1 100").execute()
        self.assertEqual(response.status, "no_effect")

    def test_remove_with_wrong_id_amount(self):
        response = CommandTest.process_command(self, remove.name + " 66 1").execute()
        self.assertEqual(response.status, "no_effect")


class InventoryCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        PlayerEquipment.objects.filter(player=self.player).delete()
        ItemType.objects.filter(name="test_type").delete()
        item_type = ItemType(name="test_type")
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        PlayerEquipment.objects.filter(player=self.player).delete()
        self.eq = PlayerEquipment(player=self.player, item=self.item)
        self.eq.save()

    def test_show_with_empty_equipment(self):
        self.eq.delete()
        response = CommandTest.process_command(self, inventory.name).execute()
        self.assertEqual(response.status, "no_effect")

    def test_show_without_empty_equipment(self):
        response = CommandTest.process_command(self, inventory.name).execute()
        self.assertEqual(response.status, "success")

    def test_show_with_string_number(self):
        response = CommandTest.process_command(self, inventory.name + " str").execute()
        self.assertEqual(response.status, "no_effect")

    def test_show_with_wrong_index(self):
        response = CommandTest.process_command(self, inventory.name + " -1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_show_item_desc(self):
        response = CommandTest.process_command(self, inventory.name + " 1").execute()
        self.assertEqual(response.status, "success")

    def test_show_with_wrong_syntax(self):
        response = CommandTest.process_command(self, inventory.name + " str str").execute()
        self.assertEqual(response.status, "no_effect")


class RemoveCommand2Test(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        PlayerEquipment.objects.filter(player=self.player).delete()
        ItemType.objects.filter(name="test_type").delete()
        item_type = ItemType(name="test_type")
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        PlayerEquipment.objects.filter(player=self.player).delete()
        eq = PlayerEquipment(player=self.player, item=self.item)
        eq.save()

    def test_execute_on_edge(self):
        response = CommandTest.process_command(self, remove.name + " string").execute()
        self.assertEqual(response.status, "no_effect")
        self.assertEqual(self.player.get_inventory_amount(), 1)



class InCommand(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)

    def test_with_wrong_syntax(self):
        response = CommandTest.process_command(self, inn.name).execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_wrong_number(self):
        response = CommandTest.process_command(self, inn.name + " test").execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_wrong_index(self):
        response = CommandTest.process_command(self, inn.name + " -1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_wrong_wrong_item(self):
        player = Player.objects.get(user=self.user)
        item_type = ItemType(name="test_type")
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        PlayerEquipment.objects.filter(player=player).delete()
        player.add_to_equipment(self.item)
        response = CommandTest.process_command(self, inn.name + " 1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_success_and_two_times(self):
        player = Player.objects.get(user=self.user)
        eq_slot = EquipmentSlot(name="test", multiplicity=1)
        eq_slot.save()

        item_type = ItemType(name="test_type", equipment_slot=eq_slot)
        item_type.save()
        Item.objects.filter(name="item_test").delete()

        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()

        PlayerEquipment.objects.filter(player=player).delete()
        player.add_to_equipment(self.item)
        response = CommandTest.process_command(self, inn.name + " 1").execute()
        self.assertEqual(response.status, "success")

        player.add_to_equipment(self.item)
        response = CommandTest.process_command(self, inn.name + " 1").execute()
        self.assertEqual(response.status, "no_effect")


class OutCommand(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)
        eq_slot = EquipmentSlot(name="test", multiplicity=1)
        eq_slot.save()
        item_type = ItemType(name="test_type", equipment_slot=eq_slot)
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        PlayerEquipment.objects.filter(player=self.player).delete()
        self.player.equip_item(self.item)

    def test_with_wrong_syntax(self):
        response = CommandTest.process_command(self, out.name).execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_wrong_number(self):
        response = CommandTest.process_command(self, out.name + " test").execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_wrong_index(self):
        response = CommandTest.process_command(self, out.name + " -1").execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_success(self):
        response = CommandTest.process_command(self, out.name + " 1").execute()
        self.assertEqual(response.status, "success")

class SkillCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)

    def test_show_all_skillst(self):
        response = CommandTest.process_command(self, skills.name + " all").execute()
        self.assertEqual(response.status, "success")

    def test_show_my_skills(self):
        response = CommandTest.process_command(self, skills.name + " my").execute()
        self.assertEqual(response.status, "success")

    def test_bad_syntax(self):
        response = CommandTest.process_command(self, skills.name + " aa").execute()
        self.assertEqual(response.status, "no_effect")

class CharacterCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)

    def test_without_equipped_items(self):
        response = CommandTest.process_command(self, character.name).execute()
        self.assertEqual(response.status, "no_effect")

    def test_with_equipped_items(self):
        player = Player.objects.get(user=self.user)
        eq_slot = EquipmentSlot(name="test", multiplicity=1)
        eq_slot.save()
        item_type = ItemType(name="test_type", equipment_slot=eq_slot)
        item_type.save()
        Item.objects.filter(name="item_test").delete()
        self.item = Item(item_type=item_type, name="item_test", description="test desc", value=0)
        self.item.save()
        player.equip_item(self.item)
        response = CommandTest.process_command(self, character.name).execute()
        self.assertEqual(response.status, "success")


if __name__ == '__main__':
    unittest.main()