import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.tests import CommandTest
from game_app.command.settings.command_definition import *

from game_app.models import MapPassage
from django.utils import unittest


class MoveCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)

    def simulate_user_move_from_corner(self, direction):
        process_command = CommandTest.process_command(self, direction)
        response = process_command.execute()
        return response

    def test_execute(self):
        #user goes normally

        expected_results = [
            "no_effect",
            "success",
            "success",
            "no_effect"
        ]

        get_expected_results = dict(zip([north.name, south.name, east.name, west.name], expected_results))

        for direction, expected_result in get_expected_results.items():
            response = self.simulate_user_move_from_corner(direction)
            self.assertEqual(response.status, expected_result)

class PassageCommandTest(CommandTest):
    def setUp(self):
        from_map = Map.objects.get(pk=1)
        to_map = Map.objects.get(pk=2)
        MapPassage.objects.all().filter(name='testPassage', from_map=from_map, to_map=to_map).delete()
        self.passage = MapPassage(name='testPassage', from_map=from_map, to_map=to_map)
        self.passage.save()
        CommandTest.custom_set_up(self, 1)

    def test_normal_passage(self):
        response = CommandTest.process_command(self, passage.name + " testPassage").execute()
        self.assertEqual(response.status, "success")
        player = Player.objects.get(user=self.user)
        self.assertEqual(player.location.pk, 2)

    def test_without_passage(self):
        response = CommandTest.process_command(self, passage.name + " testPassage2").execute()
        self.assertEqual(response.status, "no_effect")
        player = Player.objects.get(user=self.user)
        self.assertEqual(player.location.pk, 1)

    def test_without_given_name_with_passage(self):
        response = CommandTest.process_command(self, passage.name).execute()
        self.assertEqual(response.status, "success")
        player = Player.objects.get(user=self.user)
        self.assertEqual(player.location.pk, 2)

    def test_without_given_name_without_passage(self):
        self.passage.delete()
        response = CommandTest.process_command(self, passage.name).execute()
        self.assertEqual(response.status, "no_effect")
        player = Player.objects.get(user=self.user)
        self.assertEqual(player.location.pk, 1)


if __name__ == '__main__':
    unittest.main()