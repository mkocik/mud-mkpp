import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.settings.command_definition import *

from game_app.controllers.basic_controllers import UserController

from game_app.command.process.processing import CommandFactory

from django.utils import unittest
from game_app.models import *
from game_app.tests.command.settings.websocket_handler_test import WebsocketTest


class PlayersCommandTest(WebsocketTest):
    def setUp(self):
        WebsocketTest.setUp(self)
        self.request = self.factory.get('/commandExe/', {"command": player.name},
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

    def test_execute_being_alone(self):

        self.request.user = self.user

        other_players = Player.objects.all()

        if other_players is not None:
            other_players = other_players.filter(location=self.location)
            other_players.delete()
            ws_handler.unregister("test2")

        command_object = CommandFactory.get_command_object(self.request)
        response = command_object.execute()

        self.assertEqual(response.status, "no_effect")

    def test_execute_being_together(self):
        self.setUp()

        self.request.user = self.user
        command_object = CommandFactory.get_command_object(self.request)
        response = command_object.execute()

        self.assertEqual(response.status, "success")

class SayCommandTest(WebsocketTest):
    def test_execute_together(self):
        command = say.name + " " + "foo"
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user

        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        self.assertEqual(response.status, "success")

class WhisperCommandTest(WebsocketTest):
    def say(self, listener_name):
        command = whisper.name + " " + listener_name + " " + "foo"
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        return response

    def test_whisper(self):
        player = UserController.get_player_from_user(self.user2)
        player.set_creature(-1)
        response = self.say(self.user2.username)
        self.assertEqual(response.status, "success")

    def test_whisper_to_not_existing_person(self):
        response = self.say("foo")
        self.assertEqual(response.status, "no_effect")

    def test_whisper_to_yourself(self):
        response = self.say(self.user.username)
        self.assertEqual(response.status, "no_effect")

    def test_whisper_to_person_in_combat(self):
        player = UserController.get_player_from_user(self.user2)
        player.set_creature(0)
        response = self.say(self.user2.username)
        #pprint.pprint(vars(response))
        self.assertEqual(response.status, "no_effect")
        player.set_creature(-1)

    def test_whisper_to_logged_out_person(self):
        ws_handler.unregister(self.user2.username)
        response = self.say(self.user2.username)
        self.assertEqual(response.status, "no_effect")
        ws_handler.register(self.user2.username, self.web_socket)

    def test_whisper_to_person_on_other_location(self):
        start_location_id = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                            level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)).pk
        self.player2.set_location(Map.objects.get(pk=start_location_id+1))
        response = self.say(self.user2.username)
        self.assertEqual(response.status, "no_effect")

if __name__ == '__main__':
    unittest.main()