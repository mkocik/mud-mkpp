import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")


from MUD_ADMIN.game.asset.definition.asset import RiddleAsset
from game_app.tests import CommandTest
from MUD_ADMIN.game.asset.quest.quest import kill_monster_quest

from game_app.command.settings.command_definition import *

from django.utils import unittest
from game_app.models import *


class AnswerCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.player = Player.objects.get(user=self.user)

    def test_execute_without_asset(self):
        self._delete_assets()
        response = CommandTest.process_command(self, answer.name + " foo").execute()
        self.assertTrue(response.status == "no_effect")

    def test_execute_with_without_passage(self):
        response = CommandTest.process_command(self, answer.name + " foo").execute()
        self.assertTrue(response.status == "no_effect")

    def test_execute_with_passage(self):

        Riddle.objects.filter(name='Zagadka mnicha').delete()
        riddle = Riddle(name='Zagadka mnicha', question='foo', answer='foo2', justification='foo3')
        riddle.save()

        passage_object = self._create_passage(riddle)

        #enter passage should fail - user didn't solve passage
        response = CommandTest.process_command(self, passage.name + " " + passage_object.name).execute()
        self.assertTrue(response.status == "no_effect")

        riddle = RiddleAsset(passage_object)

        response = CommandTest.process_command(self, answer.name + " " + ' '.join(riddle.get_answer())).execute()
        self.assertTrue(response.status == "success")

        MapPassage.objects.all().delete()

    def _delete_assets(self):
        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def _create_passage(self, riddle):
        from_map = Map.objects.get(pk=1)
        to_map = Map.objects.get(pk=2)

        MapPassage.objects.all().filter(name='foo', from_map=from_map, to_map=to_map).delete()

        passage = MapPassage(name="foo", from_map=from_map, to_map=to_map,
                             asset=riddle)
        passage.save()
        return passage



class QuestListCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)


    def test_execute_with_quest_on_list(self):
        asset_in_map = self._create_npc_with_graph()
        player = Player.objects.get(user=self.user)

        quest = QuestObject(name=kill_monster_quest.name)
        quest.save()

        Quest(asset_in_map=asset_in_map, player=player, quest=quest).save()

        response = CommandTest.process_command(self, quest_list.name).execute()
        self.assertTrue(response.status == "success")

        response = CommandTest.process_command(self, quest_list.name + " 1").execute()
        self.assertTrue(response.status == "success")

        self._delete_assets()

    def _delete_assets(self):
        AssetInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

    def _create_npc_with_graph(self, graph_name="john_graph"):
        graph = DialogGraph.objects.get(name=graph_name)

        if NPC.objects.filter(name="Mniszek", graph=graph).count() > 0:
            npc = NPC.objects.get(name="Mniszek", graph=graph)
        else:
            npc = NPC(name="Mniszek", graph=graph)
            npc.save()

        self.npc = npc

        self.location = Map.objects.get(pk=self.location_pk)

        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(map=self.location,
                                  asset=npc, is_busy=False)

        asset_in_map.save()
        self.asset_in_map = asset_in_map

        return asset_in_map


if __name__ == '__main__':
    unittest.main()