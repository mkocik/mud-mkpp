import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")


from game_app.command.settings.command_definition import helpp, talk, mapp

from game_app.command.settings.command_settings import command_array, command_shortcut
from game_app.controllers.basic_controllers import UserController
from game_app.command.process.processing import ProcessCommand
from game_app.command.process.processing import CommandFactory

from game_app.command.settings import command_settings

from django.test.client import Client

from django.utils import unittest
from django.test.client import RequestFactory
from game_app.models import *
from django.test import TestCase

class ProcessCommandTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.user = User.objects.get(username="test")
        player = UserController.get_player_from_user(self.user)
        player.set_creature(-1)

    def execute_command(self, command, in_combat=None):

        if in_combat is not None:
            player = UserController.get_player_from_user(self.user)
            if in_combat:
                player.set_creature(0)
            else:
                player.set_creature(-1)

        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        process_command = ProcessCommand(request)
        response = process_command.execute()
        return response

    def test_execute(self):
        player = UserController.get_player_from_user(self.user)
        PlayerParameter.objects.filter(player=player).delete()

        params = PlayableParameter.objects.all()
        for param in params:
            PlayerParameter.objects.create(player=player, parameter=param, state=param.default_val)

        #normal mode
        for command_name in command_array:
            response = self.execute_command(command_name)
            self.assertTrue(response.status in ["success", "no_effect", "invalid_command"])

        #shortcuts in normal mode
        for shortcut in command_shortcut:
            response = self.execute_command(shortcut)
            self.assertTrue(response.status in ["success", "no_effect", "invalid_command"])

        #creature mode
        for command_name in command_array:
            response = self.execute_command(command_name, True)
            self.assertTrue(response.status in ["success", "no_effect", "invalid_command"])

        #foo command processing
        command_name = "foo"
        response = self.execute_command(command_name)
        self.assertTrue(response.status in ["success", "invalid_command"])


    def test_do_command(self):
        response = self.execute_command(helpp.name, False)
        self.assertTrue(response.status == 'success')

        response = self.execute_command("foo")
        self.assertTrue(response.status == 'invalid_command')

    def test_incorrect_command_syntax(self):
        response = self.execute_command(mapp.name + " " + "foo")
        self.assertTrue(response.status == 'invalid_command')

    def test_execute_attack_in_combat(self):
        from MUD_ADMIN.game.battle.attack_definition import normal_attack

        response = self.execute_command(normal_attack.name, True)
        self.assertTrue(response.status in ["no_effect", "success"])



    def test_execute_other_command_in_combat(self):
        response = self.execute_command(talk.name, True)
        self.assertTrue(response.status == "invalid_command")

    def test_execute_help_command(self):
        player = UserController.get_player_from_user(self.user)
        player.asset_in_map = AssetInMap.objects.get(asset=Asset.objects.get(name='John'))
        player.save()

        response = self.execute_command(helpp.name, True)
        self.assertTrue(response.status == "success")

        player = UserController.get_player_from_user(self.user)
        player.asset_in_map = AssetInMap.objects.get(asset=Asset.objects.get(name='prosta łatwa skrzynia'))
        player.save()

        response = self.execute_command(helpp.name, True)
        self.assertTrue(response.status == "success")

        player = UserController.get_player_from_user(self.user)
        player.asset_in_map = None
        player.in_trade = True
        player.save()

        response = self.execute_command(helpp.name, True)
        self.assertTrue(response.status == "success")

        player = UserController.get_player_from_user(self.user)
        player.in_trade = False
        player.creature = 1
        player.save()

        response = self.execute_command(helpp.name, True)
        self.assertTrue(response.status == "success")

        player = UserController.get_player_from_user(self.user)
        player.creature = -1
        player.stealing_from = AssetInMap.objects.get(asset=Asset.objects.get(name='John'))
        player.save()

        response = self.execute_command(helpp.name, True)
        self.assertTrue(response.status == "success")

        player = UserController.get_player_from_user(self.user)
        player.stealing_from = None
        player.save()

    def test_specific_manual_in_combat(self):
        response = self.execute_command(helpp.name + ' 1', True)
        self.assertTrue(response.status == "success")

        response = self.execute_command(helpp.name + ' aaa', True)
        self.assertTrue(response.status == "no_effect")

        response = self.execute_command(helpp.name + ' 9999', True)
        self.assertTrue(response.status == "no_effect")



class CommandFactoryTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.factory = RequestFactory()

    def test_get_command_object(self):

        user = User.objects.get(username="test")
        player = UserController.get_player_from_user(user)

        PlayerParameter.objects.filter(player=player).delete()

        params = PlayableParameter.objects.all()
        for param in params:
            PlayerParameter.objects.create(player=player, parameter=param, state=param.default_val)

        for direction in command_settings.command_array:
            request = self.factory.get('/commandExe/', {"command": direction},
                                       HTTP_X_REQUESTED_WITH='XMLHttpRequest')

            request.user = user

            command_factory = CommandFactory()
            command_object = command_factory.get_command_object(request)

            if command_object:
                response = command_object.execute()
                self.assertTrue(response.status in ["success", "no_effect", "creature"])


if __name__ == '__main__':
    unittest.main()
