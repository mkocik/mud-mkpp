import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.utils.websockets.battle_websocket import ws_handler
from game_app.controllers.basic_controllers import UserController

from game_app.command.process.processing import ProcessCommand
from game_app.command.settings.command_definition import talk

from unittest.mock import Mock

from django.test.client import Client

from django.test.client import RequestFactory
from game_app.models import *
from django.test import TestCase


class WebsocketTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        client = Client()
        client2 = Client()

        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")

        self.player = Player.objects.get(user=self.user)
        self.player2 = Player.objects.get(user=self.user2)

        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player2.set_location(Map.objects.get(pk=1))  #place (0,0)

        self.location = Map.objects.get(pk=3)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)


class WebSocketHandlerTest(WebsocketTest):
    def setUp(self):
        WebsocketTest.setUp(self)
        self.factory = RequestFactory()

    def test_registration_with_no_user(self):
        is_registered = ws_handler.register("foo", self.web_socket)
        self.assertFalse(is_registered)

    def test_registration_with_no_player(self):
        foo_name = "testx"
        try:
            user3 = User.objects.get(username=foo_name)
            user3.delete()
        except User.DoesNotExist:
            pass
        user3 = User(username=foo_name)
        user3.save()

        is_registered = ws_handler.register(user3, self.web_socket)
        self.assertFalse(is_registered)

    def test_unregistration_fighting_player(self):
        player = UserController.get_player_from_user(self.user)

        MonsterInMap.objects.filter(map=self.location).delete()
        MonsterInMap.objects.filter(player=player).delete()
        self.player.set_location(self.location)
        monster_in_map = MonsterInMap(map=self.location, monster=Monster.objects.all()[0], player=player)
        monster_in_map.save()

        is_unregistered = ws_handler.unregister(self.user.username)
        self.assertTrue(is_unregistered)
        ws_handler.register(self.user.username, self.web_socket)

    def test_talk_to_npc_and_loggout(self):
        MonsterInMap.objects.filter(map=self.location).delete()
        AssetInMap.objects.filter(map=self.location).delete()
        npc_in_map = AssetInMap(asset=NPC.objects.all()[0], map=self.location)
        npc_in_map.save()

        process_command = self.process_command(talk.name)
        response = process_command.execute()
        self.assertTrue(response.status == 'success')

    def process_command(self, command):
        request = self.factory.get('/commandExe/', {"command": command},
                                   HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        player = Player.objects.get(user=self.user)
        player.set_location(self.location)  #place (0,0)
        player.set_creature(-1)
        process_command = ProcessCommand(request)
        return process_command