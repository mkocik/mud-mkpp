#this 2 lines are needed to mock Client object in Django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")
from game_app.tests import CommandTest
from game_app.command.settings.command_definition import search, steal

from game_app.models import *


class SearchCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.command_name = search.name

    def preparing(self, player, npc_state, player_param_state):
        AssetInMap.objects.filter(map=self.location).delete()
        NPC.objects.filter(name="test").delete()
        ItemType.objects.filter(name="test").delete()

        dialog = DialogGraph(name="complex_graph")
        dialog.save()

        npc = NPC(name="test", type_name=None, graph=dialog)
        npc.save()

        asset_in_map = AssetInMap(asset=npc, map=self.location, state=1)
        asset_in_map.save()

        npc_attr = NPCAttributes(npc=asset_in_map.asset, state=npc_state,
                                 attribute=AllParameter.objects.get(name="Zwinność"))
        npc_attr.save()

        item_type = ItemType(name="test", equipment_slot=None)
        item_type.save()

        item = Item(item_type=item_type, name="test", description="test")
        item.save()

        npc_eq = NPCEquipment(asset_in_map=asset_in_map, item=item)
        npc_eq.save()

        player.stealing_from = asset_in_map
        player.save()

        player_param = PlayerParameter.objects.get(parameter=AllParameter.objects.get(name="Zręczność"), player=player)
        player_param.state = player_param_state
        player_param.save()

    def test_search_command_success(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "success")

    def test_bad_asset_index(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name + " 2").execute()
        self.assertTrue(response.status == "no_effect")

    def test_bad_asset_name(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name + " name").execute()
        self.assertTrue(response.status == "no_effect")

    def test_good_asset_name(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name + " test").execute()
        self.assertTrue(response.status == "success")


    def test_with_ban(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 10, 0)

        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "success")

        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "no_effect")

    def test_without_object(self):
        AssetInMap.objects.filter(map=self.location).delete()
        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "no_effect")


class StealTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.command_name = steal.name

    def preparing(self, player, npc_state, player_param_state):
        AssetInMap.objects.filter(map=self.location).delete()
        NPC.objects.filter(name="test").delete()
        ItemType.objects.filter(name="test").delete()

        dialog = DialogGraph(name="complex_graph")
        dialog.save()

        npc = NPC(name="test", type_name=None, graph=dialog)
        npc.save()

        asset_in_map = AssetInMap(asset=npc, map=self.location, state=1)
        asset_in_map.save()

        npc_attr = NPCAttributes(npc=asset_in_map.asset, state=npc_state,
                                 attribute=AllParameter.objects.get(name="Zwinność"))
        npc_attr.save()

        item_type = ItemType(name="test", equipment_slot=None)
        item_type.save()

        item = Item(item_type=item_type, name="test", description="test")
        item.save()

        npc_eq = NPCEquipment(asset_in_map=asset_in_map, item=item)
        npc_eq.save()

        player.stealing_from = asset_in_map
        player.save()

        player_param = PlayerParameter.objects.get(parameter=AllParameter.objects.get(name="Zręczność"), player=player)
        player_param.state = player_param_state
        player_param.save()


    def test_search_command_with_no_stealth_status(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 10, 0)
        player.stealing_from = None
        player.save()

        response = self.process_command(self.command_name + " 1").execute()
        self.assertTrue(response.status == "no_effect")

    def test_search_command_with_less_attribute_player(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 10, 0)

        response = self.process_command(self.command_name + " 1").execute()
        self.assertTrue(response.status == "success")

    def test_search_command_value_error(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 10, 0)

        response = self.process_command(self.command_name + " aa").execute()
        self.assertTrue(response.status == "no_effect")

    def test_search_command_banned(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 10, 0)

        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "no_effect")

        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "no_effect")


    def test_search_command_with_greater_attribute_player(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name + " 1").execute()
        self.assertTrue(response.status == "success")

    def test_search_command_wrong_index(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 0, 10)

        response = self.process_command(self.command_name + " 5").execute()
        self.assertTrue(response.status == "no_effect")