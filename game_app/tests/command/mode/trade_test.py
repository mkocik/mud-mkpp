#this 2 lines are needed to mock Client object in Django
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.tests import CommandTest

from game_app.command.settings.command_definition import buy, sell, show
from game_app.models import *


class BuyCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.command_name = buy.name

    def preparing(self, player, no_of_items, gold):
        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(asset=NPC.objects.get(name='Mnich'), map=self.location, state=1)
        asset_in_map.save()

        player.in_trade = True
        player.asset_in_map = asset_in_map
        player.save()

        items = Item.objects.all()[:no_of_items]

        for item in items:
            NPCEquipment(item=item, asset_in_map=asset_in_map).save()

        player.gold = gold
        player.save()

    def test_buy_without_trade_mode(self):
        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "no_effect")


    def test_buy_with_wrong_index(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 1000)

        response = self.process_command(self.command_name + ' 99999').execute()
        self.assertTrue(response.status == "no_effect")

    def test_buy_without_item(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 1000)

        response = self.process_command(self.command_name + ' foo').execute()
        self.assertTrue(response.status == "no_effect")

    def test_buy_without_item_in_npc(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 1000)

        response = self.process_command(
            self.command_name + " " + Item.objects.all().order_by("-id")[0].name).execute()
        self.assertTrue(response.status == "no_effect")

    def test_buy_without_npc_money(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 0)

        response = self.process_command(self.command_name + " 1").execute()
        self.assertTrue(response.status == "no_effect")

    def test_buy(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 100000)

        response = self.process_command(self.command_name + " 1").execute()
        self.assertTrue(response.status == "success")


class SellCommandTest(BuyCommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.command_name = sell.name

    def preparing(self, player, no_of_items, gold):
        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(asset=NPC.objects.all()[0], map=self.location)
        asset_in_map.save()

        player.in_trade = True
        player.asset_in_map = asset_in_map
        player.save()

        items = Item.objects.all()[:no_of_items]

        for item in items:
            PlayerEquipment(item=item, player=player).save()

        asset_in_map.gold = gold
        asset_in_map.save()


class TradeWithNPC(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)

    def test_trade(self):
        player = Player.objects.get(user=self.user)
        BuyCommandTest.preparing(self, player, 5, 1000)

        command_test = SellCommandTest()
        command_test.setUp()
        response = command_test.process_command("6").execute()
        self.assertTrue(response.status == "success")

    # def test_update(self):
    #     initial_no_of_equipment = NPCEquipment.objects.all().count()
    #
    #     AssetInMapController().update(1)
    #
    #     final_no_of_equipments = NPCEquipment.objects.all().count()
    #     self.assertTrue(initial_no_of_equipment == final_no_of_equipments)


class ShowStoreCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 1)
        self.command_name = show.name

    def preparing(self, player, no_of_items, gold):
        AssetInMap.objects.filter(map=self.location).delete()
        asset_in_map = AssetInMap(asset=NPC.objects.all()[0], map=self.location)
        asset_in_map.save()

        player.in_trade = True
        player.asset_in_map = asset_in_map
        player.save()

    def test_show(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 100000)
        response = self.process_command(self.command_name).execute()
        self.assertTrue(response.status == "success")

    def test_show_err(self):
        player = Player.objects.get(user=self.user)
        self.preparing(player, 5, 100000)
        response = self.process_command(self.command_name + " a ").execute()
        self.assertTrue(response.status == "no_effect")