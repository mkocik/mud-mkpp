import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.settings.command_definition import *

from game_app.controllers.basic_controllers import UserController
from unittest.mock import Mock
from game_app.tests import CommandTest

from django.test.client import Client

from django.utils import unittest
from game_app.models import *


class FightCommandTest(CommandTest):
    def setUp(self):
        CommandTest.custom_set_up(self, 5)

        User.objects.all().filter(username="opponent").delete()

        c = Client()
        #registration process
        response = c.post('/register/', {"username": "opponent", "password": "opponent"})
        self.assertTrue(response)

        self.opponent = User.objects.get(username="opponent")
        self.opponent.save()
        Player.objects.all().filter(user=self.opponent).delete()
        player = Player(location=Map.objects.get(pk=self.location_pk), user=self.opponent)
        player.save()

        ws_handler.register(self.opponent.username, Mock())

    def test_fight_with_monster(self):
        monster = self._create_monster()

        command_object = CommandTest.process_command(self, fight.name)
        response = command_object.execute()
        self.assertTrue(response.status == "success")

    def test_fight_without_monster(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()

        command_object = CommandTest.process_command(self, fight.name)
        response = command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def test_fight_with_busy_monster(self):
        self._create_fighting_monster()

        command_object = CommandTest.process_command(self, fight.name)
        response = command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def test_fight_with_player(self):
        player = self._create_opponent()
        player.set_creature(-1)

        command_object = CommandTest.process_command(self, fight.name + " z " + "opponent")
        response = command_object.execute()
        self.assertTrue(response.status == "success")

    def test_fight_with_busy_player(self):
        opponent = self._create_opponent()
        opponent.set_creature(0)

        command_object = CommandTest.process_command(self, fight.name + " z " + "opponent")
        response = command_object.execute()
        self.assertTrue(response.status == "no_effect")

    def _create_opponent(self):
        Player.objects.filter(user=self.opponent).delete()
        player = Player(location=Map.objects.get(pk=self.location_pk), user=self.opponent)
        player.save()
        return player

    def _create_monster(self):
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        monster = MonsterInMap(map=Map.objects.get(pk=self.location_pk), monster=Monster.objects.all()[0], player=None)
        monster.save()
        return monster

    def _create_fighting_monster(self):
        player = UserController.get_player_from_user(self.user)

        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.location_pk)).delete()
        monster = MonsterInMap(map=Map.objects.get(pk=self.location_pk), monster=Monster.objects.all()[0],
                                player=player)
        monster.save()


if __name__ == '__main__':
    unittest.main()