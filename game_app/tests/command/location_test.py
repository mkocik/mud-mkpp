#this 2 lines are needed to mock Client object in Django
import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.settings.command_definition import west, south
from game_app.command.process.processing import CommandFactory

from django.test.client import Client

from django.utils import unittest
from django.test.client import RequestFactory
from game_app.models import *
from django.test import TestCase


class LocationTest(TestCase):
    def setUp(self):
        self.c = Client()
        self.factory = RequestFactory()

    def custom_set_up(self, command):
        self.request = self.factory.get('/commandExe/', {"command": command},
                                        HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        self.user = User.objects.get(username="test")
        self.request.user = self.user

        self.player = Player.objects.get(user=self.user)
        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player.set_creature(-1)

        self.command_object = CommandFactory.get_command_object(self.request)

    def test_move_with_monster(self):
        self.custom_set_up(south.name)

        map = Map.objects.get(pk=2)
        MonsterInMap.objects.all().filter(map=map).delete()
        monster = Monster.objects.all().filter(aggression=100)[0]
        monster_in_map = MonsterInMap(map=map, monster=monster, player=None)
        monster_in_map.save()

        response = self.command_object.execute()
        # pprint.pprint(vars(response))
        self.assertTrue(response.get_creature() in [0,-1])

    def test_move_with_asset(self):
        self.custom_set_up(south.name)

        map = Map.objects.get(pk=2)
        AssetInMap.objects.all().filter(map=map).delete()
        asset = AssetInMap(map=map, asset=Asset.objects.all()[0])
        asset.save()

        response = self.command_object.execute()
        self.assertTrue(response.status == "success")

    def test_move_to_starting_location(self):
        self.custom_set_up(west.name)
        start_location_id = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                            level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)).pk
        map = Map.objects.get(pk=start_location_id + 1)
        self.player.set_location(map)  #place (0,0)

        response = self.command_object.execute()
        self.assertEqual(response.get_creature(), -1)

    def test_move_safe_mode(self):
        self.custom_set_up(west.name)

        map = Map.objects.get(pk=2)
        monster = Monster.objects.all().filter(aggression=100)[0]
        MonsterInMap.objects.all().filter(map=map).delete()
        monster_in_map = MonsterInMap(map=map, monster=monster, player=None)
        monster_in_map.save()

        # my_logger.info(self.player.get_creature())
        response = self.command_object.execute()
        self.assertEqual(response.get_creature(), -1)


if __name__ == '__main__':
    unittest.main()
