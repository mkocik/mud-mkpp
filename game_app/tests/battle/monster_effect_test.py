import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")
from unittest.mock import Mock

from django.test import Client, RequestFactory, TestCase
from django.utils import unittest
from django.contrib.auth.models import User

from MUD_ADMIN.game.battle.attack_definition import eruption, freeze, kill, jump_attack, normal_attack
from game_app.command.execution.fight.monster_fight import MonsterFightController
from game_app.battle.attack import *
from game_app.models import Monster, MonsterInMap


class DetailsMonsterAttackUsageTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        client = Client()
        client2 = Client()

        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")

        self.player = Player.objects.get(user=self.user)
        self.player2 = Player.objects.get(user=self.user2)

        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player2.set_location(Map.objects.get(pk=1))  #place (0,0)

        self.location = Map.objects.get(pk=3)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)

        self.request = self.factory.get('/monsterAttack/', HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        self.request.user = self.user

        self.player = Player.objects.get(user=User.objects.get(username='test'))
        self.player.creature = 0
        self.player.save()

        MonsterInMap.objects.filter(monster=Monster.objects.all()[0], map=self.player.location).delete()
        monster_in_map = MonsterInMap(monster=Monster.objects.all()[0], map=self.player.location)
        monster_in_map.player = self.player
        monster_in_map.save()
        self.monster_in_map = monster_in_map
        self.fight_controller = MonsterFightController()
        ws_handler.stop_all_effects(self.monster_in_map)
        ws_handler.stop_all_effects(self.player)

    def test_immediate_effect(self):
        self.player.set_parameters(1, 1, 1, 1)
        initial_hp = 2
        self.player.set_hp(initial_hp)

        response = self.fight_controller.monster_attack(self.request, eruption)

        player = UserController.get_player_from_user(self.user)
        self.assertTrue(initial_hp < player.get_hp())


    def test_effect_to_next_hit(self):
        ws_handler.stop_all_effects(self.player)
        self.player.set_parameters(1, 1, 1, 1)
        self.assertTrue(ws_handler.creature_has_effect(self.player, Paralysis) is False)

        response = self.fight_controller.monster_attack(self.request, freeze)
        print(response.message)
        if 'chybił' not in response.message:
            player = UserController.get_player_from_user(self.user)
            self.assertTrue(ws_handler.creature_has_effect(player, Paralysis) is True)

            response = self.fight_controller.monster_attack(self.request, normal_attack)
            player = UserController.get_player_from_user(self.user)
            self.assertTrue(ws_handler.creature_has_effect(player, Paralysis) is False)

    def test_effect_to_end_of_battle(self):
        self.player.set_parameters(1, 1, 1, 1)
        initial_hp = 2
        self.player.set_hp(initial_hp)

        response = self.fight_controller.monster_attack(self.request, jump_attack)
        player = UserController.get_player_from_user(self.user)
        self.assertTrue(initial_hp < player.get_hp())

    def test_effect_for_some_time(self):
        self.player.set_parameters(1, 1, 1, 1)
        self.monster_in_map.hp = 10000
        self.monster_in_map.save()
        self.assertTrue(ws_handler.creature_has_effect(self.monster_in_map, Paralysis) is False)

        response = self.fight_controller.monster_attack(self.request, eruption)
        if 'chybił' not in response.message:
            player = UserController.get_player_from_user(self.user)
            self.assertTrue(ws_handler.creature_has_effect(player, Paralysis) is True)

            ws_handler.stop_all_effects(player)
            self.assertTrue(ws_handler.creature_has_effect(player, Paralysis) is False)


if __name__ == '__main__':
    unittest.main()