import os
from django.contrib.auth.models import User
from game_app.command.process.processing import CommandFactory, ProcessCommand

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from unittest.mock import Mock

from django.test import Client, RequestFactory
from django.utils import unittest

from MUD_ADMIN.game.battle.attack_definition import ogre_hit, eruption, freeze, normal_attack, harm_limb, poison_attack
from game_app.battle.attack import *
from game_app.controllers.battle_controllers import BattleEffectController
from game_app.tests import BasicAttackUsageTest
from django.test import TestCase

class DetailsUserUserAttackUsageTest(TestCase):
    def setUp(self):
        client = Client()
        client2 = Client()
        self.factory = RequestFactory()
        client = Client()
        client2 = Client()

        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")

        self.player = Player.objects.get(user=self.user)
        self.player2 = Player.objects.get(user=self.user2)

        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player2.set_location(Map.objects.get(pk=1))  #place (0,0)

        self.location = Map.objects.get(pk=3)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)
        self.player.set_creature(self.user2.pk)
        self.player2.set_creature(self.user.pk)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)

    def special_attack(self, command):
        request = self.factory.get('/commandExe/', {"command": command}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()
        return response

    def special_attack_process_command(self, command):
        request = self.factory.get('/commandExe/', {"command": command}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')
        request.user = self.user
        response = ProcessCommand(request).execute()
        return response

    def test_immediate_effect(self):
        #checking fatality effect
        self.player.set_parameters(1, 1000, 1000, 1000)
        hp_attribute = self.player2.get_current_hp()
        hp_attribute.current_state = 2
        hp_attribute.save()

        response = self.special_attack(eruption.name)
        player2 = UserController.get_player_from_user(self.user2)
        self.assertTrue(player2.get_hp() > hp_attribute.current_state)


    def test_effect_to_next_hit(self):
        self.player.set_parameters(1, 1000, 1000, 1000)
        self.assertTrue(ws_handler.creature_has_effect(self.player2, Paralysis) is False)

        response = self.special_attack(freeze.name)
        self.assertTrue(ws_handler.creature_has_effect(self.player2, Paralysis) is True)

        response = self.special_attack(normal_attack.name)
        self.assertTrue(ws_handler.creature_has_effect(self.player2, Paralysis) is False)

    def test_effect_to_end_of_battle(self):
        initial_durability = self.player2.get_clear_parameter('Wytrzymałość')
        self.player.set_parameters(1, 1000, 1000, 1000)

        response = self.special_attack(harm_limb.name)
        player = Player.objects.get(pk=self.player2.pk)
        final_durability = player.get_clear_parameter('Wytrzymałość')
        self.assertTrue(initial_durability > final_durability)

        player = Player.objects.get(pk=player.pk)
        player.resurrection()
        durability_after_death = player.get_clear_parameter('Wytrzymałość')
        self.assertTrue(durability_after_death == initial_durability)


    def test_effect_for_some_time(self):
        self.player.set_parameters(10, 1000, 1000, 1000)
        initial_strength = self.player.get_clear_parameter('Siła')

        response = self.special_attack(ogre_hit.name)
        player = UserController.get_player_from_user(self.user)
        strength_after_effect = player.get_clear_parameter('Siła')

        self.assertTrue(initial_strength < strength_after_effect)

        player2 = UserController.get_player_from_user(self.user2)
        ws_handler.stop_all_effects(player2)
        player = UserController.get_player_from_user(self.user)

        final_strength = player.get_clear_parameter('Siła')
        self.assertTrue(final_strength == initial_strength)


class EffectTest(DetailsUserUserAttackUsageTest):
    def setUp(self):
        DetailsUserUserAttackUsageTest.setUp(self)

    def test_poison(self):
        self.player.set_parameters(10, 1000, 1000, 1000)
        initial_hp = self.player.get_hp()

        response = self.special_attack(poison_attack.name)
        player = UserController.get_player_from_user(self.user2)
        hp_after_attack = player.get_hp()
        self.assertTrue(initial_hp > hp_after_attack)

        is_poisoned = ws_handler.creature_has_effect(self.player2, Poison)
        self.assertTrue(is_poisoned is True)

        player_effects = ws_handler.get_battle_effects(player.user)

        for effect in player_effects:
            if type(effect) is Poison:
                BattleEffectController().execute_interval(id(effect))

        player = UserController.get_player_from_user(self.user2)
        final_hp = player.get_hp()
        self.assertTrue(hp_after_attack > final_hp)

    def test_effect_availability(self):
        self.player.set_parameters(10, 1000, 1000, 1000)
        self.player.experience = 10
        self.player.save()
        response = self.special_attack_process_command(ogre_hit.name)
        self.assertTrue(response.status == 'no_effect')

        player = UserController.get_player_from_user(self.user)
        player.experience = 110
        player.save()
        response = self.special_attack_process_command(ogre_hit.name)
        self.assertTrue(response.status == 'success')

if __name__ == '__main__':
    unittest.main()