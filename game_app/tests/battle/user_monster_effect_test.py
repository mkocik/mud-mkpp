import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from unittest.mock import Mock

from django.test import Client, RequestFactory

from django.utils import unittest
from django.contrib.auth.models import User

from MUD_ADMIN.game.battle.attack_definition import eruption, freeze, normal_attack, jump_attack
from game_app.battle.attack import *
from game_app.command.process.processing import CommandFactory
from game_app.models import Monster, MonsterInMap
from django.test import TestCase

class DetailsUserMonsterAttackUsageTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        client = Client()
        client2 = Client()

        self.user = User.objects.get(username="test")
        self.user2 = User.objects.get(username="test2")

        self.player = Player.objects.get(user=self.user)
        self.player2 = Player.objects.get(user=self.user2)

        self.player.set_location(Map.objects.get(pk=1))  #place (0,0)
        self.player2.set_location(Map.objects.get(pk=1))  #place (0,0)

        self.location = Map.objects.get(pk=3)

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)

        client = Client()
        client2 = Client()

        self.web_socket = Mock()
        self.web_socket2 = Mock()
        ws_handler.register("test", self.web_socket)
        ws_handler.register("test2", self.web_socket2)

        monster_in_map = MonsterInMap(monster=Monster.objects.all()[0], map=self.player.location)
        self.player = Player.objects.get(user=User.objects.get(username='test'))
        self.player.creature = 0
        self.player.save()
        MonsterInMap.objects.filter(map=Map.objects.get(pk=self.player.location.pk)).delete()

        monster_in_map.player = self.player
        monster_in_map.save()
        self.monster_in_map = monster_in_map


    def special_attack(self, command):
        request = self.factory.get('/commandExe/', {"command": command}, HTTP_X_REQUESTED_WITH='XMLHttpRequest')

        request.user = self.user
        command_object = CommandFactory.get_command_object(request)
        response = command_object.execute()

        return response

    def get_monster_in_map(self, monster_in_map):
        try:
            return MonsterInMap.objects.get(pk=monster_in_map.pk)
        except MonsterInMap.DoesNotExist:
            return None

    def test_immediate_effect(self):
        #checking fatality effect
        self.player.set_parameters(1, 1000, 1000, 1000)
        self.monster_in_map.hp = 2
        self.monster_in_map.save()

        response = self.special_attack(eruption.name)
        monster_in_map = self.get_monster_in_map(self.monster_in_map)
        self.assertTrue(monster_in_map is None)


    def test_effect_to_next_hit(self):
        ws_handler.stop_all_effects(self.monster_in_map)
        self.player.set_parameters(1, 1000, 1000, 1000)
        self.assertTrue(ws_handler.creature_has_effect(self.monster_in_map, Paralysis) is False)

        response = self.special_attack(freeze.name)
        self.assertTrue(ws_handler.creature_has_effect(self.monster_in_map, Paralysis) is True)

        response = self.special_attack(normal_attack.name)
        self.assertTrue(ws_handler.creature_has_effect(self.monster_in_map, Paralysis) is False)

    def test_effect_to_end_of_battle(self):
        self.player.set_parameters(10000, 1000, 1000, 1000)
        initial_hp = self.monster_in_map.get_hp()

        response = self.special_attack(jump_attack.name)
        monster_in_map = self.get_monster_in_map(self.monster_in_map)
        self.assertTrue(monster_in_map is None)
        self.assertTrue(ws_handler.get_battle_effects(monster_in_map) == [])

    def test_effect_for_some_time(self):
        self.player.set_parameters(1, 1000, 1000, 1000)
        self.monster_in_map.hp = 10000
        self.monster_in_map.save()
        self.assertTrue(ws_handler.creature_has_effect(self.monster_in_map, Paralysis) is False)

        response = self.special_attack(eruption.name)
        monster_in_map = self.get_monster_in_map(self.monster_in_map)
        self.assertTrue(ws_handler.creature_has_effect(monster_in_map, Paralysis) is True)

        monster_in_map = self.get_monster_in_map(self.monster_in_map)
        ws_handler.stop_all_effects(monster_in_map)

        self.assertTrue(ws_handler.creature_has_effect(monster_in_map, Paralysis) is False)


if __name__ == '__main__':
    unittest.main()