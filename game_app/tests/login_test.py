import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from django.utils import unittest
from django.contrib.auth.models import User
from django.test.client import Client
from django.test import TestCase


#login test must be the first test running
class LoginTest(TestCase):
    def test_execute(self):

        c = Client()

        #registration process
        response  = c.post('/register/', {"username":"test", "password":"test"})
        self.assertTrue(response)

        response  = c.login(username="test", password="test2")
        self.assertFalse(response)

        response  = c.login(username="test", password="test")
        self.assertTrue(response)

        #no such user
        response  = c.login(username="foo", password="foo")
        self.assertFalse(response)

        #test superuser logging
        response  = c.login(username="root", password="root")
        self.assertTrue(response)

        try:
            users = User.objects.all()
            users.delete()
        except:
            pass

        #creates users for testing
        user = User(username="test")
        user.save()
        user = User(username="test2")
        user.save()
        user = User(username="test3")
        user.save()

if __name__ == '__main__':
    unittest.main()
