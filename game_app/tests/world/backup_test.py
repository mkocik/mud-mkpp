
from django.utils import unittest

from django.test.client import Client

from django.test import TestCase

from game_app.world.backup.create_backup import *

from game_app.world.backup.restore_backup import *


class BackupTest(TestCase):
    def test_execute(self):
        c = Client()
        response  = c.login(username="root", password="root")
        self.assertTrue(response)
        bck = Backup()
        tables = ['PlayableParameter','Monster','EquipmentSlot','ItemType','Item','LandType','MapLevel','Map','MonsterLoot','MonsterParameter','MonsterLandType','ItemLandType',
        'ItemParameter','Player','ReportTime','ReportResponse', 'PlayerParameter','PlayerEquipment','PlayerEquipmentSlot','NormalAsset','Riddle','Stone','Chest','DialogGraph','QuestObject',
        'ChestLoot','NPC','MapPassage','AssetInMap','DialogState''Quest']

        bck.create_backup(tables)

        res = RestoreBackup().execute(tables,bck.current_directory,'del')
        res = RestoreBackup().execute(tables,bck.current_directory,'mod')


if __name__ == '__main__':
    unittest.main()