# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, IntegrityError
#to synchronise db: python manage.py syncdb
from django.db import transaction


class LandType(models.Model):
    class Meta: app_label = "game_app"

    land_name = models.CharField(max_length=20, null=False)
    monster_probability = models.IntegerField(null=False)
    item_probability = models.IntegerField(null=False)
    default_description = models.TextField(default="")
    number = models.IntegerField(null=False, unique=True)

    def __str__(self):
        return self.land_name

    def __unicode__(self):
        return self.__str__()

    def set_monster_probability(self, percent):
        self.monster_probability = percent
        self.save()

    def set_item_probability(self, percent):
        self.item_probability = percent
        self.save()


class MapLevel(models.Model):
    class Meta:
        app_label = "game_app"

    level = models.IntegerField(null=False, default=0)
    name = models.CharField(null=False, max_length=20)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'level ' + str(self.level) + ': ' + self.name


class Map(models.Model):
    class Meta:
        app_label = "game_app"
        ordering = ('level', 'x', 'y',)

    x = models.IntegerField(null=False)
    y = models.IntegerField(null=False)
    land_type = models.ForeignKey("LandType", null=False)
    description = models.TextField()
    #level = models.IntegerField(null=False, default=0)
    level = models.ForeignKey("MapLevel", null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(%s, %s, %s)" % (self.x, self.y, self.level.level)

    # @transaction.atomic
    def drop_gold(self, amount):
        try:
            # with transaction.atomic():
                gold_in_map = GoldInMap.objects.get(map=self)
                gold = gold_in_map.amount
                gold_in_map.amount = amount + gold
                gold_in_map.save()
        # except IntegrityError:
        #     self.drop_gold(amount)
        except GoldInMap.DoesNotExist:
            gold_in_map = GoldInMap(map=self, amount=amount)
            gold_in_map.save()

    def get_gold_in_map(self):
        try:
            gold_in_map = GoldInMap.objects.get(map=self)
            return gold_in_map.amount
        except GoldInMap.DoesNotExist:
            return 0

    def is_direction_available(self, curr_id, coord_x, coord_y):
        try:
            map_obj = Map.objects.get(pk=curr_id)
            return Map.objects.get(x=map_obj.x + coord_x, y=map_obj.y + coord_y, level=map_obj.level)
        except Map.DoesNotExist:
            return None

    def get_passages(self):
        try:
            passages = MapPassage.objects.filter(from_map=self)
            return passages
        except MapPassage.DoesNotExist:
            return None

    def is_passage(self):
        try:
            passages = MapPassage.objects.filter(from_map=self).count()
            return passages > 0
        except MapPassage.DoesNotExist:
            return False

    def get_items(self):
        from game_app.models import ItemInMap

        try:
            items = ItemInMap.objects.filter(map=self)
            return items
        except ItemInMap.DoesNotExist:
            return None

    def get_assets_in_map(self):
        from game_app.models import AssetInMap

        return AssetInMap.objects.filter(map=self)


    def get_specific_asset_in_map(self, asset_name):
        from game_app.models import AssetInMap, Asset

        try:
            asset = Asset.objects.get(name=asset_name)
            assets_in_map = AssetInMap.objects.filter(map=self, asset=asset)
            return assets_in_map[0]
        except:
            return None

    def get_asset_in_map_by_index(self, index):
        try:
            assets = self.get_assets_in_map()

            if index - 1 >= len(assets):
                return None

            return assets[index - 1]

        except:
            return None


    def remove_item(self, item):
        item.delete()


    def get_single_item(self):
        from game_app.models import ItemInMap

        try:
            item_in_map = ItemInMap.objects.get(map=self)
            return item_in_map
        except:
            return None

    #get first of item with given name from current field
    def get_item(self, name):
        from game_app.models import Item, ItemInMap

        try:
            item = Item.objects.get(name=name)
            item_in_map = ItemInMap.objects.filter(map=self, item=item)
            return item_in_map[0]
        except:
            return None

    def get_surroundings(self):
        location = self
        surroundings = {}

        from game_app.command.settings.command_settings import direction_coordinates

        for offsetX, offsetY, directionName in direction_coordinates:
            if self.is_direction_available(location.pk, offsetX, offsetY):
                surroundings[directionName] = Map.objects.get(x=location.x + offsetX, y=location.y + offsetY,
                                                              level=location.level)
        return surroundings

    def get_monster_in_map(self):
        from game_app.models import MonsterInMap

        try:
            monster_in_maps = MonsterInMap.objects.filter(map=self)

            if monster_in_maps.count() == 1:
                return MonsterInMap.objects.get(map=self)
            else:
                return None

        except MonsterInMap.DoesNotExist:
            return None

    def get_monster_in_map_simulation(self, monster):
        from game_app.models import MonsterInMap
        try:
            monster_in_map = MonsterInMap.objects.get(map=self, monster=monster)
            return monster_in_map
        except MonsterInMap.DoesNotExist:
            return None

    def get_item_in_map(self):
        from game_app.models import ItemInMap

        try:
            item_in_map = ItemInMap.objects.filter(map=self)
        except:
            item_in_map = None
        return item_in_map

    def get_item_by_id_from_dict(self, number, amount=1):
        try:
            items = self.get_items_in_map_as_dict()

            if number - 1 >= len(items):
                return None
            counter = 1

            for it in items:
                if counter == number:

                    if amount > items[it]:
                        return False
                    return it

                counter += 1

            return None

        except:
            return None

    def get_items_by_name(self, name, amount, player):
        from game_app.models import ItemInMap

        item_in_map = ItemInMap.objects.filter(map=self)
        for item in item_in_map:
            if item.item.name == name:
                if amount > 0:
                    player.add_to_equipment(item.item)
                    player.location.remove_item(item)
                    amount -= 1

    def get_items_in_map_as_dict(self):
        from game_app.models import ItemInMap

        try:
            item_in_map = ItemInMap.objects.filter(map=self)
            dict = {}
            items_name = []

            for item in item_in_map:
                if item.item.name in items_name:

                    for key, value in dict.items():
                        if key.item.name == item.item.name:
                            dict[key] += 1

                else:
                    items_name.append(item.item.name)
                    dict[item] = 1

        except:
            dict = None
        return dict

    def get_monster_land_types(self):
        from game_app.models import MonsterLandType

        return MonsterLandType.objects.filter(land_type=self.land_type)

    def get_item_land_types(self):
        from game_app.models import ItemLandType

        return ItemLandType.objects.filter(land_type=self.land_type)

    def get_players(self):
        from game_app.models import Player

        players = Player.objects.all()
        players_in_location = []

        for player in players:
            if player.location == self:
                players_in_location.append(player)

        return players_in_location

    def create_monster(self, monster):
        from game_app.models import MonsterInMap

        monster_in_map = MonsterInMap(map=self, monster=monster)
        monster_in_map.save()
        return monster_in_map

    def create_item(self, item):
        from game_app.models import ItemInMap

        item_in_map = ItemInMap(map=self, item=item)
        item_in_map.save()
        return item_in_map

    def put_item(self, item):
        from game_app.models import ItemInMap

        item_in_map = ItemInMap(item=item, map=self)
        item_in_map.save()

    def get_passage(self):
        try:
            map_passage = MapPassage.objects.filter(from_map=self)
            return map_passage
        except:
            return None

    def get_riddle(self):
        passage = self.get_passage()

        if passage:
            #there will be only one passage on map where asset stands
            return passage[0].asset
        else:
            return None


    def get_loots_from_chest(self, chest_name):
        from game_app.models import Chest

        chest = Chest.objects.get(name=chest_name)
        return chest.get_loots()


class MapPassage(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(null=False, max_length=50)
    from_map = models.ForeignKey('Map', related_name='from_map', null=False)
    to_map = models.ForeignKey('Map', related_name='to_map', null=False)
    asset = models.ForeignKey('Riddle', null=True, blank=True)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        string_representation = '%s from %s to %s' % (self.name, self.from_map.__str__(), self.to_map.__str__())
        if self.asset:
            string_representation += ' with riddle: %s' % self.riddle.__str__()

        return string_representation

    def get_map(self):
        return self.from_map


class GoldInMap(models.Model):
    class Meta: app_label = "game_app"

    map = models.ForeignKey('Map', null=False)
    amount = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(" + str(self.map.x) + ", " + str(self.map.y) + ") -> " + str(self.amount)