# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from random import randint

from django.db import models
from django.contrib.auth.models import User

from MUD_ADMIN.game import properties
from MUD_ADMIN.game.asset.definition.asset_type import Coffer, Person
from game_app.battle.interfaces import IFighter
from game_app.command.highlighter import Highlighter
from game_app.models import Quest, MapLevel
from language.pl_pl_parameters import *
from game_app.utils.discover.discover_attributes_functions import attributes_functions_dictionary


class AllParameter(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(max_length=45, null=False)
    default_val = models.IntegerField(default=0)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class PlayableParameter(AllParameter):
    class Meta:
        app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class StaticParameter(AllParameter):
    class Meta: app_label = "game_app"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()


class AllAttribute(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(max_length=45, null=False)
    default_val = models.IntegerField(default=0)
    function_name = models.CharField(max_length=45, null=False)

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()


class PlayableAttribute(AllAttribute):
    class Meta: app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class StaticAttribute(AllAttribute):
    class Meta: app_label = "game_app"

    def __str__(self):
        return self.name

    def __unicode__(self):
        return self.__str__()


class PlayerSpellbook(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    spell = models.ForeignKey('Spellbook', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.__str__() + " - " + self.spell.__str__()


class AllPlayerItem(models.Model):
    class Meta:
        app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    item = models.ForeignKey('Item', null=False)


class PlayerEquipment(AllPlayerItem):
    class Meta:
        app_label = "game_app"


class PlayerEquipmentSlot(AllPlayerItem):
    class Meta:
        app_label = "game_app"


class ReportTime(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player')
    start_date = models.DateTimeField(null=False)
    end_date = models.DateTimeField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.user.username + " " + str(self.start_date) + " " + str(self.end_date)


class ReportResponse(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player')
    response = models.TextField(null=False)
    date = models.DateTimeField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.user.username + " " + str(self.date)


class Player(models.Model):
    class Meta:
        app_label = "game_app"

    from MUD_ADMIN.game.properties import user_colors

    user = models.OneToOneField(User)
    location = models.ForeignKey('Map', null=False, default=0)
    gold = models.IntegerField(default=0)
    experience = models.IntegerField(default=0)
    distribution_points = models.IntegerField(default=0)
    skills_points = models.IntegerField(default=0)
    color = models.IntegerField(default=randint(0, len(user_colors) - 1))
    creature = models.IntegerField(default=-1)
    # creature == -1 - not fighting
    # creature == 0 - for monsters,
    # creature  > 0 - for other user, as his primary key
    asset_in_map = models.ForeignKey('AssetInMap', null=True, blank=True, default=None, related_name="asset_in_map")
    in_trade = models.BooleanField(default=False, null=False)
    stealing_from = models.ForeignKey('AssetInMap', null=True, blank=True, default=None, related_name="stealing_from")

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.user.username

    def is_in_combat(self):
        return self.creature > -1

    def is_busy(self):
        return self.asset_in_map is not None

    def ban_player(self, npc):
        from game_app.models.AssetModel import NPCBan
        ban = NPCBan(player=self, npc=npc)
        ban.save()

    def is_player_banned(self, npc):
        from game_app.models.AssetModel import NPCBan
        from MUD_ADMIN.game.properties import NPC_BAN_DELAY
        from datetime import timedelta
        from django.utils import timezone

        try:
            bans = NPCBan.objects.filter(player=self, npc=npc)

            for ban in bans:
                if ban.date + timedelta(seconds=NPC_BAN_DELAY) > timezone.now():
                    return True

                else:
                    ban.delete()
            return False

        except NPCBan.DoesNotExist:
            return False

    def get_gold_from_map(self):
        from game_app.models import GoldInMap

        try:
            gold = GoldInMap.objects.get(map=self.location)
            amount = gold.amount
            self.increase_gold(amount)
            gold.delete()
            return amount
        except:
            return None

    def get_attribute_value(self, name):
        attr = self.get_attribute(name)
        if attr is None:
            return 0
        else:
            return attr.current_state

    def get_skill_value(self, name):
        skill = self.get_skill(name)
        if skill is None:
            return 0
        else:
            return skill.value

    def increase_lvl(self):
        self.add_distribution_points(properties.attribute_points_for_lvl(IFighter(self)))
        self.add_skill_points(properties.skills_points_for_lvl(IFighter(self)))
        self.regain_all_attributes()

    def get_current_hp(self):
        try:
            hp = AllAttribute.objects.get(name=HP_NAME)

            return PlayerAttribute.objects.get(attribute=hp,player=self)
        except:
            return 0

    def get_hp(self):
        hp_attribute = self.get_current_hp()
        if hp_attribute != 0:
            return hp_attribute.current_state
        else:
            return 0

    def get_max_hp(self):
        static_attr = StaticAttribute.objects.get(name=HP_NAME)
        return static_attr.default_val + self.get_attribute_mods(static_attr.function_name)

    def get_attribute_mods(self, function_name):
        fighter = IFighter(player=self)
        return attributes_functions_dictionary[function_name](fighter)

    def get_all_attributes(self):
        return PlayerAttribute.objects.filter(player=self)


    def natural_armor(self):
        try:
            natural_armor = properties.armor_durability(IFighter(self))
            return natural_armor
        except:
            return 0

    def armor_from_items(self):
        from game_app.models import ItemParameter,PlayerEquipmentSlot,StaticParameter
        try:
            items_armor = 0
            player_eq = PlayerEquipmentSlot.objects.filter(player=self)

            armor_par = StaticParameter.objects.get(name=ARMOR)

            for single_eq in player_eq:
                try:  #maybe item has no attributes a- then throws except
                    armor_param = ItemParameter.objects.get(item=single_eq.item, parameter=armor_par)
                except:
                    armor_param = 0
                if armor_param is not int:
                    items_armor += armor_param.state
                else:
                    items_armor += armor_param

            return items_armor
        except:
            return 0

    def get_all_attributes_with_mods(self):
        attributes = PlayerAttribute.objects.filter(player=self)
        for attr in attributes:
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attr.max_state = static_attr.default_val + self.get_attribute_mods(static_attr.function_name)
        return attributes


    def get_player_and_asset_items_description(self):
        if self.asset_in_map is not None:
            message = self.asset_in_map.get_equipment_description(self)
            message += "<br/>" + self.get_equipment_description(self.asset_in_map.asset)
            return message
        else:
            return None

    def regain_all_attributes(self):
        try:
            attrs = PlayerAttribute.objects.filter(player=self)
            for attr in attrs:
                static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
                attr.current_state = static_attr.default_val + self.get_attribute_mods(static_attr.function_name)
                attr.save()
        except:
            return None

    def move(self, direction):

        from game_app.command.settings.command_settings import direction_coordinates

        target = None
        for offsetX, offsetY, directionName in direction_coordinates:
            if direction == directionName:
                target = self.location.is_direction_available(self.location.pk, offsetX, offsetY)

        if target is not None:
            self.location = target
            self.save()
            return self.location

        return []

    def move_to_index(self, index):
        self.location.pk = index
        self.save()

    def increase_gold(self, amount):
        self.gold = self.gold + amount
        self.save()

    def add_distribution_points(self, amount):
        self.distribution_points = self.distribution_points + amount
        self.save()

    def add_skill_points(self, amount):
        self.skills_points = self.skills_points + amount
        self.save()

    def remove_distribution_points(self, amount):
        self.distribution_points = self.distribution_points - amount
        self.save()

    def is_slot_empty(self, item):
        from game_app.models import EquipmentSlot

        if item.item_type.equipment_slot is None:
            return None

        equipment_slots = EquipmentSlot.objects.all()
        all_eq_slots = {}

        for eq in equipment_slots:
            all_eq_slots[eq.name] = eq.multiplicity

        try:
            player_equipment_slots = PlayerEquipmentSlot.objects.filter(player=self)
            for p_eq in player_equipment_slots:
                all_eq_slots[p_eq.item.item_type.equipment_slot.name] -= 1

            if all_eq_slots[item.item_type.equipment_slot.name] > 0:
                return True
            else:
                return False
        except PlayerEquipmentSlot.DoesNotExist:
            return True

    def equip_item(self, item):
        try:
            eq_del = PlayerEquipment.objects.filter(player=self, item=item)
            eq = PlayerEquipmentSlot(player=self, item=item)
            eq.save()
            eq_del[0].delete()
        except:
            return None

    def get_all_equipped_slots(self):
        try:
            eq_items = PlayerEquipmentSlot.objects.filter(player=self)
            return eq_items
        except:
            return None

    def get_item_param_list(self):
        try:
            dict = {}
            eq_items = PlayerEquipmentSlot.objects.filter(player=self)

            for eq_item in eq_items:
                params = eq_item.item.get_params(self)
                if params is not None:
                    for key, value in params.items():
                        if key in dict:
                            dict[key] += value
                        else:
                            dict[key] = value

            for param in self.get_all_parameters():
                if param.pk not in dict:
                    dict[param.pk] = 0

            return dict
        except PlayerEquipmentSlot.DoesNotExist:
            return None

    def get_item_modifier_in_param(self, player_parameter_id):
        try:
            param_list = self.get_item_param_list()
            if param_list is None:
                return None
            for key, value in param_list.items():
                if key == player_parameter_id:
                    return value
            return None
        except:
            return None

    def remove_from_equipped_slots_by_index(self, index):
        try:
            eq_items = PlayerEquipmentSlot.objects.filter(player=self)
            if index <= 0 or index > len(eq_items):
                return False
            eq = PlayerEquipment(player=self, item=eq_items[index - 1].item)
            eq.save()
            slot_to_delete = eq_items[index - 1]
            slot_to_delete.delete()
            return True
        except:
            return None

    def get_parameter_object(self, name):
        try:
            parameters = PlayerParameter.objects.filter(player=self)

            for param in parameters:
                if param.get_name().lower() == name.lower():
                    return param

            return None
        except PlayableParameter.DoesNotExist:
            return None


    def get_attribute(self, name):
        try:
            attributes = PlayerAttribute.objects.filter(player=self)
            for attr in attributes:
                if attr.attribute.name.lower() == name.lower():
                    return attr
        except:
            return None
        return None

    def get_all_skills(self):
        return PlayerSkill.objects.filter(player=self)

    def get_skill(self, id):
        try:
            skills = Skill.objects.all()
            counter = 0
            for skill in skills:
                if(id-1==counter):
                    return skill
                counter+=1
        except:
            return None
        return None

    def get_skill_by_name(self,name):
       try:
           skill =  Skill.objects.get(name=name)
           player_skill = PlayerSkill.objects.get(player=self,skill=skill)
           return player_skill
       except:
            return None

    def add_skill(self,skill,value):
        try:
            player_skill = PlayerSkill.objects.get(skill=skill,player=self)
            player_skill.value = player_skill.value + value
            player_skill.save()
            self.skills_points = self.skills_points - value
            self.save()
        except PlayerSkill.DoesNotExist:
            player_skill = PlayerSkill(player=self,skill=skill,value=value)
            player_skill.save()
            self.skills_points = self.skills_points - value
            self.save()

    def get_item_from_inventory_by_number(self, number):
        try:
            player_equipment = self.get_all_inventory_as_dict()
            items = [item for item in player_equipment.keys()]
            if number - 1 >= len(items):
                return None
            return items[number - 1]
        except:
            return None

    def get_equipment_description(self, asset):
        from game_app.command.execution.mode.trade import TradePriceRates
        equipment_quantity = self.get_all_inventory_as_dict()

        message = "Posiadasz: " + GOLD + " " + str(self.gold) + "<br/>"

        if len(equipment_quantity.keys()) > 0:
            message += "<br> Posiadasz przedmioty: <br>"
        else:
            message += "<br> Nie posiadasz przedmiotów. <br>"
            return message

        index = 1

        for equipment, quantity in equipment_quantity.items():

            item_price = TradePriceRates().get_price(equipment.item.value, asset, self)
            message += "%s. %s (%s) -- %s <br>" % (index, equipment.item.name, quantity, item_price)
            index += 1

        return message + '<br>'

    def delete_item_from_inventory_by_number(self, number, amount=1):
        from game_app.models import ItemInMap

        try:
            items = self.get_all_inventory_as_dict()
            counter = 1

            for key, value in items.items():

                if counter == number:

                    if value < amount:
                        return None

                    name = key.item.name
                    all_eq = PlayerEquipment.objects.filter(player=self)

                    for eq_item in all_eq:

                        if eq_item.item.name == name:

                            if amount > 0:
                                item = ItemInMap(map=self.location, item=eq_item.item)
                                item.save()

                                eq_item.delete()
                                amount -= 1
                    return True

                counter += counter

            return None

        except:
            return None

    def get_inventory_amount(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self)
            return len(equipment)
        except:
            return 0

    def get_all_inventory(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self)
            return equipment
        except:
            return None

    def get_all_inventory_as_dict(self):
        try:
            equipment = PlayerEquipment.objects.filter(player=self)

            dict = {}
            eq_name = []

            for eq in equipment:
                if eq.item.name in eq_name:
                    for key, value in dict.items():
                        if key.item.name == eq.item.name:
                            dict[key] += 1
                else:
                    eq_name.append(eq.item.name)
                    dict[eq] = 1
        except:
            dict = None
        return dict

    def add_to_equipment(self, item):
        equipment = PlayerEquipment(player=self, item=item)
        equipment.save()

    def add_to_param(self, param, amount):
        attributes_dict_before = {}
        for attr in PlayerAttribute.objects.filter(player=self):
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attributes_dict_before[attr.attribute.name] = self.get_attribute_mods(static_attr.function_name)

        param.increase_param(amount)

        attributes_dict_after = {}
        for attr in PlayerAttribute.objects.filter(player=self):
            static_attr = StaticAttribute.objects.get(name=attr.attribute.name)
            attributes_dict_after[attr.attribute.name] = self.get_attribute_mods(static_attr.function_name)

        for attr_name,value in attributes_dict_before.items():
            difference = attributes_dict_after[attr_name] - attributes_dict_before[attr_name]
            if difference != 0:
                try:
                    attr_to_change = AllAttribute.objects.get(name=attr_name)
                    player_attr = PlayerAttribute.objects.get(player=self,attribute=attr_to_change)
                    player_attr.current_state+=difference
                    player_attr.save()
                except:
                    return None


    def get_level(self):
        return properties.experience_function(self.experience)

    def increase_experience(self, xp):
        self.experience = self.experience + xp
        self.save()

    def get_clear_parameter(self, name):
        parameter = PlayableParameter.objects.get(name=name)
        parameter_object = PlayerParameter.objects.get(player=self, parameter=parameter)
        return parameter_object.state

    def get_parameter(self, name):
        from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
        if name == 'Pancerz':
            return ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(self)

        else:
            try:
                parameter = PlayableParameter.objects.get(name=name)
                parameter_object = PlayerParameter.objects.get(player=self, parameter=parameter)
                parameter_value = self.get_item_modifier_in_param(parameter_object.pk) + parameter_object.state
                return parameter_value
            except:
                return

    def get_all_parameters(self):
        return PlayerParameter.objects.filter(player=self)

    def get_all_parameters_with_mods(self):
        params = PlayerParameter.objects.filter(player=self)
        for param in params:
            param.state = self.get_parameter(param.parameter.name)
        return params

    def reduce_hp(self, difference):
        hp = self.get_current_hp()
        hp.current_state -= difference
        hp.save()
        return hp.current_state

    def regain_stamina(self):
        stamina = StaticAttribute.objects.get(name=STAMINA)
        player_stamina = PlayerAttribute.objects.get(player=self,attribute=stamina)
        player_stamina.current_state = stamina.default_val + self.get_attribute_mods(stamina.function_name)
        player_stamina.save()

    def resurrection(self):
        from game_app.models import Map
        from game_app.utils.websockets.battle_websocket import ws_handler

        self.location.drop_gold(self.gold)

        self.gold = 0

        # if self.creature == 0:
        #     monster_in_map = self.location.get_monster_in_map()
        #     ws_handler.stop_all_effects(monster_in_map)
        # elif self.creature > 0:
        #     try:
        #         opponent = Player.objects.get(pk=self.creature)
        #         ws_handler.stop_all_effects(opponent)
        #     except Player.DoesNotExist:
        #         pass

        self.set_creature(-1)
        self.location = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                        level=MapLevel.objects.get(level=properties.START_LOCATION_LVL))
        self.regain_all_attributes()
        self.save()

        ws_handler.stop_all_effects(self)


    def set_creature(self, creature):
        self.creature = creature
        self.save()

    def get_opponent(self):
        try:
            user = User.objects.get(pk=self.creature)
            return Player.objects.get(user=user)
        except User.DoesNotExist:
            return None

    def get_passage(self):
        from game_app.models import MapPassage

        try:
            map_passage = MapPassage.objects.get(from_map=self.location)
            return map_passage
        except:
            return None

    def enter_passage(self, passage):
        from game_app.models import MapPassage

        try:
            self.location = MapPassage.objects.get(name=passage, from_map=self.location).to_map
            self.save()
            return True
        except MapPassage.DoesNotExist:
            return False

    #function for tests
    def set_location(self, location):
        self.location = location
        self.save()

    def set_parameter(self, parameter_name, state):
        try:
            if state is not None:
                parameter = PlayableParameter.objects.get(name=parameter_name)
                player_parameter = PlayerParameter.objects.get(player=self, parameter=parameter)
                player_parameter.state = state
                player_parameter.save()
        except:
            return

    def set_parameters(self, strength_value, agility_value, dexterity_value, intelligence_value, durability_value=0):
        from django.db import transaction
        try:
            with transaction.atomic():
                strength = PlayableParameter.objects.get(name="Siła")
                dexterity = PlayableParameter.objects.get(name="Zręczność")
                agility = PlayableParameter.objects.get(name="Zwinność")
                durability = PlayableParameter.objects.get(name="Wytrzymałość")
                intelligence = PlayableParameter.objects.get(name=INTELLIGENCE)
        except:
            return None

        PlayerParameter.objects.all().filter(player=self).delete()
        PlayerParameter(player=self, parameter=agility, state=agility_value).save()
        PlayerParameter(player=self, parameter=strength, state=strength_value).save()
        PlayerParameter(player=self, parameter=dexterity, state=dexterity_value).save()
        PlayerParameter(player=self, parameter=durability, state=durability_value).save()
        PlayerParameter(player=self, parameter=intelligence, state=intelligence_value).save()
        self.save()


    def set_hp(self, hp):
        hp_param = self.get_current_hp()
        hp_param.current_state = hp
        hp_param.save()

    def get_quest_list(self):
        try:
            quests = Quest.objects.filter(player=self)
            return quests
        except Quest.DoesNotExist:
            return None

    def get_quest_objects_and_quests_in_db(self):
        from game_app.utils.discover.discover_graph import get_quest_by_name

        try:
            quests = Quest.objects.filter(player=self, state__gt=0)

            player_quests = []

            for quest in quests:
                quest_object = get_quest_by_name(quest.quest.name)
                player_quests.append([quest_object, quest])

            return player_quests

        except Quest.DoesNotExist:
            return None

    def check_if_quest_is_accomplished(self, asset_name = ""):

        player_quests = self.get_quest_objects_and_quests_in_db()

        if player_quests:
            message = ''

            for quest_object, quest_in_db in player_quests:
                task = quest_object.tasks[quest_in_db.state - 1]

                if task.check(self, asset_name):
                    message += '<br><br> Wykonałeś zadanie %s! <br>' % Highlighter.bold(task.manual_name)

                    quest_in_db.state += 1
                    quest_in_db.save()

                    if quest_in_db.state - 1 == len(quest_object.tasks):
                        #quest is completed
                        message += '<br> Wykonałeś quest %s! <br><br>' % Highlighter.bold(quest_in_db.quest.name)

                        quest_in_db.state = 0
                        quest_in_db.save()

                        if quest_object.immediate_reward is True and quest_object.get_award_description_after_accomplish() != '':
                            msg = quest_object.award_player(self)
                            if msg != None:
                                message = msg
                            else:
                                message += quest_object.get_award_description_after_accomplish()
                                quest_in_db.state = -1
                                quest_in_db.save()

            return message

        else:
            return ''

    def get_avg_damage(self):
        import random
        from MUD_ADMIN.game.characteristics.characteristics import all_characteristics

        return (random.randint(((list(filter(lambda x: x.name == MIN_DMG, all_characteristics)))[0])(self),((list(filter(lambda x: x.name == MAX_DMG, all_characteristics)))[0])(self)))/2

    def has_item(self, item):
        try:
            no_of_equipments = AllPlayerItem.objects.filter(player=self, item=item).count()
            return no_of_equipments > 0
        except PlayerEquipment.DoesNotExist:
            return False

    def get_equipment(self):
        return AllPlayerItem.objects.filter(player=self).order_by('id')

    def is_opening_chest(self):
        if self.asset_in_map is not None:
            return self.asset_in_map.asset.type_name == Coffer().name
        else:
            return False

    def is_talking(self):
        if self.asset_in_map is not None:
            return self.asset_in_map.asset.type_name == Person().name
        else:
            return False

    def is_stealing(self):
        return self.stealing_from is not None

    def get_ws_id(self):
        return self.user


class PlayerParameter(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    parameter = models.ForeignKey('PlayableParameter', null=False)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.player.__str__() + ' - ' + self.parameter.__str__()

    def get_name(self):
        return self.parameter.name

    def get_param_id(self):
        return self.pk

    def increase_param(self, amount):
        self.state = self.state + amount
        self.save()



class PlayerAttribute(models.Model):
    class Meta: app_label = "game_app"

    player = models.ForeignKey('Player', null=False)
    attribute = models.ForeignKey('AllAttribute', null=False)
    current_state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.attribute.__str__()

    def get_name(self):
        return self.attribute.name

    def get_attr_id(self):
        return self.id


class Skill(models.Model):
    class Meta: app_label = "game_app"
    ordering = ('name',)

    name = models.CharField(null=False,max_length=255)
    description = models.TextField()

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name.__str__()


class PlayerSkill(models.Model):
    class Meta:
        app_label = "game_app"

    skill = models.ForeignKey('Skill',null=False)
    player = models.ForeignKey('Player',null=False)
    value = models.IntegerField(default=0)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.skill.__str__() + " " + self.player.user.__str__()
