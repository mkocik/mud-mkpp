# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from MUD_ADMIN.game.asset.complex.chest import AbstractChest
from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractNormalAsset, AbstractStone
from MUD_ADMIN.game.asset.complex.npc import AbstractNPC
from MUD_ADMIN.game.asset.complex.riddle import AbstractRiddle
from language.pl_pl_parameters import *
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class Asset(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(null=False, max_length=50)
    #this field is filled in subclass constructor
    type_name = models.CharField(null=True, blank=True, max_length=50)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name

    def get_parameter(self, name):
        from game_app.models.PlayerModel import PlayableParameter

        if name == 'Pancerz':
            return ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(self)

        else:
            try:
                attribute = PlayableParameter.objects.get(name=name)
                attribute_object = NPCAttributes.objects.get(npc=self, attribute=attribute)
                attribute_value = attribute_object.state
                return attribute_value
            except:
                return None

    def get_skill_by_name(self, name):
        return None

class AssetInMap(models.Model):
    class Meta:
        app_label = "game_app"

    asset = models.ForeignKey('Asset', null=False)
    map = models.ForeignKey('Map', null=False)
    is_used = models.BooleanField(default=False)
    is_busy = models.BooleanField(default=False)
    state = models.IntegerField(default=None, null=True, blank=True, max_length=500)
    gold = models.IntegerField(default=0, null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.asset.__str__() + " " + self.map.__str__()

    def set_state(self, state):
        self.state = state
        self.save()

    def set_is_used(self):
        self.is_used = True
        self.save()

    def check_if_quest_was_assigned(self, quest_name, player):

        try:
            quest = Quest.objects.get(player=player, asset_in_map=self, quest=QuestObject.objects.get(name=quest_name))
            return quest is not None

        except Quest.DoesNotExist:
            return False

    def assign_quest(self, quest_name, player):

        if QuestObject.objects.filter(name=quest_name).count() > 0:
            quest_object = QuestObject.objects.filter(name=quest_name)[0]
        else:
            quest_object = QuestObject(name=quest_name)
            quest_object.save()

        quest = Quest(quest=quest_object, player=player, asset_in_map=self)
        quest.save()
        return

    def get_start_point(self, player):
        try:
            dialog_state = DialogState.objects.get(player=player, asset_in_map=self)
            return dialog_state.state
        except DialogState.DoesNotExist:
            return None

    def set_start_point(self, player, state):
        try:
            dialog_state = DialogState.objects.get(player=player, asset_in_map=self)
            dialog_state.state = state
            dialog_state.save()
        except DialogState.DoesNotExist:
            self.create_start_point(player, state)

    def create_start_point(self, player, state):
        state = DialogState(player=player, asset_in_map=self, state=state)
        state.save()

    def check_reward(self, player):
        try:
            quest = Quest.objects.filter(player=player, asset_in_map=self, state=0)
            return quest
        except Quest.DoesNotExist:
            return None


    def get_equipment(self):
        try:
            equipment = NPCEquipment.objects.filter(asset_in_map=self).order_by('id')

            dict = {}
            eq_name = []

            for eq in equipment:
                if eq.item.name in eq_name:
                    for key, value in dict.items():
                        if key.item.name == eq.item.name:
                            dict[key] += 1
                else:
                    eq_name.append(eq.item.name)
                    dict[eq] = 1
        except:
            dict = None
        return dict

    def get_item_from_equipment_by_number(self, number):
        try:
            equipment = self.get_equipment()
            items = [item for item in equipment.keys()]
            if number - 1 >= len(items):
                return None
            return items[number - 1]
        except:
            return None

    def steal_from_asset(self, player, index):
        try:
            equipment = self.get_equipment()
            asset_items = [item for item in equipment.keys()]

            if index - 1 >= len(asset_items):
                return None

            item = asset_items[index - 1]

            itm = item.item
            player.add_to_equipment(itm)

            item.delete()
            return itm
        except:
            return None

    def get_equipment_with_quantity(self):
        equipments = self.get_equipment()

        equipment_quantity = {}

        for equipment in equipments:
            if equipment in equipment_quantity.keys():
                equipment_quantity[equipment] += 1
            else:
                equipment_quantity[equipment] = 1

        return equipment_quantity

    def get_equipment_description(self, player):
        from game_app.command.execution.mode.trade import TradePriceRates

        equipment_quantity = self.get_equipment()

        message = self.asset.name + " posiada: " + GOLD + " " + str(self.gold) + "<br/>"

        #I don't know why this warning/error appears
        if len(equipment_quantity.keys()) > 0:
            message += "<br> %s ma na stanie obecnie następujące przedmioty: <br>" % self.asset.name
        else:
            message += "<br> %s nie ma na stanie obecnie żadnych przedmiotów. <br>" % self.asset.name
            return message

        index = 1

        for equipment, quantity in equipment_quantity.items():
            item_value = TradePriceRates().get_price(equipment.item.value, player, self.asset)

            message += "%s. %s (%s) -- %s <br>" % (index, equipment.item.name, quantity, item_value)
            index += 1

        return message + '<br>'

    def get_stealing_description(self):
        equipment_quantity = self.get_equipment()

        message = "Znajdujesz się w trybie kradzieży! Wpisz 0 aby wyjść. <br/><br/>"

        if len(equipment_quantity.keys()) > 0:
            message += "<br> %s posiada obecnie następujące przedmioty: <br>" % self.asset.name
        else:
            message += "<br> %s nie posiada obecnie żadnych przedmiotów. <br>" % self.asset.name
            return message

        index = 1

        for equipment, quantity in equipment_quantity.items():
            message += "%s. %s (%s) <br>" % (index, equipment.item.name, quantity)
            index += 1

        return message + '<br>'


class NormalAsset(Asset):
    class Meta: app_label = "game_app"

    def __init__(self, *args, **kwargs):
        super(NormalAsset, self).__init__(*args, **kwargs)
        self.type_name = AbstractNormalAsset.type.name

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'Normal asset: ' + self.name

    def get_name(self):
        return self.name


class NPC(Asset):
    class Meta:
        app_label = "game_app"

    graph = models.ForeignKey('DialogGraph', null=False)

    def __init__(self, *args, **kwargs):
        super(NPC, self).__init__(*args, **kwargs)
        self.type_name = AbstractNPC.type.name

    #Overriding
    def save(self, *args, **kwargs):
        if self.pk is not None:
            updated_npc = NPC.objects.get(pk=self.pk)

            #if graph was changed
            if updated_npc.graph != self.graph:
                asset_in_maps = AssetInMap.objects.filter(asset=self)

                for asset_in_map in asset_in_maps:
                    asset_in_map.state = None
                    asset_in_map.save()
                    DialogState.objects.filter(asset_in_map=asset_in_map).delete()

        super(NPC, self).save(*args, **kwargs)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'NPC: ' + self.name

    def get_name(self):
        return self.name


class NPCAttributes(models.Model):
    class Meta: app_label = "game_app"

    npc = models.ForeignKey('NPC', null=False)
    attribute = models.ForeignKey('AllParameter', null=False)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.attribute.__str__()

    def get_name(self):
        return self.attribute.name

    def get_param_id(self):
        return self.pk

    def increase_param(self, amount):
        self.state = self.state + amount
        self.save()


class NPCBan(models.Model):
    class Meta: app_label = "game_app"

    npc = models.ForeignKey('AssetInMap', null=False)
    date = models.DateTimeField(null=False, auto_now_add = True)
    player = models.ForeignKey('Player', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.npc.asset.name + " " + self.player.user.username + " " + str(self.date)


class DialogState(models.Model):
    class Meta: app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    asset_in_map = models.ForeignKey('AssetInMap', null=False)
    player = models.ForeignKey('Player', null=False)
    state = models.IntegerField(null=True, blank=True)

    def __str__(self):
        return 'NPC: ' + self.asset_in_map.__str__() + ' -> ' + self.player.__str__() + ' state: ' + str(
            self.state)


class Stone(Asset):
    class Meta: app_label = "game_app"

    message = models.CharField(null=False, max_length=500)

    def __init__(self, *args, **kwargs):
        super(Stone, self).__init__(*args, **kwargs)
        self.type_name = AbstractStone.type.name

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'Stone: ' + self.name + ' ' + self.message[0:10]

    def get_name(self):
        return self.name


class Riddle(Asset):
    class Meta: app_label = "game_app"

    #type_name = models.CharField(null=False, max_length=50, default=AbstractRiddle.complex.name)
    #name = models.CharField(null=False, max_length=500)
    question = models.CharField(null=False, max_length=500)
    answer = models.CharField(null=False, max_length=500)
    justification = models.CharField(null=True, blank=True, max_length=500)

    def __init__(self, *args, **kwargs):
        super(Riddle, self).__init__(*args, **kwargs)
        self.type_name = AbstractRiddle.type.name

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'Riddle: ' + self.name + ' ' + self.question[0:10]

    def get_answer(self):
        return self.answer.split(",")

    def get_name(self):
        return self.name


class Chest(Asset):
    class Meta:
        app_label = "game_app"

    max_loot_amount = models.IntegerField(default=1, null=False)
    item_probability = models.IntegerField(default=50, null=False)
    resistance = models.IntegerField(default=0, null=True, blank=True)
    graph = models.CharField(default='', blank=True, max_length=500)
    experience = models.IntegerField(default=0, null=False)

    def __init__(self, *args, **kwargs):
        super(Chest, self).__init__(*args, **kwargs)
        self.type_name = AbstractChest.type.name

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        if self.graph and self.graph != '':
            return 'Chest: ' + self.name + ' tasks: ' + self.graph
        else:
            return 'Chest: ' + self.name + ' resistance: ' + str(self.resistance)


    def get_loots(self):
        try:
            chest_loots = ChestLoot.objects.all().filter(chest=self)
            return chest_loots
        except ChestLoot.DoesNotExist:
            return None

    def get_sum_of_loot_probability(self):
        items = self.get_loots()

        sum = 0

        for item in items:
            sum += item.probability

        return sum

    def get_name(self):
        return self.name


class ChestLoot(models.Model):
    class Meta:
        app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    chest = models.ForeignKey('Chest')
    item = models.ForeignKey('Item', null=False)
    probability = models.IntegerField(null=False)

    def __str__(self):
        return self.chest.__str__() + " - " + self.item.__str__()


class DialogGraph(models.Model):
    class Meta:
        app_label = "game_app"

    def __unicode__(self):
        return self.__str__()

    name = models.CharField(null=False, max_length=500)

    def __str__(self):
        return self.name


class Quest(models.Model):
    class Meta:
        app_label = "game_app"

    asset_in_map = models.ForeignKey('AssetInMap', null=False)
    player = models.ForeignKey('Player', null=False)
    quest = models.ForeignKey('QuestObject', null=False)
    state = models.IntegerField(default=1, null=False)

    # 1 - quest begins
    # 2 - quest began, first task is accomplished
    # 2+N - quest began, (N-1) task is accomplished
    # 0 - quest is completed, player hasn't got rewards
    # -1 - quest is completed, player got rewards

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return 'Quest: ' + self.quest.name + ' given by: ' + self.asset_in_map.__str__() + ' for: ' + self.player.__str__()


class QuestObject(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(null=False, max_length=500)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class NPCDefaultEquipment(models.Model):
    class Meta:
        app_label = "game_app"

    npc = models.ForeignKey('NPC', null=False)
    item = models.ForeignKey('Item', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.npc.__str__() + ' -> ' + self.item.__str__()


class NPCEquipment(models.Model):
    class Meta:
        app_label = "game_app"

    asset_in_map = models.ForeignKey('AssetInMap', null=False)
    item = models.ForeignKey('Item', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.asset_in_map.__str__() + ' -> ' + self.item.__str__()