# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models import *


class ItemType(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(null=False, max_length=50)
    equipment_slot = models.ForeignKey('EquipmentSlot', blank=True, null=True)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class EquipmentSlot(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(null=False, max_length=50)
    multiplicity = models.IntegerField(default=1)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name



class Item(models.Model):
    class Meta:
        app_label = "game_app"
        ordering = ('name',)

    item_type = models.ForeignKey('ItemType', null=False)
    name = models.CharField(max_length=50, null=False, unique=True)
    description = models.TextField()
    value = models.FloatField(default=0)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name

    def get_params(self, player):
        from game_app.models import PlayerParameter

        try:
            if self.item_type.equipment_slot is not None:

                dict = {}
                params = ItemParameter.objects.filter(item=self)

                if len(params) == 0:
                    return None

                for param in params:

                    if param.parameter is not None:
                        if param.parameter.name == ARMOR:
                            continue

                        player_param = PlayerParameter.objects.get(parameter=param.parameter, player=player)
                        dict[player_param.pk] = param.state

                return dict

            else:
                return None

        except :
            return None

    def get_param_desc(self):
        try:
            if self.item_type.equipment_slot is not None:
                msg = "<br>Modyfikatory: "
                params = ItemParameter.objects.filter(item=self)

                if len(params) == 0:
                    msg += "brak  "
                for param in params:
                    if param.parameter is None:
                        name = ARMOR
                    else:
                        name = param.parameter.name
                    if param.state > 0:
                        msg = msg + name + " +" + str(param.state) + ", "
                    else:
                        msg = msg + name + " " + str(param.state) + ", "
                msg = msg[:-2]
                return msg
            else:
                return ""
        except:
            return ""


class ItemLandType(models.Model):
    class Meta:
        app_label = "game_app"

    item = models.ForeignKey("Item", null=False)
    land_type = models.ForeignKey("LandType", null=False)
    probability = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.item.__str__() + " - " + self.land_type.__str__()


class ItemParameter(models.Model):
    class Meta:
        app_label = "game_app"

    item = models.ForeignKey("Item", null=False)
    parameter = models.ForeignKey("AllParameter")
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.item.__str__() + " - " + self.parameter.__str__()


class ItemInMap(models.Model):
    class Meta:
        app_label = "game_app"

    map = models.ForeignKey('Map', null=False)
    item = models.ForeignKey('Item', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.item.__str__() + " - " + self.map.__str__()