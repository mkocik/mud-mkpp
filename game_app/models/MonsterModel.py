# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import random
import datetime

from django.db import models
from django.core.validators import MinValueValidator

from language.pl_pl_parameters import *


class Monster(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(max_length=50, null=False)
    description = models.TextField()
    default_hp = models.IntegerField(null=False)
    gold_min = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    gold_max = models.IntegerField(default=0, validators=[MinValueValidator(0)])
    experience = models.IntegerField(default=1)
    aggression = models.IntegerField(default=50)
    max_loot_amount = models.IntegerField(default=1)
    item_probability = models.IntegerField(default=50)
    special_attack_probability = models.IntegerField(default=50)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name

    def get_armor(self):
        from game_app.models import PlayableParameter
        from game_app.battle.interfaces import IFighter

        try:
            param = PlayableParameter.objects.filter(name="Wytrzymałość")
            durability = MonsterParameter.objects.get(parameter=param, monster=self)
            natural_armor = IFighter.get_defence(durability.state)

            return float(natural_armor)
        except:
            return 0

    def get_parameter(self, name):
        from game_app.models import PlayableParameter

        parameter = PlayableParameter.objects.get(name=name)
        parameter_value = MonsterParameter.objects.get(monster=self, parameter=parameter).state
        return parameter_value

    def set_parameter(self, parameter_name, state):
        from game_app.models import PlayableParameter

        try:
            parameter = PlayableParameter.objects.get(name=parameter_name)
            player_parameter = MonsterParameter.objects.get(monster=self, parameter=parameter)
            player_parameter.state = state
            player_parameter.save()
        except:
            return


    #for tests
    def set_parameters(self, strength_value, agility_value, dexterity_value, intelligence_value):
        from game_app.models import PlayableParameter

        strength = PlayableParameter.objects.get(name="Siła")
        dexterity = PlayableParameter.objects.get(name="Zręczność")
        agility = PlayableParameter.objects.get(name="Zwinność")
        intelligence = PlayableParameter.objects.get(name=INTELLIGENCE)

        MonsterParameter.objects.all().filter(monster=self).delete()
        monster_agility = MonsterParameter(monster=self, parameter=agility, state=agility_value)
        monster_strong = MonsterParameter(monster=self, parameter=strength, state=strength_value)
        monster_dexterity = MonsterParameter(monster=self, parameter=dexterity, state=dexterity_value)
        monster_intelligence = MonsterParameter(monster=self, parameter=intelligence, state=intelligence_value)

        monster_agility.save()
        monster_strong.save()
        monster_dexterity.save()
        monster_intelligence.save()
        self.save()


class MonsterLandType(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey("Monster", null=False)
    land_type = models.ForeignKey("LandType", null=False)
    probability = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.land_type.__str__()


class MonsterParameter(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey('Monster', null=False)
    parameter = models.ForeignKey('PlayableParameter', blank=True, null=True, default=None)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.parameter.__str__()


class MonsterEquipment(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey('Monster', null=False)
    item = models.ForeignKey('Item', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.item.__str__()


class Spellbook(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(max_length=20, null=False)
    description = models.TextField()
    is_destructive = models.BooleanField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class MonsterSpellbook(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey('Monster', null=False)
    spell = models.ForeignKey('Spellbook', null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.spell.__str__()


class MonsterInMap(models.Model):
    class Meta:
        app_label = "game_app"

    map = models.ForeignKey('Map', null=False)
    monster = models.ForeignKey('Monster', null=False)
    player = models.ForeignKey('Player', blank=True, null=True, default=None)
    hp = models.IntegerField(null=True)

    #settings default value for hp from other table
    def __init__(self, *args, **kwargs):
        monster = kwargs.get('monster')
        super(MonsterInMap, self).__init__(*args, **kwargs)
        if monster is not None:
            self.hp = self.monster.default_hp

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return "(" + str(self.map.x) + ", " + str(self.map.y) + "lvl: " + str(
            self.map.level) + ") -> " + self.monster.name

    def set_player(self, player):
        self.player = player
        self.save()

    def reduce_hp(self, difference):
        self.hp -= difference
        self.save()
        return self.hp

    #for tests
    def set_hp(self, hp):
        self.hp = hp
        self.save()

    def get_hp(self):
        return self.hp

    def get_ws_id(self):
        return self.__str__()

    def get_clear_parameter(self):
        return 0

    def resurrection(self):
        # from game_app.utils.websockets.battle_websocket import ws_handler
        #
        # if self.player is not None:
        #     ws_handler.stop_all_effects(self.player)
        #     ws_handler.stop_all_effects(self)

        MonsterInMap.objects.get(pk=self.pk).delete()

    def random_attack(self):
        from MUD_ADMIN.game.battle.attack_definition import normal_attack
        from game_app.command.settings.command_settings import get_special_attack

        random_int = random.randint(0, 2)

        if random_int in [0, 1]:
            return normal_attack
        else:

            try:
                monster_attacks = MonsterAttack.objects.filter(monster=self.monster)
            except MonsterAttack.DoesNotExist:
                return normal_attack

            drawing = []

            for monster_attack in monster_attacks:
                if monster_attack.probability:

                    monster_attack_object = get_special_attack[monster_attack.attack.name]
                    for i in range(1, monster_attack.probability):
                        drawing.append(monster_attack_object)

            return random.choice(drawing)


class MonsterLoot(models.Model):
    class Meta: app_label = "game_app"

    monster = models.ForeignKey("Monster")
    item = models.ForeignKey("Item")
    amount = models.IntegerField(default=1)
    probability = models.IntegerField(default=1)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.item.__str__()


class Characteristic(models.Model):
    class Meta: app_label = "game_app"

    name = models.CharField(max_length=255, null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class MonsterCharacteristic(models.Model):
    class Meta:
        app_label = "game_app"

    monster = models.ForeignKey("Monster")
    characteristic = models.ForeignKey("Characteristic")
    value = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.characteristic.__str__()


class MonsterAttack(models.Model):
    class Meta:
        app_label = "game_app"

    monster = models.ForeignKey("Monster")
    attack = models.ForeignKey("Attack")
    probability = models.IntegerField(default=1, null=True, blank=True)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.monster.__str__() + " - " + self.attack.__str__() + ' z prawd. ' + str(self.probability)


class Attack(models.Model):
    class Meta:
        app_label = "game_app"

    name = models.CharField(max_length=255, null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.name


class SpellParameter(models.Model):
    class Meta: app_label = "game_app"

    spell = models.ForeignKey('Spellbook', null=False)
    parameter = models.ForeignKey('PlayableParameter', null=False)
    state = models.IntegerField(null=False)

    def __unicode__(self):
        return self.__str__()

    def __str__(self):
        return self.spell.__str__() + " - " + self.parameter.__str__()


class SimulationInfo(models.Model):
    class Meta: app_label = "game_app"

    monster1 = models.ForeignKey('Monster', null=False, related_name='monster1')
    monster2 = models.ForeignKey('Monster', null=False, related_name='monster2')
    start_time = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return self.monster1.__str__() + " vs. " + self.monster2.__str__()

    def __unicode__(self):
        return self.__str__()


class SimulationData(models.Model):
    class Meta: app_label = "game_app"

    simulation = models.ForeignKey('SimulationInfo', null=False)
    player_number = models.IntegerField(null=False)
    event_description = models.CharField(max_length=255, blank=True)
    time = models.DateTimeField(default=datetime.datetime.now, blank=True)

    def __str__(self):
        return "%s:  %s" % (self.simulation.__str__(), self.event_description)