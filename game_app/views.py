# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import datetime
import pprint

from django.utils import html
from django.contrib.auth.decorators import user_passes_test
from django.contrib import auth
from django.http import HttpResponseRedirect
from django.utils.timezone import utc
from MUD.settings import my_logger

from game_app.command.execution.fight.monster_fight import MonsterFightController
from game_app.command.process.response import AssetResponseObject
from game_app.controllers.battle_controllers import BattleController, BattleEffectController
from game_app.controllers.basic_controllers import UserController
from game_app.controllers.simulation_controller import InitSimulationController, PreSimulationController, \
    PostSimulationController
from game_app.controllers.simulation_settings import parameter_names
from game_app.models import *
from game_app.command.highlighter import Highlighter
from language.pl_pl_parameters import *
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
from game_app.utils.wrappers import *


@is_user_not_authenticated
def index(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


def login(request):
    auth.logout(request)

    if request.method != "POST":
        c = {}
        c.update(csrf(request))
        return render_to_response('index.html', c)

    username = request.POST['username']
    password = request.POST['password']
    user = auth.authenticate(username=username, password=password)

    if user and user.is_active:

        auth.login(request, user)
        player = UserController.get_player_from_user(request.user)
        if player or request.user.is_superuser:
            return HttpResponseRedirect("/game/")

        c = {"message": "Brak gracza dla wybranego użytkownika. Skontaktuj się z administratorem"}
        c.update(csrf(request))
        auth.logout(request)

        return render_to_response('index.html', c)

    c = {"message": "Wprowadzono niepoprawne dane. Błędny login lub hasło. Spróbuj ponownie."}
    c.update(csrf(request))
    return render_to_response('index.html', c)


def logout(request):
    auth.logout(request)
    c = {}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@is_request_method_post
def register(request):
    username = request.POST['username']
    password = request.POST['password']

    if_exists = User.objects.filter(username=username)
    if if_exists:
        c = {"message": "Użytkownik o takim loginie już istnieje."}
        c.update(csrf(request))
        return render_to_response('register.html', c)

    try:
        start_location = Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                         level=MapLevel.objects.get(level=properties.START_LOCATION_LVL))
    except:
        c = {"message": "Pole startowe wybrane w pliku konfiguracyjnym nie istnieje."}
        c.update(csrf(request))
        return render_to_response('register.html', c)

    user = User.objects.create_user(username=username, password=password)
    user.save()

    player = Player.objects.create(user=user, location=start_location, gold=properties.STARTING_GOLD_VALUE)
    params = PlayableParameter.objects.all()
    for param in params:
        PlayerParameter.objects.create(player=player, parameter=param, state=param.default_val)

    attrs = AllAttribute.objects.all()
    for attr in attrs:
        PlayerAttribute.objects.create(player=player, attribute=attr, current_state=attr.default_val)

    player.regain_all_attributes()  #important!

    c = {'message': "Utworzono konto. Zaloguj się."}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@is_user_authenticated_in_game
@is_user_active
def game(request):
    player = UserController.get_player_from_user(request.user)
    if player or request.user.is_superuser:
        return redirection_with_greeting_message(request)

    c = {"message": "Brak gracza dla wybranego użytkownika. Skontaktuj się z administratorem"}
    c.update(csrf(request))
    return render_to_response('index.html', c)


@is_ajax_request
def commandExe(request):
    from game_app.command.process.processing import ProcessCommand

    player = Player.objects.get(user=request.user)
    old_lvl = player.get_level()

    process_command = ProcessCommand(request)
    response = process_command.execute()

    player = Player.objects.get(user=request.user)  #do not remove - its player with new params

    #turns off html from input
    command = html.escape(request.GET['command'])

    c = {
        'status': response.status,
        'command': command,
        'map': response.get_map()
    }

    if type(response) is AssetResponseObject:
        c['update_asset_in_map'] = response.update_asset_in_map
        c['update_asset_type'] = response.update_asset_type

    if player is not None:
        c['creature'] = player.creature

        #get xp if monster was killed
        level = player.get_level()

        if old_lvl < level:
            for i in range(0, level - old_lvl):
                player.increase_lvl()
                response.message = response.get_message() + Highlighter.information(
                    "<br><br> Zdobyłeś nowy poziom. <br>Rozdziel uzyskane atrybuty!")

        attributes = player.get_all_attributes()
        attrs_array = {}
        for attr in attributes:
            attrs_array[attr.get_attr_id()] = attr.current_state

        attributes_max = player.get_all_attributes_with_mods()
        attrs_array_max = {}
        for attr in attributes_max:
            attrs_array_max[attr.get_attr_id()] = attr.max_state

        c['attributes'] = str(attrs_array)
        c['attributes_max'] = str(attrs_array_max)

        armor = ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player)

        #xp and hp changes after killing monster
        c['experience'] = player.experience
        c['level'] = player.get_level()
        c['gold'] = player.gold
        c['gold_name'] = GOLD

        parameters = player.get_all_parameters()
        param_array = {}

        for param in parameters:
            param_array[param.get_param_id()] = param.state

        c['parameters'] = str(param_array)
        item_param_list = player.get_item_param_list()
        c['parametersItems'] = item_param_list
        c['armor'] = armor

    c['message'] = response.get_message()

    return HttpResponse(json.dumps(c), content_type='application/json')


@is_ajax_request
def monsterAttack(request):
    attack_property = BattleController().get_monster_attack(request)

    if attack_property is None:
        return HttpResponse(json.dumps({}), content_type='application/json')

    response = MonsterFightController().monster_attack(request, attack_property)

    c = {
        'message': response.get_message(),
        'resurrection': response.get_resurrection(),
        'status': response.status
    }

    player = Player.objects.get(user=request.user)
    attributes = player.get_all_attributes()

    attributes_array = {}
    for attribute in attributes:
        attributes_array[attribute.get_attr_id()] = attribute.current_state

    attributes_max = player.get_all_attributes_with_mods()
    attributes_array_max = {}
    for attribute in attributes_max:
        attributes_array_max[attribute.get_attr_id()] = attribute.max_state

    c['attributes'] = str(attributes_array)
    c['attributes_max'] = str(attributes_array_max)

    return HttpResponse(json.dumps(c), content_type='application/json')


@is_ajax_request
def stopEffect(request):
    from game_app.utils.websockets.battle_websocket import ws_handler

    effect_id = request.GET['effect_id']
    effect_id = int(effect_id)

    ws_handler.stop_effect(effect_id)
    return HttpResponse(json.dumps({}), content_type='application/json')


@is_ajax_request
def startInterval(request):
    effect_id = request.GET['effect_id']
    effect_id = int(effect_id)

    stop_interval = BattleEffectController().execute_interval(effect_id)

    c = {'stop': stop_interval}

    return HttpResponse(json.dumps(c), content_type='application/json')


@is_ajax_request
def report_response(request):
    player = UserController.get_player_from_user(request.user)
    response = request.GET['response']
    now = datetime.datetime.utcnow().replace(tzinfo=utc)

    report = ReportResponse(player=player, response=response, date=now)
    report.save()


@user_passes_test(lambda u: u.is_superuser)
def create(request):
    from game_app.world import create_world

    create_world.Creator().execute()
    return render_to_response('worldCreator.html')


@user_passes_test(lambda u: u.is_superuser)
def create_map_f(request):
    from game_app.world import create_map

    create_map.CreateMap().execute()
    return render_to_response('mapCreator.html')


@user_passes_test(lambda u: u.is_superuser)
def create_backup(request):
    results = create_backup.Backup().create_backup(request.POST.getlist('choosen_tables'))
    c = {'results': results}
    c.update(csrf(request))
    return render_to_response('create.html', c)


@user_passes_test(lambda u: u.is_superuser)
def restore_backup(request):
    results = restore_backup.RestoreBackup().execute(request.POST.getlist('choosen_tables'),
                                                     request.POST.get('directory'), request.POST.get('type'))
    c = {'results': results}
    c.update(csrf(request))
    return render_to_response('create.html', c)


@user_passes_test(lambda u: u.is_superuser)
def choose_backup(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('chooseBackup.html', c)


@user_passes_test(lambda u: u.is_superuser)
def choose_backup_restore(request):
    import os

    c = {}
    dir_names = []
    for dir_name in os.listdir(os.getcwd() + '/BACKUP'):
        if dir_name.startswith("backup_"):
            dir_names.append(dir_name)
    c['dir_names'] = dir_names
    c.update(csrf(request))
    return render_to_response('chooseBackupRestore.html', c)


@is_ajax_request
@user_passes_test(lambda u: u.is_superuser)
def get_backup_tables(request):
    from game_app.world.backup.restore_backup import TableRequestParser

    process_command = TableRequestParser(request)
    table_names = process_command.process_command()

    c = {'table_names': table_names, 'status': 'success'}
    return HttpResponse(json.dumps(c), content_type='application/json')


@user_passes_test(lambda u: u.is_superuser)
def set_simulation(request):
    from game_app.command.settings.command_settings import special_attacks
    c = {}

    monsters = Monster.objects.all()
    c['monsters'] = InitSimulationController.prepare_monster_objects(monsters)

    special_attack_names = list(map(lambda x: x.name, special_attacks))
    c['special_attacks'] = special_attack_names
    c['parameter_names'] = parameter_names

    c['additional_attributes'] = True

    c.update(csrf(request))
    return render_to_response('simulation/setSimulation.html', c)


@user_passes_test(lambda u: u.is_superuser)
def run_simulation(request):
    from game_app.command.settings.command_settings import special_attacks
    c = {}

    monsters = PreSimulationController.create_monsters_from_request(request)
    c['monsters'] = InitSimulationController.prepare_monster_objects(monsters)
    special_attack_names = list(map(lambda x: x.name, special_attacks))
    c['special_attacks'] = special_attack_names
    c['parameter_names'] = parameter_names

    SimulationInfo.objects.create(monster1=monsters[0], monster2=monsters[1])



    c.update(csrf(request))
    return render_to_response('simulation/runSimulation.html', c)


@user_passes_test(lambda u: u.is_superuser)
def simulationAttack(request):
    c = {}
    c.update(csrf(request))

    monster1_pk = request.GET['monster1_pk']
    monster2_pk = request.GET['monster2_pk']
    monster1 = Monster.objects.get(pk=monster1_pk)
    monster2 = Monster.objects.get(pk=monster2_pk)
    attack_property = BattleController().get_monster_attack_from_monster(monster1)

    response = MonsterFightController().monster_monster_attack(request, attack_property)

    first_monster_is_attacking = monster1_pk < monster2_pk
    monster1 = Monster.objects.get(pk=monster1_pk)
    monster2 = Monster.objects.get(pk=monster2_pk)
    monsters = [monster1, monster2]
    PostSimulationController.save_info(monsters, first_monster_is_attacking, response.message)

    if response.status == "no_effect":
        if first_monster_is_attacking is False:
            monsters = [monster2, monster1]
        PostSimulationController.gather_conclusion(monsters)
        #PostSimulationController.remove_fake_monsters(monsters)

    c = {
        'left_div': first_monster_is_attacking,
        'status': response.status,
        'command': attack_property.name,
        'message': response.message,
        'number_of_lines': response.message.count("<br>")
    }

    return HttpResponse(json.dumps(c), content_type='application/json')


@user_passes_test(lambda u: u.is_superuser)
def set_balancing(request):
    c = {}

    monsters = Monster.objects.all()
    monster_names = map(lambda x: x.name, monsters)
    c['monsterNames'] = monster_names

    c.update(csrf(request))
    return render_to_response('simulation/setBalancing.html', c)


@user_passes_test(lambda u: u.is_superuser)
def run_balancing(request):
    c = {}
    c.update(csrf(request))
    return render_to_response('simulation/runBalancing.html', c)
