# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.battle.effect_type import ToNextHitType
from game_app.utils.websockets.user_websocket import WsHandler
from game_app.models import *


class WsUserBattleHandler(WsHandler):
    def __init__(self):
        WsHandler.__init__(self)
        self.battle_effects = {}

    def handle_time_effects(self, time_battle_effects):
        if time_battle_effects:
            for effect in time_battle_effects:
                if type(effect.defender) is Player:
                    self.say_to_users([effect.defender.get_ws_id()], 'StopEW-%s-%s' % (id(effect), effect.type.time))
                else:
                    #if interval effect involves MonsterInMap we should run script on attacker side
                    if type(effect.attacker) is Player:
                        self.say_to_users([effect.attacker.get_ws_id()],
                                          'StopEW-%s-%s' % (id(effect), effect.type.time))
                    else:
                        pass # TODO

    def start_interval_script(self, effect):
        from MUD_ADMIN.game.properties import DEFAULT_EFFECT_INTERVAL

        if type(effect.defender) is Player:
            self.say_to_users([effect.defender.get_ws_id()],
                              'StartInterval-%s-%s' % (id(effect), DEFAULT_EFFECT_INTERVAL))
        else:
            #if interval effect involves MonsterInMap we should run script on attacker side
            self.say_to_users([effect.attacker.get_ws_id()],
                              'StartInterval-%s-%s' % (id(effect), DEFAULT_EFFECT_INTERVAL))

    def get_interval_effect(self, effect_id):
        for user in self.battle_effects.keys():
            battle_effects = self.battle_effects[user]

            for effect in battle_effects:
                if id(effect) == effect_id:
                    return effect

        return None

    def get_battle_effects(self, user):
        if user in self.battle_effects.keys():
            return self.battle_effects[user]
        else:
            return []

    def set_battle_effects(self, user, battle_effects):
        from game_app.battle.effect import MultipleEffect

        for effect in battle_effects:
            if type(effect) is MultipleEffect:
                battle_effects.remove(effect)
                for single_effect in effect.list_of_effects:
                    battle_effects.append(single_effect)

        self.battle_effects[user] = battle_effects

    def creature_has_effect(self, creature, effect_type):
        battle_effects = self.get_battle_effects(creature.get_ws_id())

        if battle_effects:
            for effect in battle_effects:
                if type(effect) is effect_type:
                    return True

        return False

    def stop_effect(self, effect_id):

        for user in self.battle_effects.keys():
            for effect in self.battle_effects[user]:
                # print('$$$$')
                # print(id(effect))
                # print(effect_id)
                if id(effect) == effect_id:
                    effect.undo()
                    self.battle_effects[user].remove(effect)

    def stop_effects_to_next_hit(self, user):
        battle_effects = self.get_battle_effects(user)

        if battle_effects:
            to_next_hit_effects = list(filter(lambda x: type(x.type) is ToNextHitType, battle_effects))

            if to_next_hit_effects:
                for effect in to_next_hit_effects:
                    effect.undo()

            current_effects = [item for item in battle_effects if item not in to_next_hit_effects]
            self.set_battle_effects(user, current_effects)


    def stop_all_effects(self, creature):
        battle_effects = self.get_battle_effects(creature.get_ws_id())

        if battle_effects:
            for effect in battle_effects:
                effect.undo()

        self.set_battle_effects(creature.get_ws_id(), [])


ws_handler = WsUserBattleHandler()