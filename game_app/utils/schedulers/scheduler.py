# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from threading import Timer

from game_app.models.AssetModel import AssetInMap
from game_app.controllers import AssetInMapController
from MUD_ADMIN.game.properties import ASSET_STATES_RESET


def sync_assets():

    assets_in_map = AssetInMap.objects.all()

    for asset in assets_in_map:
        AssetInMapController().update(asset.id)

    sync_timer = Timer(ASSET_STATES_RESET, sync_assets, ())
    sync_timer.start()




#asset scheduler starting
sync_assets()