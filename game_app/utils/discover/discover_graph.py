# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")


from MUD_ADMIN.game.asset.dialog import TestGraph
from MUD_ADMIN.game.asset.graph_template import Graph
from MUD_ADMIN.game.asset.quest.quest import *
from inspect import getmembers, isclass

import sys

#######################################
# Quest object discover
#

all_qame_quests = []

for name, item in list(locals().items()):
    if type(item) == Quest:
        all_qame_quests.append(item)

def get_quest_by_name(name):
    for quest in all_qame_quests:
        if quest.name == name:
            return quest

#########################################
# Dialog graph class discover
#

npc_graph_dictionary = {}

for name, class_object in getmembers(sys.modules[TestGraph.__module__]):

    if isclass(class_object) and class_object != Graph:
        npc_graph_dictionary[class_object.name] = class_object

if __name__ == "__main__":
    print(npc_graph_dictionary)


