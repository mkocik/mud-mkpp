# -*- coding: utf-8 -*-
from __future__ import unicode_literals

#######################################
# Commands discover
#

from MUD_ADMIN.game.battle.attack_definition import *

command_properties_array = []


for a, item in list(locals().items()):
    if isinstance(item, CommandProperties):
        command_properties_array.append(item)

get_command_properties = {}

for command_properties in command_properties_array:
    get_command_properties[command_properties.name] = command_properties



if __name__ == '__main__':
    # for x in command_properties_array:
    #     print(x.shortcut)
    # print(command_properties_array)
    from game_app.command.settings.command_settings import special_attacks
    print(special_attacks)
