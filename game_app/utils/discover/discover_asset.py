# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from MUD_ADMIN.game.asset.definition.asset import LifeSource
from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractAsset
from MUD_ADMIN.game.asset.complex.npc import AbstractNPC
from game_app.utils.discover.discover_skills import *


##########################################
# getting all abstract asset classes to one list
#

abstract_asset_classes = []

for name, class_object in getmembers(sys.modules['MUD_ADMIN.game.asset.definition.asset_abstract']):

    if isclass(class_object):

        #class_object extends AbstractAsset
        if AbstractAsset in getmro(class_object):
            abstract_asset_classes.append(class_object)


##################################
# getting lists and dictionaries (name=>object) of all asset complex
#
all_asset_class_dictionary = {}

for name, class_object in getmembers(sys.modules[LifeSource.__module__]):

    if isclass(class_object) and class_object not in abstract_asset_classes:

        #class extends AbstractAsset
        if AbstractAsset in getmro(class_object) and class_object != AbstractNPC:
            all_asset_class_dictionary[class_object.type.name] = class_object

all_asset_array = sorted(all_asset_class_dictionary.keys())


if __name__ == "__main__":
    print('==========')
    print(all_asset_array)
    print(all_asset_class_dictionary)

    A = "Co mam dla Ciebie zrobić? <br>" \
        "1 - Powtórzyć wiadomość A <br>" \
        "2 - Exit"

    B = "Cześć jestem testową postacią niezależna NPC. <br>" + A

    start_points = [A, B]
    key_points = {A: A}




