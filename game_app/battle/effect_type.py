# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class EffectType(object):
    pass


class ImmediateEffectType(EffectType):
    type_name = 'Efekt natychmiastowy'


class TimeEffectType(EffectType):
    type_name = 'Efekt czasowy'

    def __init__(self, time):
        self.time = time

    def get_time(self):
        return self.time


class ToNextHitType(EffectType):
    type_name = 'Efekt do następnego uderzenia'


class PermanentEffectType(EffectType):
    type_name = 'Efekt do końca bitwy'