# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division  #forces float division
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics


class IFighter(object):
    def __init__(self, player):
        self.player = player

    def get_agility(self):
        pass

    def get_parameter(self, name):
        return self.player.get_parameter(name)

    def get_attribute(self,name):
        return self.player.get_attribute(name)

    def get_characteristics(self, name):
        for char in all_characteristics:
            if char.name == name:
                return char(self.player)
        return None

    def get_defence(self):
        from game_app.models import Player

        if type(self.player) is Player:
            return self.player.get_parameter("Pancerz")
        else:
            return 0

    @classmethod
    def get_real_damage(cls, damage, armor=0):
        injury = int(damage - armor)

        if injury < 1:
            injury = 1

        return injury


class Fighter(IFighter):
    def __init__(self, player):
        IFighter.__init__(self, player)


class NPCInterface(object):
    def __init__(self, npc):
        self.npc = npc

    def get_parameter(self, name):
        return self.npc.get_parameter(name)

