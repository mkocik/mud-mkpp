# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division  #forces float division
from game_app.command.highlighter import Highlighter

from game_app.models import Player, PlayableParameter
from game_app.models.MonsterModel import MonsterInMap
from game_app.utils.websockets.battle_websocket import ws_handler
from MUD_ADMIN.game.battle.winner_bonus import IWinnerBonus


class Effect(object):
    def __init__(self, type=None, probability=None, is_attacking=True):
        self.type = type
        self.probability = probability
        self.defender = None
        self.attacker = None
        self.is_attacking = is_attacking

    #by default both functions are useless
    def execute(self, attacker, defender, injury):
        pass

    def undo(self):
        pass

    def execute_function(self, attacker, defender, injury):
        if self.is_attacking:
            self.save_participants(attacker, defender)
            self.execute(attacker, defender, injury)
        else:
            self.save_participants(defender, attacker)
            self.execute(defender, attacker, injury)

    def get_type(self):
        return self.type

    def get_probability(self):
        return self.probability

    def get_participant(self, creature):
        if creature is None:
            return None

        try:
            return type(creature).objects.get(pk=creature.pk)
        except type(creature).DoesNotExist:
            return None

    def get_attacker(self):
        return self.get_participant(self.attacker)

    def get_defender(self):
        return self.get_participant(self.defender)

    def save_participants(self, attacker, defender):
        self.attacker = attacker
        self.defender = defender

    def get_manual(self):
        return '- %s <br>' % self.name


class MultipleEffect(Effect):
    name = 'Efekt wielokrotny'

    def __init__(self, probability, list_of_effects):
        Effect.__init__(self, None, probability)
        self.list_of_effects = list_of_effects
        self.manual = ''

    def execute(self, attacker, defender, injury):
        message = ''

        for effect in self.list_of_effects:
            effect.execute(attacker, defender, injury)
            message += '- %s <br>' % effect.name

        self.manual = message

    def undo(self):
        for effect in self.list_of_effects:
            effect.undo()

    def get_list_of_effects(self):
        return self.list_of_effects

    def get_manual(self):
        return self.manual


class Poison(Effect):
    name = 'Zatrucie'
    description = 'Owner co sekundę traci k% zdrowia.'

    def __init__(self, type, probability, k):
        Effect.__init__(self, type, probability, True)
        self.k = k

    def interval_execute(self):
        from game_app.command.execution.fight.user_fight import UserUserFightController

        defender = self.get_defender()
        attacker = self.get_attacker()

        if defender:
            injury = int((self.k / 100) * defender.get_hp())
            if injury == 0:
                injury = 1

            hp = defender.reduce_hp(injury)

            if hp <= 0:
                ws_handler.say_to_users([defender.get_ws_id()], 'Zostałeś zabity.')
                ws_handler.say_to_users([attacker.get_ws_id()], 'Pokonałeś swojego przeciwnika')
                ws_handler.say_to_users([defender.get_ws_id()], UserUserFightController().create_update_array(defender))

                ws_handler.stop_all_effects(defender)
                ws_handler.stop_all_effects(attacker)


                defender.resurrection()
                if type(attacker) is Player:
                    attacker.set_creature(-1)
                    attacker.regain_stamina()

                    triumph_message = ""

                    old_lvl = attacker.get_level()
                    if type(defender) is MonsterInMap:
                        xp = defender.monster.experience
                        defender.delete()
                        attacker.increase_experience(xp)
                    elif type(defender) is Player:
                        triumph_response = IWinnerBonus(attacker, defender).execute()
                        triumph_message = triumph_response.winner_message


                    level = attacker.get_level()

                    if old_lvl < level:
                        for i in range(0, level - old_lvl):
                            attacker.increase_lvl()
                            triumph_message = triumph_message + Highlighter.information(
                            "<br><br> Zdobyłeś nowy poziom. <br>Rozdziel uzyskane atrybuty!")

                    if triumph_message != "":
                        ws_handler.say_to_users([attacker.get_ws_id()], triumph_message)
                        ws_handler.say_to_users([attacker.get_ws_id()], UserUserFightController().create_update_array(attacker))


            else:
                ws_handler.say_to_users([attacker.get_ws_id()],
                                        'Twój przeciwnik otrzymał %s pkt. obrażeń z powodu trucizny' % injury)
                ws_handler.say_to_users([defender.get_ws_id()], 'Otrzymałeś %s pkt. obrażeń z powodu trucizny' % injury)
                ws_handler.say_to_users([defender.get_ws_id()], UserUserFightController().create_update_array(defender))

            return injury
        else:
            return None

    def execute(self, attacker, defender, injury):
        ws_handler.start_interval_script(self)

    def undo(self):
        attacker = self.get_attacker()
        defender = self.get_defender()

        if defender is not None:
            ws_handler.say_to_users([defender.get_ws_id()], 'Trucizna przestała działać na ciebie.')
        if attacker is not None:
            ws_handler.say_to_users([attacker.get_ws_id()], 'Trucizna przestała działać na twojego przeciwnika.')



class CharacteristicModification(Effect):
    name = 'Modyfikacja cechy'
    description = 'Wskazana cecha (np. bojowa) ownera zmienia się o k (lub k%).'

    def __init__(self, k, type, probability=None):
        Effect.__init__(self, type, probability, True)
        self.k = k


class Paralysis(Effect):
    name = 'Paraliż'
    description = 'Owner nie może wydawać żadnych poleceń, atakować, uciekać itp.'


class CriticalHit(Effect):
    name = 'Trafienie krytyczne'
    description = 'Owner otrzymuje dodatkowo K% obrażeń zadanych przez atak, który wywołał efekt.'

    def __init__(self, k, type, probability=None):
        Effect.__init__(self, type, probability, True)
        self.k = k

    def execute(self, attacker, defender, injury):
        new_injury = injury * self.k
        defender.reduce_hp(new_injury)

        if defender.get_hp() > 0:
            string_injury = Highlighter.bold(str(new_injury) + ' pkt.')

            message = 'Otrzymałeś %s obrażeń z powodu uderzenia krytycznego. ' % string_injury
            ws_handler.say_to_users([defender.get_ws_id()], message)

            message = 'Twój przeciwnik otrzymał %s obrażeń z powodu uderzenia krytycznego.' % string_injury
            ws_handler.say_to_users([attacker.get_ws_id()], message)


class ParameterAbsorption(Effect):
    name = 'Absorpcja parametru'
    description = 'Zabiera k (lub k%) wybranego parametru (np. zdrowia) przeciwnikowi ' + \
                  'i dodaje n% zabranej wartości do odpowiedniego parametru atakującego.'

    def __init__(self, parameter, k, percentage, n, type, probability=None, is_attacking=True):
        Effect.__init__(self, type, probability, is_attacking)
        self.parameter = PlayableParameter.objects.get(name=parameter)
        self.k = k
        self.n = n
        self.percentage = percentage
        self.initial_attacker_parameter = None
        self.initial_defender_parameter = None
        self.manual = self.name

    def get_manual(self):
        return '- %s' % self.manual

    def execute(self, attacker, defender, injury):
        from game_app.command.execution.fight.user_fight import UserUserFightController

        if type(attacker) is MonsterInMap:
            attacker = attacker.monster
        if type(defender) is MonsterInMap:
            defender = defender.monster

        self.save_participants(attacker, defender)

        self.initial_attacker_parameter = attacker.get_parameter(self.parameter.name)
        self.initial_defender_parameter = defender.get_parameter(self.parameter.name)

        if self.percentage:
            value_taken_away = self.initial_defender_parameter * (self.k / 100)
        else:
            value_taken_away = self.k

        if type(defender) is Player:
            defender.set_parameter(self.parameter.name, self.initial_defender_parameter - value_taken_away)
            ws_handler.say_to_users([defender.get_ws_id()], UserUserFightController().create_update_array(defender))
        if type(attacker) is Player:
            attacker.set_parameter(self.parameter.name, self.initial_attacker_parameter + value_taken_away)
            ws_handler.say_to_users([attacker.get_ws_id()], UserUserFightController().create_update_array(attacker))

        self.manual = self.name + ' %s' % self.parameter.name

    def undo(self):
        attacker = self.get_attacker()
        defender = self.get_defender()

        if type(attacker) is Player:
            attacker.set_parameter(self.parameter.name, self.initial_attacker_parameter)

        if type(defender) is Player:
            defender.set_parameter(self.parameter.name, self.initial_defender_parameter)


class Fatality(Effect):
    name = 'Atak kończący'
    description = 'Jeżeli ownerowi zostało poniżej k (lub k%) początkowego zdrowia, owner umiera.'

    def __init__(self, type, k, percentage, probability=None):
        Effect.__init__(self, type, probability)
        self.k = k
        self.percentage = percentage

    def execute(self, attacker, defender, injury):

        if self.percentage is False:
            lim = self.k

        else:
            if type(defender) is Player:
                lim = defender.get_max_hp() * (self.k / 100)
            else:
                lim = defender.monster.default_hp * (self.k / 100)

        if defender.get_hp() < lim:

            message = 'Miał miejsce atak kończący.'
            message = Highlighter.frame(message)

            ws_handler.say_to_users([attacker.get_ws_id()], message)
            ws_handler.say_to_users([defender.get_ws_id()], message)

            defender.set_hp(-1)

