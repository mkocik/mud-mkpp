# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.command.settings.command_types import SpecialAttackTypeCommand
from game_app.command.settings.command_definition import *
from game_app.command.execution.mode.battle import AttackCommand
from game_app.battle.effect import *
from game_app.models import StaticAttribute


class SpecialAttack(CommandProperties):
    def __init__(self, name, description,
                 cost_attributes,
                 mod_attributes=None,
                 time=DEFAULT_ATTACK_TIME,
                 effect_array=None,
                 command_class=AttackCommand, type=SpecialAttackTypeCommand, shortcut=None,
                 availability_function=None, only_one_argument=True, order=99):

        assert(len(list(filter(lambda x: x < 0, cost_attributes.values()))) == 0), "Cost can not be negative!"

        CommandProperties.__init__(self, name, description, command_class=command_class, shortcut=shortcut, type=type,
                                   availability_function=availability_function, only_one_argument=only_one_argument,
                                   order=order)

        self.effect_array = effect_array
        self.mod_attributes = mod_attributes
        self.cost_attributes = cost_attributes
        self.availability_function = availability_function
        self.time = time
        self.probability = None

    def set_probability(self, probability):
        self.probability = probability

    def get_probability(self):
        return self.probability

    def get_mod_attributes(self):
        return self.mod_attributes

    def get_description(self):
        if self.availability_function is not None:
            return '%s (%s)' % (self.description, self.availability_function)
        else:
            return self.description



    def get_time(self):
        return self.time

    def get_effect_array(self):
        return self.effect_array

    def take_cost(self, player):
        from game_app.models import PlayerAttribute

        if self.can_take_cost(player):
            for key, value in self.cost_attributes.items():
                static_attribute = StaticAttribute.objects.get(name=key)
                player_attribute = PlayerAttribute.objects.get(player=player, attribute=static_attribute)

                player_attribute.current_state -= value
                player_attribute.save()

            return True

        return None

    def can_take_cost(self, player):
        from game_app.models import PlayerAttribute

        can_take_cost = True

        for key, value in self.cost_attributes.items():
            static_attribute = StaticAttribute.objects.get(name=key)
            player_attribute = PlayerAttribute.objects.get(player=player, attribute=static_attribute)

            if player_attribute.current_state < value:
                can_take_cost = False

        return can_take_cost

    def __str__(self):
        message = 'Nazwa ataku: %s <br>' % Highlighter().bold(self.name)

        if self.shortcut:
            message += 'Skrót: %s <br>' % Highlighter().bold(self.shortcut)

        if self.mod_attributes:
            message += 'Zmiany parametrow: <br>'

            for index, mod_attribute in enumerate(self.mod_attributes, start=1):
                message += '%s. %s - %s' % (str(index),
                                            Highlighter.bold(mod_attribute.name),
                                            Highlighter.bold(str(mod_attribute.k)))

                if mod_attribute.percentage:
                    message += '%'

                message += '<br>'

        if self.cost_attributes:
            message += 'Koszt: <br>'
            index = 1

            for cost_object, value in self.cost_attributes.items():
                message += '%s. %s - %s <br>' % \
                           (Highlighter().bold(str(index)),
                            Highlighter().bold(cost_object.__str__()),
                            Highlighter().bold(str(value)))
                index += 1

        if self.effect_array is not None:
            message += 'Efekty: <br>'

            for index, effect in enumerate(self.effect_array, start=1):
                message += '%s. %s, prawdopodobieństwo: %s <br>' % \
                           (Highlighter().bold(str(index)),
                            Highlighter().bold(effect.name),
                            Highlighter().bold(str(effect.probability)))

        if self.availability_function:
            message += self.availability_function.__str__()

        return message


class AttributeBattleModification(object):
    def __init__(self, creature, mod_attributes):
        self.initial_creature = creature
        self.mod_attributes = mod_attributes
        self.initial_parameters = []

    def execute(self):
        creature = self.initial_creature
        if self.mod_attributes:
            for mod in self.mod_attributes:
                self.initial_parameters.append(creature.get_parameter(mod.name))
                creature.set_parameter(mod.name, mod.get_value(creature))

    def undo(self):
        creature = (type(self.initial_creature)).objects.get(pk=self.initial_creature.pk)

        if self.mod_attributes:
            for index, mod in enumerate(self.mod_attributes):
                creature.set_parameter(mod.name, self.initial_parameters[index])


class ParameterModification(object):
    def __init__(self, name, k, percentage):
        self.name = name
        self.k = k
        self.percentage = percentage

    def get_percentage(self):
        return self.percentage

    def get_k(self):
        return self.k

    def get_name(self):
        return self.name

    def get_value(self, player):
        parameter_value = player.get_parameter(self.name)

        if parameter_value is not None:
            if self.percentage is True:
                return parameter_value * (self.k / 100)
            else:
                return parameter_value + self.k
        else:
            return None


class AttackAvailabilityFunction(object):
    def __init__(self, function, description):
        self.function = function
        self.description = description

    def get_function(self):
        return self.function

    def __str__(self):
        return self.description

