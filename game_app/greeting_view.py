from django.shortcuts import render_to_response
from MUD.settings import my_logger
from MUD_ADMIN.game import properties
from MUD_ADMIN.game.asset.definition.asset_type import Coffer, Person
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
from game_app.controllers.basic_controllers import UserController, AssetController
from game_app.models import PlayerParameter
from language.pl_pl_parameters import *


def redirection_with_greeting_message(request):
    if request.user.is_superuser:
        c = {'superuser': request.user.is_superuser}

    else:
        from game_app.command.location import MoveController, LocationController
        from game_app.command.settings.command_settings import get_special_attack_time

        # if player game was interrupted program clears all activities:
        # for example battles and talks with NPC
        player = UserController.get_player_from_user(request.user)

        if player.creature > -1:
            from game_app.utils.websockets.battle_websocket import ws_handler
            ws_handler.stop_all_effects(player)

            if player.creature > 0:
                opponent = player.get_opponent()
                opponent.set_creature(-1)

            elif player.creature == 0:
                monster_in_map = player.location.get_monster_in_map()

                if monster_in_map is not None:
                    monster_in_map.user = None
                    monster_in_map.save()

            player.resurrection()

        if player.asset_in_map:
            player.asset_in_map = None
            player.save()

            specific_assets = player.location.get_assets_in_map()

            if specific_assets:

                for specific_asset in specific_assets:
                    asset_object = AssetController.get_asset_object(specific_asset)

                    if type(asset_object.type) in [Coffer, Person]:
                        asset_object.exit_graph(player)

        parameter_list = PlayerParameter.objects.filter(player=player)
        item_param_list = player.get_item_param_list()
        location = player.location

        attributes_list = player.get_all_attributes_with_mods()

        greeting_message = "Witaj %s." % request.user.username
        moving_controller = MoveController()
        location_message = moving_controller.get_full_description_safe_mode(location, request.user)

        map_fields = LocationController.get_map_fields(location)

        c = {'nick': request.user.username,
             'paramlist': parameter_list,
             'itemParamList': item_param_list,
             'attrList': attributes_list,
             'greetingMessage': greeting_message,
             'locationMessage': location_message,
             'timeout': properties.COMMAND_TIMEOUT,

             'experience': player.experience,
             'level': player.get_level(),
             'mapFields': map_fields,
             'gold': player.gold,
             'gold_name': GOLD,
             'armor': ((list(filter(lambda x: x.name == ARMOR, all_characteristics)))[0])(player),
             'get_special_attack_time': get_special_attack_time
        }
        my_logger.error(get_special_attack_time)

    return render_to_response('game.html', c)