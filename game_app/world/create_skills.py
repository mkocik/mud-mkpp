# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models import Skill
from game_app.world.create_world import ICreator
from game_app.utils.discover.discover_skills import skills_description_dictionary

class CreateSkills(ICreator):
    def execute(self):
        Skill.objects.all().delete()

        for name in skills_description_dictionary.keys():
            skill = Skill(name=name,description=skills_description_dictionary.get(name))
            skill.save()

        return 0
