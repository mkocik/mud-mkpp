# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import abc
import codecs
import sys

from game_app.models.ItemModel import *
from game_app.models.MapModel import *
from game_app.models.MonsterModel import *
from game_app.models.PlayerModel import *
from language.pl_pl_parameters import *


class ICreator:
    def create_world(self, file_name):
        f = self.open_csv_file('MUD_ADMIN/creator/world_creator/', file_name)

        rows = self.read_csv_file(f)
        self.parse_rows(rows, file_name, self.create_element_function)
        return 0

    def create_asset(self, file_name):
        f = self.open_csv_file('MUD_ADMIN/creator/asset_creator/', file_name)

        rows = self.read_csv_file(f)
        self.parse_rows(rows, file_name, self.create_element_function)
        return 0

    def create_element_function(self, words, file_name=None):
        pass

    @abc.abstractmethod
    def execute(self):
        pass

    def open_csv_file(self, path_to_directory, file_name):
        try:
            f = codecs.open(path_to_directory + file_name, 'r', encoding='utf-8')
            print("Otworzono " + file_name)
            return f
        except IOError:
            print("Otwieranie " + file_name + " zakończone niepowodzeniem!")
            sys.exit(-1)

    def read_csv_file(self, f):
        rows = f.read().split("\n")
        del rows[0]
        return rows

    def parse_rows(self, rows, file_name, create_element_function):
        for row in rows:
            if row != "":
                words = row.split(";")
                words[-1] = words[-1].rstrip()

                create_element_function(words, file_name)
            else:
                print("Omienięto pusty wiersz w " + file_name)
        print("Skończono zapis " + file_name)


class Creator(object):
    def execute(self):
        self.fillHiddenTablesModel()

        self.deleteUsers()
        #CreateLandTypes().execute()
        from game_app.world.create_map import CreateMap
        CreateMap().execute()  #delete in lower line

        #Map.objects.all().delete()
        CreatePlayableParameter().execute()
        CreateEquipmentSlot().execute()
        CreateItemType().execute()
        CreateItem().execute()
        CreateItemLandType().execute()
        CreateItemParameter().execute()
        CreateItemInMap().execute()
        CreateAttack().execute()
        CreateMonster().execute()
        CreateMonsterParameters().execute()
        CreateMonsterLandTypes().execute()
        CreateMonsterEquipment().execute()
        CreateMonsterLoot().execute()
        #CreateSpellbook().execute()
        #CreateMonsterSpellbook().execute()
        CreateMonsterInMap().execute()
        #CreateSpellParameters().execute()
        CreateMapPassage().execute()

        from game_app.world.create_asset import CreateAssets

        CreateAssets().execute()
        CreateChestLoot().execute()

        from game_app.world.create_skills import CreateSkills

        skill_creator = CreateSkills()
        skill_creator.execute()

        from game_app.world.create_characteristics import CreateCharacteristics

        chars_creator = CreateCharacteristics()
        chars_creator.execute()

        return True

    def fillHiddenTablesModel(self):
        StaticParameter.objects.all().delete()
        armor = StaticParameter(name=ARMOR)
        armor.save()

        try:
            StaticAttribute.objects.get(name=HP_NAME)
        except StaticAttribute.DoesNotExist:
            att = StaticAttribute(name=HP_NAME,default_val=properties.STARTING_HP,function_name="hp_mod_function")
            att.save()

        try:
            StaticAttribute.objects.get(name=STAMINA)
        except StaticAttribute.DoesNotExist:
            att = StaticAttribute(name=STAMINA,default_val=properties.STARTING_STAMINA,function_name="stamina_mod_function")
            att.save()

    def deleteUsers(self):
        Player.objects.all().delete()
        PlayerParameter.objects.all().delete()
        #PlayerSpellbook.objects.all().delete()
        #save superuser
        root = User.objects.get(pk=1)
        User.objects.all().delete()
        root.save()


class CreateLandTypes(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 5:
            landType = LandType(land_name=words[0], monster_probability=words[1], item_probability=words[2],
                                 default_description=words[3], number=words[4])
            landType.save()

    def execute(self):

        while LandType.objects.count():
            LandType.objects.all()[0].delete()

        file_name = "LandType.csv"
        ICreator.create_world(self, file_name)

        return 0



# class CreateMap(ICreator):
#     def create_element_function(self, words, file_name=None):
#         if len(words) == 5:
#             try:
#                 land_type = LandType.objects.get(pk=words[2])
#             except LandType.DoesNotExist:
#                 print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
#                 return -2
#
#             landType = Map(x=words[0], y=words[1], land_type=land_type, description=words[3], level=words[4])
#             landType.save()
#
#     def execute(self):
#
#         Map.objects.all().delete()
#
#         file_name = "Map.csv"
#         ICreator.create_world(self, file_name)
#
#         return 0


class CreateMapPassage(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                from_map = Map.objects.get(pk=words[1])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2
            try:
                to_map = Map.objects.get(pk=words[2])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            passage = MapPassage(name=words[0], from_map=from_map, to_map=to_map)
            passage.save()

    def execute(self):

        MapPassage.objects.all().delete()

        file_name = "MapPassage.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreatePlayableParameter(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            param = PlayableParameter(name=words[0], default_val=words[1])
            param.save()

    def execute(self):
        PlayableParameter.objects.all().delete()

        file_name = "PlayableParameter.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateEquipmentSlot(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            eq_slot = EquipmentSlot(name=words[0], multiplicity=words[1])
            eq_slot.save()
        else:
            print("Omienięto niepełny wiersz w " + file_name)

    def execute(self):

        EquipmentSlot.objects.all().delete()

        file_name = "EquipmentSlot.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateItemType(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:

            try:
                eq_slot = EquipmentSlot.objects.get(pk=words[1])
            except EquipmentSlot.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany EquipmentSlot nie istnieje")
                return -2

            item = ItemType(name=words[0], equipment_slot=eq_slot)
            item.save()

        elif len(words) == 1:

            item = ItemType(name=words[0])
            item.save()

        else:
            print("Omienięto niepełny wiersz w ItemType.csv")

    def execute(self):

        ItemType.objects.all().delete()

        file_name = "ItemType.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateItem(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 4:
            try:
                item_t = ItemType.objects.get(pk=words[0])
            except ItemType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ przedmiotu nie istnieje")
                return -2

            item = Item(item_type=item_t, name=words[1], description=words[2], value=words[3])
            item.save()
        else:
            print("Omienięto pusty wiersz w " + file_name)

    def execute(self):

        Item.objects.all().delete()

        file_name = "Item.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateItemLandType(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                item = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            try:
                land_type = LandType.objects.get(pk=words[1])
            except LandType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                return -2

            item_land_type = ItemLandType(item=item, land_type=land_type, probability=words[2])
            item_land_type.save()

    def execute(self):

        file_name = "ItemLandType.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateItemParameter(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:

            try:
                item_t = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            try:
                param = PlayableParameter.objects.get(pk=words[1])
            except PlayableParameter.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
                return -2

            parameter = ItemParameter(item=item_t, parameter=param, state=words[2])
            parameter.save()

        elif len(words) == 2:
            #if parameter==None then it is armor
            try:
                item_t = Item.objects.get(pk=words[0])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            parameter = ItemParameter(item=item_t, state=words[1])
            parameter.save()

    def execute(self):

        ItemParameter.objects.all().delete()

        file_name = "ItemParameter.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateItemInMap(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                map = Map.objects.get(pk=words[0])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            try:
                item = Item.objects.get(pk=words[1])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            item_in_map = ItemInMap(map=map, item=item)
            item_in_map.save()

    def execute(self):

        ItemInMap.objects.all().delete()

        file_name = "ItemInMap.csv"
        ICreator.create_world(self, file_name)

        return 0

class CreateAttack(ICreator):
    def create_element_function(self, words, file_name=None):
        pass

    def execute(self):
        from game_app.command.settings.command_settings import special_attack_names
        Attack.objects.all().delete()

        for attack_name in special_attack_names:
            Attack(name=attack_name).save()

        return 0

class CreateMonster(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) in [9, 10]:

            monster = Monster(name=words[0], description=words[1], gold_min=words[2], gold_max=words[3],
                               experience=words[4], default_hp=words[5], aggression=words[6],
                               max_loot_amount=words[7], item_probability=words[8])

            monster.save()

            parameters = PlayableParameter.objects.all()
            for parameter in parameters:
                param_monster = MonsterParameter(monster=monster, parameter=parameter, state=parameter.default_val)
                param_monster.save()

            if len(words) == 10:
                attacks = words[9].split(',')

                for attack_name in attacks:
                    MonsterAttack(monster=monster, attack=Attack.objects.get(name=attack_name)).save()

    def execute(self):
        MonsterEquipment.objects.all().delete()
        MonsterParameter.objects.all().delete()
        MonsterInMap.objects.all().delete()
        MonsterSpellbook.objects.all().delete()
        Monster.objects.all().delete()
        MonsterLandType.objects.all().delete()
        MonsterLoot.objects.all().delete()

        file_name = "Monster.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterParameters(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                parameter = PlayableParameter.objects.get(pk=words[1])
            except PlayableParameter.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
                return -2

            monster_params = MonsterParameter.objects.get(monster=monster, parameter=parameter)
            monster_params.state = words[2]
            monster_params.save()

    def execute(self):

        file_name = "MonsterParameter.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterLandTypes(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                land_type = LandType.objects.get(pk=words[1])
            except LandType.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                return -2

            monster_land_type = MonsterLandType(monster=monster, land_type=land_type, probability=words[2])
            monster_land_type.save()

    def execute(self):

        file_name = "MonsterLandType.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterEquipment(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                item = Item.objects.get(pk=words[1])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            monster_eq = MonsterEquipment(monster=monster, item=item)
            monster_eq.save()

    def execute(self):

        file_name = "MonsterEquipment.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterLoot(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 4:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                item = Item.objects.get(pk=words[1])
            except PlayableParameter.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany przedmiot nie istnieje")
                return -2

            monster_loot = MonsterLoot(monster=monster, item=item, amount=words[2], probability=words[3])
            monster_loot.save()

    def execute(self):

        file_name = "MonsterLoot.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateSpellbook(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            if words[2] == 'True':
                spellbook = Spellbook(name=words[0], description=words[1], is_destructive=True)
            else:
                spellbook = Spellbook(name=words[0], description=words[1], is_destructive=False)
            spellbook.save()

    def execute(self):
        SpellParameter.objects.all().delete()
        Spellbook.objects.all().delete()

        file_name = "Spellbook.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterSpellbook(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                spell = Spellbook.objects.get(pk=words[1])
            except Spellbook.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane zaklecie nie istnieje")
                return -2

            monster_spellbook = MonsterSpellbook(monster=monster, spell=spell)
            monster_spellbook.save()

    def execute(self):

        file_name = "MonsterSpellbook.csv"
        ICreator.create_world(self, file_name)

        return 0


class CreateMonsterInMap(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            try:
                monster = Monster.objects.get(pk=words[0])
            except Monster.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany potwor nie istnieje")
                return -2

            try:
                map = Map.objects.get(pk=words[1])
            except Map.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane pole nie istnieje")
                return -2

            if map != Map.objects.get(x=properties.START_LOCATION_X, y=properties.START_LOCATION_Y,
                                      level=MapLevel.objects.get(level=properties.START_LOCATION_LVL)):
                monster_in_map = MonsterInMap(map=map, monster=monster)
                monster_in_map.save()

    def execute(self):

        file_name = "MonsterInMap.csv"
        ICreator.create_world(self, file_name)

        return 0


# class CreateSpellParameters(ICreator):
#     def create_element_function(self, words, file_name=None):
#         if len(words) == 3:
#             try:
#                 spell = Spellbook.objects.get(pk=words[0])
#             except Spellbook.DoesNotExist:
#                 print("Ominięto zapis w " + file_name + " -> podane zaklecie nie istnieje")
#                 return -2
#
#             try:
#                 parameter = PlayableParameter.objects.get(pk=words[1])
#             except PlayableParameter.DoesNotExist:
#                 print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
#                 return -2
#
#             spell_params = SpellParameter(spell=spell, parameter=parameter, state=words[2])
#             spell_params.save()
#
#     def execute(self):
#
#         file_name = "SpellParameter.csv"
#         ICreator.create_world(self, file_name)
#
#         return 0


class CreateChestLoot(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 3:
            try:
                chest = Chest.objects.get(pk=words[0])
            except Chest.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podane chest")
                return -2

            try:
                item = Item.objects.get(pk=words[1])
            except Item.DoesNotExist:
                print("Ominięto zapis w " + file_name + " -> podany parametr nie istnieje")
                return -2

            chest_loot = ChestLoot(chest=chest, item=item, probability=words[2])
            chest_loot.save()

    def execute(self):

        file_name = "ChestLoot.csv"
        ICreator.create_world(self, file_name)

        return 0
