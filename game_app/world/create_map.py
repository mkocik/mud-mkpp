# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import codecs
import os

from game_app.models import Map, LandType, MapLevel
from game_app.world.create_world import ICreator


class CreateMap(ICreator):
    def execute(self):

        map_creator_path = "MUD_ADMIN/creator/map_creator"

        CreateLandTypes().execute()

        while Map.objects.count():
            Map.objects.all()[0].delete()

        MapLevel.objects.all().delete()

        for file_name in os.listdir(os.getcwd() + '/' + map_creator_path):
            #if 2==2:
            if file_name.endswith(".csv"):
                try:
                    print(file_name)

                    file_name_without_extension = file_name.split(".")[0]
                    level = file_name_without_extension.split("_")[-1]

                    level = MapLevel(level=level, name=level)
                    level.save()


                    f = codecs.open(map_creator_path + '/' + file_name, 'r', encoding='utf-8')
                    print("Otworzono " + map_creator_path + file_name)

                    rows = f.read().split("\n")
                    height = 0

                    for height in range(0, len(rows)):
                        row = rows[height]

                        if row != "":

                            words = row.split(";")
                            words[-1] = words[-1].rstrip()

                            for width in range(0, len(words)):

                                try:
                                    if words[width] != "" and words[width] != "-1":
                                        land_type = LandType.objects.get(number=words[width])
                                        map = Map(x=width, y=height, land_type=land_type,
                                                  description=land_type.default_description, level=level)
                                        map.save()

                                except LandType.DoesNotExist:
                                    print("Ominięto zapis w " + file_name + " -> podany typ terenu nie istnieje")
                        else:
                            print("Omienięto niepełny wiersz w " + file_name)

                except IOError:
                    print('error')
                    pass


class CreateLandTypes(ICreator):
    def create_element_function(self, words, file_name=None):
        if len(words) == 5:
            landType = LandType(land_name=words[0], monster_probability=words[1], item_probability=words[2],
                                 default_description=words[3], number=words[4])
            landType.save()

    def execute(self):

        while LandType.objects.count():
            LandType.objects.all()[0].delete()

        file_name = "LandType.csv"
        ICreator.create_world(self, file_name)

        return 0

