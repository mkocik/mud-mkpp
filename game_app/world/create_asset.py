# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractStone

from MUD_ADMIN.game.asset.complex.chest import AbstractChest

from MUD_ADMIN.game.asset.complex.npc import AbstractNPC

from game_app.models import Stone, Asset, Riddle, Chest, NormalAsset, AssetInMap, DialogGraph, Map, MapLevel, \
    QuestObject
from game_app.world.create_world import ICreator


class CreateAssets(ICreator):
    def execute(self):
        Asset.objects.all().delete()
        NormalAsset.objects.all().delete()

        #in future file csv should be created
        normal_asset = NormalAsset(name='Źródło życia')
        normal_asset.save()

        CreateStone().execute()
        CreateRiddle().execute()
        CreateChest().execute()
        CreateNPC().execute()
        CreateAssetInMap().execute()
        CreateQuest().execute()
        CreateDialogGraph().execute()
        CreateNPCAttributes().execute()


class CreateDialogGraph(object):
    def execute(self):
        from game_app.utils.discover.discover_graph import npc_graph_dictionary

        for name in npc_graph_dictionary:
            if DialogGraph.objects.filter(name=name).count() == 0:
                graph = DialogGraph(name=name)
                graph.save()

        return 0

class CreateNPC(CreateAssets):
    def create_element_function(self, words, file_name=None):
        from game_app.models import NPC

        if len(words) == 2:
            name = words[0]
            graph_name = words[1]

            if DialogGraph.objects.filter(name=graph_name).count() >0:
                dialog_graph = DialogGraph.objects.get(name=graph_name)
            else:
                dialog_graph = DialogGraph(name=graph_name)
                dialog_graph.save()

            npc = NPC(name=name, graph=dialog_graph, type_name=AbstractNPC.type.name)
            npc.save()

    def execute(self):
        from game_app.models import NPC

        DialogGraph.objects.all().delete()
        NPC.objects.all().delete()

        file_name = 'NPC.csv'

        ICreator.create_asset(self, file_name)
        return 0


class CreateStone(CreateAssets):
    def create_element_function(self, words, file_name=None):
        if len(words) == 2:
            name = words[0]
            message = words[1]

            stone = Stone(name=name, message=message, type_name=AbstractStone.type.name)
            stone.save()

    def execute(self):
        Stone.objects.all().delete()

        file_name = 'Stone.csv'
        ICreator.create_asset(self, file_name)

        return 0


class CreateChest(CreateAssets):
    def create_element_function(self, words, file_name=None):
        if len(words) == 4:
            name = words[0]
            resistance = words[1]

            if resistance == '':
                resistance = 0
            else:
                resistance = int(words[1])

            graph = words[2]
            experience = words[3]

            if experience == '':
                experience = 0
            else:
                experience = int(words[3])

            chest = Chest(name=name, resistance=resistance, graph=graph, experience=experience,
                          type_name=AbstractChest.type.name)
            chest.save()
        else:
            print("Ominięto niepełny wiersz w " + file_name)

    def execute(self):
        Chest.objects.all().delete()

        file_name = 'Chest.csv'
        ICreator.create_asset(self, file_name)

        return 0


class CreateRiddle(CreateAssets):
    def create_element_function(self, words, file_name=None):

        if len(words) in [3, 4]:
            name = words[0]
            question = words[1]
            answer = words[2]

            if len(words) == 4:
                justification = words[3]
            else:
                justification = None

            riddle = Riddle(name=name, question=question, answer=answer, justification=justification)
            riddle.save()

    def execute(self):
        Riddle.objects.all().delete()

        file_name = 'Riddle.csv'
        ICreator.create_asset(self, file_name)

        return 0


class CreateAssetInMap(CreateAssets):
    def create_element_function(self, words, file_name=None):
        if len(words) == 4:
            asset = Asset.objects.get(name=words[0])
            x = words[1]
            y = words[2]
            level = words[3]

            map = Map.objects.get(x=x, y=y, level=MapLevel.objects.get(level=level))
            asset_in_map = AssetInMap(asset=asset, map=map)
            asset_in_map.save()

    def execute(self):
        AssetInMap.objects.all().delete()

        file_name = 'AssetInMap.csv'
        ICreator.create_asset(self, file_name)

        return 0


class CreateQuest(object):
    def execute(self):
        QuestObject.objects.all().delete()

        from game_app.utils.discover.discover_graph import all_qame_quests

        for quest in all_qame_quests:
            quest = QuestObject(name=quest.name)
            quest.save()

        return 0


class CreateNPCAttributes(ICreator):
    def create_element_function(self, words, file_name=None):
        from game_app.models import AllParameter, NPC, NPCAttributes
        print(len(words))
        if len(words) == 3:
            attr = AllParameter.objects.get(name=words[0])
            npc = NPC.objects.get(name=words[1])
            value = int(words[2])
            npc_attr = NPCAttributes(attribute=attr, npc=npc, state=value)
            npc_attr.save()

    def execute(self):
        from game_app.models import NPCAttributes
        while NPCAttributes.objects.count():
            NPCAttributes.objects.all()[0].delete()

        file_name = "NPCAttributes.csv"
        ICreator.create_asset(self, file_name)

        return 0