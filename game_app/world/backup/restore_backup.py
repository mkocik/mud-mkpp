import os
import codecs

from game_app.models import *
from game_app.world.create_world import Creator


class TableRequestParser:
    def __init__(self, request):
        self.directory = request.GET['directory']

    def process_command(self):
        table_names = []
        for table_name in os.listdir(os.getcwd() + '/BACKUP/' + self.directory):
            if table_name.endswith("_Backup.csv"):
                table_names.append(table_name.split(".")[0].split("_")[0])
        return table_names


class RestoreBackup:
    def execute(self, tables, directory, mode):
        self.results = []
        tables_sorted = self.sort_tables(tables)

        Creator().fillHiddenTablesModel()

        for table in tables_sorted:
            try:
                constructor = globals()[table+"Restore"]
                instance = constructor()
                res = instance.restore(directory, mode)
                if res:
                    self.results.extend(res)
            except:
                self.results.append("Exception occurred while trying to create class " + table+"Restore")

        if len(self.results) == 0:
            self.results.append("Succesfully restored BACKUP")

        return self.results

    def sort_tables(self,tables):
        sorted = []
        if 'PlayableParameter' in tables: sorted.append('PlayableParameter')
        if 'Monster' in tables: sorted.append('Monster')
        if 'EquipmentSlot' in tables: sorted.append('EquipmentSlot')
        if 'ItemType' in tables: sorted.append('ItemType')
        if 'Item' in tables: sorted.append('Item')
        if 'LandType' in tables: sorted.append('LandType')
        if 'MapLevel' in tables: sorted.append('MapLevel')
        if 'Map' in tables: sorted.append('Map')
        if 'MonsterLoot' in tables: sorted.append('MonsterLoot')
        if 'MonsterParameter' in tables: sorted.append('MonsterParameter')
        if 'MonsterLandType' in tables: sorted.append('MonsterLandType')
        if 'ItemLandType' in tables: sorted.append('ItemLandType')
        if 'ItemParameter' in tables: sorted.append('ItemParameter')
        if 'Player' in tables: sorted.append('Player')
        if 'ReportTime' in tables: sorted.append('ReportTime')
        if 'ReportResponse' in tables: sorted.append('ReportResponse')
        if 'PlayerParameter' in tables: sorted.append('PlayerParameter')
        if 'PlayerEquipment' in tables: sorted.append('PlayerEquipment')
        if 'PlayerEquipmentSlot' in tables: sorted.append('PlayerEquipmentSlot')
        if 'NormalAsset' in tables: sorted.append('NormalAsset')
        if 'Riddle' in tables: sorted.append('Riddle')
        if 'Stone' in tables: sorted.append('Stone')
        if 'Chest' in tables: sorted.append('Chest')
        if 'DialogGraph' in tables: sorted.append('DialogGraph')
        if 'QuestObject' in tables: sorted.append('QuestObject')
        if 'ChestLoot' in tables: sorted.append('ChestLoot')
        if 'NPC' in tables: sorted.append('NPC')
        if 'MapPassage' in tables: sorted.append('MapPassage')
        if 'AssetInMap' in tables: sorted.append('AssetInMap')
        if 'DialogState' in tables: sorted.append('DialogState')
        if 'Quest' in tables: sorted.append('Quest')
        return sorted




class MonsterRestore:
    def restore(self,directory,mode):
        file = directory + "/Monster_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MonsterRestore!")
        return self.results

    def restore_by_deletion(self):
        Monster.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 10: #last item is always empty
                try:
                    monster = Monster(name=data[0],description=data[1],default_hp=data[2],gold_min=data[3],gold_max=data[4],
                                  experience=data[5],aggression=data[6],max_loot_amount=data[7],item_probability=data[8])
                    monster.save()
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterRestore")
            else:
                self.results.append("Row " + row + " has been ommited in MonsterRestore  - wrong array length")

    def restore_by_modification(self):
        current_monsters = [x.name for x in Monster.objects.all()]  #names

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if data[0] in current_monsters:
                try:
                    monster = Monster.objects.get(name=data[0])
                    monster.description = data[1]
                    monster.default_hp = data[2]
                    monster.gold_min = data[3]
                    monster.gold_max = data[4]
                    monster.experience = data[5]
                    monster.aggression = data[6]
                    monster.max_loot_amount = data[7]
                    monster.item_probability = data[8]
                    monster.save()
                except:
                    self.results.append("Changes in row: " + row + " has not been made in MonsterRestore")

            else:
                try:
                    monster = Monster(name=data[0],description=data[1],default_hp=data[2],gold_min=data[3],gold_max=data[4],
                                      experience=data[5],aggression=data[6],max_loot_amount=data[7],item_probability=data[8])
                    monster.save()
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterRestore")


class PlayerRestore:
    def restore(self,directory,mode):
        file = directory + "/Player_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in PlayerRestore!")
        return self.results

    def restore_by_deletion(self):
        Player.objects.all().delete()
        User.objects.filter(is_superuser=False).delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 9: #last item is always empty
                try:
                    try:
                        lvl = MapLevel.objects.get(name=data[1].split("_")[2])
                        map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                        user = User(username=data[0],password=data[6])
                        user.save()
                        player = Player(user=user,location=map,gold=data[2],experience=data[3],
                                  distribution_points=data[4],color=data[5])
                        player.save()
                    except:
                        try:
                            lvl = MapLevel.objects.get(level=properties.START_LOCATION_LV)
                            map=Map.objects.get(x=properties.START_LOCATION_X,y=properties.START_LOCATION_Y,level=lvl)
                            self.results.append("Player " + user.username + " can not be placed on his actual map location because this place does not exist. "
                                                                            "He has been placed on the new Starting Position from properties file.")
                            user = User(username=data[0],password=data[7])
                            user.save()
                            player = Player(user=user,location=map,gold=data[2],experience=data[3],
                                  distribution_points=data[4],skills_points=data[5],color=data[6])
                            player.save()
                        except MapLevel.DoesNotExist:
                            self.results.append("Exception in PlayerRestore! MapLevel.DoesNotExist for player " + user.username)
                        except Map.DoesNotExist:
                            self.results.append("Player " + user.username + " can not be placed on his actual map location or on Starting Position. "
                                                                            "This points do not exist")
                        except User.DoesNotExist:
                            self.results.append("Exception in PlayerRestore! User.DoesNotExist for player " + user.username)
                        except:
                            self.results.append("Exception in PlayerRestore! Unknown exception for player " + user.username)
                except:
                    self.results.append("Row " + row + " has been ommited in PlayerRestore")
            else:
                self.results.append("Row " + row + " has been ommited in PlayerRestore  - wrong array length")

    def restore_by_modification(self):
        current_players = [x.user.username for x in Player.objects.all()]  #names

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if data[0] in current_players:
                try:
                    user = User.objects.get(username=data[0],password=data[7])
                    player = Player.objects.get(user=user)
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                    player.location = map
                    player.gold = data[2]
                    player.experience = data[3]
                    player.distribution_points = data[4]
                    player.skills_points = data[5]
                    player.color = data[6]
                    player.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! MapLevel.DoesNotExist for player " + user.username)
                except Map.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! Map.DoesNotExist for player " + user.username)
                except Player.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! Player.DoesNotExist for player " + user.username)
                except User.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! User.DoesNotExist for player " + user.username)
                except:
                    self.results.append("Exception in PlayerRestore! Unknown exception for player " + user.username)

            else:
                try:
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                    user = User(username=data[0],password=data[7])
                    user.save()
                    player = Player(user=user,location=map,gold=data[2],experience=data[3],
                              distribution_points=data[4],skills_points=data[5],color=data[6])
                    player.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! MapLevel.DoesNotExist for player " + user.username)
                except Map.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! Map.DoesNotExist for player " + user.username)
                except Player.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! Player.DoesNotExist for player " + user.username)
                except User.DoesNotExist:
                    self.results.append("Exception in PlayerRestore! User.DoesNotExist for player " + user.username)
                except:
                    self.results.append("Exception in PlayerRestore! Unknown exception for player " + user.username)

class PlayableParameterRestore:
    def restore(self,directory,mode):
        file = directory + "/PlayableParameter_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in PlayableParameterRestore!")
        return self.results

    def restore_by_deletion(self):
        PlayableParameter.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 3: #last item is always empty
                try:
                    param = PlayableParameter(name=data[0],default_val=data[1])
                    param.save()
                except:
                    self.results.append("Row " + row + " has been ommited in PlayableParameterRestore")
            else:
                self.results.append("Row " + row + " has been ommited in PlayableParameterRestore - wrong array length")

    def restore_by_modification(self):
        current_params = [x.name for x in PlayableParameter.objects.all()]  #names

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if data[0] in current_params:
                try:
                    param = PlayableParameter.objects.get(name=data[0])
                    param.default_val = data[1]
                    param.save()
                except:
                    self.results.append("Changes in row: " + row + " has not been made in PlayableParameterRestore")

            else:
                try:
                    param = PlayableParameter(name=data[0],default_val=data[1])
                    param.save()
                except:
                    self.results.append("Row " + row + " has been ommited in PlayableParameterRestore")


class EquipmentRestore:
    def restore(self,directory,mode):
        file = directory + "/Equipment_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in EquipmentRestore!")
        return self.results

    def restore_by_deletion(self):
        PlayerEquipment.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 3: #last item is always empty
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)
                    item = Item.objects.get(name=data[1])

                    eq = PlayerEquipment(player=player,item=item)
                    eq.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Player.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in EquipmentRestore - wrong array length")

    def restore_by_modification(self):

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                user = User.objects.get(username=data[0])
                player = Player.objects.get(user=user)
                item = Item.objects.get(name=data[1])

                eq = PlayerEquipment.objects.get(item=item,player=player)
            except: #there are no this item for this player in equipment
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)
                    item = Item.objects.get(name=data[1])

                    eq = PlayerEquipment(player=player,item=item)
                    eq.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Player.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in EquipmentRestore. Unknown Exception occurred.")



class PlayerEquipmentSlotRestore:
    def restore(self,directory,mode):
        file = directory + "/PlayerEquipmentSlot_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in PlayerEquipmentSlotRestore!")
        return self.results

    def restore_by_deletion(self):
        PlayerEquipmentSlot.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 3: #last item is always empty
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)
                    item = Item.objects.get(name=data[1])

                    eq = PlayerEquipmentSlot(player=player,item=item)
                    eq.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Player.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore - wrong array length")

    def restore_by_modification(self):

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                user = User.objects.get(username=data[0])
                player = Player.objects.get(user=user)
                item = Item.objects.get(name=data[1])

                eq = PlayerEquipmentSlot.objects.get(item=item,player=player)
            except: #there are no this item for this player in equipment
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)
                    item = Item.objects.get(name=data[1])

                    eq = PlayerEquipmentSlot(player=player,item=item)
                    eq.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Player.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in PlayerEquipmentSlotRestore. Unknown Exception occurred.")


class ReportTimeRestore:
    def restore(self,directory,mode):
        file = directory + "/ReportTime_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ReportTimeRestore!")
        return self.results

    def restore_by_deletion(self):
        ReportTime.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 4: #last item is always empty
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    time = ReportTime(player=player,start_date=data[1],end_date=data[2])
                    time.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ReportTimeRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                user = User.objects.get(username=data[0])
                player = Player.objects.get(user=user)

                time = ReportTime.objects.get(player=player,start_date=data[1],end_date=data[2])
            except: #there are no this item for this player in equipment
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    time = ReportTime(player=player,start_date=data[1],end_date=data[2])
                    time.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportTimeRestore. Unknown Exception occurred.")


class ReportResponseRestore:
    def restore(self,directory,mode):
        file = directory + "/ReportResponse_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ReportResponseRestore!")
        return self.results

    def restore_by_deletion(self):
        ReportResponse.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 4: #last item is always empty
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    res = ReportResponse(player=player,response=data[1],date=data[2])
                    res.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ReportResponseRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                user = User.objects.get(username=data[0])
                player = Player.objects.get(user=user)

                res = ReportResponse.objects.get(player=player,response=data[1],date=data[2])
            except: #there are no this item for this player in equipment
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    res = ReportResponse(player=player,response=data[1],date=data[2])
                    res.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Unknown Exception occurred.")


class PlayerParameterRestore:
    def restore(self,directory,mode):
        file = directory + "/PlayerParameter_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in PlayerParameterRestore!")
        return self.results

    def restore_by_deletion(self):
        PlayerParameter.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 4: #last item is always empty
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    param = PlayableParameter.objects.get(name=data[1])

                    res = PlayerParameter(player=player,parameter=param,state=data[2])
                    res.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Player.DoesNotExist Exception")
                except PlayableParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. PlayableParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in PlayerParameterRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                user = User.objects.get(username=data[0])
                player = Player.objects.get(user=user)

                param = PlayableParameter.objects.get(name=data[1])

                res = PlayerParameter.objects.get(player=player,parameter=param)
                res.state = data[2]
                res.save()
            except: #there are no this item for this player in equipment
                try:
                    user = User.objects.get(username=data[0])
                    player = Player.objects.get(user=user)

                    param = PlayableParameter.objects.get(name=data[1])

                    res = PlayerParameter(player=player,parameter=param,state=data[2])
                    res.save()
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Player.DoesNotExist Exception")
                except PlayableParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. PlayableParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ReportResponseRestore. Unknown Exception occurred.")


class MonsterLandTypeRestore:
    def restore(self,directory,mode):
        file = directory + "/MonsterLandType_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MonsterLandTypeRestore!")
        return self.results

    def restore_by_deletion(self):
        MonsterLandType.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 4: #last item is always empty
                try:
                    monster = Monster.objects.get(name=data[0])
                    land_type = LandType.objects.get(land_name=data[1])

                    res = MonsterLandType(monster=monster,land_type=land_type,probability=data[2])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. Monster.DoesNotExist Exception")
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. LandType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                monster = Monster.objects.get(name=data[0])
                land_type = LandType.objects.get(land_name=data[1])

                res = MonsterLandType.objects.get(monster=monster,land_type=land_type)
                res.probability = data[2]
                res.save()
            except: #there are no this item for this player in equipment
                try:
                    monster = Monster.objects.get(name=data[0])
                    land_type = LandType.objects.get(land_name=data[1])

                    res = MonsterLandType(monster=monster,land_type=land_type,probability=data[2])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. Monster.DoesNotExist Exception")
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. LandType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterLandTypeRestore. Unknown Exception occurred.")


class MonsterParameterRestore:
    def restore(self,directory,mode):
        file = directory + "/MonsterParameter_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MonsterParameterRestore!")
        return self.results

    def restore_by_deletion(self):
        MonsterParameter.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 4: #last item is always empty
                try:
                    monster = Monster.objects.get(name=data[0])
                    parameter = PlayableParameter.objects.get(name=data[1])

                    res = MonsterParameter(monster=monster,parameter=parameter,state=data[2])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. Monster.DoesNotExist Exception")
                except PlayableParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. PlayableParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in MonsterParameterRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                monster = Monster.objects.get(name=data[0])
                parameter = PlayableParameter.objects.get(name=data[1])

                res = MonsterParameter.objects.get(monster=monster,parameter=parameter)
                res.state = data[2]
                res.save()
            except: #there are no this item for this player in equipment
                try:
                    monster = Monster.objects.get(name=data[0])
                    parameter = PlayableParameter.objects.get(name=data[1])

                    res = MonsterParameter(monster=monster,parameter=parameter,state=data[2])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. Monster.DoesNotExist Exception")
                except PlayableParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. PlayableParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterParameterRestore. Unknown Exception occurred.")


class MonsterLootRestore:
    def restore(self,directory,mode):
        file = directory + "/MonsterLoot_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MonsterLootRestore!")
        return self.results

    def restore_by_deletion(self):
        MonsterLoot.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 5: #last item is always empty
                try:
                    monster = Monster.objects.get(name=data[0])
                    item = Item.objects.get(name=data[1])

                    res = MonsterLoot(monster=monster,item=item,amount=data[2],probability=data[3])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Monster.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in MonsterLootRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                monster = Monster.objects.get(name=data[0])
                item = Item.objects.get(name=data[1])

                res = MonsterLoot.objects.get(monster=monster,item=item)
                res.amount = data[2]
                res.probability = data[3]
                res.save()
            except: #there are no this item for this player in equipment
                try:
                    monster = Monster.objects.get(name=data[0])
                    item = Item.objects.get(name=data[1])

                    res = MonsterLoot(monster=monster,item=item,amount=data[2],probability=data[3])
                    res.save()
                except Monster.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Monster.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MonsterLootRestore. Unknown Exception occurred.")


class ItemTypeRestore:
    def restore(self,directory,mode):
        file = directory + "/ItemType_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ItemTypeRestore!")
        return self.results

    def restore_by_deletion(self):
        ItemType.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 3: #last item is always empty
                try:
                    try:
                        slot = EquipmentSlot.objects.get(name=data[1])
                        res = ItemType(name=data[0],equipment_slot=slot)
                        res.save()
                    except:
                        res = ItemType(name=data[0],equipment_slot=None)
                        res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in ItemTypeRestore")
            else:
                self.results.append("Row " + row + " has been ommited in ItemTypeRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                slot = EquipmentSlot.objects.get(name=data[1])
                res = ItemType.objects.get(name=data[0])
                res.equipment_slot = slot
                res.save()
            except:
                try:
                    res = ItemType.objects.get(name=data[0])

                except: #there are no this item for this player in equipment
                    try:
                        slot = EquipmentSlot.objects.get(name=data[1])
                        res = ItemType(name=data[0],equipment_slot=slot)
                        res.save()
                    except:
                        try:
                            res = ItemType(name=data[0],equipment_slot=None)
                            res.save()
                        except:
                            self.results.append("Row " + row + " has been ommited in ItemTypeRestore")


class EquipmentSlotRestore:
    def restore(self,directory,mode):
        file = directory + "/EquipmentSlot_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in EquipmentSlotRestore!")
        return self.results

    def restore_by_deletion(self):
        EquipmentSlot.objects.all().delete()

        for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            if len(data) == 3: #last item is always empty
                try:
                    res = EquipmentSlot(name=data[0],multiplicity=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in EquipmentSlotRestore")
            else:
                self.results.append("Row " + row + " has been ommited in EquipmentSlotRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = EquipmentSlot.objects.get(name=data[0])
                res.multiplicity = data[1]
                res.save()
            except:
                try:
                    res = EquipmentSlot(name=data[0],multiplicity=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in EquipmentSlotRestore")


class ItemRestore:
    def restore(self,directory,mode):
        file = directory + "/Item_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ItemRestore!")
        return self.results

    def restore_by_deletion(self):
        Item.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 5: #last item is always empty
                try:

                    item_type = ItemType.objects.get(name=data[0])
                    res = Item(item_type=item_type,name=data[1],description=data[2],value=data[3])
                    res.save()
                except ItemType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemRestore. ItemType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ItemRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                item_type = ItemType.objects.get(name=data[0])

                res = Item.objects.get(name=data[1])
                res.item_type = item_type
                res.description = data[2]
                res.value = data[3]
                res.save()
            except:
                try:
                    item_type = ItemType.objects.get(name=data[0])

                    res = Item(item_type=item_type,name=data[1],description=data[2],value=data[3])
                    res.save()
                except ItemType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemRestore. ItemType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemRestore. Unknown Exception occurred.")


class ItemLandTypeRestore:
    def restore(self,directory,mode):
        file = directory + "/ItemLandType_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ItemLandTypeRestore!")
        return self.results

    def restore_by_deletion(self):
        ItemLandType.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:

                    item = Item.objects.get(name=data[0])
                    land_type = LandType.objects.get(land_name=data[1])
                    res = ItemLandType(item=item,land_type=land_type,probability=data[2])
                    res.save()
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. Item.DoesNotExist Exception")
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. LandType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                item = Item.objects.get(name=data[0])
                land_type = LandType.objects.get(land_name=data[1])

                res = ItemLandType.objects.get(item=item,land_type=land_type)
                res.probability = data[2]
                res.save()
            except:
                try:
                    item = Item.objects.get(name=data[0])
                    land_type = LandType.objects.get(land_name=data[1])
                    res = ItemLandType(item=item,land_type=land_type,probability=data[2])
                    res.save()
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. Item.DoesNotExist Exception")
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. LandType.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemLandTypeRestore. Unknown Exception occurred.")


class ItemParameterRestore:
    def restore(self,directory,mode):
        file = directory + "/ItemParameter_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ItemParameterRestore!")
        return self.results

    def restore_by_deletion(self):
        ItemParameter.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:

                    item = Item.objects.get(name=data[0])
                    parameter = AllParameter.objects.get(name=data[1])
                    res = ItemParameter(item=item,parameter=parameter,state=data[2])
                    res.save()
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. Item.DoesNotExist Exception")
                except AllParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. AllParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ItemParameterRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                item = Item.objects.get(name=data[0])
                parameter = AllParameter.objects.get(name=data[1])

                res = ItemParameter.objects.get(item=item,parameter=parameter)
                res.state = data[2]
                res.save()
            except:
                try:
                    item = Item.objects.get(name=data[0])
                    parameter = AllParameter.objects.get(name=data[1])
                    res = ItemParameter(item=item,parameter=parameter,state=data[2])
                    res.save()
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. Item.DoesNotExist Exception")
                except AllParameter.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. AllParameter.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ItemParameterRestore. Unknown Exception occurred.")



class LandTypeRestore:
    def restore(self,directory,mode):
        file = directory + "/LandType_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in LandTypeRestore!")
        return self.results

    def restore_by_deletion(self):
        LandType.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 6: #last item is always empty
                try:
                    res = LandType(land_name=data[0],monster_probability=data[1],item_probability=data[2],default_description=data[3],number=data[4])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in LandTypeRestore")
            else:
                self.results.append("Row " + row + " has been ommited in LandTypeRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = LandType.objects.get(land_name=data[0])
                res.monster_probability = data[1]
                res.item_probability = data[2]
                res.default_description = data[3]
                res.number = data[4]
                res.save()
            except:
                try:
                    res = LandType(land_name=data[0],monster_probability=data[1],item_probability=data[2],default_description=data[3],number=data[4])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in LandTypeRestore")


class MapLevelRestore:
    def restore(self,directory,mode):
        file = directory + "/MapLevel_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MapLevelRestore!")
        return self.results

    def restore_by_deletion(self):
        MapLevel.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 3: #last item is always empty
                try:
                    res = MapLevel(level=data[0],name=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in MapLevelRestore")
            else:
                self.results.append("Row " + row + " has been ommited in MapLevelRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = MapLevel.objects.get(level=data[0])
                res.name=data[1]
                res.save()
            except:
                try:
                    res = MapLevel(level=data[0],name=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in MapLevelRestore")


class MapRestore:
    def restore(self,directory,mode):
        file = directory + "/Map_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MapRestore!")
        return self.results

    def restore_by_deletion(self):
        Map.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 6: #last item is always empty
                try:
                    land_type = LandType.objects.get(land_name=data[2])
                    level = MapLevel.objects.get(level=data[4])

                    res = Map(x=data[0],y=data[1],land_type=land_type,description=data[3],level=level)
                    res.save()
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapRestore. LandType.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapRestore. MapLevel.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MapRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in MapRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                land_type = LandType.objects.get(land_name=data[2])
                level = MapLevel.objects.get(level=data[4])

                res = Map.objects.get(x=data[0],y=data[1],level=level)
                res.land_type = land_type
                res.description = data[3]
                res.save()
            except:
                try:
                    land_type = LandType.objects.get(land_name=data[2])
                    level = MapLevel.objects.get(level=data[4])

                    res = Map(x=data[0],y=data[1],land_type=land_type,description=data[3],level=level)
                    res.save()
                except LandType.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapRestore. LandType.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapRestore. MapLevel.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MapRestore. Unknown Exception occurred.")


class MapPassageRestore:
    def restore(self,directory,mode):
        file = directory + "/MapPassage_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in MapPassageRestore!")
        return self.results

    def restore_by_deletion(self):
        MapPassage.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 5: #last item is always empty
                try:
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    from_map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                    lvl = MapLevel.objects.get(level=data[2].split("_")[2])
                    to_map = Map.objects.get(x=data[2].split("_")[0],y=data[2].split("_")[1],level=lvl)
                    try:
                        asset = Asset.objects.get(name=data[3])
                        res = MapPassage(name=data[0],from_map=from_map,to_map=to_map,asset=asset)
                        res.save()
                    except Asset.DoesNotExist:
                        res = MapPassage(name=data[0],from_map=from_map,to_map=to_map,asset=None)
                        res.save()

                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. Map.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. MapLevel.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in MapPassageRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                from_map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                lvl = MapLevel.objects.get(level=data[2].split("_")[2])
                to_map = Map.objects.get(x=data[2].split("_")[0],y=data[2].split("_")[1],level=lvl)
                try:
                    asset = Asset.objects.get(name=data[3])
                    res = MapPassage.objects.get(from_map=from_map,to_map=to_map)
                    res.asset = asset
                    res.name = data[0]
                    res.save()
                except Asset.DoesNotExist:
                    res = MapPassage.objects.get(from_map=from_map,to_map=to_map)
                    res.asset = None
                    res.name = data[0]
                    res.save()

            except:
                try:
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    from_map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)
                    lvl = MapLevel.objects.get(level=data[2].split("_")[2])
                    to_map = Map.objects.get(x=data[2].split("_")[0],y=data[2].split("_")[1],level=lvl)
                    try:
                        asset = Asset.objects.get(name=data[3])
                        res = MapPassage(name=data[0],from_map=from_map,to_map=to_map,asset=asset)
                        res.save()
                    except Asset.DoesNotExist:
                        res = MapPassage(name=data[0],from_map=from_map,to_map=to_map,asset=None)
                        res.save()
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. Map.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. MapLevel.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in MapPassageRestore. Unknown Exception occurred.")


class AssetInMapRestore:
    def restore(self,directory,mode):
        file = directory + "/AssetInMap_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in AssetInMapRestore!")
        return self.results

    def restore_by_deletion(self):
        AssetInMap.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:
                    asset = Asset.objects.get(name=data[0])
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)

                    if data[2]=='None':
                        res = AssetInMap(asset=asset,map=map,state=None)
                    else:
                        res = AssetInMap(asset=asset,map=map,state=data[2])
                    res.save()
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Map.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. MapLevel.DoesNotExist Exception")
                except Asset.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Asset.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in AssetInMapRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                asset = Asset.objects.get(name=data[0])
                lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)

                res = AssetInMap.objects.get(asset=asset,map=map)
                if data[2]=='None':
                    res.state=None
                else:
                    res.state = data[2]
                res.save()
            except:
                try:
                    asset = Asset.objects.get(name=data[0])
                    lvl = MapLevel.objects.get(level=data[1].split("_")[2])
                    map = Map.objects.get(x=data[1].split("_")[0],y=data[1].split("_")[1],level=lvl)

                    if data[2]=='None':
                        res = AssetInMap(asset=asset,map=map,state=None)
                    else:
                        res = AssetInMap(asset=asset,map=map,state=data[2])
                    res.save()
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Map.DoesNotExist Exception")
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. MapLevel.DoesNotExist Exception")
                except Asset.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Asset.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in AssetInMapRestore. Unknown Exception occurred.")



class NormalAssetRestore:
    def restore(self,directory,mode):
        file = directory + "/NormalAsset_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in NormalAssetRestore!")
        return self.results

    def restore_by_deletion(self):
        NormalAsset.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 3: #last item is always empty
                try:
                    res = NormalAsset(name=data[0],type_name=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in NormalAssetRestore")
            else:
                self.results.append("Row " + row + " has been ommited in NormalAssetRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = NormalAsset.objects.get(name=data[0])
                res.type_name = data[1]
                res.save()
            except:
                try:
                    res = NormalAsset(name=data[0],type_name=data[1])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in NormalAssetRestore")


class NPCRestore:
    def restore(self,directory,mode):
        file = directory + "/NPC_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in NPCRestore!")
        return self.results

    def restore_by_deletion(self):
        NPC.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:
                    graph = DialogGraph.objects.get(name=data[2])
                    res = NPC(name=data[0],type_name=data[1],graph=graph)
                    res.save()
                except DialogGraph.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in NPCRestore. DialogGraph.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in NPCRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in NPCRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                graph = DialogGraph.objects.get(name=data[2])

                res = NPC.objects.get(name=data[0])
                res.type_name = data[1]
                res.graph = graph
                res.save()
            except:
                try:
                    graph = DialogGraph.objects.get(name=data[2])

                    res = NPC(name=data[0],type_name=data[1],graph=graph)
                    res.save()
                except DialogGraph.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in NPCRestore. DialogGraph.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in NPCRestore. Unknown Exception occurred.")


class DialogStateRestore:
    def restore(self,directory,mode):
        file = directory + "/DialogState_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in DialogStateRestore!")
        return self.results

    def restore_by_deletion(self):
        DialogState.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:
                    lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                    map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                    asset_in_map = AssetInMap.objects.get(map=map)
                    user = User.objects.get(username=data[1])
                    player = Player.objects.get(user=user)

                    if(data[2]=="None"):
                        res = DialogState(player=player,state=None,asset_in_map=asset_in_map)
                    else:
                        res = DialogState(player=player,state=data[2],asset_in_map=asset_in_map)
                    res.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. MapLevel.DoesNotExist Exception")
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Map.DoesNotExist Exception")
                except AssetInMap.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. AssetInMap.DoesNotExist Exception")
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in DialogStateRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                asset_in_map = AssetInMap.objects.get(map=map)
                user = User.objects.get(username=data[1])
                player = Player.objects.get(user=user)

                res = DialogState.objects.get(player=player,asset_in_map=asset_in_map)
                if data[2] == "None":
                    res.state = None
                else:
                    res.state = data[2]
                res.save()
            except:
                try:
                    lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                    map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                    asset_in_map = AssetInMap.objects.get(map=map)
                    user = User.objects.get(username=data[1])
                    player = Player.objects.get(user=user)

                    if(data[2]=="None"):
                        res = DialogState(player=player,state=None,asset_in_map=asset_in_map)
                    else:
                        res = DialogState(player=player,state=data[2],asset_in_map=asset_in_map)
                    res.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. MapLevel.DoesNotExist Exception")
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Map.DoesNotExist Exception")
                except AssetInMap.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. AssetInMap.DoesNotExist Exception")
                except User.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. User.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Player.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in DialogStateRestore. Unknown Exception occurred.")


class StoneRestore:
    def restore(self,directory,mode):
        file = directory + "/Stone_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in StoneRestore!")
        return self.results

    def restore_by_deletion(self):
        Stone.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:
                    res = Stone(name=data[0],type_name=data[1],message=data[2])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in StoneRestore")
            else:
                self.results.append("Row " + row + " has been ommited in StoneRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = Stone.objects.get(name=data[0])
                res.type_name = data[1]
                res.message = data[2]
                res.save()
            except:
                try:
                    res = Stone(name=data[0],type_name=data[1],message=data[2])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in StoneRestore")


class RiddleRestore:
    def restore(self,directory,mode):
        file = directory + "/Riddle_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in RiddleRestore!")
        return self.results

    def restore_by_deletion(self):
        Riddle.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 6: #last item is always empty
                try:
                    res = Riddle(name=data[0],type_name=data[1],question=data[2],answer=data[3],justification=data[4])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in RiddleRestore")
            else:
                self.results.append("Row " + row + " has been ommited in RiddleRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = Riddle.objects.get(name=data[0])
                res.type_name = data[1]
                res.question = data[2]
                res.answer = data[3]
                res.justification = data[4]
                res.save()
            except:
                try:
                    res = Riddle(name=data[0],type_name=data[1],question=data[2],answer=data[3],justification=data[4])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in RiddleRestore")



class ChestRestore:
    def restore(self,directory,mode):
        file = directory + "/Chest_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ChestRestore!")
        return self.results

    def restore_by_deletion(self):
        Chest.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 8: #last item is always empty
                try:
                    res = Chest(name=data[0],type_name=data[1],max_loot_amount=data[2],item_probability=data[3],resistance=data[4],graph=data[5],experience=data[6])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in ChestRestore")
            else:
                self.results.append("Row " + row + " has been ommited in ChestRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = Chest.objects.get(name=data[0])
                res.type_name = data[1]
                res.max_loot_amount = data[2]
                res.item_probability = data[3]
                res.resistance = data[4]
                res.graph = data[5]
                res.experience = data[6]
                res.save()
            except:
                try:
                    res = Chest(name=data[0],type_name=data[1],max_loot_amount=data[2],item_probability=data[3],resistance=data[4],graph=data[5],experience=data[6])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in ChestRestore")


class ChestLootRestore:
    def restore(self,directory,mode):
        file = directory + "/ChestLoot_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in ChestLootRestore!")
        return self.results

    def restore_by_deletion(self):
        ChestLoot.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 4: #last item is always empty
                try:
                    chest = Chest.objects.get(name=data[0])
                    item = Item.objects.get(name=data[1])

                    res = ChestLoot(chest=chest,item=item,probability=data[2])
                    res.save()
                except Chest.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Chest.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in ChestLootRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                chest = Chest.objects.get(name=data[0])
                item = Item.objects.get(name=data[1])

                res = ChestLoot.objects.get(chest=chest,item=item)
                res.data = data[1]
                res.save()
            except:
                try:
                    chest = Chest.objects.get(name=data[0])
                    item = Item.objects.get(name=data[1])

                    res = ChestLoot(chest=chest,item=item,probability=data[2])
                    res.save()
                except Chest.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Chest.DoesNotExist Exception")
                except Item.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Item.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in ChestLootRestore. Unknown Exception occurred.")


class DialogGraphRestore:
    def restore(self,directory,mode):
        file = directory + "/DialogGraph_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in DialogGraphRestore!")
        return self.results

    def restore_by_deletion(self):
        DialogGraph.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 2: #last item is always empty
                try:
                    res = DialogGraph(name=data[0])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in DialogGraphRestore")
            else:
                self.results.append("Row " + row + " has been ommited in DialogGraphRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = DialogGraph.objects.get(name=data[0])
            except:
                try:
                    res = DialogGraph(name=data[0])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in DialogGraphRestore")


class QuestRestore:
    def restore(self,directory,mode):
        file = directory + "/Quest_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in QuestRestore!")
        return self.results

    def restore_by_deletion(self):
        Quest.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 5: #last item is always empty
                try:
                    lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                    map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                    asset_in_map = AssetInMap.objects.get(map=map)
                    user = User.objects.get(username=data[1])
                    player = Player.objects.get(user=user)
                    quest = QuestObject.objects.get(name=data[2])

                    res = Quest(asset_in_map=asset_in_map,player=player,quest=quest,state=data[3])
                    res.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. MapLevel.DoesNotExist Exception")
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Map.DoesNotExist Exception")
                except AssetInMap.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. AssetInMap.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Player.DoesNotExist Exception")
                except QuestObject.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. QuestObject.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Unknown Exception occurred.")
            else:
                self.results.append("Row " + row + " has been ommited in QuestRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                asset_in_map = AssetInMap.objects.get(map=map)
                user = User.objects.get(username=data[1])
                player = Player.objects.get(user=user)
                quest = QuestObject.objects.get(name=data[2])

                res = Quest.objects.get(asset_in_map=asset_in_map,player=player)
                res.quest = quest
                player.state=data[3]
            except:
                try:
                    lvl = MapLevel.objects.get(level=data[0].split("_")[2])
                    map = Map.objects.get(x=data[0].split("_")[0],y=data[0].split("_")[1],level=lvl)
                    asset_in_map = AssetInMap.objects.get(map=map)
                    user = User.objects.get(username=data[1])
                    player = Player.objects.get(user=user)
                    quest = QuestObject.objects.get(name=data[2])

                    res = Quest(asset_in_map=asset_in_map,player=player,quest=quest,state=data[3])
                    res.save()
                except MapLevel.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. MapLevel.DoesNotExist Exception")
                except Map.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Map.DoesNotExist Exception")
                except AssetInMap.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. AssetInMap.DoesNotExist Exception")
                except Player.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Player.DoesNotExist Exception")
                except QuestObject.DoesNotExist:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. QuestObject.DoesNotExist Exception")
                except:
                    self.results.append("Row " + row + " has been ommited in QuestRestore. Unknown Exception occurred.")



class QuestObjectRestore:
    def restore(self,directory,mode):
        file = directory + "/QuestObject_Backup" + '.csv'
        f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
        self.content = f.read().split("\n");
        f.close();

        self.results = []

        if mode == "del":
            self.restore_by_deletion()
        elif mode == "mod":
            self.restore_by_modification()
        else:
            self.results.append("Wrong import mode in QuestObjectRestore!")
        return self.results

    def restore_by_deletion(self):
        QuestObject.objects.all().delete()
        for row in self.content:
            if row == "":
                continue
            data = row.split(";")
            if len(data) == 2: #last item is always empty
                try:
                    res = QuestObject(name=data[0])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in QuestObjectRestore")
            else:
                self.results.append("Row " + row + " has been ommited in QuestObjectRestore - wrong array length")

    def restore_by_modification(self):

       for row in self.content:
            if row == "":
                continue
            data = row.split(";")

            try:
                res = QuestObject.objects.get(name=data[0])
            except:
                try:
                    res = QuestObject(name=data[0])
                    res.save()
                except:
                    self.results.append("Row " + row + " has been ommited in QuestObjectRestore")


# class MonsterEquipmentRestore:
#     def restore(self,directory,mode):
#         file = directory + "/MonsterEquipment_Backup" + '.csv'
#         f = codecs.open(os.getcwd() + '/BACKUP/' + file, mode='r', encoding='utf-8')
#         self.content = f.read().split("\n");
#         f.close();
#
#         self.results = []
#
#         if mode == "del":
#             self.restore_by_deletion()
#         elif mode == "mod":
#             self.restore_by_modification()
#         else:
#             self.results.append("Wrong import mode in MonsterEquipmentRestore!")
#         return self.results
#
#     def restore_by_deletion(self):
#         MonsterEquipment.objects.all().delete()
#
#         for row in self.content:
#             if row == "":
#                 continue
#             data = row.split(";")
#
#             if len(data) == 3: #last item is always empty
#                 try:
#                     monster = Monster.objects.get(name=data[0])
#                     item = Item.objects.get(name=data[1])
#
#                     res = MonsterEquipment(monster=monster,item=item)
#                     res.save()
#                 except:
#                     self.results.append("Row " + row + " has been ommited in MonsterEquipmentRestore")
#             else:
#                 self.results.append("Row " + row + " has been ommited in MonsterEquipmentRestore - wrong array length")
#
#     def restore_by_modification(self):
#
#        for row in self.content:
#             if row == "":
#                 continue
#             data = row.split(";")
#
#             try:
#                 monster = Monster.objects.get(name=data[0])
#                 item = Item.objects.get(name=data[1])
#
#                 res = MonsterEquipment.objects.get(monster=monster,item=item)
#             except: #there are no this item for this player in equipment
#                 try:
#                     monster = Monster.objects.get(name=data[0])
#                     item = Item.objects.get(name=data[1])
#
#                     res = MonsterEquipment(monster=monster,item=item)
#                     res.save()
#                 except:
#                     self.results.append("Row " + row + " has been ommited in MonsterEquipmentRestore")