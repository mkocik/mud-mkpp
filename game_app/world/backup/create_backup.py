import os
import codecs

from game_app.models import *


class Backup:
    def create_backup(self, tables):
        self.create_directory()
        self.results = []
        for table in tables:
            try:
                constructor = globals()[table+"Backup"]
                instance = constructor()
                res = instance.create(self.current_directory)
                if res:
                    self.results.extend(res)
            except:
                self.results.append("Exception occurred while trying to create class " + table+"Backup")

        if len(self.results) == 0:
            self.results.append("Succesfully created BACKUP")

        self.results.append("Backup saved in " + self.current_directory)

        return self.results
        # PlayerBackup().create(self.current_directory)
        # ReportTimeBackup().create(self.current_directory)
        # ReportResponseBackup().create(self.current_directory)
        # PlayableParameterBackup().create(self.current_directory)
        # PlayerParameterBackup().create(self.current_directory)
        # EquipmentBackup().create(self.current_directory)
        # PlayerEquipmentSlotBackup().create(self.current_directory)
        # MonsterBackup().create(self.current_directory)
        # MonsterLandTypeBackup().create(self.current_directory)
        # MonsterParameterBackup().create(self.current_directory)
        # MonsterLootBackup().create(self.current_directory)
        # ItemTypeBackup().create(self.current_directory)
        # EquipmentSlotBackup().create(self.current_directory)
        # ItemBackup().create(self.current_directory)
        # ItemLandTypeBackup().create(self.current_directory)
        # ItemParametersBackup().create(self.current_directory)
        # LandTypeBackup().create(self.current_directory)
        # MapLevelBackup().create(self.current_directory)
        # MapBackup().create(self.current_directory)
        # MapPassageBackup().create(self.current_directory)
        # NormalAssetBackup().create(self.current_directory)
        # AssetInMapBackup().create(self.current_directory)
        # NPCBackup().create(self.current_directory)
        # DialogStateBackup().create(self.current_directory)
        # StoneBackup().create(self.current_directory)
        # RiddleBackup().create(self.current_directory)
        # ChestBackup().create(self.current_directory)
        # ChestLootBackup().create(self.current_directory)
        # DialogGraphBackup().create(self.current_directory)
        # QuestBackup().create(self.current_directory)

    def create_directory(self):
        backup_paths = "BACKUP"
        full_path = os.getcwd() + '/' + backup_paths

        if not os.path.exists(full_path):
            os.makedirs(full_path)

        last_backup_counter = 0;

        for name in os.listdir(full_path):
            if name.startswith("backup_"):
                curr_count = name.split("_")[1]
                if int(curr_count) > last_backup_counter:
                    last_backup_counter = int(curr_count)

        last_backup_counter+=1;

        self.current_directory = full_path + '/' + "backup_" + str(last_backup_counter)
        os.makedirs(self.current_directory)


class PlayerBackup:
    def create(self, directory):
        players = Player.objects.all()
        self.result = ""

        file = codecs.open(directory + '/Player_Backup.csv', 'w', encoding='utf-8')

        for player in players:
            try:
                file.write(player.user.username + ";")
                file.write(str(player.location.x) + "_" + str(player.location.y) + "_" + str(player.location.level.level) + ";")
                file.write(str(player.gold) + ";")
                file.write(str(player.experience) + ";")
                file.write(str(player.distribution_points) + ";")
                file.write(str(player.skills_points) + ";")
                file.write(str(player.color) + ";")
                file.write(player.user.password + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Player_Backup while saving " + player.user.username + " player!")

        file.close()
        return self.result


class ReportTimeBackup:
    def create(self,directory):
        reports = ReportTime.objects.all()
        self.result = []
        file = codecs.open(directory + '/ReportTime_Backup.csv', 'w', encoding='utf-8')

        for report in reports:
            try:
                file.write(report.player.user.username + ";")
                file.write(str(report.start_date) + ";")
                file.write(str(report.end_date) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ReportTime_Backup while saving " + report.player.user.username + " player time!")

        file.close()
        return self.result


class ReportResponseBackup:
    def create(self,directory):
        reports = ReportResponse.objects.all()
        self.result = []
        file = codecs.open(directory + '/ReportResponse_Backup.csv', 'w', encoding='utf-8')

        for report in reports:
            try:
                file.write(report.player.user.username + ";")
                file.write(report.response + ";")
                file.write(str(report.date) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ReportResponse_Backup while saving " + report.player.user.username + " player!")

        file.close()
        return self.result


class PlayableParameterBackup:
    def create(self,directory):
        params = PlayableParameter.objects.all()
        self.result= []
        file = codecs.open(directory + '/PlayableParameter_Backup.csv', 'w', encoding='utf-8')

        for param in params:
            try:
                file.write(param.name+ ";")
                file.write(str(param.default_val) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in PlayableParameter_Backup while saving " + param.name + "!")

        file.close()
        return self.result

class PlayerParameterBackup:
    def create(self,directory):
        params = PlayerParameter.objects.all()
        self.result= []
        file = codecs.open(directory + '/PlayerParameter_Backup.csv', 'w', encoding='utf-8')

        for param in params:
            try:
                file.write(param.player.user.username+ ";")
                file.write(param.parameter.name + ";")
                file.write(str(param.state) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in PlayerParameter_Backup while saving " + param.player.user.username + " player!")

        file.close()
        return self.result


class EquipmentBackup:
    def create(self,directory):
        equipments = PlayerEquipment.objects.all()
        self.result= []
        file = codecs.open(directory + '/Equipment_Backup.csv', 'w', encoding='utf-8')

        for equipment in equipments:
            try:
                file.write(equipment.player.user.username+ ";")
                file.write(equipment.item.name + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Equipment_Backup while saving " + equipment.player.user.username + " player!")

        file.close()
        return self.result


class PlayerEquipmentSlotBackup:
    def create(self,directory):
        equipments = PlayerEquipmentSlot.objects.all()
        self.result= []
        file = codecs.open(directory + '/PlayerEquipmentSlot_Backup.csv', 'w', encoding='utf-8')

        for equipment in equipments:
            try:
                file.write(equipment.player.user.username+ ";")
                file.write(equipment.item.name + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in PlayerEquipmentSlot while saving " + equipment.player.user.username + " player!")

        file.close()
        return self.result


class MonsterBackup:
    def create(self,directory):
        print("A")
        monsters = Monster.objects.all()
        self.result= []
        file = codecs.open(directory + '/Monster_Backup.csv', 'w', encoding='utf-8')

        for monster in monsters:
            try:
                print("A")
                file.write(monster.name+ ";")
                print("A")
                file.write(monster.description+ ";")
                print("A")
                file.write(str(monster.default_hp) + ";")
                print("A")
                file.write(str(monster.gold_min) + ";")
                print("A")
                file.write(str(monster.gold_max) + ";")
                print("A")
                file.write(str(monster.experience) + ";")
                print("A")
                file.write(str(monster.aggression) + ";")
                print("A")
                file.write(str(monster.max_loot_amount) + ";")
                print("A")
                file.write(str(monster.item_probability) + ";")
                print("A")
                file.write("\n")
            except:
                self.result.append("Exception occured in Monster_Backup while saving " + monster.name + " monster!")

        file.close()
        return self.result


class MonsterLandTypeBackup:
    def create(self,directory):
        monsters = MonsterLandType.objects.all()
        self.result= []
        file = codecs.open(directory + '/MonsterLandType_Backup.csv', 'w', encoding='utf-8')

        for monster in monsters:
            try:
                file.write(monster.monster.name+ ";")
                file.write(monster.land_type.land_name + ";")
                file.write(str(monster.probability) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in MonsterLandType_Backup while saving " + monster.monster.name + " monster!")

        file.close()
        return self.result


class MonsterParameterBackup:
    def create(self,directory):
        monsters = MonsterParameter.objects.all()
        self.result= []
        file = codecs.open(directory + '/MonsterParameter_Backup.csv', 'w', encoding='utf-8')

        for monster in monsters:
            try:
                file.write(monster.monster.name+ ";")
                file.write(monster.parameter.name + ";")
                file.write(str(monster.state) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in MonsterParameter_Backup while saving " + monster.monster.name + " monster!")

        file.close()
        return self.result


class MonsterLootBackup:
    def create(self,directory):
        loots = MonsterLoot.objects.all()
        self.result= []
        file = codecs.open(directory + '/MonsterLoot_Backup.csv', 'w', encoding='utf-8')

        for loot in loots:
            try:
                file.write(loot.monster.name+ ";")
                file.write(loot.item.name + ";")
                file.write(str(loot.amount) + ";")
                file.write(str(loot.probability) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in MonsterLoot_Backup while saving " + loot.monster.name + " monster!")

        file.close()
        return self.result


class ItemTypeBackup:
    def create(self,directory):
        types = ItemType.objects.all()
        self.result= []
        file = codecs.open(directory + '/ItemType_Backup.csv', 'w', encoding='utf-8')

        for type in types:
            try:
                file.write(type.name+ ";")
                try:
                    file.write(type.equipment_slot.name + ";")
                except:
                    file.write(";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ItemType_Backup while saving " + type.name + " complex!")

        file.close()
        return self.result


class EquipmentSlotBackup:
    def create(self,directory):
        slots = EquipmentSlot.objects.all()
        self.result= []
        file = codecs.open(directory + '/EquipmentSlot_Backup.csv', 'w', encoding='utf-8')

        for slot in slots:
            try:
                file.write(slot.name+ ";")
                file.write(str(slot.multiplicity) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in EquipmentSlot_Backup while saving " + slot.name + " slot!")

        file.close()
        return self.result


class ItemBackup:
    def create(self,directory):
        items = Item.objects.all()
        self.result= []
        file = codecs.open(directory + '/Item_Backup.csv', 'w', encoding='utf-8')

        for item in items:
            try:
                file.write(item.item_type.name+ ";")
                file.write(item.name+ ";")
                file.write(item.description+ ";")
                file.write(str(item.value) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Item_Backup while saving " + item.name + " item!")

        file.close()
        return self.result


class ItemLandTypeBackup:
    def create(self,directory):
        items = ItemLandType.objects.all()
        self.result= []
        file = codecs.open(directory + '/ItemLandType_Backup.csv', 'w', encoding='utf-8')

        for item in items:
            try:
                file.write(item.item.name+ ";")
                file.write(item.land_type.land_name+ ";")
                file.write(str(item.probability) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ItemLandType_Backup while saving " + item.item.name + " item!")

        file.close()
        return self.result


class ItemParametersBackup:
    def create(self,directory):
        params = ItemParameter.objects.all()
        self.result= []
        file = codecs.open(directory + '/ItemParameter_Backup.csv', 'w', encoding='utf-8')

        for param in params:
            try:
                file.write(param.item.name+ ";")
                file.write(param.parameter.name+ ";")
                file.write(str(param.state) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ItemParameter_Backup while saving " + param.item.name + " item!")

        file.close()
        return self.result


class LandTypeBackup:
    def create(self,directory):
        types = LandType.objects.all()
        self.result= []
        file = codecs.open(directory + '/LandType_Backup.csv', 'w', encoding='utf-8')

        for type in types:
            try:
                file.write(type.land_name+ ";")
                file.write(str(type.monster_probability) + ";")
                file.write(str(type.item_probability) + ";")
                file.write(type.default_description+ ";")
                file.write(str(type.number) + ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in LandType_Backup while saving " + type.land_name + " land complex!")

        file.close()
        return self.result


class MapLevelBackup:
    def create(self,directory):
        lvls = MapLevel.objects.all()
        self.result= []
        file = codecs.open(directory + '/MapLevel_Backup.csv', 'w', encoding='utf-8')

        for lvl in lvls:
            try:
                file.write(str(lvl.level) + ";")
                file.write(lvl.name+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in MapLevel_Backup while saving " + lvl.name + " lvl!")

        file.close()
        return self.result


class MapBackup:
    def create(self,directory):
        maps = Map.objects.all()
        self.result= []
        file = codecs.open(directory + '/Map_Backup.csv', 'w', encoding='utf-8')

        for map in maps:
            try:
                file.write(str(map.x) + ";")
                file.write(str(map.y) + ";")
                file.write(map.land_type.land_name+ ";")
                file.write(map.description+ ";")
                file.write(str(map.level.level)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Map_Backup while saving " + str(map.x) + " " + str(map.y) + " " + str(map.level.level) + " map!")

        file.close()
        return self.result


class MapPassageBackup:
    def create(self,directory):
        passages = MapPassage.objects.all()
        self.result= []
        file = codecs.open(directory + '/MapPassage_Backup.csv', 'w', encoding='utf-8')

        for passage in passages:
            try:
                file.write(passage.name + ";")
                file.write(str(passage.from_map.x) + "_" + str(passage.from_map.y) + "_" + str(passage.from_map.level.level) + ";")
                file.write(str(passage.to_map.x) + "_" + str(passage.to_map.y) + "_" + str(passage.to_map.level.level) + ";")
                try:
                    file.write(passage.asset.name + ";")
                except:
                    file.write(";")
                file.write("\n")
            except:
                self.result.append("Exception occured in MapPassage_Backup while saving " + passage.name + " passage!")

        file.close()
        return self.result


class NormalAssetBackup:
    def create(self,directory):
        assets = NormalAsset.objects.all()
        self.result= []
        file = codecs.open(directory + '/NormalAsset_Backup.csv', 'w', encoding='utf-8')

        for asset in assets:
            try:
                file.write(asset.name+ ";")
                file.write(asset.type_name+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in NormalAsset_Backup while saving " + asset.name + " asset!")

        file.close()
        return self.result


class AssetInMapBackup:
    def create(self,directory):
        in_maps = AssetInMap.objects.all()
        self.result= []
        file = codecs.open(directory + '/AssetInMap_Backup.csv', 'w', encoding='utf-8')

        for in_map in in_maps:
            try:
                file.write(in_map.asset.name+ ";")
                file.write(str(in_map.map.x) + "_" + str(in_map.map.y) + "_" + str(in_map.map.level.level) + ";")
                file.write(str(in_map.state)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in AssetInMap_Backup while saving " + in_map.asset.name + " asset!")

        file.close()
        return self.result


class NPCBackup:
    def create(self,directory):
        npcs = NPC.objects.all()
        self.result= []
        file = codecs.open(directory + '/NPC_Backup.csv', 'w', encoding='utf-8')

        for npc in npcs:
            try:
                file.write(npc.name+ ";")
                file.write(npc.type_name+ ";")
                file.write(npc.graph.name+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in NPC_Backup while saving " + npc.name + " NPC!")

        file.close()
        return self.result


class DialogStateBackup:
    def create(self,directory):
        dialogs = DialogState.objects.all()
        self.result= []
        file = codecs.open(directory + '/DialogState_Backup.csv', 'w', encoding='utf-8')

        for dialog in dialogs:
            try:
                file.write(str(dialog.asset_in_map.map.x) + "_" + str(dialog.asset_in_map.map.y) + "_" + str(dialog.asset_in_map.map.level.level) + ";")
                file.write(dialog.player.user.username+ ";")
                file.write(str(dialog.state)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in DialogState_Backup while saving for " + dialog.player.user.username + " player!")

        file.close()
        return self.result


class StoneBackup:
    def create(self,directory):
        stones = Stone.objects.all()
        self.result= []
        file = codecs.open(directory + '/Stone_Backup.csv', 'w', encoding='utf-8')

        for stone in stones:
            try:
                file.write(stone.name+ ";")
                file.write(stone.type_name+ ";")
                file.write(stone.message+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Stone_Backup while saving " + stone.name + " Stone!")

        file.close()
        return self.result


class RiddleBackup:
    def create(self,directory):
        riddles = Riddle.objects.all()
        self.result= []
        file = codecs.open(directory + '/Riddle_Backup.csv', 'w', encoding='utf-8')

        for riddle in riddles:
            try:
                file.write(riddle.type_name+ ";")
                file.write(riddle.name+ ";")
                file.write(riddle.question+ ";")
                file.write(riddle.answer+ ";")
                try:
                    file.write(riddle.justification+ ";")
                except:
                    file.write(";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Riddle_Backup while saving " + riddle.name + " Riddle!")

        file.close()
        return self.result


class ChestBackup:
    def create(self,directory):
        chests = Chest.objects.all()
        self.result= []
        file = codecs.open(directory + '/Chest_Backup.csv', 'w', encoding='utf-8')

        for chest in chests:
            try:
                file.write(chest.name+ ";")
                file.write(chest.type_name+ ";")
                file.write(str(chest.max_loot_amount)+ ";")
                file.write(str(chest.item_probability)+ ";")
                file.write(str(chest.resistance)+ ";")
                file.write(chest.graph+ ";")
                file.write(str(chest.experience)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Chest_Backup while saving " + chest.name + " Chest!")

        file.close()
        return self.result


class ChestLootBackup:
    def create(self,directory):
        loots = ChestLoot.objects.all()
        self.result= []
        file = codecs.open(directory + '/ChestLoot_Backup.csv', 'w', encoding='utf-8')

        for loot in loots:
            try:
                file.write(loot.chest.name+ ";")
                file.write(loot.item.name+ ";")
                file.write(str(loot.probability)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in ChestLoot_Backup while saving " + loot.chest.name + " ChestLoot!")

        file.close()
        return self.result


class DialogGraphBackup:
    def create(self,directory):
        graphs = DialogGraph.objects.all()
        self.result= []
        file = codecs.open(directory + '/DialogGraph_Backup.csv', 'w', encoding='utf-8')

        for graph in graphs:
            try:
                file.write(graph.name+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in DialogGraph_Backup while saving " + graph.name + " DialogGraph!")

        file.close()
        return self.result


class QuestBackup:
    def create(self,directory):
        quests = Quest.objects.all()
        self.result= []
        file = codecs.open(directory + '/Quest_Backup.csv', 'w', encoding='utf-8')

        for quest in quests:
            try:
                file.write(str(quest.asset_in_map.map.x) + "_" + str(quest.asset_in_map.map.y) + "_" + str(quest.asset_in_map.map.level.level) + ";")
                file.write(quest.player.user.username+ ";")
                file.write(quest.quest.name+ ";")
                file.write(str(quest.state)+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in Quest_Backup while saving " + quest.name + " Quest!")

        file.close()
        return self.result


class QuestObjectBackup:
    def create(self,directory):
        quests = QuestObject.objects.all()
        self.result = []
        file = codecs.open(directory + '/QuestObject_Backup.csv', 'w', encoding='utf-8')

        for quest in quests:
            try:
                file.write(quest.name+ ";")
                file.write("\n")
            except:
                self.result.append("Exception occured in QuestObject_Backup while saving " + quest.name + " QuestObject!")

        file.close()
        return self.result