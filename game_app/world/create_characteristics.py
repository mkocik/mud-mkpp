# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from game_app.models import Characteristic
from game_app.world.create_world import ICreator
from MUD_ADMIN.game.characteristics.characteristics import all_characteristics

class CreateCharacteristics(ICreator):
    def execute(self):
        Characteristic.objects.all().delete()

        for char in all_characteristics:
            char = Characteristic(name=char.name)
            char.save()

        return 0
