# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division  #forces float division

import math
import random

from language.pl_pl_parameters import *

START_LOCATION_X = 0
START_LOCATION_Y = 0
START_LOCATION_LVL = 0

COMMAND_TIMEOUT = 8000  #in miliseconds

STARTING_HP = 100
STARTING_GOLD_VALUE = 10

MAX_EQUIPMENT_AMOUNT = 63
ASSET_STATES_RESET = 60000  #216000 for one hour - in ms
NPC_BAN_DELAY = 10 #216000 for one hour - in ms

DEFAULT_ATTACK_TIME = 1000 #in miliseconds
DEFAULT_EFFECT_INTERVAL = 6000
STARTING_STAMINA = 100

DEFAULT_BUYING_RATE = 1.2
DEFAULT_SELLING_RATE = 0.9

# changed in usr/local/lib/python2.7/dist-packages/django/core/management/commands/runserver.py
# during server configuration set DEFAULT_PORT from properties
# do not forget to set self.addr from 127.0.0.1 to 0
DEFAULT_PORT = 8001

PARAMETERS_COUNTING_IN_TRADE = ('Inteligencja', )
SKILLS_COUNTING_IN_TRADE = ('Negocjacje', )

user_colors = [
    "gray", "black", "red", "maroon",
    "olive", "lime", "green", "brown",
    "aqua", "teal", "blue", "navy",
    "fuchsia", "purple", "pink", "orange"
]


def armor_durability(player):
    return player.get_parameter("Wytrzymałość") / 10

def attribute_points_for_lvl(player):
    val = player.get_parameter("Inteligencja")/10
    if val > 1:
        return val
    return 1

def skills_points_for_lvl(player):
    val = player.get_parameter("Inteligencja")/20
    if val > 1:
        return val
    return 1

def experience_function(player_experience):
    exp_to_lvl = 10
    lvl = 1

    while player_experience >= exp_to_lvl:
        player_experience = player_experience - exp_to_lvl
        exp_to_lvl *= 10
        lvl += 1
    return lvl

def search_npc_success_chance_function(thief, target):
    return (thief.get_parameter("Zręczność") - target.get_parameter("Zwinność")
            + get_stealing_bonus_from_skill(thief.player)) + random.randint(-10,10) > 0

def steal_from_NPC_success_chance_function(thief, target, item):
    return (thief.get_parameter("Zręczność") - target.get_parameter("Zwinność") - (item.value/10)
            + get_stealing_bonus_from_skill(thief.player)) + random.randint(-10,10) > 0


def get_stealing_bonus_from_skill(player):
    from MUD_ADMIN.game.skills.skills import PickPocketingSkill
    from game_app.utils.discover.discover_skills import all_skills_class_dictionary

    bonus_value = 0
    if PickPocketingSkill.name in all_skills_class_dictionary.keys():
        bonus_value += (all_skills_class_dictionary[PickPocketingSkill.name])().execute(player)

    return bonus_value


#admin can use second parameter, now it is useless
def attack_strength_function(attacker, defender=None):
    from MUD_ADMIN.game.characteristics.characteristics import all_characteristics
    import random
    return random.randint(((list(filter(lambda x: x.name == MIN_DMG, all_characteristics)))[0])(attacker),
                          ((list(filter(lambda x: x.name == MIN_DMG, all_characteristics)))[0])(attacker))


def attack_probability_function(attacker, defender):
    from MUD_ADMIN.game.characteristics.characteristics import all_characteristics

    attack = ((list(filter(lambda x: x.name == ATTACK, all_characteristics)))[0])(attacker)
    defence = ((list(filter(lambda x: x.name == DEFENCE, all_characteristics)))[0])(defender)

    x = function_y1(attack, defence)
    return real_attack_probability_function(x)


def function_y1(attacker_parameter, defender_parameter):
    balance_attack_parameter = 20  # balance parameter to attack_probability_function

    return (attacker_parameter - defender_parameter) / balance_attack_parameter


def real_attack_probability_function(x):
    if x >= 0:
        y = math.pow(2, x - 1)
    else:
        y = 1 - math.pow(2, -x - 1)

    return math.ceil(100 * y)


def buy_price_function(buyer_parameter, seller_parameter):

    balance_parameter = 200
    x = (buyer_parameter - seller_parameter) / balance_parameter

    if DEFAULT_BUYING_RATE <= x + 1:
        return x + 1
    else:
        return DEFAULT_BUYING_RATE


def sell_price_function(buyer_parameter, seller_parameter):
    balance_parameter = 20
    x = (buyer_parameter - seller_parameter) / balance_parameter

    if x < 0:
        y = math.pow(2, x - 1)
    else:
        y = 1 - math.pow(2, -x - 1)

    y *= DEFAULT_SELLING_RATE

    if y > 0:
        rate = DEFAULT_SELLING_RATE - y
    else:
        rate = DEFAULT_SELLING_RATE

    return rate










