# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from __future__ import division  #forces float division
from random import randint
from math import ceil

from game_app.models import ItemInMap, GoldInMap, PlayerParameter


class WinnerBonus(object):
    pass


class WinnerBonusExperience(WinnerBonus):
    MIN_EXPERIENCE_FOR_DEFEATING = 10

    @classmethod
    def _increase_experience_properly(cls, winner, value):
        min_value = cls.MIN_EXPERIENCE_FOR_DEFEATING

        if value < min_value:
            value = min_value

        winner.increase_experience(value)
        message = "Zdobyłeś %s doświadczenia." % value
        return WinnerBonusResponse(message)

    @classmethod
    def increase_experience(cls, winner, loser):
        value = loser.experience / 10
        return cls._increase_experience_properly(winner, value)

    @classmethod
    def increase_experience_by_difference(cls, winner, loser, balance=10):
        value = ceil(abs(winner.experience - loser.experience) / balance)
        return cls._increase_experience_properly(winner, value)


class IWinnerBonusItem(WinnerBonus):
    @classmethod
    def _take_away_items(cls, winner, equipments, quantity):
        pass

    @classmethod
    def take_away_random_items(cls, winner, loser, quantity=2):
        equipments = loser.get_equipment()
        return cls._take_away_items(winner, equipments, quantity)


    @classmethod
    def take_away_random_percentage_of_items(cls, winner, loser, percentage=20):
        equipments = loser.get_equipment()

        quantity = ceil(len(equipments) * percentage / 100)
        return cls._take_away_items(winner, equipments, quantity)

    @classmethod
    def take_away_precious_items(cls, winner, loser, quantity=2):
        equipments = loser.get_equipment()
        equipments = sorted(equipments, key=lambda x: x.item.value, reverse=True)
        return cls._take_away_items(winner, equipments, quantity)

    @classmethod
    def take_away_precious_percentage_of_items(cls, winner, loser, percentage=20):
        equipments = loser.get_equipment()
        equipments = sorted(equipments, key=lambda x: x.item.value, reverse=True)

        quantity = ceil(len(equipments) * percentage / 100)
        return cls._take_away_items(winner, equipments, quantity)


class WinnerBonusGettingItem(IWinnerBonusItem):
    @classmethod
    def _take_away_items(cls, winner, equipments, quantity):
        initial_winner_message = 'Zabrałeś przeciwnikowi następujące przedmioty: <br>'
        winner_message = initial_winner_message

        loser_message = 'Twój przeciwnik po walce zabrał ci następujące przedmioty: <br>'

        for i in range(quantity):

            if len(equipments) > 0:
                equipment = equipments[randint(0, len(equipments) - 1)]

                equipment.player = winner
                equipment.save()
                winner_message += '- %s' % equipment.item.name
                loser_message += '- %s' % equipment.item.name
            else:
                return WinnerBonusResponse(winner_message, loser_message)

        if winner_message != initial_winner_message:
            return WinnerBonusResponse(winner_message, loser_message)
        else:
            return None


class WinnerBonusLeavingItem(IWinnerBonusItem):
    @classmethod
    def _take_away_items(cls, winner, equipments, quantity):

        initial_winner_message = 'Przeciwnik pozostawił po sobie następujące przedmioty: <br>'
        winner_message = initial_winner_message

        loser_message = 'Z powodu śmierci na polu walki straciłeś następujące przedmioty: <br>'

        for i in range(quantity):
            equipment = equipments[randint(0, len(equipments) - 1)]

            if equipment.id is not None:
                winner_message += '- %s' % equipment.item.name
                loser_message += '- %s' % equipment.item.name

                location = winner.location
                ItemInMap.objects.create(map=location, item=equipment.item)
                equipment.delete()
            else:
                return WinnerBonusResponse(winner_message, loser_message)

        if winner_message != initial_winner_message:
            return WinnerBonusResponse(winner_message, loser_message)
        else:
            return None


class WinnerBonusGold(WinnerBonus):
    @classmethod
    def take_away_percentage_of_gold(cls, winner, loser, percentage=80):
        amount = ceil(loser.gold * percentage / 100)
        loser.gold -= amount
        loser.save()

        winner.gold += amount
        winner.save()

        if amount > 0:
            winner_message = 'Zabrałeś przeciwnikowi %s złota.' % amount
            loser_message = 'Straciłeś po walce %s złota.' % amount
            return WinnerBonusResponse(winner_message, loser_message)
        else:
            return None

    @classmethod
    def leave_percentage_of_gold(cls, winner, loser, percentage=80):
        amount = ceil(loser.gold * percentage / 100)
        loser.gold -= amount
        loser.save()

        try:
            gold_in_map = GoldInMap.objects.get(map=winner.location)
            gold_in_map.amount += amount
            gold_in_map.save()
        except GoldInMap.DoesNotExist:
            gold_in_map = GoldInMap(map=winner.location, amount=amount)
            gold_in_map.save()

        if amount > 0:
            winner_message = 'Twój przeciwnik opuścił %s złota.' % amount
            loser_message = 'Straciłeś po walce %s złota.' % amount
            return WinnerBonusResponse(winner_message, loser_message)
        else:
            return None


class WinnerBonusGettingParameter(WinnerBonus):
    @classmethod
    def decrease_parameter(cls, winner, loser, no_of_drawing=2, difference=1):
        winner_message = 'Dzięki zwycięstwu podwyższyłeś wartości parametrów: <br>'
        parameters = PlayerParameter.objects.filter(player=loser)

        used_parameters = []

        for i in range(no_of_drawing):
            index_parameter = randint(0, len(parameters) - 1)
            parameter = parameters[index_parameter]

            parameter.state -= difference
            parameter.save()

            if parameter not in used_parameters:
                used_parameters.append(parameter)
                winner_message += parameter.get_name()

        return WinnerBonusResponse(winner_message)

# this list contains all function from which
# one is executed after defeating player by another player

winner_bonus_functions = [
    #TriumphOverGettingItem.take_away_precious_items,
    WinnerBonusExperience.increase_experience_by_difference
    #TriumphOverGettingParameter.decrease_parameter
]


class IWinnerBonus(object):
    def __init__(self, winner, loser):
        self.winner = winner
        self.loser = loser

    def execute(self):
        if len(winner_bonus_functions) > 0:
            index = randint(0, len(winner_bonus_functions) - 1)

            effect_function = winner_bonus_functions[index]
            return effect_function(self.winner, self.loser)
        else:
            return None


class WinnerBonusResponse(object):
    def __init__(self, message_for_winner, message_for_loser=''):
        self.winner_message = message_for_winner
        self.loser_message = message_for_loser

    def get_message_for_winner(self):
        return self.winner_message

    def get_message_for_loser(self):
        return self.loser_message