# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from game_app.battle.attack import SpecialAttack, AttackAvailabilityFunction, ParameterModification
from game_app.command.settings.command_definition import *
from game_app.battle.effect import *
from game_app.battle.effect_type import *

#############################
# Battle commands
#

normal_attack = SpecialAttack(
    "atakuj",
    "wykonuje zwykły atak w czasie walki",
    shortcut="q",
    time=1000,
    cost_attributes={STAMINA: 0},
    order=1)

kill = SpecialAttack(
    "zabij",
    "",
    type=CheatingTypeCommand,
    time=1000,
    mod_attributes=[#ParameterModification('Siła', k=1000, percentage=False),
                    ParameterModification('Zręczność', k=1000, percentage=False),
                    ParameterModification('Zwinność', k=1000, percentage=False)],
    cost_attributes={STAMINA: 15},
    effect_array=[Fatality(type=ImmediateEffectType(), probability=100, k=100, percentage=True)],
    order=7)


ogre_hit = SpecialAttack(
    name='ogrzy_cios',
    description='zwiększa obrażenia 2x, ale przez kolejne 3 sekundy masz obniżoną obronę o 50%; '
                'trwa dwa razy dłużej niż zwykły atak. ' ,
    shortcut='oc',
    cost_attributes={HP_NAME: 10, STAMINA: 5},
    effect_array=[ParameterAbsorption(parameter='Siła', k=50, percentage=True, n=0, type=TimeEffectType(3000),
                                      probability=100)], #IS ATTACKING!!
    order=2,
    availability_function=AttackAvailabilityFunction(lambda player: player.experience > 100,
                                                     'dostępne od 100 pkt. doświadczenia')
)

freeze = SpecialAttack(
    'zamroź',
    'przy trafieniu powoduje paraliż u przeciwnika i obniżenie obrony o 100% do czasu otrzymania przez niego kolejnego ciosu',
    shortcut='fr',
    cost_attributes={STAMINA: 35},
    effect_array=[
        MultipleEffect(probability=100,
                       list_of_effects=[Paralysis(ToNextHitType()),
                                        ParameterAbsorption('Wytrzymałość', 100, True, 0, ToNextHitType())])],
    order=3)

harm_limb = SpecialAttack(
    'po_łapkach',
    'przy trafieniu powoduje obniżenie obrony przeciwnika o 1 do końca bitwy',
    shortcut='pł',
    cost_attributes={STAMINA: 25},
    effect_array=[
        ParameterAbsorption(type=PermanentEffectType(), probability=100, parameter='Wytrzymałość', k=1, percentage=False,
                            n=0)],
    order=4)

eruption = SpecialAttack(
    'erupcja',
    'atak zadaje bardzo niskie obrażenia, ale jest po 15% szansy (niezależnie) na krwotok, paraliż, absorpcję zdrowia i atak kończący',
    shortcut='er',
    cost_attributes={STAMINA: 20},
    effect_array=[Poison(type=PermanentEffectType(), probability=100, k=1),
                  Paralysis(type=TimeEffectType(5000), probability=100),
                  ParameterAbsorption(type=ToNextHitType(), probability=15, parameter='Wytrzymałość', k=15,
                                      percentage=True, n=0),
                  Fatality(type=ImmediateEffectType(), probability=100, k=20, percentage=True)],
    order=5)




poison_attack = SpecialAttack(
    'pluje_jadem',
    'Daje dużą szansę na zatrucie przeciwnika, ale nie zadaje obrażeń',
    shortcut='pj',
    cost_attributes={STAMINA: 10},
    effect_array=[Poison(type=PermanentEffectType(), probability=100, k=34)],
    order=6)

jump_attack = SpecialAttack(
    'z_półobrotu',
    'Daje duże szanse na uderzenie krytyczne',
    shortcut='zp',
    cost_attributes={STAMINA: 15},
    effect_array=[CriticalHit(type=PermanentEffectType(), probability=100, k=1)],
    order=7)


