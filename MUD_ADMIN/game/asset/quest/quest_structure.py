# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import abc

from game_app.models import *


class Quest(object):
    def __init__(self, name, description, tasks, rewards=None, immediate_reward=False):
        self.name = name
        self.description = description
        self.tasks = tasks
        self.rewards = rewards
        self.immediate_reward = immediate_reward

    def get_name(self):
        return self.name

    def get_immediate_reward(self):
        return self.immediate_reward

    def get_description(self):
        name = Highlighter.bold(self.name)

        receive_message = Highlighter.bold('Otrzymałeś zadanie: ' + self.name)
        message = name + '<br>' + self.description + '<br><br>' + receive_message

        if self.rewards:

            reward_message = 'W nagrodę otrzymasz: <br>'

            for reward in self.rewards:
                reward_message = reward.get_description()

                if reward_message != '':
                    reward_message = reward_message + '<br>'

            message += reward_message

        message += '<br> 1 - wyjdź'

        return message

    def award_player(self, player):
        from game_app.models import AllPlayerItem

        for task in self.tasks:
            if type(task) is AcquireItemTask and task.return_item is True:
                try:
                    item = AllPlayerItem.objects.get(player=player, item=task.item)
                    item.delete()

                except AllPlayerItem.DoesNotExist:
                    return ""

        if self.rewards:
            for reward in self.rewards:
                reward.award_player(player)

        return None

    def get_award_description(self, introduction):
        description = ''

        if self.rewards:

            description = introduction

            for reward in self.rewards:
                description += reward.get_description() + '<br>'

            description += '<br>'

        return description

    def get_award_description_before_accomplish(self):
        return self.get_award_description('<br><br> W nagrodę otrzymasz: <br>')

    def get_award_description_after_accomplish(self):
        return self.get_award_description('<br><br> W nagrodę otrzymujesz: <br>')


class Task(object):
    def __init__(self, name, description):
        self.name = name
        self.description = description

    @abc.abstractmethod
    def check(self, player):
        pass

    @abc.abstractmethod
    def get_manual(self):
        pass


class ReachFieldTask(Task):
    manual_name = 'Odkrycie pola'
    manual_description = 'Musisz dojść do pola wyznaczonego w zadaniu. Jeśli stoi na nim potwór, musisz go pokonać'

    def __init__(self,
                 x,
                 y,
                 level,
                 name=None,
                 description=None):
        Task.__init__(self, name, description)

        self.map = Map.objects.get(x=x, y=y, level=MapLevel.objects.get(level=level))

    def check(self, player, name):
        return player.location == self.map and self.map.get_monster_in_map() is None

    def get_manual(self):
        field_info = ' (%s,%s,%s): ' % (self.map.x, self.map.y, self.map.level)
        return self.manual_name + field_info + self.manual_description


class KillMonsterTask(Task):
    manual_name = 'Zabicie potwora'
    manual_description = 'Musisz zabić potwora, który znajduje się na określonym polu.'

    def __init__(self,
                 x,
                 y,
                 level,
                 monster_name,
                 name=None,
                 description=None):
        Task.__init__(self, name, description)

        monster = Monster.objects.get(name=monster_name)
        map = Map.objects.get(x=x, y=y, level=MapLevel.objects.get(level=level))

        MonsterInMap.objects.filter(monster=monster, map=map).delete()
        monster_in_map = MonsterInMap(monster=monster, map=map)
        monster_in_map.save()

        self.monster_in_map = monster_in_map

    def get_monster(self):
        return self.monster_in_map.monster

    def get_map(self):
        return self.monster_in_map.map

    def check(self, player, name):
        location_is_right = (self.monster_in_map.map == player.location)
        monster_is_dead = player.location.get_monster_in_map() is None
        return location_is_right and monster_is_dead

    def get_manual(self):
        map = self.monster_in_map.map
        field_info = ' (%s,%s,%s): ' % (map.x, map.y, map.level)
        return self.manual_name + ' ' + self.monster_in_map.__unicode__() + ', który znajduje się na polu ' + field_info


class AcquireItemTask(Task):
    manual_name = 'Zdobycie przedmiotu'
    manual_description = 'Musisz zdobyć (kupić, ukraść bądź znaleźć) przedmiot określony w zadaniu'

    def __init__(self,
                 item_name,
                 name=None,
                 description=None,
                 return_item=True):
        Task.__init__(self, name, description)
        self.item = Item.objects.get(name=item_name)
        self.return_item = return_item

    def check(self, player, name):
        return player.has_item(self.item)

    def get_manual(self):
        return_message = ''
        if self.return_item is False:
            return_message = "Będziesz mógł zachować ten przedmiot dla siebie po wykonaniu zadania."

        return self.manual_name + ' ' + self.item.__unicode__() + ' ' + return_message

    def get_return_item(self):
        return self.return_item

    def get_item(self):
        return self.item


class SpeakWithNPCTask(Task):
    manual_name = 'Rozmowa z postacią niezależną'
    manual_description = 'W rozmowie z określoną postacią niezależną musisz dość do określonego stanu rozmowy.'

    def __init__(self,
                 x,
                 y,
                 level,
                 dialog_state,
                 dialog_state_name,
                 name,
                 description=None):
        Task.__init__(self, name, description)

        self.npc_name = name
        self.dialog_state = dialog_state
        self.dialog_state_name = dialog_state_name

    def check(self, player, name):
        from game_app.controllers.basic_controllers import AssetController
        location = player.location
        asset_in_map = location.get_specific_asset_in_map(name)

        if asset_in_map:
            asset_object = AssetController.get_asset_object(asset_in_map)

            dialog_state = asset_in_map.state

            npc_is_right = self.npc_name == asset_in_map.asset.name

            if dialog_state is not None:
                dialog_state_is_reached = self.dialog_state in asset_object.vertexes[dialog_state]
            else:
                dialog_state_is_reached = False

            return npc_is_right and dialog_state_is_reached
        else:
            return False

    def get_manual(self):
        npc_name = self.npc_name
        return 'W rozmowie z %s: %s' % (npc_name, self.dialog_state_name)


class UseAssetTask(Task):
    manual_name = 'Użycie obiektu'
    manual_description = 'Musisz użyć obiektu, który znajduje się na określonym w zadaniu polu.'

    def __init__(self,
                 x,
                 y,
                 level,
                 name,
                 description=None):
        Task.__init__(self, name, description)

        map = Map.objects.get(x=x, y=y, level=MapLevel.objects.get(level=level))
        asset = Asset.objects.get(name=name)
        asset_in_map = AssetInMap.objects.get(map=map, asset=asset)

        self.asset_in_map = asset_in_map

    def check(self, player, name):
        location = player.location
        asset_in_map = location.get_specific_asset_in_map(name)

        return location == self.asset_in_map.map and asset_in_map.is_used

    def get_manual(self):
        asset_name = self.asset_in_map.asset.name

        map = self.asset_in_map.map
        field_info = ' (%s,%s,%s): ' % (map.x, map.y, map.level)
        return "Musisz użyć obiektu %s na polu: %s." % (asset_name, field_info)


class Reward(object):
    def award_player(self, player):
        pass

    def get_description(self):
        pass


class GoldReward(Reward):
    def __init__(self, money):
        self.money = money

    def award_player(self, player):
        player.increase_gold(self.money)

    def get_description(self):
        return ' złoto w ilości: ' + str(self.money)


class ParameterReward(Reward):
    def __init__(self, parameter_name, amount):
        self.parameter_name = parameter_name
        self.amount = amount

    def award_player(self, player):
        from game_app.models import PlayerParameter,PlayableParameter
        try:
            parameter = PlayerParameter.objects.get(player=player,
                                                    parameter=PlayableParameter.objects.get(name=self.parameter_name))
            parameter.state += self.amount
            parameter.save()
        except PlayerParameter.DoesNotExist:
            return

    def get_description(self):
        return ' %s punkt/-y/-ów do parametru %s' % (str(self.amount), self.parameter_name)


class ItemReward(Reward):
    def __init__(self, item_name):
        self.item = Item.objects.get(name=item_name)

    def award_player(self, player):
        from game_app.models import PlayerEquipment
        reward = PlayerEquipment(player=player, item=self.item)
        reward.save()

    def get_description(self):
        return ' przedmiot: %s ' % self.item.name


class ExperienceReward(Reward):
    def __init__(self, amount):
        self.experience = amount

    def award_player(self, player):
        player.increase_experience(self.experience)

    def get_description(self):
        return ' %s punkt/-y/-ów do doświadczenia' % str(self.experience)