# -*- coding: utf-8 -*-
from __future__ import unicode_literals
import os

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "MUD.settings")

from MUD_ADMIN.game.asset.quest.quest_structure import Quest, GoldReward, ReachFieldTask, KillMonsterTask, \
    AcquireItemTask, SpeakWithNPCTask, UseAssetTask, ParameterReward, ItemReward, ExperienceReward


reach_field_quest = Quest(
    name='Wejdź do pokoju prodziekana.',
    description='Twoim zadaniem jest wejść do pokoju prodziekana i znaleźć jego lewy róg.',
    tasks=[ReachFieldTask(2, 2, 0)],
    rewards=[ExperienceReward(100)]
)

kill_monster_quest = Quest(
    name='Zabij małego robota.',
    description='Przepełnił ci się już nieskończoną ilość razy. Trzeba skończyć z nim raz na zawsze. Zabij stos!',
    tasks=[KillMonsterTask(2, 2, 0, 'Mały robot')],
    rewards=[GoldReward(100), ExperienceReward(100)],
    immediate_reward=True
)

acquire_item_quest = Quest(
    name='Zdobądź Buty ASCII.',
    description='Ostatnie moje buty zostały skradzione przez bezdomnych studentów. Proszę przynieś mi nowe buty.',
    tasks=[AcquireItemTask('Buty ASCII', return_item=True)],
    rewards=[ParameterReward('Wytrzymałość', 10)]
)

dialog_state = 'Tego nie wie nikt'
dialog_state_name = 'Dowiedz się czy Piotr Proc zda swoją sesję'
npc_name = 'Mniszek'
item_name = 'Buty ASCII'

speak_with_npc_quest = Quest(
    name='Zdobądź sekretną informację.',
    description='Od tej informacji zależy dalsze istnienie naszego świata. Dowiedz się czy nasz twórca zda swoją sesję.',
    tasks=[SpeakWithNPCTask(0, 3, 0, dialog_state, dialog_state_name, npc_name)],
    rewards=[ItemReward(item_name)]
)

asset_name = 'prosta łatwa skrzynia'

use_asset_quest = Quest(
    name='Znajdź szufladkę Dirichleta.',
    description='Aby ją znaleźć musisz otworzyć skrzynię w sali wykładowej.',
    tasks=[UseAssetTask(5, 5, 0, asset_name)]
)

complex_asset_quest = Quest(
    name='Wejdź do pokoju prodziekana i zabij stos.',
    description='To jest skomplikowane zadanie. Opis tasków masz w dzienniku aktywności.',
    tasks=[ReachFieldTask(2, 1, 0), KillMonsterTask(2, 2, 0, 'Mały robot')],
    rewards=[ExperienceReward(100)]
)


