# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import abc
import random

from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractAsset
from MUD_ADMIN.game.asset.definition.asset_type import Person
from game_app.command.highlighter import Highlighter
from game_app.command.process.response import AssetResponseObject


class AbstractTemplateNPC(AbstractAsset):
    __metaclass__ = abc.ABCMeta
    type = Person()

    def __init__(self, asset_in_map):
        from game_app.utils.discover.discover_graph import npc_graph_dictionary
        from game_app.models import NPC

        self.asset_in_map = asset_in_map

        person = NPC.objects.filter(name=asset_in_map.asset.name,
                                 type_name=asset_in_map.asset.type_name)[0]


        self.name = person.name
        self.person = person

        graph_class = npc_graph_dictionary[person.graph.name]

        self.graph = graph_class.graph
        self.vertexes = graph_class.vertexes

        if hasattr(graph_class, 'start_points'):
            self.start_points = graph_class.start_points
        else:
            self.start_points = None

        if hasattr(graph_class, 'key_points'):
            self.key_points = graph_class.key_points
        else:
            self.key_points = None

        if hasattr(graph_class, 'quest_points'):
            self.quest_points = graph_class.quest_points
        else:
            self.quest_points = None

        self.state = None

    def show_equipment(self):
        return "<br/>" + self.asset_in_map.get_equipment_description()


class AbstractNPC(AbstractTemplateNPC):
    def __init__(self, asset_in_map):
        AbstractTemplateNPC.__init__(self, asset_in_map)

    def get_reward_message(self, player):
        from game_app.utils.discover.discover_graph import get_quest_by_name

        quest_to_reward = self.asset_in_map.check_reward(player)

        intro_message = ''

        if quest_to_reward:

            for quest in quest_to_reward:
                quest_object = get_quest_by_name(quest.quest.name)
                msg =  quest_object.award_player(player) #if any error - msg is not None
                if msg != None:
                   return msg + "<br/>"

                quest.state = -1
                quest.save()

                if quest_object.get_award_description_after_accomplish() != '':
                    intro_message += 'Dziękuję za wykonanie questu %s. %s' % (
                        Highlighter.bold(quest.quest.name), quest_object.get_award_description_after_accomplish())

        return intro_message

    def get_quest_message(self, player, normal_message=''):
        response = AssetResponseObject(player)

        quest = self.quest_points[self.state]
        quest_name = quest.name

        if self.asset_in_map.check_if_quest_was_assigned(quest_name, player) is False:
            self.asset_in_map.assign_quest(quest_name, player)
            normal_message_array = normal_message.split('1 -')

            if len(normal_message) == 2:
                return response.success(normal_message_array[0] + quest.get_award_description_before_accomplish() + '1 -' +
                                    normal_message_array[1])
            else:
                return response.success(normal_message_array[0] + quest.get_award_description_before_accomplish())

        else:
            message = 'Jesteś w trakcie wykonywania tego zadania, bądź już je wykonanałeś. <br>' \
                      'Nie możesz podjąć się go drugi raz. <br><br>'
            return response.no_effect(message + normal_message)

    def talking(self, player):
        from MUD_ADMIN.game.asset.dialog import TRADE_MESSAGE
        response = AssetResponseObject(player)

        self.asset_in_map.set_state(self.vertexes.index(self.state))

        completion_message = player.check_if_quest_is_accomplished(asset_name=self.asset_in_map.asset.name)

        if self.key_points is not None and self.state in self.key_points.keys():
            key_state = self.key_points[self.state]
            self.asset_in_map.set_start_point(player, self.vertexes.index(key_state))
            return response.success(self.state + completion_message)

        elif self.quest_points is not None and self.state in self.quest_points.keys():
            #self.asset_in_map.set_state(self.vertexes.index(self.state))
            return self.get_quest_message(player, self.state + completion_message)

        else:
            trade_message = ''

            if self.state == TRADE_MESSAGE:
                player.in_trade = True
                player.save()

                trade_message = player.get_player_and_asset_items_description()

            #self.asset_in_map.set_state(self.vertexes.index(self.state))
            return response.success(self.state + trade_message + completion_message)

    def execute(self, player):

        response = AssetResponseObject(player)

        if self.asset_in_map.state is None:

            player.asset_in_map = self.asset_in_map
            player.save()

            reward_message = self.get_reward_message(player)

            start_point = self.asset_in_map.get_start_point(player)

            if start_point is None:
                if self.start_points is not None:
                    self.state = random.choice(self.start_points)
                else:
                    self.state = self.vertexes[0]
            else:
                self.state = self.vertexes[start_point]

            self.asset_in_map.set_state(self.vertexes.index(self.state))

            talking_response = self.talking(player)

            self.state = reward_message + talking_response.message
            talking_response.set_message(reward_message + talking_response.message)

            return talking_response

        else:
            message = self.name + " jest teraz zajęty(-a), bo rozmawia z innym graczem."
            return response.no_effect(message)

    def go_to_next_state(self, string_number, player):

        self.state = self.vertexes[self.asset_in_map.state]

        number = int(string_number) - 1

        if number >= len(self.graph[self.state]):
            message = "Podano błędny numer! <br><br>" + self.state
            return AssetResponseObject(player).no_effect(message)

        else:
            self.state = self.graph[self.state][number]

        if number == -1 or self.state == 0:
            return self.exit_graph(player)

        else:
            return self.talking(player)

    def exit_graph(self, player):
        self.asset_in_map.set_state(None)

        player.asset_in_map = None
        player.save()

        player.in_trade = False
        player.save()

        return AssetResponseObject(player, self.asset_in_map.id, "npc").success('')

    def search_npc(self):
        return self.asset_in_map.get_stealing_description()

    def steal_from_npc(self, player, index):
        self.asset_in_map.steal_from_asset(player, index)