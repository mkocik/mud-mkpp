# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import abc

from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractAsset
from MUD_ADMIN.game.asset.definition.asset_type import LogicalTest


class AbstractRiddle(AbstractAsset):
    __metaclass__ = abc.ABCMeta

    type = LogicalTest()
    description = "Jeśli ją rozwiążesz, otworzysz sobie drogę do nowego przejścia."

    def __init__(self, asset_in_map):
        from game_app.models import Riddle

        AbstractAsset.__init__(self)

        riddle = Riddle.objects.get(name=asset_in_map.asset.name,
                                    type_name=asset_in_map.asset.type_name)

        self.answer = riddle.get_answer()
        self.message_on_success = 'Gratulacje! To poprawna odpowiedź.'
        self.message_on_failure = 'Niestety to nie jest poprawna odpowiedź'
        self.justification = riddle.justification
        self.question = riddle.question

    def get_question(self):
        return self.question

    def get_answer(self):
        return self.answer

    def check_answer(self, answer):

        if isinstance(self.answer, list):
            return answer in self.answer
        else:
            return answer == self.answer

    def get_response(self, answer):

        if self.check_answer(answer):
            return self.get_message_on_success()
        else:
            return self.get_message_on_failure()

    def get_message_on_success(self):

        if self.justification:
            return self.message_on_success + '<br>' + self.justification
        else:
            return self.message_on_success

    def get_message_on_failure(self):
        return self.message_on_failure


