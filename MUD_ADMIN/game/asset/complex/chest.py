# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import abc

from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractAsset
from MUD_ADMIN.game.asset.definition.asset_type import Coffer
from game_app.command.process.response import AssetResponseObject


class AbstractChest(AbstractAsset):
    type = Coffer()
    description = "Jeśli będziesz wystarczający zręczny, otworzysz ją."

    def __init__(self, asset_in_map):
        from game_app.models import Chest

        AbstractAsset.__init__(self)

        self.asset_in_map = asset_in_map
        self.chest = Chest.objects.get(name=asset_in_map.asset.name,
                                       type_name=asset_in_map.asset.type_name)

        self.resistance = self.chest.resistance
        self.max_loot_amount = self.chest.max_loot_amount
        self.item_probability = self.chest.item_probability
        self.experience = self.chest.experience

        if self.chest.graph != '':
            self.graph = self.chest.graph.split(',')
        else:
            self.graph = ''

    def get_resistance(self):
        return self.resistance

    def get_max_loot_amount(self):
        return self.max_loot_amount

    def get_item_probability(self):
        return self.item_probability

    @abc.abstractmethod
    def check_result(self, player):
        pass

    @abc.abstractmethod
    def get_state(self):
        pass

    @abc.abstractmethod
    def set_state(self, index):
        pass

    def open_chest(self, player):
        player.asset_in_map = None
        player.save()

        if self.asset_in_map.is_used is False:
            loots = player.location.get_loots_from_chest(self.asset_in_map.asset.name)
        else:
            message = "Niestety, skrzynia jest pusta. Ktoś ją już otworzył."
            return AssetResponseObject(player).no_effect(message)

        self.asset_in_map.is_used = True
        self.asset_in_map.save()
        completion_message = player.check_if_quest_is_accomplished(asset_name=self.asset_in_map.asset.name)

        if loots:

            player.increase_experience(self.experience)

            if self.experience > 0:
                experience_info = "<br> Otrzymałeś %s pkt. doświadczenia. <br>" % self.experience
            else:
                experience_info = ""

            message = "Udało ci się otworzyć skrzynię!" + experience_info + "<br>" + 'Zbierz, przedmioty.'

            for loot in loots:
                player.location.create_item(loot.item)

        else:
            message = "Niestety, skrzynia jest pusta."

        response = self.exit_graph(player)

        return response.success(message + completion_message)

    def execute(self, player):
        if self.check_result(player):

            return self.open_chest(player)

        else:
            response = AssetResponseObject(player)

            if self.graph == '':
                message = "Niestety, nie udało ci się otworzyć skrzyni."
                return response.no_effect(message)

            else:
                from game_app.command.settings.command_settings import open_chest_commands

                state = self.get_state()

                if state is None:
                    commands = ', '.join(open_chest_commands)
                    message = "To nie jest zwyczajna skrzynia, aby ją otworzyć musisz użyć specjalnej listy komend %s. " \
                              "Aby wyjść wciśnij 0." % commands

                    player.asset_in_map = self.asset_in_map
                    player.save()

                    self.set_state(0)
                    return response.success(message)
                else:
                    message = 'Ta skrzynia jest obecnie otwierana przez inną postać. Nie możesz jej otworzyć.'
                    return response.no_effect(message)

    def exit_graph(self, player):
        player.asset_in_map = None
        player.save()

        self.asset_in_map.set_state(None)
        return AssetResponseObject(player, self.asset_in_map.id, 'chest').success('')


class AbstractSimpleChest(AbstractChest):
    def __init__(self, asset_in_map):
        AbstractChest.__init__(self, asset_in_map)

        self.state = None
        self.asset_in_map = asset_in_map

    def get_state(self):
        return self.state

    def set_state(self, state):
        return self.asset_in_map.set_state(state)

    def go_to_next_state(self, command, player):
        response = AssetResponseObject(player)
        self.state = self.asset_in_map.state

        if command == '0':
            self.asset_in_map.set_state(None)

            player.asset_in_map = None
            player.save()
            return response.success('')

        if self.state is None:
            self.state = 0
            next_state = self.graph[0]
        else:
            next_state = self.graph[self.state]

        if next_state in ['-', command]:
            self.state += 1
            message = "Dobrze."

        else:
            self.state = None
            player.asset_in_map = None
            player.save()
            message = "Złamałeś wytrych"

            self.asset_in_map.set_state(self.state)
            return response.no_effect(message)

        if self.state == len(self.graph):
            self.asset_in_map.set_state(None)

            return self.open_chest(player)
        else:
            self.asset_in_map.set_state(self.state)
            return response.success(message)

    def check_result(self, player):
        return self.agility_test(player)

    def agility_test(self, player):
        if self.graph == '':
            return player.get_parameter('Zręczność') + self.get_chest_bonus_from_skills(player) >= self.resistance
        else:
            return False

    def get_chest_bonus_from_skills(self,player):
        from MUD_ADMIN.game.skills.skills import SimpleChestSkill
        from game_app.utils.discover.discover_skills import all_skills_class_dictionary

        bonus_value = 0
        if SimpleChestSkill.name in all_skills_class_dictionary.keys():
            bonus_value += (all_skills_class_dictionary[SimpleChestSkill.name])().execute(player)

        return bonus_value