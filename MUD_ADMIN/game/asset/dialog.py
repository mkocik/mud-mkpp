# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.asset.graph_template import Graph

TRADE_MESSAGE = "Wszedłeś w tryb handlu. Możesz go opuścić używajać polecenia '1'."

class TestGraph(Graph):
    name = "test_graph"

    A = "Co mam dla Ciebie zrobić? <br>" \
        "1 - Powtórzyć wiadomość A <br>" \
        "0 - Exit"

    B = "Cześć jestem testową postacią niezależna NPC. <br>" + A

    vertexes = [A, B]
    start_points = [A, B]
    key_points = {A: B}

    graph = {
        A: [A, 0],
        B: [A, 0]
    }


class ComplexGraph(Graph):
    name = "complex_graph"

    A = "Chcesz zadanie? <br>" \
        "1 - Nie, dziękuję <br>" \
        "2 - Tak, chętnie"

    B = "Zastanowiłeś się, czy chcesz zadanie? <br>" \
        "1 - Nie, dziękuję <br>" \
        "2 - Tak, chętnie"

    C = "Zabiłeś goblina <br>" \
        "1 - Jeszcze nie <br>" \
        "2 - Tak, oto jego głowa"

    D = "Zastanów się nad tym jeszcze <br>" \
        "1 - Exit"

    E = "Zabij goblina <br>" \
        "1 - Ok, to idę go zabić. Do zobaczenia. <br>" \
        "2 - Gdzie go znajdę?"

    F = "Pośpiesz się. <br>" \
        "1 - Możesz mi przypomnieć gdzie go znajdę? <br>" \
        "2 - Już idę"

    G = "Dziękuję za fatygę. <br>" \
        "1 - Dzięki przyda się."

    H = "Na bagnach <br>" \
        "1 - Ok, to idę na bagna. Do zobaczenia."

    I = "Dziękuję to za fatygę <br>" \
        "1 - Nie ma za co."

    vertexes = [A, B, C, D, E, F, G, H, I]
    start_points = [A, B, C]
    key_points = {D: B}

    graph = {
        A: [D, E],
        B: [D, E],
        C: [F, G],
        D: [0],
        E: [0, H],
        F: [H, 0],
        G: [I],
        H: [0],
        I: [0]
    }


class JohnGraph(Graph):
    name = "john_graph"

    A = "Cześć nazywam się John. W czym mogę ci pomóc? <br>" \
        "1 - Możesz powtórzyć? <br>" \
        "2 - Czy moge cie o cos zapytać? <br>" \
        "0 - Exit"

    B = "Czy chcesz mnie jeszcze o coś zapytać? <br>" \
        "1 - Kto wygrał Mistrzostwa Świata w Brazylii? <br>" \
        "2 - Czy Piotr Proc zdał swoją sesję? <br>" \
        "0 - Exit"

    C = "Na parterze w pokoju 1.17. Jest dostępny we wtorki w godzinach 8-11. <br>" + B
    D = "Niemcy. <br>" + B
    E = "Tak zdał :P <br>" + B

    vertexes = [A, B, C, D, E]

    graph = {
        A: [A, C, 0],
        C: [D, E, 0],
        D: [D, E, 0],
        E: [D, E, 0]
    }


class PersonOrdering(Graph):
    from MUD_ADMIN.game.asset.quest.quest import reach_field_quest, kill_monster_quest, acquire_item_quest, \
        speak_with_npc_quest, use_asset_quest

    name = 'person_ordering_graph'

    A = "Cześć jestem Foo. Mam dla ciebie kilka questów. Reflektujesz? <br>" \
        "1 - Tak <br>" \
        "2 - Nie <br>"

    B = "Oto one: <br>" \
        "1 - wejdź do pokoju prodziekana <br>" \
        "2 - zabij stos <br>" \
        "3 - zdobądź buty ASCI <br>" \
        "4 - zdobądź sekretną informację <br>" \
        "5 - znajdź szufladkę Dirichleta <br>" \
        "6 - pohandluj ze mną <br>" \
        "0 - wyjdź <br>"

    C = 'Zastanawia mnie, co znajduje się w pewnym miejscu. Odkryj to miejsce dla mnie. <br>' \
        '0 - wyjdź'

    D = 'Zabij znienawidzonego przeze mnie wroga. Wskaże ci, gdzie się znajduje. <br>' \
        '0 - wyjdź'

    E = 'Zdobądź upragniony przeze mnie przedmiot. <br>' \
        '0 - wyjdź'

    F = 'Porozmawiaj z moim starym znajomym. Ma coś dla ciebie.' \
        '0 - wyjdź'

    G = 'Szukam pewnego przedmiotu. Przypuszczam, że zdobędę go, gdy otworzę skrzynię na pewnym polu. <br>' \
        'Niestety nie udało mi się tego zrobić samemu. <br>' \
        '0 - wyjdź'

    H = TRADE_MESSAGE

    vertexes = [A, B, C, D, E, F, G, H]

    graph = {
        A: [B, 0],
        B: [C, D, E, F, G, H, 0],
        C: [0],
        D: [0],
        E: [0],
        F: [0],
        G: [0],
        H: [0],
    }

    quest_points = {
        C: reach_field_quest,
        D: kill_monster_quest,
        E: acquire_item_quest,
        F: speak_with_npc_quest,
        G: use_asset_quest
    }



