# -*- coding: utf-8 -*-
from __future__ import unicode_literals


class AssetType(object):
    def __init__(self, name, command_name):
        self.name = name
        self.command_name = command_name

    def get_command_name(self):
        return self.command_name

    def get_name(self):
        return self.name

class Person(AssetType):
    def __init__(self):
        AssetType.__init__(self, "Postać niezależna", "rozmowa")


class Source(AssetType):
    def __init__(self):
        AssetType.__init__(self, "Zwykły asset", "użyj")


class Rock(AssetType):
    def __init__(self):
        AssetType.__init__(self, "Kamień", "użyj")


class LogicalTest(AssetType):
    def __init__(self):
        AssetType.__init__(self, "Zagadka logiczna", None)


class Coffer(AssetType):
    def __init__(self):
        AssetType.__init__(self, "skrzynia", "otwórz")

