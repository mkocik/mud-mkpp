# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import abc

from MUD_ADMIN.game.asset.definition.asset_type import Rock, Source, Person


class AbstractAsset(object):
    __metaclass__ = abc.ABCMeta

    type = Person()

    def get_command_to_execute(self):
        return self.type.command_name

    @abc.abstractmethod
    def execute(self, player):
        pass


class AbstractNormalAsset(AbstractAsset):
    __metaclass__ = abc.ABCMeta

    type = Source()


class AbstractStone(AbstractAsset):
    __metaclass__ = abc.ABCMeta

    type = Rock()

    def __init__(self, asset_in_map):
        from game_app.models import Stone
        stone = Stone.objects.get(name = asset_in_map.asset.name)
        self.message = stone.message

    def get_message(self):
        return self.message



