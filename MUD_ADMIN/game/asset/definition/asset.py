# -*- coding: utf-8 -*-
from __future__ import unicode_literals
from MUD_ADMIN.game.asset.complex.chest import AbstractSimpleChest
from MUD_ADMIN.game.asset.complex.riddle import AbstractRiddle

from MUD_ADMIN.game.properties import STARTING_HP

from MUD_ADMIN.game.asset.definition.asset_abstract import AbstractAsset, AbstractStone, \
    AbstractNormalAsset

# MUD mud_admin can extend this module for own purposes, by
# creating new class (which extends one of the abstract classes) with method execute
from MUD_ADMIN.game.asset.complex.npc import AbstractNPC
from game_app.command.process.response import AssetResponseObject


class LifeSource(AbstractNormalAsset):
    description = "W tym miejscu możesz napić się uzdrawiającej wody."

    def __init__(self, normal_asset=None):
        AbstractAsset.__init__(self)

    def execute(self, player):
        response = AssetResponseObject(player)
        player.set_hp(STARTING_HP)

        message = "Napiłeś się wody ze źródła życia. Odzyskujesz wszyskie siły."
        return response.success(message)


class SimpleNPC(AbstractNPC):
    description = "Możesz porozmawiać z tą postacią."

    def __init__(self, person):
        AbstractNPC.__init__(self, person)


class MysteriousStone(AbstractStone):
    description = "Jego działanie jest jedną wielką zagadką."

    def __init__(self, stone):
        AbstractStone.__init__(self, stone)

    def execute(self, player):
        response = AssetResponseObject(player)
        return response.success(self.message)


class RiddleAsset(AbstractRiddle):
    def __init__(self, riddle):
        AbstractRiddle.__init__(self, riddle)

    def execute(self, player=None):
        response = AssetResponseObject(player)
        message = 'Musisz najpierw rozwiązać zagadkę!' + '<br>' + self.question

        return response.no_effect(message)


class SimpleChest(AbstractSimpleChest):
    def __init__(self, chest):
        AbstractSimpleChest.__init__(self, chest)
