# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from MUD_ADMIN.game.skills.abstract_skills import *

class SimpleChestSkill(AbstractSkill):
    description = "Ułatwia otwieranie prostych skrzyń."
    name = "Złodziejskie palce"

    def execute(self, player):
        skill = player.get_skill_by_name(self.name)
        if skill is not None:
            return skill.value
        else:
            return 0


class PickPocketingSkill(AbstractSkill):
    description = "Ułatwia kradzież przedmiotów."
    name = "Kieszonkostwo"

    def execute(self, player):
        skill = player.get_skill_by_name(self.name)
        if skill is not None:
            return skill.value
        else:
            return 0


class ObservationSkill(AbstractSkill):
    description = "Dzięki niej nie dasz się tak łatwo okraść."
    name = "Spostrzegawczość"

    def execute(self, player):
        skill = player.get_skill_by_name(self.name)
        if skill is not None:
            return skill.value
        else:
            return 0