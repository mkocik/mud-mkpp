# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import abc

class AbstractSkill(object):
    __metaclass__ = abc.ABCMeta


    @abc.abstractmethod
    def execute(self,player):
        pass
