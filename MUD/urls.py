
from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.contrib import admin
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^admin/', include(admin.site.urls)),
    url(r'^reports/response/', 'game_app.views.report_response'),
    url(r'^login/', 'game_app.views.login'),
    url(r'^logout/', 'game_app.views.logout'),
    url(r'^register/', 'game_app.views.register'),
    url(r'^game/', 'game_app.views.game'),
    url(r'^commandExe/', 'game_app.views.commandExe'),
    url(r'^monsterAttack/', 'game_app.views.monsterAttack'),
    url(r'^stopEffect/', 'game_app.views.stopEffect'),
    url(r'^startInterval/', 'game_app.views.startInterval'),
    url(r'^create/world/', 'game_app.views.create'),
    url(r'^create/map/', 'game_app.views.create_map_f'),
    url(r'^create/backup/', 'game_app.views.create_backup'),
    url(r'^choose/backup/', 'game_app.views.choose_backup'),
    url(r'^restore/backup/choose/', 'game_app.views.choose_backup_restore'),
    url(r'^restore/backup/', 'game_app.views.restore_backup'),
    url(r'^get/backup/tables/', 'game_app.views.get_backup_tables'),
    url(r'^simulation/run/', 'game_app.views.run_simulation'),
    url(r'^simulation/', 'game_app.views.set_simulation'),
    url(r'^simulationAttack/', 'game_app.views.simulationAttack'),
    url(r'.*', 'game_app.views.index')
) + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

urlpatterns += staticfiles_urlpatterns()