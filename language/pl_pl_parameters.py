# -*- coding: utf-8 -*-
from __future__ import unicode_literals

GOLD = "Złoto"

#for language files we have to use this names only in html - toDo`
ARMOR = "Pancerz" #change only before recreating database
NATURAL_ARMOR = "Pancerz naturalny"
ITEM_ARMOR = "Pancerz z przedmiotów"
AVG_TEN_DAMAGE = "Średnie obrażenia"

HP_NAME = "Życie" #change only before recreating database
INTELLIGENCE = "Inteligencja" #change only before recreating database
STAMINA = "Kondycja"

CHARISMA = "Charyzma"
ATTACK = "Atak"
DEFENCE = "Obrona"
MIN_DMG = "Minimalne obrażenia"
MAX_DMG = "Maksymalne obrażenia"
ATTACK_SPEED = "Szybkość ataku [ms]"

