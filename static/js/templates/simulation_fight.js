var in_combat;
var monster1_pk;
var monster2_pk;

function startSimulation(monster1, monster2) {
    var pause = 3000; //miliseconds
    var checkingTime = 200;
    in_combat = 0;

    monster1_pk = monster1.pk;
    monster2_pk = monster2.pk;

    fightWithMonster();
    var checking_in_combat = setInterval(checkInCombat, checkingTime);
    var attacking = setInterval(fightWithMonster, pause);

    function checkInCombat() {
        if (in_combat == -1) {
            clearInterval(attacking);
            clearInterval(checking_in_combat);
        }
    }

}


function fightWithMonster() {

    var data = {

        monster1_pk: monster1_pk,
        monster2_pk: monster2_pk
    };

    $.ajax({
        dataType: "json",
        url: "/simulationAttack/",
        timeout: 1000,
        data: data,
        success: function (data) {
            afterSimulationAttack(data);
            fightWithMonster2();
        },
        error: errorInAjaxResponse
    });


}

function fightWithMonster2(){
    data = {
        monster1_pk: monster2_pk,
        monster2_pk: monster1_pk
    };

    $.ajax({
        dataType: "json",
        url: "/simulationAttack/",
        timeout: 1000,
        data: data,
        success: function (data) {
            afterSimulationAttack(data);
        },
        error: errorInAjaxResponse
    });
}

function stopFighting() {
    in_combat = -1;
}

function afterSimulationAttack(data) {

    if (data.status == "no_effect") {
        stopFighting();
        $("#showSimulationResults").show();
    }

    var div;
    var div2;
    if (data.left_div == true) {
        div = $("#leftHalfDiv");
        div2 = $("#rightHalfDiv");
    } else {
        div = $("#rightHalfDiv");
        div2 = $("#leftHalfDiv");
    }

    appendRepeatMessage2(div, div2, data.message, data.number_of_lines);


}

function appendRepeatMessage2(div, div2, message, number_of_lines) {
    if (message != "" && message != null) {
        message = getFrameToSimulationMessage(message);

        div2.append("<br><br>");
        for(var i=1; i<number_of_lines; i++){
            div2.append("<br><br>");
        }

        div.append(message);
    }
}

function getFrameToSimulationMessage(message) {
    return "<div class='simulationMessageFrame'>" + message + "</div>"
}