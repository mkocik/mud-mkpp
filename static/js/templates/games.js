

function triggerCommandExecution() {
    var value = $('#consoleInput').val();
    executeCommand(value);
}

function executeCommand(command) {

    var data = {command: command};

    $.ajax({
        dataType: "json",
        url: "/commandExe/",
        timeout: my_timeout,
        data: data,
        success: function (data) {
            updateView(data);
            manageHistory(data);
            handleBattleWithMonster(data);
        }
    });
}
