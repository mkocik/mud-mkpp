var commandHistory = [];
var historyIterator = 0;
var waitingForCommandExecution = false;

in_combat = -1;
var already_fighting = -1;
var start;
var end;


function commandExe() {
    historyIterator = commandHistory.length;

    var value = $('#consoleInput').val();
    if (value.trim() != "") {
        executeCommand(value);
    }
}


function executeCommand(command) {

    if (waitingForCommandExecution) {
        return false
    }

    start = new Date().getTime();

    // during the battle we cannot execute command
    // more often than certain limit
    if (in_combat > -1 && typeof end != 'undefined') {
        if ((start - end) < 1000) {
            return false;
        }
    }

    waitingForCommandExecution = true;
    var data = {command: command};

    $.ajax({
        dataType: "json",
        url: "/commandExe/",
        timeout: my_timeout,
        data: data,
        success: function (data) {

            if (data.creature != null) {
                in_combat = data.creature;
            }

            changeView(command, data);

            // in the battle
            // we gets response from console
            // with some delay

            $('#consoleInput').val("");
            if (already_fighting > -1 && in_combat != -1) {

                var pause;

                if(command in get_special_attack_time){
                    pause = get_special_attack_time[command];
                }else{
                    pause = 1000;
                }

                setTimeout(function () {afterCommand(data)}, pause)
            } else {
                afterCommand(data)
            }

        },
        error: errorInAjaxResponse
    });
}

function afterCommand(data) {

    appendResponseMessage(data.message);

    scrollInput();

    //monster attack, if we are attacked
    if (in_combat == -1) {
        already_fighting = -1;
    } else if (in_combat > 0) {
        already_fighting = in_combat;
    } else if (in_combat == 0 && already_fighting == -1) {
        startFightingWithMonster();
    }

    waitingForCommandExecution = false;
}

function sendMessage() {

    $.ajax({
        dataType: "json",
        url: "/reports/response/",
        data: {response: $("#reportResponse").val()},
        timeout: my_timeout,
        success: function (data) {
            $('.close-reveal-modal').trigger('click');
        },
        error: errorInAjaxResponse
    });

}


function errorInAjaxResponse(jqXHR, exception) {

    if (jqXHR.status === 0) {
        alert('Stracono połączenie z internetem');
    } else if (jqXHR.status == 404) {
        alert('Requested page not found. [404]');
    } else if (jqXHR.status == 500) {
        alert('Internal Server Error [500].');
    } else if (exception === 'parsererror') {
        alert('Requested JSON parse failed.');
    } else if (exception === 'timeout') {
        alert('Time out error.');
    } else if (exception === 'abort') {
        alert('Ajax request aborted.');
    } else {
        alert('Uncaught Error.\n' + jqXHR.responseText);
    }

    waitingForCommandExecution = false;

}
