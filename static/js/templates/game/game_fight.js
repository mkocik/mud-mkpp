function startFightingWithMonster() {

    already_fighting = 0;

    var pause = 3000; //miliseconds
    var checkingTime = 200;

    fightWithMonster();
    var checking_in_combat = setInterval(checkInCombat, checkingTime);
    var attacking = setInterval(fightWithMonster, pause);

    function checkInCombat() {
        if (in_combat == -1) {
            already_fighting = -1;
            clearInterval(attacking);
            clearInterval(checking_in_combat);
        }
    }
}

function fightWithMonster() {

    $.ajax({
        dataType: "json",
        url: "/monsterAttack/",
        timeout: my_timeout,
        success: function (data) {

            updateAttributes(data.attributes);
            updateGold(data.gold);

            if (data.resurrection) {
                $('#storyContent').empty();
                stopFighting();
            }

            if (data.status == "no_effect") {
                stopFighting();
            }

            appendResponseMessage(data.message);

            //scroll up when we died and bottom if not
            if (data.resurrection) {
                $("#storyContent").animate({ scrollTop: 0 }, "speed");
            } else {
                scrollInput();
            }

        },
        error: errorInAjaxResponse
    });

}

function stopFighting() {
    in_combat = -1;
    already_fighting = -1;
}

function stopEffectFunction(object_id, pause) {

    setTimeout(function () {
        stopEffect(object_id)
    }, pause);

}


function stopEffect(effect_id) {

    var data = {effect_id: effect_id};

    $.ajax({
        dataType: "json",
        url: "/stopEffect/",
        timeout: my_timeout,
        data: data,
        success: function (data) {
        },
        error: errorInAjaxResponse
    });

}

function startInterval(effect_id, interval) {

    var data = {effect_id: effect_id};

    var timer = setInterval(
        function intervalEffect(effect) {

            $.ajax({
                dataType: "json",
                url: "/startInterval/",
                timeout: my_timeout,
                data: data,
                success: function (data) {

                    scrollInput();

                    if (data.stop == true) {
                        clearInterval(timer);
                    }

                },
                error: errorInAjaxResponse
            });

        }, interval);
}


