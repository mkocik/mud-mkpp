function manageHistory(value) {

    historyIterator = commandHistory.length;

    if (commandHistory.indexOf(value) < 0) {
        commandHistory[historyIterator++] = value;
    } else {

        // if command was used already
        // we delete previous occurrence
        // and add at the end of list

        var index = commandHistory.indexOf(value);
        if (index > -1) {
            commandHistory.splice(index, 1);
        }

        historyIterator = commandHistory.length;
        commandHistory[historyIterator++] = value;
    }

}

function browsingHistory()
{
    var consoleInput = $('#consoleInput');

    consoleInput.keydown(function (e) {

        if (e.keyCode == 40 || e.keyCode == 38) {

            if (e.keyCode == 40) { //down

                if (typeof commandHistory[historyIterator + 1] != 'undefined') {
                    consoleInput.val(commandHistory[++historyIterator]);

                } else {
                    historyIterator = commandHistory.length;
                    consoleInput.val("");

                }
            } else if (e.keyCode == 38) {  //up

                if (typeof commandHistory[historyIterator - 1] != 'undefined') {
                    consoleInput.val(commandHistory[--historyIterator]);
                }
            }

            //this code forces cursor in consoleInput to set at the end of it
            //delay this is needed for Chrome browser

            setTimeout((function (el) {
                var strLength = el.value.length;
                return function () {
                    if (el.setSelectionRange !== undefined) {
                        el.setSelectionRange(strLength, strLength);
                    } else {
                        $(el).val(el.value);
                    }
                }
            }(this)), 5);
        }


    });
}