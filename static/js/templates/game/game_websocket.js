function initialiseWebsocket() {

    var player_nick;

    if ($(document).find("#playerNick"))
        player_nick = $('#playerNick').html().trim();
    else
        player_nick = "";

    if (player_nick != "") {
        var ws = new WebSocket('ws://' + window.location.host + '/ws?username=' + player_nick);

        ws.onmessage = function (message) {

            chat_message = message.data;

            try {
                obj = JSON.parse(chat_message);
                if (obj['param_and_attr_update']=true)
                    updateAttributes(obj['attributes']);
                    updateAttributesMax(obj['attributes_max']);
                    updateParameters((obj['parameters']));
                    updateArmor(obj['armor']);
                    updateXP(obj['xp']);
                    return;
            } catch (e) {

            }

            if (isSpecialMessage(chat_message)) {
                executeSpecialMessages(chat_message);
            } else {
                appendChatMessage(chat_message);
                scrollInput();
            }

        };


        var specialMessages = [
            "Zostałeś zabity.",
            "Życie-",
            "EW-",
            "StopEW-",
            "StartInterval-",
            "StartMonsterInterval-"
        ];

        function isSpecialMessage(message) {
            for (var i = 0; i < specialMessages.length; i = i + 1) {
                var specialMessage = specialMessages[i];

                if (message.indexOf(specialMessage) > -1) {
                    return true;
                }
            }
            return false;
        }

        function executeSpecialMessages(message) {
            basicSpecialMessages(message);
            viewUpdateSpecialMessages(message);
            battleEffectSpecialMessages(message);
        }

        function basicSpecialMessages(message) {
            if (message.indexOf("Zostałeś zabity.") > -1) {
                $('#storyContent').empty();
                updateHP(100);

                appendChatMessage(message);
                scrollInput();
            }
        }

        function viewUpdateSpecialMessages(message) {
            if (message.indexOf("Życie-") > -1) {
                var hp = message.split("-")[1];
                updateHP(hp);
            }
        }

        function battleEffectSpecialMessages(message) {
            var arguments = message.split("-");
            var effect_id = arguments[1];
            var time = (arguments[2]);

            if (message.indexOf("StopEW-") > -1) {
                stopEffectFunction(effect_id, time);
            }

            if (message.indexOf('StartInterval-') > -1) {
                startInterval(effect_id, time);
            }

        }
    }
}