function showMap(map) {

    var texturePath = "/static/img/textures/";

    $(".adding img").attr("src", "");

    for (y = 0; y < map.length; y++) {

        for (x = 0; x < map[y].length; x++) {

            field = map[y][x];

            if (field != null) {
                var texture_id_name = "#texture-" + y + "-" + x;
                $(texture_id_name).attr("src", texturePath + field["land_name"] + ".jpg");

                var texture_div_id_name = "#texture-div-" + y + "-" + x;
                $(texture_div_id_name).attr("background", texturePath + field["land_name"] + ".jpg");

                var texture_user_id_name = "#texture-user-" + y + "-" + x;

                if(field["user"]){
                    $(texture_user_id_name).attr("src", texturePath+"player.png");
                }else{
                    $(texture_user_id_name).attr("src", "");
                }

                var texture_passage_id_name = "#texture-passage-" + y + "-" + x;
                if(field["passage"]){
                    $(texture_passage_id_name).attr("src", texturePath+"passage.png");
                }else{
                    $(texture_passage_id_name).attr("src", "");
                }

            }
        }

    }

    $('#revealModalButton').click();
}