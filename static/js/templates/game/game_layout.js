function getFrameToMessage(message) {
    return "<div class='messageFrame'>" + message + "</div>"
}



function getNormalMessage(message) {
    return "<div class='normalMessage'>" + message + "</div>"
}

function getRepeatCommandMessage(message) {
    message = "> " + message;
    return "<div class='repeatCommandMessage '>" + message + "</div>";
}

function changeView(command, response) {
    updateState(response);
    colorInput(response.status);

    appendRepeatMessage(command);

    scrollInput();
    manageHistory(command);
}


function colorInput(status) {

    var commandDiv = $('#commandDiv');

    if (status === 'success') {
        commandDiv.removeClass("has-error");
        commandDiv.removeClass("has-warning");
    } else if (status === 'combat') {
        commandDiv.removeClass("has-error");
        commandDiv.addClass("has-warning");
    }
    else if (status === 'no_effect') {
        commandDiv.removeClass("has-error");
        commandDiv.addClass("has-warning");
    }
    else if (status === 'invalid_command') {
        commandDiv.removeClass("has-warning");
        commandDiv.addClass("has-error");
    }

}

function updateState(data) {
    updateMap(data.map);

    updateXP(data.experience);
    updateGold(data.gold);
    updateLVL(data.level);
    updateArmor(data.armor);

    updateItemParameters(data.parametersItems);
    updateParameters(data.parameters);
    updateAttributes(data.attributes);
    updateAttributesMax(data.attributes_max);

}

function scrollInput() {
    var myDiv = document.getElementById("storyContent");
    myDiv.scrollTop = myDiv.scrollHeight;

    end = new Date().getTime();
}

function appendRepeatMessage(message) {

    if (message != "") {
        message = getRepeatCommandMessage(message);
        appendMessage(message)
    }
}

function appendResponseMessage(message) {

    if (message != "" && message != null) {
        message = getFrameToMessage(message);
        appendMessage(message)
    }

}

function appendChatMessage(message){

    if (message != "") {
        message = getNormalMessage(message);
        appendMessage(message)
    }

}

function appendMessage(message) {
    $("#storyContent").append(message);
}

function updateHP(hp) {
    if (hp != null)
        $("#hpMessage").html("Życie: " + hp);
}

function updateMap(map) {
    if (map != null)
        showMap(map);
}


function updateXP(experience) {
    if (experience != null)
        $("#xpMessage").html("Doświadczenie: " + experience);
}

function updateGold(gold) {
    if (gold != null)
        $("#goldMessage-val").html(gold);
}

function updateLVL(lvl) {
    if (lvl != null)
        $("#lvlMessage").html("Poziom: " + lvl);
}

function updateArmor(armor) {
    if (armor != null)
        $("#armorMessage-val").html(armor);
}



function updateItemParameters(params) {

    if (params != null) {

        params = JSON.stringify(params);
        params = params.replace('{', '').replace('}', '').split("'").join("").split('"').join("");
        params = params.split(',');

        for (index = 0; index < params.length; index++) {

            $('#item_mod_' + params[index].split(":")[0].trim()).removeClass("greenInfo");
            $('#item_mod_' + params[index].split(":")[0].trim()).removeClass("redInfo");

            if (parseInt(params[index].split(":")[1].trim()) > 0) {

                $('#item_mod_' + params[index].split(":")[0].trim()).addClass("greenInfo");
                $('#item_mod_' + params[index].split(":")[0].trim()).html("+" + params[index].split(":")[1].trim())

            } else if (parseInt(params[index].split(":")[1].trim()) < 0) {

                $('#item_mod_' + params[index].split(":")[0].trim()).addClass("redInfo");
                $('#item_mod_' + params[index].split(":")[0].trim()).html(params[index].split(":")[1].trim())

            } else
                $('#item_mod_' + params[index].split(":")[0].trim()).html("")

        }

    }


}

function updateParameters(parameters) {

    if (parameters != null) {

        parameters = parameters.replace('{', '').replace('}', '').split("'").join("");
        parameters = parameters.split(',');

        for (index = 0; index < parameters.length; index++) {
            $('#param_' + parameters[index].split(":")[0].trim()).html(parameters[index].split(":")[1].trim())
        }

    }

}

function updateAttributes(parameters) {

        if (parameters != null) {

        parameters = parameters.replace('{', '').replace('}', '').split("'").join("");
        parameters = parameters.split(',');

        for (index = 0; index < parameters.length; index++) {
            $('#attr_' + parameters[index].split(":")[0].trim()).html(parameters[index].split(":")[1].trim())
        }

    }
}

function updateAttributesMax(parameters) {

        if (parameters != null) {

        parameters = parameters.replace('{', '').replace('}', '').split("'").join("");
        parameters = parameters.split(',');

        for (index = 0; index < parameters.length; index++) {
            $('#attr_max_' + parameters[index].split(":")[0].trim()).html(parameters[index].split(":")[1].trim())
        }

    }
}

function moveCursorToEnd(textField) {
    var endIndex = textField.value.length;

    if (textField.setSelectionRange) {
        return textField.setSelectionRange(endIndex, endIndex);
    }else{
        return null;
    }
}