function addAttack(special_attacks) {
    $(".add_attack").click(function () {

        var id = $(this).attr("id");
        var substrings = id.split("_");
        var no = substrings[substrings.length - 1];


        var attack_id = $(this).prev().val();
        var attack = special_attacks[attack_id - 1];
        var append_div = getAttackDiv(attack, no);

        $(this).parent().parent().next().find("td").last().append(append_div);

        deleteAttack();
    });
}

function deleteAttack() {
    $(".delete_attack").click(function () {
        $(this).prev().remove();
        $(this).next().remove();
        $(this).remove();
    });
}

function changeMonster(no, monsters) {
    $("#monster_" + no).change(function () {

        $("#monster_attacks_" + no).html('');

        if ($(this).val() == "") {
            return;
        }

        var index = $(this).val() - 1;
        var monster = monsters[index];

        $("#hp_" + no).val(monster.hp);
        $("#special_attack_probability_" + no).val(monster.special_attack_probability);

        //setting parameters
        for (var param_name in monster.parameters) {
            var input_name = "#" + param_name + "_" + no;

            if (monster.parameters.hasOwnProperty(param_name)) {
                $(input_name).val(monster.parameters[param_name]);
            }
        }

        //setting attacks
        (monster.attacks).forEach(function (attack) {
            var append_div = getAttackDiv(attack, no);
            $("#monster_attacks_" + no).append(append_div);
        });

        $("#simulationSettings :input").attr("disabled", true);
    });
}


function getAttackDiv(attack, no) {
    var append_div = '<input name="monster_attacks_'+no+'" value=' + attack + ' readonly ' +
        'style="width: 120px;"> </input>';
    append_div += '<input type="button" class="delete_attack" value="Delete"/>';
    append_div += '<br/>';

    return append_div;
}



